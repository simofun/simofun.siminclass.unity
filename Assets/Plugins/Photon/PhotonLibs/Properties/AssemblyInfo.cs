using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: AssemblyCompany("Exit Games GmbH")]
[assembly: AssemblyConfiguration("Unity-Debug")]
[assembly: AssemblyCopyright("(c) Exit Games GmbH, http://www.exitgames.com")]
[assembly: AssemblyDescription("Photon .Net Client Library. Debug.")]
[assembly: AssemblyFileVersion("4.1.3.0")]
[assembly: AssemblyInformationalVersion("4.1.3.0")]
[assembly: AssemblyProduct("Photon .Net Client Library. Debug.")]
[assembly: AssemblyTitle("Photon3Unity3D")]
[assembly: AssemblyVersion("4.1.3.0")]
