﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using KarmaFramework.UI;

namespace Assets.Scripts.UI
{
    public class MainMenuInformationPanel : MonoBehaviour
    {
        public enum ButtonState { Hidden = 0, Visible = 1, NotInteractable = 2 }

        [SerializeField]
        private Text text;

        [SerializeField]
        private Button button;

        [SerializeField]
        private Text buttonLabel;
        [SerializeField]
        private KarmaText Header;

        public void Show(string text, ButtonState buttonState, string buttonLabel = null, UnityAction onButtonClicked = null)
        {
            button.onClick.RemoveAllListeners();

            this.text.text = text; // admit it, that was c00l

            if (buttonState == ButtonState.Hidden)
                button.gameObject.SetActive(false);
            else
            {
                button.gameObject.SetActive(true);
                button.interactable = buttonState == ButtonState.Visible;

                this.buttonLabel.text = buttonLabel;
                button.onClick.AddListener(onButtonClicked);
            }

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
