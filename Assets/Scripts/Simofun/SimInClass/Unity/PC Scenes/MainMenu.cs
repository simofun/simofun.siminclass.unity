﻿using KarmaFramework.API;
using KarmaFramework.Localization;
using Simofun.Karma.Unity.Web.Api;
using Simofun.SimInClass.Unity.HardCodeds;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.UI
{
	public class MainMenu : MonoBehaviour
	{
		public Button Tutorial;
		public Button SinglePlayer;
		public Button MultiPlayer;
		public Button LoadReport;
		public Button Quit;
		public Button Information;

		public Button About;
		public GameObject AboutPanel;
		public Button AboutOK;

		public Text ErrorText;

		public MainMenuInformationPanel InformationPanel;

		public GameObject NoMultiplayerImage;
		private ApplicationConfig _appConfig;
		private bool _multiplayerPermissionQueryFinished = false;
		private bool _hasMultiplayerPermission;
		private bool _wasMultiplayerPermissionRequested;

		IKarmaWebApiRestClient webApiClient;

		#region Construct
		[Inject]
		public virtual void Construct(IKarmaWebApiRestClient webApiClient)
		{
			this.webApiClient = webApiClient;
		}
		#endregion

		void Start()
		{
			_appConfig = InterLevelData.AppConfig;

			ErrorText.text = string.Empty;

			#region Tutorial Button

			Tutorial.onClick.AddListener(() =>
			{
				InterLevelData.GameTier = "Tutorial";
				SceneManager.LoadScene(Scenes.TutorialSelection);
			});
			Tutorial.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("MainMenuTutorialButton");

			#endregion

			#region Single Player Button

			SinglePlayer.onClick.AddListener(() =>
			{
				InterLevelData.GameTier = "Level 1";
				//Old Interface

				StartCoroutine(
					SiniftaUtility.GetUserProgress(
						this.webApiClient,
						InterLevelData.AppConfig.UserInfo,
						() => SceneManager.LoadScene(Scenes.ScenarioEdit)));

				//New Interface
				//KarmaNetwork.LoadLevel("SelectStages");

				//KarmaNetwork.LoadLevel("ScenarioEdit_TestUI");
			});
			SinglePlayer.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("MainMenuSinglePlayerButton");

			#endregion

			#region Multiplayer Button

			MultiPlayer.onClick.AddListener(() =>
			{
				StartCoroutine(
					SiniftaUtility.GetUserProgress(
						this.webApiClient,
						InterLevelData.AppConfig.UserInfo, () =>
						{
							InterLevelData.GameTier = "Level 1";
							SceneManager.LoadScene(Scenes.MultiplayerMenu);
						}));
			});
			MultiPlayer.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("MainMenuMultiplayerButton");

#if UNITY_ANDROID || UNITY_IOS
			MultiPlayer.gameObject.SetActive(false);
#endif
			#endregion

			#region Load Report Button
			LoadReport.onClick.AddListener(() => SceneManager.LoadScene(Scenes.ViewReports));
			#endregion

			#region Quit Button
			Quit.onClick.AddListener(() =>
			{
				_appConfig.UserInfo = null;
				SceneManager.LoadScene(Scenes.Login);
			});
			//if (Information)
			//{
			//    Information.onClick.AddListener(() => { About(); });
			//}
			if (About != null)
			{
				About.onClick.AddListener(() => { AboutPanel.SetActive(true); });
			}
			if (AboutOK != null)
				AboutOK.onClick.AddListener(() => { AboutPanel.SetActive(false); });
			#endregion

			_hasMultiplayerPermission = _appConfig.IsolatedEnvironment || (_appConfig.UserInfo.HasMultiplayerPermission);

			if (_hasMultiplayerPermission)
			{
				MultiPlayer.interactable = true;
				NoMultiplayerImage.SetActive(false);
			}
			else
			{
				// Appointment things
				StartCoroutine(MultiplayerRequestControl());
			}
		}

		private IEnumerator MultiplayerRequestControl()
		{
			var userId = _appConfig.UserInfo.Id;
			bool prevRequestBool = false;
			bool requestBool = false;

			while (!_hasMultiplayerPermission)
			{
				var www = this.webApiClient.GetMultiplayerPermissionInfo(userId);

				yield return www;

				bool.TryParse(www.text, out _hasMultiplayerPermission);

				if (_hasMultiplayerPermission)
				{
					MultiPlayer.interactable = true;
					NoMultiplayerImage.SetActive(false);
					yield break;
				}

				var wwwApp = this.webApiClient.GetMultiplayerRequestInfo(userId);

				yield return wwwApp;

				bool.TryParse(wwwApp.text, out requestBool);
				_multiplayerPermissionQueryFinished = true;

				if (prevRequestBool != requestBool)
				{
					_wasMultiplayerPermissionRequested = requestBool;
					prevRequestBool = requestBool;
				}

				yield return new WaitForSecondsRealtime(10f);
			}
		}
		//public void About()
		//{
		//    var buttonVisibility = MainMenuInformationPanel.ButtonState.Visible;
		//    InformationPanel.Show(StringId.CreditsHeader,
		//                          Localizer.Instance.GetString(StringId.Credits), buttonVisibility,
		//                          Localizer.Instance.GetString(StringId.Ok),
		//                          () =>
		//                          {

		//                              InformationPanel.Hide();
		//                          });

		//}
		public void MultiplayerButtonOnPointerEnter()
		{
			if (_multiplayerPermissionQueryFinished && !_hasMultiplayerPermission)
			{
				MainMenuInformationPanel.ButtonState buttonVisibility = _wasMultiplayerPermissionRequested ? MainMenuInformationPanel.ButtonState.NotInteractable : MainMenuInformationPanel.ButtonState.Visible;
				string buttonLabel = _wasMultiplayerPermissionRequested ? Localizer.Instance.GetString("MultiplayerRequestSent") : Localizer.Instance.GetString("RequestMultiplayer");
				InformationPanel.Show(Localizer.Instance.GetString("NoMultiplayerContactSimsoft"), buttonVisibility, buttonLabel, () =>
				{
					_wasMultiplayerPermissionRequested = true;
					MultiplayerButtonOnPointerEnter();

					this.webApiClient.PostMultiplayerRequest(_appConfig.UserInfo.Email, string.Empty);
				});
			}
			else
				InformationPanel.Hide();
		}

		public void ShowReportOnPointerEnter()
		{
			InformationPanel.Show(Localizer.Instance.GetString("ViewReportButtonHint", SiniftaUtility.ReportsSaveDir), MainMenuInformationPanel.ButtonState.Hidden);
		}

		public void SinglePlayerButtonsOnPointerEnter()
		{
			InformationPanel.Hide();
		}
	}
}
