﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Managers
{

    public class Trackball : MonoBehaviour 
    {
        private Vector2 _trackballSize;
        private Vector2 _trackballOrigin;
        [SerializeField]
        private Image _trackballLeftArrow;
        [SerializeField]
        private Image _trackballRightArrow;
        [SerializeField]
        private Image _trackballUpArrow;
        [SerializeField]
        private Image _trackballDownArrow;

        private Image _trackballCenter;
        private bool _state;

        private void Start()
        {
            _trackballCenter = GetComponent<Image>();
            SetState(false);
        }

        public Vector2 GetTrackballRotation()
        {
            if(!_state)
            {
                return Vector2.zero;
            }
            var delta = (Vector2)Input.mousePosition - _trackballOrigin;

            // Throws float NaN error because of _trackballSize is not initialized
            delta.x = Mathf.Clamp(delta.x /*/ (16 * _trackballSize.x)*/, -1f, 1f);
            delta.y = Mathf.Clamp(delta.y /*/ (16 * _trackballSize.y)*/, -1f, 1f);
            SetTrackballArrowColor(delta);
            return delta;
        }

        private void SetTrackballArrowColor(Vector2 delta)
        {
            var fadedColor = new Color(1f, 1f, 1f, 0.4f);

            _trackballLeftArrow.color = (delta.x < 0)
                ? new Color(1f, 1f, 1f, .4f - 1.2f * delta.x)
                : fadedColor;
            _trackballRightArrow.color = (delta.x > 0)
                ? new Color(1f, 1f, 1f, .4f + 1.2f * delta.x)
                : fadedColor;
            _trackballUpArrow.color = (delta.y > 0)
                ? new Color(1f, 1f, 1f, .4f + 1.2f * delta.y)
                : fadedColor;
            _trackballDownArrow.color = (delta.y < 0)
                ? new Color(1f, 1f, 1f, .4f - 1.2f * delta.y)
                : fadedColor;

        }

        public void SetState(bool state)
        {
            if(!_state && state)
            {
                transform.position = Input.mousePosition;
                _trackballOrigin = Input.mousePosition;
            }

            _state = state;
            _trackballCenter.enabled = state;
            _trackballDownArrow.enabled = state;
            _trackballLeftArrow.enabled = state;
            _trackballRightArrow.enabled = state;
            _trackballUpArrow.enabled = state;
        }
    }
}
