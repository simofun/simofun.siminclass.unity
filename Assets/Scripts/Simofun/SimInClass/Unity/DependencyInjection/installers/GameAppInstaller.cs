﻿using Sinifta.Actors;
using Sinifta.AI;
using Sinifta.Interactables;
using Sinifta.Views;

namespace Simofun.SimInClass.Unity.DependencyInjection
{
	using KarmaFramework.Installers;
	using KarmaFramework.Interactables.Apps;
	using KarmaFramework.ScenarioCore;
	using Simofun.Karma.Unity.DependencyInjection;
	using Simofun.Karma.Unity.Domain.Model;
	using System;
	using UnityEngine;
	using Zenject;

	public class GameAppInstaller : AppInstallerBase
	{
		#region Unity Fields
		[SerializeField]
		GameAppInstallerSettings gameSettings;
		#endregion

		#region Fields
		[Inject]
		GameState gameState;

		[Inject]
		IScenarioPath scenarioPath;
		#endregion

		#region AppInstaller Methods
		public override void InstallBindings()
		{
			base.InstallBindings();

			this.Container.Inject(this.gameState.Scenario);
			this.Container.Inject(this.gameState.Environment);
			this.Container.BindInstance(this.gameState.Environment);
			
			// Install teacher
			this.Container.BindFactory<Teacher, TeacherFacade, TeacherFacade.Factory>()
				.FromSubContainerResolve()
				.ByNewPrefabInstaller<PlayerInstaller<Teacher, TeacherView, TeacherFacade, StudentAnimController>>(
					this.gameSettings.PlayerPrefab);

			// Install student
			this.Container.BindFactory<string, Student, StudentFacade, StudentFacade.Factory>()
				.FromSubContainerResolve()
				.ByNewPrefabInstaller<NPCInstaller<Student, StudentView, StudentFacade, StudentReasoner, StudentAnimController>>(
					this.KarmaConfigSettings.ActorDummy);

			this.Container.Bind<Board>().WithId("BlackBoard").AsCached()
				.WithArguments(Board.BoardType.BlackBoard, "BlackBoard");
			this.Container.Bind<Board>().WithId("SmartBoard").AsCached()
				.WithArguments(Board.BoardType.SmartBoard, "SmartBoard");
			this.Container.Bind<Tablet>().AsSingle().WithArguments("Tablet");
			this.Container.Bind<Activity>().AsSingle().WithArguments("Activity");
			this.Container.BindFactory<Type, string, GadgetApplication, GadgetApplication.Factory>()
				.FromFactory<GadgetApplication.CustomFactory>();
			this.Container.Bind<AppResourceConfig>().AsSingle()
				.WithArguments(this.scenarioPath.Path_ + this.gameState.Scenario.Name);
		}
		#endregion

		#region Nested Types
		[Serializable]
		public class GameAppInstallerSettings
		{
			public GameObject PlayerPrefab;
		}
		#endregion
	}
}
