﻿#if UNITY_ANDROID || UNITY_IOS
#define MOBILE
#endif

using Sinifta.AI;
using UniRx;

namespace Simofun.SimInClass.Unity.DependencyInjection
{
	using KarmaFramework.API;
	using KarmaFramework.Localization;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.DependencyInjection;
	using Simofun.Karma.Unity.Domain.Model;
	using Simofun.Karma.Unity.Web;
	using Simofun.Karma.Unity.Web.Api;
	using Simofun.Karma.Web;
	using Simofun.Karma.Web.Api;
	using Simofun.SimInClass.Unity.Domain.Model;
	using UnityEngine;
	using Zenject;

	[CreateAssetMenu(
		fileName = nameof(AppProjectInstaller),
		menuName = "App/Data/DependencyInjection/Installer/" + nameof(AppProjectInstaller))]
	public class AppProjectInstaller : ScriptableObjectInstaller<AppProjectInstaller>
	{
		#region Unity Fields
		[SerializeField]
		KarmaConfigSettings karmaConfigSettings;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();
		#endregion

		#region Protected Properties
		protected KarmaConfigSettings KarmaConfigSettings => this.karmaConfigSettings;
		#endregion

		#region Unity Methods
		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion

		#region ProjectInstaller Methods
		public override void InstallBindings()
		{
			var lang = Localizer.Instance.GetInitialLanguage();
			var langIndex = (int)lang;
			Localizer.Instance.Initialize(this.KarmaConfigSettings.Localization[langIndex].text, lang);

			this.Container.Bind<IEnvironmentPath>().To<ResourcesEnvironmentPath>().AsCached().NonLazy();
			this.Container.Bind<IScenarioPath>().To<ResourcesScenarioPath>().AsCached().NonLazy();
			this.Container.Bind<IScene>().To<Scene>().AsSingle();
			this.Container.Bind<IKarmaWebApiUrlProvider>().To<KarmaWebApiUrlProvider>()
				.AsSingle()
				.WithArguments(InterLevelData.AppConfig.ApiIp)
				.NonLazy();
#if MOBILE
			this.Container.Bind<IKarmaWebApiRestClient>().To<LocalKarmaWebApiRestClient>().AsSingle().NonLazy();
#else
			this.Container.Bind<IKarmaWebApiRestClient>().To<KarmaWebApiRestClient>().AsSingle().NonLazy();
#endif
			this.Container.Bind<IKarmaWebUrlProvider>().To<KarmaWebUrlProvider>()
				.AsSingle()
				.WithArguments(InterLevelData.AppConfig.WebIp)
				.NonLazy();
			this.Container.Bind<IKarmaWebClient>().To<KarmaWebClient>().AsSingle().NonLazy();
			this.Container.Bind<SimpleReasonerData>()
				.AsSingle()
				.WithArguments(this.KarmaConfigSettings.ReasonerData.text);

			KarmaMessageBus.OnEvent<ChangeLanguageRequestEvent>().Subscribe(ev =>
			{
				Localizer.Instance.ChangeLanguage(this.KarmaConfigSettings.Localization[(int)ev.type].text, ev.type);

				this.Container.Rebind<IEnvironmentPath>().To<ResourcesEnvironmentPath>().AsCached().NonLazy();
				this.Container.Rebind<IScenarioPath>().To<ResourcesScenarioPath>().AsCached().NonLazy();
			}).AddTo(this.disposables);
		}
		#endregion
	}
}
