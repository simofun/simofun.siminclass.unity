﻿namespace Simofun.Unity.Utils
{
	using System;

	public static class ReadingTimeUtil
	{
		#region Public Fields
		public const int WPM = 130;
		#endregion

		#region Public Methods
		public static int GetWordCount(string str) => str.Split(' ').Length;

		public static TimeSpan GetReadingTime(int wordCount) => TimeSpan.FromSeconds(((double)60 / WPM) * wordCount);

		public static TimeSpan GetReadingTime(string str) => GetReadingTime(GetWordCount(str));
		#endregion
	}
}
