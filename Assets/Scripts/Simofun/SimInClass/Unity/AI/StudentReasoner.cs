﻿using System;
using System.Collections.Generic;
using System.Linq;
using KarmaFramework.ActionCore;
using KarmaFramework.AISettingsNamespace;
using KarmaFramework.CharacterCore;
using Sinifta.Actions;
using Sinifta.Actions.StudentActions;
using Sinifta.Actions.TeacherActions;
using Sinifta.Actors;
using UnityEngine;
using Random = System.Random;

namespace Sinifta.AI
{

    public class StudentReasonerDecideParameters : ReasonerDecideExtraParameters
    {
        public float distanceToTeacher;
    }

    public class StudentMindUpdateParams : MindUpdateParams
    {
        public int RepeatCount { get; set; }
        public float LectureOvertime { get; set; }
        public float DistanceToAction { get; set; }
        public float DeltaTime { get; set; }
    }

    public class StudentReasoner : IReasoner
    {
        private struct MisbehaviourProbability
        {
            public readonly BaseAction action;
            public readonly float probability;

            public MisbehaviourProbability(BaseAction action, float probability)
            {
                this.action = action;
                this.probability = probability;
            }
        }

        private int _nextMBTicks;
        private readonly SimpleReasonerData _actionData;
        public SimpleReasonerDataObject StudentState { get; set; }
        public SimpleReasonerDataObject TeacherState { get; set; }
        private Random Random { get; set; }
        private const float MbThreshold = 100f;

        private readonly Dictionary<SimpleReasonerDataObject, int> _actionCounts;
        private readonly Dictionary<Type, SimpleReasonerDataObject> _actionDictionary;
        private readonly List<SimpleReasonerDataObject> _timingList;
        private readonly List<MisbehaviourProbability> _misbehaviourProbs;

        public StudentReasoner(SimpleReasonerData actionData)
        {
            Random = new Random(); // TODO: get seed from network for determinism
            _nextMBTicks = Random.Next(0, 100);
            _timingList = new List<SimpleReasonerDataObject>();
            _misbehaviourProbs = new List<MisbehaviourProbability>();
            _actionData = actionData as SimpleReasonerData;
            _actionDictionary = _actionData.ActionDictionary;
            _actionCounts = new Dictionary<SimpleReasonerDataObject, int>();
        }
        ~StudentReasoner()
        {
            Dispose();
        }
        public void Dispose() => GC.SuppressFinalize(this);
        public int ApplyDealMethod(BaseAction dealMethod, BaseAction target)
        {
            var dealVal = 0;
            var dealMethodData = _actionDictionary[dealMethod.GetType()];
            var targetData = _actionDictionary[target.GetType()];

            if (TryDeal(targetData, dealMethodData))
            {
                Debug.Log("successful deal against " + target.Name + " using " + dealMethod.Name);
                dealVal = 1 + targetData.CorrectDealDictionary[dealMethodData.Name];
            }
            else if (dealMethodData.BehaviorType.ToLower().Equals("wrongdeal") || dealMethodData.Severity > targetData.Severity)
            {
                Debug.Log("wrong deal against " + target.Name + " using " + dealMethod.Name);
                dealVal = -1;
            }
            else
            {
                Debug.Log("ignored deal against " + target.Name + " using " + dealMethod.Name);
            }

            return dealVal;
        }

        public string DecideOnAction(ReasonerDecideParameters prms)
        {
            if (WillMisbehave(prms))
            {
                var action = FindBestMisBehavior(prms.personality, prms.inputSnapshot);
                if (action != null)
                {
                    StudentState = _actionDictionary[action.GetType()];
                    return StudentState.ActionName;
                }
            }
            
            else if (TeacherState != null && getOceanDist(prms.personality, TeacherState.OceanEffectHolder) < _actionData.MinOceanDist)
            {
                if (StudentState == null || Equals(StudentState.Action, typeof(Idle)) || !StudentState.BehaviorType.ToLower().Equals("neutral"))
                {
                    StudentState = FindNeutralActionDataObject(TeacherState);
                }
            }

            return StudentState != null ? StudentState.ActionName : null;
        }

        public List<string> GetDeals(BaseAction action)
        {
            var deals = _actionDictionary.Values
                .Where(a => a.Enabled && a.BehaviorType.Equals("deal") || a.BehaviorType.Equals("WrongDeal"))
                .ToList();
            return deals.Select(a => a.ActionName).ToList();
        }

        public MindSnapshot UpdateMind(ReasonerUpdateParameters prms)
        {
            float knoStep = _actionData.KnoDecayStep;
            float conStep = _actionData.ConDecayStep;
            float enjStep = _actionData.EnjDecayStep;
            float persStep = _actionData.PersStep;

            MindSnapshot mindSnapshot = prms.inputSnapshot;
            var coefParams = prms.coefficientParameters as StudentMindUpdateParams;

            SimpleReasonerDataObject action;
            try
            {
                action = _actionDictionary[coefParams.AppliedAction.GetType()];
            }
            catch (KeyNotFoundException)
            {
                Debug.LogError("no action for: " + coefParams.AppliedAction.GetType());
                return new MindSnapshot();
            }

            float ocean = getOceanDist(prms.personality, action.OceanEffectHolder);

            if (action.EnjEffect > 0f)
                enjStep = _actionData.EnjIncreaseStep;
            if (action.ConEffect > 0f)
                conStep = _actionData.ConIncreaseStep;
            if (action.KnoEffect > 0f)
                conStep = _actionData.KnoIncreaseStep;

            if (action.BehaviorType.ToLower().Equals("teach"))
            {
                if (StudentState != null && StudentState.BehaviorType.ToLower().Equals("mb"))
                    knoStep = 0;

                TeacherState = action;

                if (TeacherState != null && ocean < _actionData.MinOceanDist)
                {
                    if (StudentState == null || Equals(StudentState.Action, typeof(Idle)) || !StudentState.BehaviorType.ToLower().Equals("neutral"))
                    {
                        StudentState = FindNeutralActionDataObject(TeacherState);
                    }
                }
            }

            var inverseDistToAction = 0.5f;
            if (coefParams.DistanceToAction < 5f)
            {
                inverseDistToAction += 0.5f - coefParams.DistanceToAction / 10f;
            }

            var concentrationGain = action.ConEffect * conStep * inverseDistToAction * (2.25f - ocean);

            if (action.ConEffect < 0f)
                concentrationGain *= (0.5f + mindSnapshot.Params["Concentration"] / 200f);

            var enjoymentGain = action.EnjEffect * enjStep * inverseDistToAction * (2.25f - ocean);
            if (action.EnjEffect < 0f)
                enjoymentGain *= (0.5f + mindSnapshot.Params["Enjoyment"] / 200f);

            var knowledgeGain = action.KnoEffect * knoStep * (0.5f + 0.01f * mindSnapshot.Params["Concentration"]) * (2.25f - ocean);
            if (action.KnoEffect < 0f)
                knowledgeGain *= (0.5f + mindSnapshot.Params["Knowledge"] / 200f);

            if (StudentState != null && StudentState.BehaviorType.ToLower().Equals("mb"))
            {
                concentrationGain *= 3f;
                enjoymentGain *= 2f;
            }
            else
            {
                concentrationGain *= 1.5f;
            }

            mindSnapshot.Params["Concentration"] = (int)Mathf.Clamp(mindSnapshot.Params["Concentration"] + concentrationGain * coefParams.DeltaTime, 0f, 100f);
            mindSnapshot.Params["Enjoyment"] = (int)Mathf.Clamp(mindSnapshot.Params["Enjoyment"] + enjoymentGain * coefParams.DeltaTime, 0f, 100f);
            mindSnapshot.Params["Knowledge"] = (int)Mathf.Clamp(mindSnapshot.Params["Knowledge"] + knowledgeGain * coefParams.DeltaTime, 0f, 100f);

            persStep *= coefParams.DeltaTime;

            prms.personality.O = Mathf.Clamp(prms.personality.O + persStep * (action.OceanEffectHolder.O - prms.personality.O), 0f, 1f);
            prms.personality.C = Mathf.Clamp(prms.personality.C + persStep * (action.OceanEffectHolder.C - prms.personality.C), 0f, 1f);
            prms.personality.E = Mathf.Clamp(prms.personality.E + persStep * (action.OceanEffectHolder.E - prms.personality.E), 0f, 1f);
            prms.personality.A = Mathf.Clamp(prms.personality.A + persStep * (action.OceanEffectHolder.A - prms.personality.A), 0f, 1f);
            prms.personality.N = Mathf.Clamp(prms.personality.N + persStep * (action.OceanEffectHolder.N - prms.personality.N), 0f, 1f);

            return mindSnapshot;
        }

        //public void ActionCompleted(StudentAction action)
        //{
        //    if (StudentState != null && StudentState.Action == action.GetType())
        //        StudentState = null;
        //}


        private float getOceanDist(OceanPersonality personality, OceanEffectHolder oceanEffects)
        {
            //calculate vector distance sum [ (xi - yi)^2 ]
            float t, sq = 0f;
            t = (personality.O - oceanEffects.O);
            sq += t * t;
            t = (personality.C - oceanEffects.C);
            sq += t * t;
            t = (personality.E - oceanEffects.E);
            sq += t * t;
            t = (personality.A - oceanEffects.A);
            sq += t * t;
            t = (personality.N - oceanEffects.N);
            sq += t * t;
            return (float)Math.Sqrt(sq);
        }

        private BaseAction FindBestMisBehavior(OceanPersonality personality, MindSnapshot mss)
        {
            _misbehaviourProbs.Clear();

            bool includeAllMisbehaviours;

            if (Random.NextDouble() < 0.19f * (personality.E * 0.5f + personality.N - personality.A))
                includeAllMisbehaviours = true;
            else
                includeAllMisbehaviours = false;

            //float minDist = 10000f;
            float totalProbability = 0f;

            foreach (SimpleReasonerDataObject action in _actionData.ActionList)
            {
                if (action.BehaviorType.ToLower().Equals("mb") && action.Enabled)
                {
                    if (includeAllMisbehaviours || (action.ConRange.IsSatisfied(mss.Params["Concentration"]) && action.EnjRange.IsSatisfied(mss.Params["Enjoyment"])))
                    {
                        float temp = (1f / getOceanDist(personality, action.OceanEffectHolder));
                        if (temp <= 0.1f)
                            temp = 0.1f;

                        totalProbability += temp;

                        _misbehaviourProbs.Add(new MisbehaviourProbability(action.Action, temp));
                    }
                }
            }

            if (_misbehaviourProbs.Count > 0)
            {
                float random = ((float)Random.NextDouble()) * totalProbability;
                int i = 0;
                while (i < _misbehaviourProbs.Count && random > _misbehaviourProbs[i].probability)
                {
                    random -= _misbehaviourProbs[i].probability;
                    i++;
                }

                if (i < _misbehaviourProbs.Count)
                    return _misbehaviourProbs[i].action;

                return null;
            }

            return null;
        }

        private bool WillMisbehave(ReasonerDecideParameters prm)
        {
            //Student will not misbehave 
            //a.when already misbehaving
            if (StudentState != null && StudentState.BehaviorType.ToLower().Equals("mb"))
                return false;

            var sPrm = prm.extra as StudentReasonerDecideParameters;
            //b.when teacher is too close
            if (sPrm .distanceToTeacher < 1.5f)
                return false;

            if (TeacherState == null)
                TeacherState = _actionDictionary[typeof(TeacherIdle)];

            //student will misbehave
            //a.when the current teaching method is not good for his character and he's prone to MB 
            if (getOceanDist(prm.personality, TeacherState.OceanEffectHolder) > _actionData.MinOceanDist && Random.NextDouble() < 0.5f + (prm.personality.N - prm.personality.C) * 0.5f)
            {
                return true;
            }

            //b. with some probability
            if (Random.NextDouble() < _actionData.RandomMBProb)
                return true;

            return false;
        }

        private void PutInLists(SimpleReasonerDataObject action)
        {
            int val = 0;
            if (_actionCounts.TryGetValue(action, out val))
            {
                val += 1;
                _actionCounts[action] = val;
            }
            else
                _actionCounts.Add(action, 1);
            _timingList.Add(action);

        }

        private bool TryDeal(SimpleReasonerDataObject misbehavior, SimpleReasonerDataObject dealMethod)
        {
            if (misbehavior.CorrectDealDictionary.ContainsKey(dealMethod.Name))
            {
                StudentState = null;
                return true;
            }

            return false;
        }

        public SimpleReasonerDataObject FindNeutralActionDataObject(SimpleReasonerDataObject teacherAction)
        {
            if (teacherAction != null)
            {
                if (teacherAction.NeutralStateList != null && teacherAction.NeutralStateList.Count > 0)
                {
                    var s = teacherAction.NeutralStateList[Random.Next(teacherAction.NeutralStateList.Count)];
                    if (s != "")
                    {
                        var sType = Type.GetType(s);
                        if (sType != null)
                        {
                            SimpleReasonerDataObject result = _actionDictionary[sType];
                            if (result.Enabled)
                                return result;
                        }
                    }
                }
            }

            return null;
        }
    }


}
