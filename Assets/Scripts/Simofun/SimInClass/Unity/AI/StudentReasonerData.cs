﻿using LitJson;
using System;
using System.Xml.Serialization;
using System.Linq;
using System.Collections.Generic;
using KarmaFramework.ActionCore;
using UnityEngine;
using System.Xml.Linq;
using KarmaFramework.KarmaUtils;
using KarmaFramework.ScenarioCore;

namespace Sinifta.AI
{
    public class SimpleReasonerData
    {
        public List<BaseAction> BaseActionList { get; set; }
        public List<SimpleReasonerDataObject> ActionList { get; set; }
        public Dictionary<string, string> _typeToName;
        public float ConDecayStep { get; set; }
        public float EnjDecayStep { get; set; }
        public float KnoDecayStep { get; set; }
        public float KnoIncreaseStep { get; set; }
        public float ConIncreaseStep { get; set; }
        public float EnjIncreaseStep { get; set; }
        public float PersStep { get; set; }
        public float RandomMBProb { get; set; }
        public float MinOceanDist { get; set; }

        public Dictionary<Type, SimpleReasonerDataObject> ActionDictionary;

        public SimpleReasonerData(string jsonText, ActionConfig actionConfig)
        {
            _typeToName = new Dictionary<string, string>();

            JsonData d = new JsonData();

            JsonReader reader = new JsonReader(jsonText);
            try
            {
                d = JsonMapper.ToObject(reader);

            }
            catch (JsonException e)
            {
                Debug.Log(e.StackTrace);
            }
            ActionList = JsonMapper.ToObject<List<SimpleReasonerDataObject>>(d["ActionList"].ToJson()).ToList();

            ActionDictionary = new Dictionary<Type, SimpleReasonerDataObject>();
            foreach (SimpleReasonerDataObject simpleReasonerDataObject in ActionList)
            {
                simpleReasonerDataObject.DistributeFields();
                //if (_typeToName.ContainsKey(simpleReasonerDataObject.Action.ToString()))
                //    simpleReasonerDataObject.ActionName = _typeToName[simpleReasonerDataObject.Action.ToString()];                
                simpleReasonerDataObject.ActionName = actionConfig.GetNameFromType(simpleReasonerDataObject.Action.GetType());
                ActionDictionary.Add(simpleReasonerDataObject.Action.GetType(), simpleReasonerDataObject);
            }

            ConDecayStep = (float)(double)d["ConDecayStep"];
            EnjDecayStep = (float)(double)d["EnjDecayStep"];
            KnoDecayStep = (float)(double)d["KnoDecayStep"];
            KnoIncreaseStep = (float)(double)d["KnoIncreaseStep"];
            ConIncreaseStep = (float)(double)d["ConIncreaseStep"];
            EnjIncreaseStep = (float)(double)d["EnjIncreaseStep"];
            PersStep = (float)(double)d["PersStep"];
            RandomMBProb = (float)(double)d["RandomMBProb"];
            MinOceanDist = (float)(double)d["MinOceanDist"];
        }


    }

    public class SimpleReasonerDataObject
    {
        public float ConMin { get; set; }
        public float EnjMin { get; set; }
        public float KnoMin { get; set; }
        public float ConMax { get; set; }
        public float EnjMax { get; set; }
        public float KnoMax { get; set; }
        public float ConEffect { get; set; }
        public float EnjEffect { get; set; }
        public float KnoEffect { get; set; }
        public float O { get; set; }
        public float C { get; set; }
        public float E { get; set; }
        public float A { get; set; }
        public float N { get; set; }
        public bool IsForced { get; set; }
        public BaseAction Action { get; set; }
        public string ActionName { get; set; }
        public char Owner { get; set; }
        public string Name { get; set; }
        public string BehaviorType { get; set; }
        public int Severity { get; set; }
        public SimpleReasonerCondition ConRange { get; set; }
        public SimpleReasonerCondition EnjRange { get; set; }
        public SimpleReasonerCondition KnoRange { get; set; }

        public OceanEffectHolder OceanEffectHolder { get; set; }

        public bool Enabled { get; set; }
        public bool OccursRandomly { get; set; }
        public List<string> CorrectDealList { get; set; }
        public List<string> NeutralStateList { get; set; }

        public List<string> TpackList { get; set; }

        [XmlIgnore]
        public Dictionary<string, int> CorrectDealDictionary { get; set; }

        public SimpleReasonerDataObject()
        {
        }

        public void DistributeFields()
        {
            this.ConRange = new SimpleReasonerCondition(ConMin, ConMax);
            this.EnjRange = new SimpleReasonerCondition(EnjMin, EnjMax);
            this.KnoRange = new SimpleReasonerCondition(KnoMin, KnoMax);

            OceanEffectHolder = new OceanEffectHolder(O, C, E, A, N);

            Action = Activator.CreateInstance(Type.GetType(Name)) as BaseAction;
            ActionName = Action.Name;

            CorrectDealDictionary = new Dictionary<string, int>();
            if (CorrectDealList != null && CorrectDealList.Count > 0)
            {
                foreach (string s in CorrectDealList)
                {
                    string[] splittedString = s.Split(':');
                    CorrectDealDictionary.Add(splittedString[0], int.Parse(splittedString[1]));
                }
            }

            CorrectDealList = null;
        }
    }

    public class SimpleReasonerCondition
    {
        public float MinValue { get; set; }
        public float MaxValue { get; set; }

        public SimpleReasonerCondition()
        {

        }

        public SimpleReasonerCondition(float x, float y)
        {
            MinValue = x;
            MaxValue = y;
        }

        public bool IsSatisfied(float value)
        {
            return value <= MaxValue + 0.1f && value >= MinValue - 0.1f;
        }
    }

    public class OceanEffectHolder
    {
        public OceanEffectHolder(float O, float C, float E, float A, float N)
        {
            this.O = O;
            this.C = C;
            this.E = E;
            this.A = A;
            this.N = N;
        }

        public OceanEffectHolder()
        {
        }

        public float O { get; set; }
        public float C { get; set; }
        public float E { get; set; }
        public float A { get; set; }
        public float N { get; set; }
    }
}
