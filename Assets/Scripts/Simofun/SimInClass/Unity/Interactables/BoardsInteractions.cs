﻿using KarmaFramework.Interactables.Views;
using Simofun.Karma.UniRx.Unity;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using UnityEngine.EventSystems;
namespace Sinifta.Interactables.BoardApps
{
    public class BoardsInteractions : MonoBehaviour, IPointerDownHandler
    {
        public Board.BoardType boardType;
        static bool isBoardOpen = false;

        [SerializeField]
        Button boardButton;

        CompositeDisposable disposables = new CompositeDisposable();
        void Start()
        {
            KarmaMessageBus.OnEvent<OpenBoardEvent>().Subscribe(ev =>
            {
                if (!ev.State)
                {
                    isBoardOpen = false;
                }
            }).AddTo(disposables);

            boardButton.onClick.AddListener(ButtonOnClick);
        }
        void OnDestroy() => disposables.Dispose();
        public void OnPointerDown(PointerEventData eventData)
        {
            if (!isBoardOpen)
            {
                BoardPress();
            }
        }
        void ButtonOnClick()
        {
            if(!isBoardOpen)
            {
                BoardPress();
            }
        }

        void BoardPress()
        {
            if (boardType == Board.BoardType.SmartBoard)
            {
                isBoardOpen = true;
                KarmaMessageBus.Publish(new OpenBoardEvent() { BoardType = Board.BoardType.SmartBoard, State = true });
                //KarmaMessageBus.Publish(new OpenInteractableEvent { Id = boardType.ToString() });
                return;
            }
            if (boardType == Board.BoardType.BlackBoard)
            {
                isBoardOpen = true;
                KarmaMessageBus.Publish(new OpenBoardEvent() { BoardType = Board.BoardType.BlackBoard, State = true });
            }
        }
    }
}
