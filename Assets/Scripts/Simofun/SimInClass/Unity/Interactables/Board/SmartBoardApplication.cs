﻿using KarmaFramework.Interactables;
using KarmaFramework.Interactables.Apps;

namespace Sinifta.Interactables
{
    public class BoardApplication : GadgetApplication
    {
        public BoardApplication(string applicationName, AppResource resource) : base(applicationName, resource)
        {
        }

        public void ForceQuit()
        {

        }

        public void Focus()
        {
            //
        }
    }
}
