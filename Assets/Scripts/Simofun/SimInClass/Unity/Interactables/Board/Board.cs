﻿namespace Sinifta.Interactables
{
	using KarmaFramework.ActionCore;
	using KarmaFramework.EnvironmentCore;
	using KarmaFramework.Interactables;
	using KarmaFramework.Interactables.Apps;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Sinifta.Environment;
	using Sinifta.Interactables.BoardApps;
	using System.Collections;
	using UniRx;
	using UnityEngine;

	/// <summary>
	/// Old KARMA has 2 boards deriving from this class (SmartBoard, BlackBoard) but it has nothing but duplicate code
	/// Turns out BlackBoard is also a SmartBoard with only one drawing app lol
	/// </summary>
	public class Board : Interactable
	{
		public BoardApplication App;
		public RectTransform AppHolder;

		private Renderer _boardScreen;
		private BoardType _type;
		private GameEnvironment _classroom;
		private GadgetApplication.Factory _factory;

		public Board(BaseAction.Factory actionFactory,
			GameEnvironment classroom,
			BoardType type,
			string id,
			GadgetApplication.Factory appFactory) : base(id, actionFactory)
		{
			_type = type;
			_classroom = classroom;
			_factory = appFactory;
		}

		public void CreateApps()
		{
			_boardScreen = (_classroom as Classroom).BoardDict[_type].GetComponent<Renderer>();
			// this could also be moved to an config
			switch (_type)
			{
				case BoardType.SmartBoard:
					App = _factory.Create(typeof(SmartBoardOperatingSystemApp), "SmartBoardOperatingSystemApp") as BoardApplication;
					break;
				case BoardType.BlackBoard:
					App = _factory.Create(typeof(DrawingApp), "DrawingApp") as BoardApplication;
					break;
				default:
					break;
			}
		}

		protected override void OnOpen()
		{
			base.OnOpen();
			KarmaMessageBus.Publish(new OpenBoardEvent() { State = true, BoardType = _type});
		}

		protected override void OnClose()
		{
			base.OnClose();
			UpdateGameView();
		}

		public void UpdateGameView()
		{
			// start heavy method on thread pool
			Observable.FromCoroutine(CreateBoardTexture).Subscribe();
		}


		// This method read pixels from screen which is a heavy method
		// not recommended to call on main thread
		private IEnumerator CreateBoardTexture()
		{
			Canvas.ForceUpdateCanvases();
			yield return new WaitForEndOfFrame();
			Vector3[] cornerPositions = new Vector3[4];
			AppHolder.GetWorldCorners(cornerPositions);

			float xMin = Mathf.Infinity, xMax = Mathf.NegativeInfinity, yMin = Mathf.Infinity, yMax = Mathf.NegativeInfinity;
			for (int i = 0; i < 4; i++)
			{
				Vector3 v = cornerPositions[i];

				if (v.x < xMin) xMin = v.x;
				if (v.x > xMax) xMax = v.x;
				if (v.y < yMin) yMin = v.y;
				if (v.y > yMax) yMax = v.y;
			}

			int width = (int)(xMax - xMin);
			int height = (int)(yMax - yMin);

			var texture = new Texture2D(width, height, TextureFormat.RGB24, false);
			texture.ReadPixels(new Rect(xMin, yMin, width, height), 0, 0, false);
			texture.Apply();
			_boardScreen.material.mainTexture = texture;

			KarmaMessageBus.Publish(new OpenBoardEvent() { State = false, BoardType = _type});
		}

		public enum BoardType
		{ 
			SmartBoard,
			BlackBoard
		}        
	}

	public class OpenBoardEvent : KarmaBaseUnityEvent
	{
		public bool State { get; set; }

		public Board.BoardType BoardType { get; set; }
	}
}
