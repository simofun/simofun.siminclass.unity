﻿using KarmaFramework.Interactables.Apps;
using KarmaFramework.ScenarioCore;
using UnityEngine;

namespace Sinifta.Interactables.BoardApps
{
    public class ImageViewerApp : BoardApplication
    {
        public string ImageName { get; private set; } 
        public Texture2D Image { get; set; }

        public ImageViewerApp(AppResourceConfig config, string imageName, AppResource resource) : base(imageName, resource)
        {
            ImageName = imageName;
            Image = config.GetTexture(imageName);
        }

        public void ShowImage()
        {

        }
    }
}
