﻿namespace Sinifta.Interactables.BoardApps
{
	using KarmaFramework.Interactables.Apps;
	using KarmaFramework.ScenarioCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public class FileBrowserApp : BoardApplication
	{
		private readonly FileBrowserAppResources _resources;
		private AppResourceConfig _config;

		public FileBrowserApp(AppResourceConfig config, string applicationName, FileBrowserAppResources resource) 
			: base(applicationName, resource)
		{
			_resources = resource;
			_config = config;
		}

		public IEnumerable<GadgetButton> GetFolderButtons()
		{
			return _resources.Folders.Select(x =>
				new GadgetButton(x.Name, x.Name, () => true, null));
		}
		
		public IEnumerable<GadgetButton> GetFolderContent(int current)
		{
			var buttons = new List<GadgetButton>();
			foreach (var image in _resources.Folders[current].Images)
			{
				var button = new GadgetButton();
				button.Name = image.Name;
				var texture = _config.GetTexture(image.Name);
				var rect = new Rect(0, 0, texture.width, texture.height);
				var pivot = new Vector2(0.5f, 0.5f);
				button.Icon = Sprite.Create(texture, rect, pivot);
				buttons.Add(button);
				button.Action = () =>
				{
					KarmaMessageBus.Publish(new ShowImageOnBoardEvent() { ImageName = image.Name});
				};
			}

			return buttons;
		}
	}

	public class ShowImageOnBoardEvent : KarmaBaseUnityEvent
	{
		public string ImageName { get; set; }
	}

	[Serializable]
	public class FileBrowserAppResources : FolderableResource
	{
	}
}
