﻿using KarmaFramework.Interactables.Apps;

namespace Sinifta.Interactables.BoardApps
{
    
    public class DrawingApp : BoardApplication
    {
        public DrawingApp(string applicationName, AppResource resource) : base(applicationName, resource)
        {
        }
    }
}
