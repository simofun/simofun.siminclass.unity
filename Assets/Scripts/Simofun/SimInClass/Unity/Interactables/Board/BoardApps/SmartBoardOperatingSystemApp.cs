﻿namespace Sinifta.Interactables.BoardApps
{
	using KarmaFramework.Interactables.Apps;

	public class SmartBoardOperatingSystemApp : BoardApplication
	{
		#region Fields
		readonly Factory factory;
		#endregion

		#region Constructors
		public SmartBoardOperatingSystemApp(Factory factory,
			string appName,
			AppResource resources)
			: base(appName, resources)
		{
			this.FileBrowserApp = factory.Create(typeof(FileBrowserApp), nameof(FileBrowserApp)) as FileBrowserApp;
			this.DrawingApp = factory.Create(typeof(DrawingApp), nameof(DrawingApp)) as DrawingApp;
			this.factory = factory;
		}
		#endregion

		#region Properties
		public DrawingApp DrawingApp { get; set; }

		public FileBrowserApp FileBrowserApp { get; set; }
		#endregion

		#region Public Methods
		public ImageViewerApp OpenImage(string imageName) =>
			this.factory.Create(typeof(ImageViewerApp), imageName) as ImageViewerApp;
		#endregion
	}
}
