﻿namespace Sinifta.Interactables
{
	using KarmaFramework.ActionCore;
	using KarmaFramework.Interactables;
	using KarmaFramework.Interactables.Apps;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Sinifta.Interactables.TabletApps;

	public class Tablet : Interactable
	{
		public TabletOperatingSystemApp App;
		private GadgetApplication.Factory _appFactory;

		public Tablet(string id, BaseAction.Factory factory, GadgetApplication.Factory appFactory) 
			: base(id, factory)
		{
			_appFactory = appFactory;
		}

		public void CreateApp()
		{
			App = _appFactory.Create(typeof(TabletOperatingSystemApp), "TabletOs") as TabletOperatingSystemApp;
		}

		protected override void OnOpen()
		{
			base.OnOpen();

			KarmaMessageBus.Publish(new OpenTabletEvent { State = true});
		}
	}

	public class OpenTabletEvent : KarmaBaseUnityEvent
	{
		public bool State { get; set; }
	}
}
