namespace Sinifta.Interactables
{
    using KarmaFramework.ActionCore;
    using KarmaFramework.Interactables;
    using KarmaFramework.Interactables.Apps;
    using KarmaFramework.ScenarioCore;
    using Simofun.Karma.UniRx.Unity;
    using Simofun.Karma.Unity.Events;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Zenject;

    public class Activity : Interactable

    {
        private GadgetApplication.Factory _appFactory;
        public GameState gameState;
       
        [Inject]
        void Construct(GameState gameState)
        {
            this.gameState = gameState;
         
        }
        public Activity(string id, BaseAction.Factory factory, GadgetApplication.Factory appFactory)
            : base(id, factory)
        {
            _appFactory = appFactory;
        }

        public void CreateApp()
        {
            
        }

        protected override void OnOpen()
        {
            base.OnOpen();
            KarmaMessageBus.Publish(new OpenActivityEvent { State = true });
        }
    }
    public class OpenActivityEvent : KarmaBaseUnityEvent
    {
        public bool State { get; set; }
    }
}