﻿using KarmaFramework.Interactables.Apps;

namespace Sinifta.Interactables
{
    public class TabletApplication : GadgetApplication
    {
        public TabletApplication(string applicationName, AppResource resource) : base(applicationName, resource)
        {
        }
    }
}
