﻿using KarmaFramework.Interactables.Apps;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Sinifta.Interactables.TabletApps
{
    public class TabletOperatingSystemApp : TabletApplication
    {
        public ImageGalleryToStudentApp ImageGalleryToStudentApp { get; set; }
        public StudentTabletsApp StudentTabletsApp { get; set; }

        public TabletOperatingSystemApp(string appName, AppResource resources, Factory factory)
            : base(appName, resources)
        {
            ImageGalleryToStudentApp = factory.Create(typeof(ImageGalleryToStudentApp), "ImageGalleryToStudentApp") as ImageGalleryToStudentApp;
            StudentTabletsApp = factory.Create(typeof(StudentTabletsApp), "StudentTabletsApp") as StudentTabletsApp;
        }

    }
}
