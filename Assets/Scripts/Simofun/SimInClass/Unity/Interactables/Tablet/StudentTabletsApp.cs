﻿using KarmaFramework.ActionCore;
using KarmaFramework.CharacterCore;
using KarmaFramework.Interactables.Apps;
using System.Collections.Generic;
using System.Linq;

namespace Sinifta.Interactables.TabletApps
{
    public class StudentTabletsApp : TabletApplication
    {

        public Dictionary<NPCFacade, List<BaseAction>> StudentBehaviour
        {
            get
            {
                return _registry.GetNPCs().ToDictionary(item => item,
                    item => item.GetActions().ToList());
            }
        }

        private ActorRegistry _registry;

        public StudentTabletsApp(ActorRegistry registry, string appName, AppResource resource)
            : base(appName, resource)
        {
            _registry = registry;
        }

    }
}