﻿namespace Sinifta.Interactables.TabletApps
{
	using KarmaFramework.Interactables.Apps;
	using KarmaFramework.ScenarioCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Sinifta.Actions.TeacherActions;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public class ImageGalleryToStudentApp : TabletApplication
	{
		private ImageGalleryToStudentAppResources _resources;
		private AppResourceConfig _config;
		public List<GadgetButton> Images;

		public ImageGalleryToStudentApp(string appName, ImageGalleryToStudentAppResources r, AppResourceConfig config)
			: base(appName, r)
		{
			_resources = r;
			_config = config;
			Images = new List<GadgetButton>();
		}

		public IEnumerable<GadgetButton> GetFolderButtons()
		{
			return _resources.Folders.Select(x =>
				new GadgetButton(x.Name, x.Name, () => true, null));
		}

		public IEnumerable<GadgetButton> GetFolderContent(int current)
		{
			var buttons = new List<GadgetButton>();
			foreach (var image in _resources.Folders[current].Images)
			{
				var button = new GadgetButton();
				button.Name = image.Name;
				var texture = _config.GetTexture(image.Name);
				var rect = new Rect(0, 0, texture.width, texture.height);
				var pivot = new Vector2(0.5f, 0.5f);
				button.Icon = Sprite.Create(texture, rect, pivot);
				buttons.Add(button);
				button.Action = () =>
				{
					KarmaMessageBus.Publish(new SendImageToTabletEvent() { ImageName = image.Name, Image = texture });
					KarmaMessageBus.Publish(new OpenTabletEvent() { State = false});
					KarmaMessageBus.Publish(new ImageRequirementTryEvent() { ImageName = image.Name });
				};
			}

			return buttons;
		}
	}

	public class SendImageToTabletEvent : KarmaBaseUnityEvent
	{
		public Texture2D Image { get; set; }

		public string ImageName { get; set; }
	}

	[Serializable]
	public class ImageGalleryToStudentAppResources : FolderableResource
	{
	}
}
