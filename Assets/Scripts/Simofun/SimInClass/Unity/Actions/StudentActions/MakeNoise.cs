using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class MakeNoise : StudentAction
	{
        public MakeNoise()
        {
            Icon = "MakeNoise";
            Duration = int.MaxValue;
        }

        public MakeNoise(MakeNoise other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new MakeNoise(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("MakeNoise");
        }
	}
} 