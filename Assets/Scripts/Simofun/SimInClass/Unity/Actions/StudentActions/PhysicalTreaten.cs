using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class PhysicalTreaten : StudentAction
	{
        public PhysicalTreaten()
        {
            Icon = "Punch";
            Duration = 30;
        }

        public PhysicalTreaten(PhysicalTreaten other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new PhysicalTreaten(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PhysicalThreat");
        }
    }
}