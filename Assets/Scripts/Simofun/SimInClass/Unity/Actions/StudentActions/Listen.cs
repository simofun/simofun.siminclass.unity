﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Listen : StudentAction
    {
        public Listen(Listen other) : base(other)
        {
        }

        public Listen()
        {
            Duration = 5;
        }

        public override BaseAction Clone()
        {
            return new Listen(this);
        }
    }
}
