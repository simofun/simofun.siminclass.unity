﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class DamageMaterial : StudentAction
    {
        public DamageMaterial()
        {
            Icon = "DamageEquipment";
            Duration = 5;
        }

        public DamageMaterial(DamageMaterial other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new DamageMaterial(this);
        }

        // Action sets student animation state to "Hurt" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Hurt");
        }
    }
}