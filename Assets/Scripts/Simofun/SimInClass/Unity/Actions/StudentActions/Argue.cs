﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Argue : StudentAction
    {
        public Argue()
        {
            Duration = 10;
        }

        public Argue(Argue other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Argue(this);
        }

        // Action sets student animation state to "PlayGame" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PlayGame");
        }
    }
}