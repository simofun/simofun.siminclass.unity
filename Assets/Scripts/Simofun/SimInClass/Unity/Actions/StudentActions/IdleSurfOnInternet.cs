﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class IdleSurfOnInternet : StudentAction
    {
        public IdleSurfOnInternet()
        {
            Icon = "Texting";
            Duration = 15;
        }

        public IdleSurfOnInternet(IdleSurfOnInternet other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new IdleSurfOnInternet(this);
        }

        // Action sets student animation state to "PlayGame" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PlayGame");
        }
    }
}