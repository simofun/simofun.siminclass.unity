﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Film : StudentAction
    {
        public Film()
        {
            Icon = "Texting";
            Duration = 15;
        }

        public Film(Film other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Film(this);
        }

        // Action sets student animation state to "Film" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Film");
        }
    }
}