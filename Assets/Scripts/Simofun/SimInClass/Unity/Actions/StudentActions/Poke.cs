﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Poke : StudentAction
    {
        public Poke()
        {
            Icon = "Poke";
            Duration = 15;
        }

        public Poke(Poke other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Poke(this);
        }

        // Action sets student animation state to "Hurt" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Hurt");
        }
    }
}