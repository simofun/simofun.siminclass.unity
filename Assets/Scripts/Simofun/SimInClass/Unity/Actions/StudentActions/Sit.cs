﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Sit : StudentAction
    {
        public Sit()
        {
        }

        public Sit(Sit other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Sit(this);
        }

        // Action makes a precheck where whether he/she is sitting or not and if it returns true then the the student adjust own position and rotation accordingly to the chair and sets student animation state to "Sit" at the end.

        /*
        public override bool Precheck(ActorFacade af)
        {
           return base.Precheck() && !Actor.GetComponent<StudentAnimController>().IsSitting;
        }

        IEnumerator FitInSlotCoroutine()
        {
            const float fitTime = 1f;

            var targetPos = (Actor as StudentAvatar).ChairSlot.position;
            var targetRot = (Actor as StudentAvatar).ChairSlot.rotation;

            for (float f = 0; f < fitTime; f += KarmaTime.DeltaTime)
            {
                Actor.transform.position = Vector3.Lerp(Actor.transform.position, targetPos, KarmaTime.DeltaTime * 10);
                Actor.transform.rotation = Quaternion.Lerp(Actor.transform.rotation, targetRot, KarmaTime.DeltaTime * 10);

                yield return null;
            }

            Actor.transform.position = targetPos;
            Actor.transform.rotation = targetRot;
        }
        */

        public override void OnComplete()
        {
            base.OnComplete();
            Actor.SetAnimState("IsSitting");
        }
    }
}