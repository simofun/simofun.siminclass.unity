﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class AnswerQuestion : StudentAction
    {
        public AnswerQuestion() { }

        public AnswerQuestion(AnswerQuestion other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new AnswerQuestion(this);
        }
    }
}
