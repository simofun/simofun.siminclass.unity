﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Hurt : StudentAction
    {
        public Hurt()
        {
            Duration = 15;
        }

        public Hurt(Hurt other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Hurt(this);
        }

        // Action sets student animation state to "Hurt" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Hurt");
        }
    }
}