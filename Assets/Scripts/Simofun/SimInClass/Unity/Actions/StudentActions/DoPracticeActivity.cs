﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class DoPracticeActivity : StudentAction
    {
        public DoPracticeActivity()
        {
        }

        public DoPracticeActivity(DoPracticeActivity other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new DoPracticeActivity(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Write");
            Duration = int.MaxValue;
        }
    }
}
