﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class PersonalWork : StudentAction
    {
        public PersonalWork()
        {
            Icon = "DrawOnBook";
            Duration = 20;
        }

        public PersonalWork(PersonalWork other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new PersonalWork(this);
        }

        // Action sets student animation state to "PlayGame" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PlayGame");
        }
    }
}