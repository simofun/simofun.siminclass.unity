﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class SendInstantMessage : StudentAction
    {
        public SendInstantMessage()
        {
            Icon = "Texting";
            Duration = 30;
        }

        public SendInstantMessage(SendInstantMessage other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new SendInstantMessage(this);
        }

        // Action sets student animation state to "SendInstantMessage" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("SendInstantMessage");
        }
    }
}