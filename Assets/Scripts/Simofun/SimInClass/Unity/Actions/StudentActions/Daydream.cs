﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Daydream : StudentAction
    {
        public Daydream()
        {
            Icon = "Sleep";
            Duration = 60;
        }

        public Daydream(Daydream other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Daydream(this);
        }

        // Action sets student animation state to "Daydream" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Daydream");
        }
    }
}