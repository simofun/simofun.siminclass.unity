using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Fight : StudentAction
    {
        public Fight()
        {
            Icon = "Punch";
            Duration = 30;
        }

        public Fight(Fight other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Fight(this);
        }
    }
}