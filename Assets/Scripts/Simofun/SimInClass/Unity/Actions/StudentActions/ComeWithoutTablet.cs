using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class ComeWithoutTablet : StudentAction
    {
        public ComeWithoutTablet()
        {
        }

        public ComeWithoutTablet(ComeWithoutTablet other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new ComeWithoutTablet(this);
        }
    }
}