﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class TakeNote : StudentAction
    {
        public TakeNote()
        {
            Duration = 10;
        }

        public TakeNote(TakeNote other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TakeNote(this);
        }

        // Action sets student animation state to "Write" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Write");
        }
    }
}