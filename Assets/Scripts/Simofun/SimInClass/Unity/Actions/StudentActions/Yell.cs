﻿using KarmaFramework.ActionCore;
using KarmaFramework.CharacterCore;
using Simofun.Karma.UniRx.Unity;
using Sinifta.Actors;
using System.Collections.Generic;
using System.Linq;

namespace Sinifta.Actions.StudentActions
{
	public class Yell : StudentAction
	{
		public Yell()
		{
			Icon = "Yell";
			Duration = 15;
		}

		public Yell(Yell other) : base(other)
		{
		}

		public override BaseAction Clone()
		{
			return new Yell(this);
		}

		// Action sets student animation state to "SambaDancing" at the start.
		public override void OnStart()
		{
			base.OnStart();
			Actor.SetAnimState("SambaDancing");
		}

		public override void OnComplete()
		{
			base.OnComplete();
			KarmaMessageBus.Publish(new AdjustMindParamEvent()
			{
				Delta = new MindSnapshot(
					new Dictionary<string, int>() 
				{
						{"Concentration", -2 },
						{"Enjoyment", -2 },
						{"Knowledge", 0 }
				})
			});

			StudentFacade target = Targets.FirstOrDefault() as StudentFacade;
			if (target != null)
			{
				MindSnapshot mind = target.GetMindSnapshot();
				mind.Params["Concentration"] -= 8;
				mind.Params["Enjoyment"] -= 8;
				target.OverrideMindWithSnapshot(mind);
			}
		}
	}
}
