﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Texting : StudentAction
    {
        public Texting()
        {
            Icon = "Texting";
            Duration = int.MaxValue;
        }

        public Texting(Texting other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Texting(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Texting");
        }
    }
}
