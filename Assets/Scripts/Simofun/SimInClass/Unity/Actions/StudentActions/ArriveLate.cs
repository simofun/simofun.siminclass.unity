using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class ArriveLate : StudentAction
    {
        public ArriveLate()
        {
        }

        public ArriveLate(ArriveLate other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new ArriveLate(this);
        }
    }
}