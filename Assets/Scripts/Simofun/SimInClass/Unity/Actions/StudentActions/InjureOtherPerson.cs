﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class InjureOtherPerson : StudentAction
    {
        public InjureOtherPerson()
        {
            Duration = 15;
        }

        public InjureOtherPerson(InjureOtherPerson other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new InjureOtherPerson(this);
        }

        // Action sets student animation state to "GangnamStyle" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("GangnamStyle");
        }
    }
}