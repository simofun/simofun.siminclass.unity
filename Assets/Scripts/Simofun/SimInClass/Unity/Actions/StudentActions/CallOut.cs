﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class CallOut : StudentAction
    {
        public CallOut()
        {
            Icon = "Talking";
            Duration = 15;
        }

        public CallOut(CallOut other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new CallOut(this);
        }

        // Action sets student animation state to "CallOut" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("CallOut");
        }
    }
}