﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class PlayGame : StudentAction
    {
        public PlayGame()
        {
            Icon = "Texting";
            Duration = 60;
        }

        public PlayGame(PlayGame other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new PlayGame(this);
        }

        // Action sets student animation state to "PlayGame" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PlayGame");
        }
    }
}