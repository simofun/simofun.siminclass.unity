﻿using KarmaFramework.ActionCore;
using System.Collections.Generic;

namespace Sinifta.Actions.StudentActions
{
    public class ReturnToDesk : StudentAction
    {
        public ReturnToDesk()
        {
        }

        public ReturnToDesk(ReturnToDesk other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new ReturnToDesk(this);
        }

        // Action creates a sequence of actions which makes the student move to the chair and sit.
        public override List<BaseAction> GetSequence()
        {
            return new List<BaseAction>
            {
                /*
                 new Move()
                 {
                    Waypoint = (Actor as Student).Chair.name
                 },
                 new Sit()
                 */
            };
        }
    }
}