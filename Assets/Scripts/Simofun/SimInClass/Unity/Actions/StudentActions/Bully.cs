﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Bully : StudentAction
    {
        public Bully()
        {
            Icon = "Punch";
            Duration = 15;
        }

        public Bully(Bully other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Bully(this);
        }


        // Action sets student animation state to "Hurt" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Hurt");
        }
    }
}