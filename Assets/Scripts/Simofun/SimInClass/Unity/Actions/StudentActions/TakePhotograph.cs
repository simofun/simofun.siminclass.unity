﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class TakePhotograph : StudentAction
    {
        public TakePhotograph()
        {
            Icon = "Texting";
            Duration = 20;
        }

        public TakePhotograph(TakePhotograph other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TakePhotograph(this);
        }
        // Action sets student animation state to "PlayGame" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PlayGame");
        }
    }
}