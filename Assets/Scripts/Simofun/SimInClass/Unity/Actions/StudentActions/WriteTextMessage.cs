﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class WriteTextMessage : StudentAction
    {
        public WriteTextMessage()
        {
            Icon = "Texting";
            Duration = int.MaxValue;
        }

        public WriteTextMessage(WriteTextMessage other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new WriteTextMessage(this);
        }
        // Action sets student animation state to "PlayGame" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PlayGame");
        }
    }
}