﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Stand : StudentAction
    {
        public Stand()
        {
        }

        public Stand(Stand other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Stand(this);
        }

        public override void OnComplete()
        {
            base.OnComplete();
            //Actor.GetComponent<StudentAnimController>().IsSitting = false;
        }
    }
}