﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class InjureSelf : StudentAction
    {
        public InjureSelf(InjureSelf other) : base(other)
        {
        }

        public InjureSelf()
        {
            Duration = 15;
        }

        public override BaseAction Clone()
        {
            return new InjureSelf(this);
        }

        // Action sets student animation state to "Hurt" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Hurt");
        }
    }
}