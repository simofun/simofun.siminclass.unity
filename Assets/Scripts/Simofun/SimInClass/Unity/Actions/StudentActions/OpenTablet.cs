﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class OpenTablet : StudentAction
    {
        public OpenTablet()
        {
        }

        public OpenTablet(OpenTablet other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new OpenTablet(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            // TODO: show tablet
        }
    }
}