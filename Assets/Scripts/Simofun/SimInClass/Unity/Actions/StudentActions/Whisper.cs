using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Whisper : StudentAction
	{
        public Whisper()
        {
            Icon = "Whisper";
            Duration = 20;
        }

        public Whisper(Whisper other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Whisper(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Whisper");
        }
    }
}