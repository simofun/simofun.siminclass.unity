﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class HaveQuestion : StudentAction
    {
        public HaveQuestion()
        {
            Duration = 25;
        }

        public HaveQuestion(HaveQuestion other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new HaveQuestion(this);
        }

        // Action sets student animation state to "HaveQuestion" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("HaveQuestion");
        }
    }
}