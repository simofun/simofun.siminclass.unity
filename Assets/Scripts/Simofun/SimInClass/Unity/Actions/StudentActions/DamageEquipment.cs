﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class DamageEquipment : StudentAction
    {
        public DamageEquipment()
        {
            Duration = 15;
            Icon = "DamageEquipment";
        }

        public DamageEquipment(DamageEquipment other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new DamageEquipment(this);
        }

        // Action sets student animation state to "DamageEquipment" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("DamageEquipment");
        }
    }
}