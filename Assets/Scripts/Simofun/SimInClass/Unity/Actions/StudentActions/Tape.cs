﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Tape : StudentAction
    {
        public Tape()
        {
            Icon = "Texting";
            Duration = 30;
        }

        public Tape(Tape other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Tape(this);
        }
        // Action sets student animation state to "PlayGame" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PlayGame");
        }
    }
}