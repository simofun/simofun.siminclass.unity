﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Talk : StudentAction
    {
        public Talk()
        {
            Icon = "Talking";
            Duration = int.MaxValue;
        }

        public Talk(Talk other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Talk(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            // TODO: make this work
            //var NearestStudent = KarmaManagers.AiManager.GetNearestAnyStudent(Actor);
            //Target = NearestStudent;
            //if (NearestStudent != null)
            //{
            //    base.Starting();
            //    Actor.AnimController.SetStudentAnimation(StudentAnim.Talking);
            //    Target.AnimController.SetStudentAnimation(StudentAnim.Talking);
            //}


        }
        public override void OnComplete()
        {
            base.OnComplete();
            //Actor.AnimController.SetStudentAnimation(StudentAnim.None);
            //Target.AnimController.SetStudentAnimation(StudentAnim.None);
        }


    }
}
 