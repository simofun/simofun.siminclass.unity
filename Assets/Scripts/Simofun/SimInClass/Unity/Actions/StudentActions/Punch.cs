﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Punch : StudentAction
    {
        public Punch()
        {
            Icon = "Punch";
            Duration = 12;
        }

        public Punch(Punch other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Punch(this);
        }
        /*public override bool Precheck(ActorFacade af)
        {
            return base.Preheck() &&
                   Actor.CurrentActions.Count == 0 &&
                   Vector3.Distance(af.transform.position, Actor.transform,position) > 10;
        }*/

        // Action makes a precheck where how where he/she is from teacher and if it returns true then sets student animation state to "Hurt" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Hurt");
        }
    }
}