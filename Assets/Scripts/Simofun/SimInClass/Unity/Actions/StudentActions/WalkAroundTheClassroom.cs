using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class WalkAroundTheClassroom : StudentAction
    {
        public WalkAroundTheClassroom()
        {
        }

        public WalkAroundTheClassroom(WalkAroundTheClassroom other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new WalkAroundTheClassroom(this);
        }
    }
}