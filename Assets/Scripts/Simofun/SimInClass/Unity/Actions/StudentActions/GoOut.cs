﻿using KarmaFramework.ActionCore;
using System.Collections.Generic;

namespace Sinifta.Actions.StudentActions
{
    public class GoOut : StudentAction
    {
        public GoOut()
        {
        }

        public GoOut(GoOut other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new GoOut(this);
        }

        // Action creates a sequence of actions which makes the student move to the corridor first and then go out.
        public override List<BaseAction> GetSequence()
        {
            return new List<BaseAction>()
            {
                /*new MoveAction()
                {
                    Waypoint = "CorridorSlot"
                },
                new GoOut()*/
            };
        }

        public override void OnStart()
        {
            base.OnStart();
            //FallbackExecutableAction = _actionFactory.CreateAction("ReturnToDesk", Targets[0], new List<ActorFacade>());
        }
    }
}