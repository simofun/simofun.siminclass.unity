﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Idle : StudentAction
    {
        public Idle()
        {
            Duration = 60;
        }

        public Idle(Idle other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Idle(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            Duration = 60;
        }
    }
}
