﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class PassNotes : StudentAction
    {
        public PassNotes()
        {
            Icon = "DrawOnBook";
            Duration = 15;
        }

        public PassNotes(PassNotes other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new PassNotes(this);
        }

        // Action sets student animation state to "Write" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Write");
        }
    }
}