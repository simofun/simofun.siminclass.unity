﻿using KarmaFramework.ActionCore;
using System.Collections.Generic;

namespace Sinifta.Actions.StudentActions
{
    public class DoPresentation : StudentAction
    {
        public DoPresentation()
        {
        }

        public DoPresentation(DoPresentation other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new DoPresentation(this);
        }

        // Action creates a sequence of actions which makes the student move to the table, look, do the presentation and finally return to his/her desk.
        public override List<BaseAction> GetSequence()
        {
            return new List<BaseAction>
            {
                // new Move(),
                //new Look(),
                //new DoPresentation(),
                //new ReturnToDesk()
            };
        }
    }

}