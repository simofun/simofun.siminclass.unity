﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class ComeWithoutSupplies : StudentAction
    {
        public ComeWithoutSupplies()
        {
        }

        public ComeWithoutSupplies(ComeWithoutSupplies other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new ComeWithoutSupplies(this);
        }

        // Action sets student animation state to "ComeWithoutSupplies" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("ComeWithoutSupplies");
        }
    }
}