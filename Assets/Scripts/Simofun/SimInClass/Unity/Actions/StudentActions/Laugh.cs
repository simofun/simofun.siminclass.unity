﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Laugh : StudentAction
    {
        public Laugh(StudentAction other) : base(other)
        {
        }

        public Laugh()
        {
            Icon = "Laugh";
            Duration = 15;
        }

        public override BaseAction Clone()
        {
            return new Laugh(this);
        }

        // Action sets student animation state to "SambaDancing" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("SambaDancing");
        }
    }
}