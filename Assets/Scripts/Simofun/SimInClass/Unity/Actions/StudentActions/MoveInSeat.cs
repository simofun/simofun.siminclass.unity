using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class MoveInSeat : StudentAction
    {

        public MoveInSeat()
        {
            Icon = "PlayWithTools";
            Duration = 20;
        }

        public MoveInSeat(MoveInSeat other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new MoveInSeat(this);
        }
    }
}