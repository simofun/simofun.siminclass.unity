﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class DoActivity : StudentAction
    {
        public DoActivity()
        {
            Duration = 30;
        }

        public DoActivity(DoActivity other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new DoActivity(this);
        }

        // Action sets student animation state to "Write" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Write");
        }
    }
}