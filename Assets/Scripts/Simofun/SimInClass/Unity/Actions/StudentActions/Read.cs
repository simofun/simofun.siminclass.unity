using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Read : StudentAction
	{
        public Read()
        {
            Duration = 60;
        }

        public Read(Read other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Read(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Reading");
        }
	}
}