﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class RepeatClassRules : StudentAction
    {
        public RepeatClassRules()
        {
        }

        public RepeatClassRules(RepeatClassRules other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new RepeatClassRules(this);
        }
    }
}
