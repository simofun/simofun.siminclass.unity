﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class CallOutFrequently : StudentAction
    {
        public CallOutFrequently()
        {
            Duration = 30;
        }

        public CallOutFrequently(CallOutFrequently other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new CallOutFrequently(this);
        }

        // Action sets student animation state to "CallOut" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("CallOut");
        }
    }
}