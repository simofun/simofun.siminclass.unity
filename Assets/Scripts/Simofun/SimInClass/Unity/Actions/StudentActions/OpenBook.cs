﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class OpenBook : StudentAction
    {

        public OpenBook()
        {

        }

        public OpenBook(OpenBook other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new OpenBook(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            // TODO: show student book
        }

    }
}