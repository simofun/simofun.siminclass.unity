﻿using KarmaFramework.ActionCore;
using System.Collections.Generic;
using UnityEngine;

namespace Sinifta.Actions.StudentActions
{
    public class Move : StudentAction
    {
        private Vector3 _target;
        private Vector3 _curTarget;
        private List<Vector3> _path;
        private const float WalkSpd = 1f;
        private float RotateSpd = 10f;
        private bool _isPathEmpty;
        private bool _disableAutoRotation = false;

        public Move()
        {
        }

        public Move(Move other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Move(this);
        }

        // TODO: implement
        //public override List<BaseAction> GetSequence()
        //{
        //    var seq = new List<BaseAction>()
        //    {
        //        new Stand(),
        //        new Move()
        //    };
        //
        //    // If student is standing, don't stand again
        //    if (!Actor.Animator.GetBool("Sit"))
        //    {
        //        seq.RemoveAt(0);
        //    }
        //
        //    return seq;
        //}
        //
        //public override void Starting()
        //{
        //    // Extract target position from BaseAction
        //    base.Starting();
        //    var targetGO = GameObject.Find(ActionAs<Move>().Waypoint);
        //    if (targetGO == null)
        //    {
        //        Debug.LogError("Target with name : " + ActionAs<Move>().Waypoint + " couldn't be found. Aborting move");
        //        return;
        //    }
        //    _target = targetGO.transform.position;
        //
        //    // Prepare path
        //    _path = KarmaManagers.EventManager.GetPath(Actor.transform.position, _target);
        //    if (_path.Count == 0)
        //    {
        //        // Path not found
        //        // or start and end points are the same
        //        _isPathEmpty = true;
        //        return;
        //    }
        //
        //    _curTarget = _path[0];
        //
        //    if (_path.Count > 0)
        //    {
        //        if (Actor is TeacherAvatar)
        //        {
        //            _path.RemoveAt(0);
        //    
        //            var t = targetGO.transform.position;
        //            var a = Actor.transform.position;
        //            var targetDelta = t - a;
        //            
        //            while (Vector3.Dot(_path[0] - a, targetDelta) <= 0)
        //            {
        //                _path.RemoveAt(0);
        //            }
        //        }
        //        if (Actor is TeacherAvatar)
        //        {
        //            Actor.GetComponent<Animator>().SetFloat("Speed", 10.0f);
        //        }
        //        else
        //        {
        //            Actor.AnimController.SetSpeed(10.0f);
        //        }
        //        RotateSpd = (Actor is TeacherAvatar) ? 1f : 6.5f;
        //    }
        //    else
        //    {
        //        _isPathEmpty = true;
        //    }
        //}
        //
        //protected override ActionState Execute()
        //{
        //    if (_path == null || _isPathEmpty)
        //    {
        //        return ActionState.Failed;
        //    }
        //
        //    if ( (Actor.transform.position - _curTarget).sqrMagnitude > 0.04f)
        //    {
        //        // Look at target
        //        var toTargetHorz = Vector3.ProjectOnPlane(_curTarget - Actor.transform.position, Vector3.up);
        //
        //        if (Actor is TeacherAvatar)
        //        {
        //            _disableAutoRotation = true;
        //        }
        //
        //        if (!_disableAutoRotation)
        //        {
        //            if (Vector3.Angle(toTargetHorz, Actor.transform.forward) > 1f)
        //            {
        //                var targetRot = Quaternion.LookRotation(toTargetHorz, Vector3.up);
        //                Actor.transform.rotation = Quaternion.Slerp(Actor.transform.rotation, targetRot,
        //                    KarmaTime.DeltaTime * RotateSpd);
        //            }
        //        }
        //
        //        // Walk
        //        Actor.transform.position += (toTargetHorz.normalized * WalkSpd * KarmaTime.DeltaTime);
        //
        //        Debug.DrawLine(Actor.transform.position, _curTarget, Color.red);
        //    }
        //    else
        //    {
        //        if (_path.Count != 0)
        //        {
        //            // Get new wp
        //            _curTarget = _path[0];
        //            //_curTarget = Vector3.ProjectOnPlane(_curTarget, Vector3.up);
        //
        //            _path.RemoveAt(0);
        //        }
        //        else
        //        {
        //            if (Actor is TeacherAvatar)
        //            {
        //                Actor.GetComponent<Animator>().SetFloat("Speed", 0f);
        //            }
        //            else
        //            {
        //                Actor.AnimController.SetSpeed(0f);
        //            }
        //            // No wp remaining, complete
        //            //Actor.GetComponent<Animator>().SetFloat("Speed", 0.0f);
        //            return ActionState.Success;
        //        }
        //    }
        //
        //    return ActionState.Active;
        //}
        //
        //public override void Completing()
        //{
        //    base.Completing();
        //    if (Actor is TeacherAvatar)
        //    {
        //        //Actor.GetComponent<Animator>().SetFloat("Speed", 0f);
        //    }
        //    else
        //    {
        //        Actor.AnimController.SetSpeed(0f);
        //    }
        //}
    }
}
