﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class WaitForTeacher : StudentAction
    {
        public WaitForTeacher()
        {
        }

        public WaitForTeacher(WaitForTeacher other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new WaitForTeacher(this);
        }
        // Action sets student animation state to "HaveQuestion" at the start.
        public override void OnStart()
        {
            Actor.SetAnimState("HaveQuestion");
        }
    }
}