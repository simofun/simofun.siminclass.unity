﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class PlayWithTools : StudentAction
    {
        public PlayWithTools()
        {
            Icon = "PlayWithTools";
            Duration = 10;
        }

        public PlayWithTools(PlayWithTools other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new PlayWithTools(this);
        }

        // Action sets student animation state to "PlayWithTools" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("PlayWithTools");
        }
    }
}