﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Sleep : StudentAction
    {
        public Sleep()
        {
            Icon = "Sleep";
            Duration = int.MaxValue;
        }

        public Sleep(Sleep other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Sleep(this);
        }

        // Action sets student animation state to "Sleep" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Sleep");
        }
    }
}