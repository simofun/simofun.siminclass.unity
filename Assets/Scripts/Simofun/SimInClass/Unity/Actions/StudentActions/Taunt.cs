﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class Taunt : StudentAction
    {
        public Taunt()
        {
            Duration = 20;
        }

        public Taunt(Taunt other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new Taunt(this);
        }
        // Action sets student animation state to "SambaDancing" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("SambaDancing");
        }
    }
}