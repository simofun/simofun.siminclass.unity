﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.StudentActions
{
    public class DrawOnBook : StudentAction
    {
        public DrawOnBook()
        {
            Icon = "DrawOnBook";
            Duration = 40;
        }

        public DrawOnBook(DrawOnBook other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new DrawOnBook(this);
        }

        // Action sets student animation state to "Write" at the start.
        public override void OnStart()
        {
            base.OnStart();
            Actor.SetAnimState("Write");
        }
    }
}