﻿using KarmaFramework.ActionCore;
using Sinifta.Actors;
using System.Collections.Generic;

namespace Sinifta.Actions.StudentActions
{
    public class AskQuestion : StudentAction
    {
        public AskQuestion()
        {
        }

        public AskQuestion(AskQuestion other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new AskQuestion(this);
        }

        // Action creates a sequence of actions which makes the student stands up, ask the question and sit down. If the student misbehaves, he/she will only ask the question without standing up and sitting down.

        public override List<BaseAction> GetSequence()
        {
            var seq = new List<BaseAction>
            {
                // new Stand(),
                //new AskQuestionAction(),
                //new Sit()
            };

            if((Actor as StudentFacade).GetMindSnapshot().Params["RespectTeacher"] < 30)
            {
                // Remove sit and stand actions
            }

            return seq;
        }
        public override void OnStart()
        {
            base.OnStart();
            // interface manager show speech
        }

        public override void OnComplete()
        {
            base.OnComplete();
            // interface manager close speech
        }
    }

}