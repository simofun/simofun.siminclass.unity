﻿namespace Sinifta.Actions
{
	using KarmaFramework;
	using KarmaFramework.ActionCore;
	using KarmaFramework.CharacterCore;
	using KarmaFramework.NetworkingCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Sinifta.Actors;
	using Sinifta.Networking;
	using System;
	using System.Collections.Generic;
	using System.Linq;

	[Serializable]
	public class StudentAction : NPCAction
	{
		private float _nextMisbehaviourCheckTime;
		private Report.ActionRecord _record;
		public StudentAction() 
		{
			_record = new Report.ActionRecord()
			{
				ExecutableAction = this,
			};
		}

		public StudentAction(StudentAction other) : base(other) 
		{
			_record = new Report.ActionRecord()
			{
				ExecutableAction = this,
			};
		}


		public override void OnComplete()
		{
			base.OnComplete();

			_record.FinishTime = (int)KarmaTime.Time;
			KarmaMessageBus.Publish(_record);

			KarmaMessageBus.Publish(new ActionMindUpdateEvent()
			{
				Action = this,
				Targets = Targets.OfType<StudentFacade>().ToList()
			});

			List<string> _targetNames = new List<string>();
			foreach (var target in Targets)
			{
				_targetNames.Add(target.Name);
			}
			KarmaMessageBus.Publish(new ActionTriggeredEvent()
			{
				Action = new BaseActionRpcState()
				{
					Id = this.Id,
					Name = this.Name,
					Duration = this.Duration,
					Icon = this.Icon,
					BackgroundColor = this.BackgroundColor,
					ActorName = this.Actor.Name,
					Type = RpcActionType.Student,
					TargetActorNames = _targetNames
				}
			});
		}

		public override void OnStart()
		{
			base.OnStart();
			_nextMisbehaviourCheckTime = KarmaTime.Time + 12;
			_record.StartTime = (int)KarmaTime.Time;
		}

		public override void OnTick()
		{
			base.OnTick();
			var student = (Actor as StudentFacade);
			if (IsMisbehaviour && KarmaTime.Time >= _nextMisbehaviourCheckTime)
			{
				MindSnapshot mind = student.GetMindSnapshot();
				mind.Params["Concentration"] -= 3;
				(Actor as StudentFacade).OverrideMindWithSnapshot(mind);
				_nextMisbehaviourCheckTime += 12;
				student.UpdateMind(this);
			}

			//KarmaMessageBus.Publish(new ActionMindUpdateEvent()
			//{
			//    Action = this,
			//    Targets = Targets.OfType<StudentFacade>().ToList()
			//});
		}
		public T ActionAs<T>() where T : BaseAction
		{
			if (!(this is T))
			{
				/*Debug.LogError("ExecutableAction::Action cast error : Type of [" + this.GetType() +
							   "] couldn't be cast to [" + typeof(T) + "]");*/
				return null;
			}

			BaseAction studentAction = this;
			return (T)studentAction;
		}

		public override BaseAction Clone()
		{
			return new StudentAction(this);
		}
	}

	public class ActionMindUpdateEvent : KarmaBaseUnityEvent
	{ 
		public BaseAction Action { get; set; }

		public List<StudentFacade> Targets; 
	}
}
