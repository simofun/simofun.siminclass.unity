﻿namespace Sinifta.Actions
{
	using KarmaFramework;
	using KarmaFramework.ActionCore;
	using KarmaFramework.CharacterCore;
	using KarmaFramework.NetworkingCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Sinifta.Actors;
	using Sinifta.Networking;
	using System;
	using System.Collections.Generic;
	using System.Linq;

	[Serializable]
	public class TeacherAction : PlayerAction
	{
		private MindSnapshot _delta;
		protected Report.ActionRecord _record;

		public TeacherAction()
		{
			_record = new Report.ActionRecord()
			{
				ExecutableAction = this,
			};
		}

		public TeacherAction(TeacherAction other) : base(other) 
		{
			_record = new Report.ActionRecord()
			{
				ExecutableAction = this,
			};
		}

		public override void OnComplete()
		{
			base.OnComplete();

			_record.FinishTime = (int)KarmaTime.Time;
			KarmaMessageBus.Publish(_record);

			KarmaMessageBus.Publish(new ActionMindUpdateEvent()
			{
				Action = this,
				Targets = Targets.OfType<StudentFacade>().ToList()
			});

			List<string> _targetNames = new List<string>();
			foreach(var target in Targets)
			{
				_targetNames.Add(target.Name);
			}

			KarmaMessageBus.Publish(new ActionTriggeredEvent()
			{
				Action = new BaseActionRpcState()
				{
					Id = this.Id,
					Name = this.Name,
					Duration = this.Duration,
					Icon = this.Icon,
					BackgroundColor = this.BackgroundColor,
					ActorName = this.Actor.Name,
					Type = RpcActionType.Teacher,
					TargetActorNames = _targetNames
				}
			});

			var targetAction = Targets.FirstOrDefault().GetActions().FirstOrDefault(a => (a as NPCAction).IsMisbehaviour);
			if (targetAction != null &&
				targetAction.Timer < 12f)
			{
				var avatar = Targets.FirstOrDefault() as StudentFacade;
				if (avatar != null)
				{
					MindSnapshot mind = avatar.GetMindSnapshot();
					mind.Params["Concentration"] += 3;
					avatar.OverrideMindWithSnapshot(mind);
				}
			}

			if(_delta.Params != null)
			{
				var target = Targets.FirstOrDefault() as NPCFacade;
				var mind = target.GetMindSnapshot();
				_delta.Params["Concentration"] = mind.Params["Concentration"] - _delta.Params["Concentration"];
				_delta.Params["Enjoyment"] = mind.Params["Enjoyment"] - _delta.Params["Enjoyment"];
				_delta.Params["Knowledge"] = mind.Params["Knowledge"] - _delta.Params["Knowledge"];

				KarmaMessageBus.Publish(new ActionFeedbackEvent() { Delta = _delta });
			}
		}

		public override void OnStart()
		{
			base.OnStart();
			
			_record.StartTime = (int)KarmaTime.Time;

			if (Targets.FirstOrDefault() is NPCFacade target)
			{
				_delta = target.GetMindSnapshot();
			}

			if (Targets[0] is StudentFacade)
			{
				Targets[0].Transform.GetComponent<GazeBehaviour>().ForceChangeGazeTarget(TeacherGazeInteractions.GazeTarget);
			}
		}

		public override void OnTick()
		{
			base.OnTick();

			//KarmaMessageBus.Publish(new ActionMindUpdateEvent()
			//{
			//    Action = this,
			//    Targets = Targets.OfType<StudentFacade>().ToList()
			//});

		}

		public override BaseAction Clone()
		{
			return new TeacherAction(this);
		}
	}

	public class AdjustMindParamEvent : KarmaBaseUnityEvent
	{
		public MindSnapshot Delta { get; set; }
	}

	public class ActionFeedbackEvent : KarmaBaseUnityEvent
	{
		public MindSnapshot Delta { get; set; }
	}
}
