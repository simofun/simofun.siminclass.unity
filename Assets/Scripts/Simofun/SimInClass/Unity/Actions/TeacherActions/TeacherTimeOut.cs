﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherTimeOut : TeacherAction
    {
        public TeacherTimeOut()
        {
        }

        public TeacherTimeOut(TeacherTimeOut other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherTimeOut(this);
        }
    }
}
