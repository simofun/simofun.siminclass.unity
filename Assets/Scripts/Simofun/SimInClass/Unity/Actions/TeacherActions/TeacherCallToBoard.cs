﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherCallToBoard : TeacherAction
    {
        public TeacherCallToBoard()
        {
        }

        public TeacherCallToBoard(TeacherCallToBoard other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherCallToBoard(this);
        }

        public override void OnComplete()
        {
            base.OnComplete();

            //_actionFactory.Create("DoPresentation", Targets[0], new List<ActorFacade>());
        }
    }
}