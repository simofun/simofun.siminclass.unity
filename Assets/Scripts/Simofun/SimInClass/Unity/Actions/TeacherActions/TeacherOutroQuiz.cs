﻿using KarmaFramework.ActionCore;
using Sinifta.Actions.Interfaces;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherOutroQuiz : TeacherLecture
    {
        public TeacherOutroQuiz()
        {
            BackgroundColor = new Color32(9, 99, 199, 255);
        }

        public TeacherOutroQuiz(TeacherOutroQuiz other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherOutroQuiz(this);
        }
 
    }
}