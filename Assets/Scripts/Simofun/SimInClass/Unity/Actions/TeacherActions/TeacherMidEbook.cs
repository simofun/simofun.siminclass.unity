﻿using KarmaFramework.ActionCore;
using Sinifta.Actions.Interfaces;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherMidEbook : TeacherLecture
    {
        public TeacherMidEbook()
        {
            BackgroundColor = new Color32(113, 255, 86, 255);
        }

        public TeacherMidEbook(TeacherMidEbook other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherMidEbook(this);
        }     
    }
}