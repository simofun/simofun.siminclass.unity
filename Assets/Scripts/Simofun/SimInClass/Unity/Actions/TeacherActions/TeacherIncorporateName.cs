﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherIncorporateName : TeacherAction, IInterfaceUpdater
    {
        public TeacherIncorporateName()
        {
        }

        public TeacherIncorporateName(TeacherIncorporateName other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherIncorporateName(this);
        }
    }

}
