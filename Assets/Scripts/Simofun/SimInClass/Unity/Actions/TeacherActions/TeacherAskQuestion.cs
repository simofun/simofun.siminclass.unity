﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherAskQuestion : TeacherAction
    {
        public TeacherAskQuestion()
        {
        }

        public TeacherAskQuestion(TeacherAskQuestion other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherAskQuestion(this);
        }
    }
}
