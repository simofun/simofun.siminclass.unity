﻿using KarmaFramework.ActionCore;
using Simofun.Karma.UniRx.Unity;
using Sinifta.Actions.Interfaces;
using UnityEngine;
using static Sinifta.Environment.Classroom;

namespace Sinifta.Actions.TeacherActions
{
	public class TeacherMidConcrete : TeacherLecture, IInterfaceUpdater, ILecture
	{
		public TeacherMidConcrete()
		{
			BackgroundColor = new Color32(74, 99, 211, 255);
		}

		public TeacherMidConcrete(TeacherMidConcrete other) : base(other)
		{
		}

		public override BaseAction Clone()
		{
			return new TeacherMidConcrete(this);
		}

		public void Resume()
		{
			throw new System.NotImplementedException();
		}

		public void Suspend()
		{
			throw new System.NotImplementedException();
		}

		public override void OnStart()
		{
			base.OnStart();
			/*Loader.StartCoroutine(WaitALittle(_eventManager, _actionFactory));
			_interfaceManager.BlackScreenFlash(2f);*/

			Actor.Transform.position = GameObject.Find("PresentationSlot").transform.position;
			var classroomBackHorz = Vector3.ProjectOnPlane(GameObject.Find("ClassroomBack").transform.position, Vector3.up);
			Actor.Transform.rotation = Quaternion.LookRotation(classroomBackHorz, Vector3.up);
			KarmaMessageBus.Publish(new ClassroomChangeEvent() { State = Environment.ClassroomState.StudyGroup });
		  
		}

		/*
		 IEnumerator WaitALittle(IEventManager eventManager, IActionFactory actionFactory)
		{
			yield return new WaitForSeconds(1f);

			Actor.transform.position = GameObject.Find("PresentationSlot").transform.position;
			var classroomBackHorz = Vector3.ProjectOnPlane(GameObject.Find("ClassroomBack").transform.position, Vector3.up);
			Actor.transform.rotation = Quaternion.LookRotation(classroomBackHorz, Vector3.up);

			eventManager.ClassroomChangeState(new ClassroomChangeParams() {ClasroomState = ClassroomState.StudyGroup});
		}
		public override bool Precheck(ActorFacade af = null)
		{
			return base.Precheck(af) &&
				   Actor.History.Any(x => x is TeacherIntoClassAction);
		}
		*/
	}
}
