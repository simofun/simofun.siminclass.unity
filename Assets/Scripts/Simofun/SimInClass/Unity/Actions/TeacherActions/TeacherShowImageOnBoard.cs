﻿using KarmaFramework.ActionCore;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherShowImageOnBoard : TeacherAction
    {
        public string Tooltip { get; set; }

        private string _imgName;

        private bool _isSuccessful;

        private AudioClip _actionCompletedSound;

        public TeacherShowImageOnBoard()
        {
        }

        public TeacherShowImageOnBoard(TeacherShowImageOnBoard other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherShowImageOnBoard(this);
        }
    }
}
