﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherSalute : TeacherAction
    {
        public TeacherSalute()
        {
        }

        public TeacherSalute(TeacherSalute other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherSalute(this);
        }
    }
}