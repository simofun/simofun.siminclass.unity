﻿using KarmaFramework.ActionCore;
using Sinifta.Actions.Interfaces;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherOutroHomework : TeacherLecture
    {
        public TeacherOutroHomework()
        {
            BackgroundColor = new Color32(146, 11, 222, 255);
        }

        public TeacherOutroHomework(TeacherOutroHomework other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherOutroHomework(this);
        }
        
    }
}