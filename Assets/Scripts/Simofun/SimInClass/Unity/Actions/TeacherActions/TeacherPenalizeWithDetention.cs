﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherPenalizeWithDetention : TeacherAction, IInterfaceUpdater
    {
        public TeacherPenalizeWithDetention()
        {
        }

        public TeacherPenalizeWithDetention(TeacherPenalizeWithDetention other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherPenalizeWithDetention(this);
        }
    }
}
