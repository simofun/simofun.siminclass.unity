﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherRemoveItem : TeacherAction
    {
        public TeacherRemoveItem()
        {
        }

        public TeacherRemoveItem(TeacherRemoveItem other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherRemoveItem(this);
        }
    }
}