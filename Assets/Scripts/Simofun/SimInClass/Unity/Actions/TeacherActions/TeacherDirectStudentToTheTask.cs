﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherDirectStudentToTheTask : TeacherAction
    {
        public TeacherDirectStudentToTheTask()
        {
        }

        public TeacherDirectStudentToTheTask(TeacherDirectStudentToTheTask other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherDirectStudentToTheTask(this);
        }
    }
}
