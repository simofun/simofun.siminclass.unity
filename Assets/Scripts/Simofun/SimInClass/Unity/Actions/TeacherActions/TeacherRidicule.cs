﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherRidicule : TeacherAction
    {
        public TeacherRidicule()
        {
        }

        public TeacherRidicule(TeacherRidicule other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherRidicule(this);
        }
    }
}
