﻿using KarmaFramework;
using KarmaFramework.ActionCore;
using Simofun.Karma.UniRx.Unity;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherLookAtStudent : TeacherAction
    {
        public TeacherLookAtStudent()
        {
        }

        public TeacherLookAtStudent(TeacherLookAtStudent other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherLookAtStudent(this);
        }

       
        public override void OnStart()
        {
            base.OnStart();
            Duration = 0f;
        }
        public override void OnComplete()
        {
            _record.FinishTime = (int)KarmaTime.Time;
            KarmaMessageBus.Publish(_record);
            Stop();
        }
    }
}
