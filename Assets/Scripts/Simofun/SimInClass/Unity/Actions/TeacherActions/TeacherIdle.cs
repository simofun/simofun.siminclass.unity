﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherIdle : TeacherAction
    {
        public TeacherIdle()
        {
        }

        public TeacherIdle(TeacherIdle other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherIdle(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            Duration = int.MaxValue;
        }
    }
}
