﻿using KarmaFramework.ActionCore;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherIntroQuestion : TeacherLecture
    {
        public TeacherIntroQuestion()
        {
            BackgroundColor = new Color32(74, 200, 4, 255);
        }

        public TeacherIntroQuestion(TeacherIntroQuestion other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherIntroQuestion(this);
        }
    }
}