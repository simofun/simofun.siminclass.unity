﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherUseHandSignal : TeacherAction
    {
        public TeacherUseHandSignal()
        {
        }

        public TeacherUseHandSignal(TeacherUseHandSignal other) : base(other)
        {
        }

        public override BaseAction Clone()   
        {
            return new TeacherUseHandSignal(this);
        }
    }
}
