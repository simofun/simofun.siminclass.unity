﻿using KarmaFramework.ActionCore;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherSendImage : TeacherAction
    {
        public string Tooltip { get; set; }
		
		private string _imgName;

        private bool _isSuccessful;

        private AudioClip _actionCompletedSound;

        public TeacherSendImage()
        {
        }

        public TeacherSendImage(TeacherSendImage other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherSendImage(this);
        }
    }
}
