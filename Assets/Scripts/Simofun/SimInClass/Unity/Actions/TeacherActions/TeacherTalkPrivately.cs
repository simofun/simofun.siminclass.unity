﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherTalkPrivately : TeacherAction, IInterfaceUpdater
    {
        public TeacherTalkPrivately()
        {
        }

        public TeacherTalkPrivately(TeacherTalkPrivately other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherTalkPrivately(this);
        }
    }
}
