﻿using KarmaFramework.ActionCore;
using Simofun.Karma.UniRx.Unity;
using Sinifta.Actions.Interfaces;
using Sinifta.Views;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
	public class TeacherMidBook : TeacherLecture
	{
		public TeacherMidBook()
		{
			BackgroundColor = new Color32(167, 189, 54, 255);
		}

		public TeacherMidBook(TeacherMidBook other) : base(other)
		{
		}

		public override BaseAction Clone()
		{
			return new TeacherMidBook(this);
		}
	
	}
}
