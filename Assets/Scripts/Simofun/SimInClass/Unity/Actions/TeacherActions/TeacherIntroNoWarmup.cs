﻿using KarmaFramework.ActionCore;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherIntroNoWarmup : TeacherLecture
    {
        public TeacherIntroNoWarmup()
        {
            BackgroundColor = new Color32(5, 190, 236, 255);
        }

        public TeacherIntroNoWarmup(TeacherIntroNoWarmup other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherIntroNoWarmup(this);
        }
    }
}