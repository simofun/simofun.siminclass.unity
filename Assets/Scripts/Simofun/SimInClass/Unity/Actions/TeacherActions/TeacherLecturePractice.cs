﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherLecturePractice : TeacherAction
    {
        public TeacherLecturePractice()
        {
        }

        public TeacherLecturePractice(TeacherLecturePractice other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherLecturePractice(this);
        }
    }
}
