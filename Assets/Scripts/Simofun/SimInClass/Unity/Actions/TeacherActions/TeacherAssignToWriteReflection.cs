﻿using System.Linq;
using KarmaFramework.ActionCore;
using KarmaFramework.CharacterCore;
using Sinifta.Actors;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherAssignToWriteReflection : TeacherAction
    {
        public TeacherAssignToWriteReflection()
        {
        }

        public TeacherAssignToWriteReflection(TeacherAssignToWriteReflection other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherAssignToWriteReflection(this);
        }
        public override void OnComplete()
        {
            base.OnComplete();

            var target = Targets.FirstOrDefault() as NPCFacade;
            if (target != null)
            {
                var targetAct = target.GetActions().FirstOrDefault() as StudentAction;
                if (targetAct != null)
                {
                    if(target is StudentFacade && targetAct.DealedState.Value != ActionDealedState.DealingIgnored)
                    {
                        target.SetAnimState("Write", 2);
                    }
                }
                else
                {
                    target.SetAnimState("Write");
                }
            }
        }

        // TODO: implement
        //public override string ToString()
        //{
        //    return Localizer.Instance.GetString(StringId.TeacherAssignToWriteReflection);
        //}
        //
        //public override void Completing()
        //{
        //    base.Completing();
        //
        //    if (TargetExecutableAction != null)
        //    {
        //        StudentAction studentAction = TargetExecutableAction.Action as StudentAction;
        //        if (studentAction != null)
        //        {
        //            ActionDealedState state = studentAction.DealedState;
        //
        //            //Debug.Log("Dealed State : " + state);
        //
        //            if (Target is StudentAvatar && state != ActionDealedState.DealingIgnored)
        //            {
        //                Target.AnimController.SetStudentAnimationWithDelayFrames(StudentAnim.Write, 2);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        StudentAvatar avatar = Target as StudentAvatar;
        //        if (avatar != null)
        //        {
        //            avatar.AnimController.SetStudentAnimation(StudentAnim.Write);
        //        }
        //    }
        //}
    }
}
