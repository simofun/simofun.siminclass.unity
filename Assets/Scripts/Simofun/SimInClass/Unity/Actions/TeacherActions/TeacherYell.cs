﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherYell : TeacherAction
    {
        public TeacherYell()
        {
        }

        public TeacherYell(TeacherYell other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherYell(this);
        }


        public override void OnStart()
        {
            base.OnStart();

            TeacherGazeInteractions.OnShout();

            GazeTarget gazeTarget = Targets[0].Transform.GetComponent<GazeBehaviour>().attachedGazeTarget;
            GazeManager.IncreaseTargetAttractivenessTemporarily(gazeTarget, 3f, 10f);
            gazeTarget.attractiveness += 0.2f;
        }
    }
}
