﻿using KarmaFramework.ActionCore;
using Sinifta.Actions.Interfaces;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherMidAbstract : TeacherLecture
    {
        public TeacherMidAbstract()
        {
            BackgroundColor = new Color32(113, 255, 86, 255);
        }

        public TeacherMidAbstract(TeacherMidAbstract other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherMidAbstract(this);
        }
    }
}