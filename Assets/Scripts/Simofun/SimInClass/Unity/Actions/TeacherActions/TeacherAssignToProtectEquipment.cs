﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherAssignToProtectEquipment : TeacherAction, IInterfaceUpdater
    {
        public TeacherAssignToProtectEquipment()
        {
        }

        public TeacherAssignToProtectEquipment(TeacherAssignToProtectEquipment other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherAssignToProtectEquipment(this);
        }
    }
}
