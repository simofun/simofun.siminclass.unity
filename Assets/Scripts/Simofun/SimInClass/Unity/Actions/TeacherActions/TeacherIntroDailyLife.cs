﻿using KarmaFramework.ActionCore;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherIntroDailyLife : TeacherLecture
    {
        public TeacherIntroDailyLife()
        {
            BackgroundColor = new Color32(255, 0, 99, 255);
        }

        public TeacherIntroDailyLife(TeacherIntroDailyLife other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherIntroDailyLife(this);
        }
    }
}