﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherWalkToStudent : TeacherAction
    {
        public TeacherWalkToStudent()
        {
        }

        public TeacherWalkToStudent(TeacherWalkToStudent other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherWalkToStudent(this);
        }
    }
}
