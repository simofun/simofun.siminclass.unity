﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherTouch : TeacherAction
    {
        public TeacherTouch()
        {
        }

        public TeacherTouch(TeacherTouch other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherTouch(this);
        }
    }
}
