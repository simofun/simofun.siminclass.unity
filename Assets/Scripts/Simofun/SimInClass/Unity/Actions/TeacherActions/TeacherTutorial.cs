﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherTutorial : TeacherAction
    {
        public TeacherTutorial()
        {
        }

        public TeacherTutorial(TeacherTutorial other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherTutorial(this);
        }
    }
}