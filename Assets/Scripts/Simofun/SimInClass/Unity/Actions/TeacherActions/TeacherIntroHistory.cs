﻿using KarmaFramework.ActionCore;
using UnityEngine;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherIntroHistory : TeacherLecture
    {
        public TeacherIntroHistory()
        {
            BackgroundColor = new Color32(205, 131, 47, 255);
        }

        public TeacherIntroHistory(TeacherIntroHistory other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherIntroHistory(this);
        }
    }
}