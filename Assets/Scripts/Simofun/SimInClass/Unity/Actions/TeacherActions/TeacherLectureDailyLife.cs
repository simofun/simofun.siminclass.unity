﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherLectureDailyLife : TeacherAction
    {
        public TeacherLectureDailyLife()
        {
        }

        public TeacherLectureDailyLife(TeacherLectureDailyLife other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherLectureDailyLife(this);
        }
    }
}
