﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherKnockOnTable : TeacherAction
    {
        public TeacherKnockOnTable()
        {
        }

        public TeacherKnockOnTable(TeacherKnockOnTable other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherKnockOnTable(this);
        }
    }
}
