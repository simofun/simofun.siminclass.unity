﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherLectureJoke : TeacherAction
    {
        public TeacherLectureJoke()
        {
        }

        public TeacherLectureJoke(TeacherLectureJoke other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherLectureJoke(this);
        }
    }
}
