﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherFocus : TeacherAction
    {
        public TeacherFocus()
        {
        }

        public TeacherFocus(TeacherFocus other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherFocus(this);
        }
    }
}
