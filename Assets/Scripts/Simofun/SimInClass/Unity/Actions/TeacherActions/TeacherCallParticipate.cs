﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherCallParticipate : TeacherAction, IInterfaceUpdater
    {

        public override void OnStart()
        {
            base.OnStart();

            GazeManager.IncreaseTargetAttractivenessTemporarily(Targets[0].Transform.GetComponent<GazeBehaviour>().attachedGazeTarget, 2f, 5f);
        }
        public TeacherCallParticipate()
        {
        }

        public TeacherCallParticipate(TeacherCallParticipate other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherCallParticipate(this);
        }
    }
}
