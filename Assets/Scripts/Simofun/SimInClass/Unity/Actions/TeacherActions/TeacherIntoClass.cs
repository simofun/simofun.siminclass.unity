﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherIntoClass : TeacherAction
    {
        public TeacherIntoClass()
        {
        }

        public TeacherIntoClass(TeacherIntoClass other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherIntoClass(this);
        }
        /*
            public override bool Precheck(ActorFacade af)
            {
                return !Actor.History.Exists(x => x.Action.GetType() == typeof (TeacherIntroClass))
            }
        */

        public override void OnStart()
        {
        }
    }
}