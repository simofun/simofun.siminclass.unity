﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherRemindRules : TeacherAction
    {
        public TeacherRemindRules()
        {
        }

        public TeacherRemindRules(TeacherRemindRules other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherRemindRules(this);
        }
    }
}
