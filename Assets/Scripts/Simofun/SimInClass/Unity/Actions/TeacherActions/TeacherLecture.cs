﻿namespace Sinifta.Actions.TeacherActions
{
	using KarmaFramework;
	using KarmaFramework.ActionCore;
	using KarmaFramework.Localization;
	using KarmaFramework.Reports;
    using KarmaFramework.ScenarioCore;
    using KarmaFramework.Views;
    using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Sinifta.Views;
	using System;
	using System.Collections.Generic;
	using System.Xml.Serialization;
	using UniRx;
    using UnityEngine;
    using Zenject;
    using static Sinifta.UI.MissionPopupView;

    public enum LessonPlanSlot
	{
		None = 0,
		Introduction = 1,
		Learning_TeachingProcess = 2,
		MeasurementAndEvaluation = 3,
		GetStarted_Drill = 4,
		Engage_Motivation = 5,
		PresentTheContent = 6,
		ProvideLearningGuidance = 7,
		ElicitPerformance_Practice = 8,
		ProvideFeedback = 9,
		AssessPerformance = 10
	}

	public class TeacherLecture : TeacherAction
	{
		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();
		#endregion

		[XmlAttribute]
		public LessonPlanSlot LessonPlanSlot { get; set; }
		[XmlArray]
		public List<LectureImageRequirement> ImageRequirements { get; set; }

		bool isSkipped;
		[Inject]
		GameState gameState;
		public TeacherLecture()
		{
			
		}

		public TeacherLecture(TeacherLecture other) : base(other)
		{
			
		}

		~TeacherLecture()
		{
			this.disposables.Dispose();
		}

		public override BaseAction Clone()
		{
			return new TeacherLecture(this);
		}

		public override void OnStart()
		{
			_record.StartTime = (int)KarmaTime.Time;

			KarmaMessageBus.Publish(new LectureStartedEvent { Lecture = this });
			KarmaMessageBus.Publish(new ShowLectureEvent { State = false });
			KarmaMessageBus.Publish(
				new ShowLectureEvent
				{
					State = true,
					speech = Text,
					StartTime = _record.StartTime,
					Duration = this.Duration
				});

			if (ImageRequirements != null && ImageRequirements.Count > 0)
			{
				string correctTabletImages = string.Empty;
				string correctBoardImages = string.Empty;

				int correctBoardImgCount = 0;
				int correctTabletImgCount = 0;

				for (int i = 0; i < ImageRequirements.Count; i++)
				{
					if (ImageRequirements[i].TechType == "FileBrowserApp")
						correctBoardImgCount++;
					else
						correctTabletImgCount++;
				}

				int boardIndex = 0;
				int tabletIndex = 0;
				for (int i = 0; i < ImageRequirements.Count; i++)
				{
					LectureImageRequirement requiredImg = ImageRequirements[i];

					if (requiredImg.TechType == "FileBrowserApp")
					{
						if (++boardIndex < correctBoardImgCount)
							correctBoardImages += "> " + requiredImg.Name + "\n";
						else
							correctBoardImages += "> " + requiredImg.Name;
					}
					else
					{
						if (++tabletIndex < correctTabletImgCount)
							correctTabletImages += "> " + requiredImg.Name + "\n";
						else
							correctTabletImages += "> " + requiredImg.Name;
					}
				}                
				if (correctTabletImgCount > 0)
				{
					var title = Localizer.Instance.GetString("TeacherSendImageTooltip");
					KarmaMessageBus.Publish(
						new ImageRequirementEvent
						{
							Title = title,
							Text = correctTabletImages
						});
				}
				if (correctBoardImgCount > 0)
				{
					var title = Localizer.Instance.GetString("TeacherShowImageOnBoardTooltip");
					KarmaMessageBus.Publish(
						new ImageRequirementEvent
						{
							Title = title,
							Text = correctBoardImages
						});
				}
				if(ImageRequirements.Count > 0)
				{
					KarmaMessageBus.OnEvent<ImageRequirementTryEvent>().Subscribe(ev =>
					{
						if(correctBoardImages.Contains(ev.ImageName) || correctTabletImages.Contains(ev.ImageName))
						{
							LectureImageRequirement ir = null;
							foreach(var item in ImageRequirements)
							{
								if (item.Name.Equals(ev.ImageName))
									ir = item;
							}
							if (ir != null)
							{
								ImageRequirements.Remove(ir);
								KarmaMessageBus.Publish(new MissionRecord { Success = true });
							}
							else
							{
								KarmaMessageBus.Publish(new MissionRecord { Success = false });
							}

							if (ImageRequirements.Count == 0)
							{
								KarmaMessageBus.Publish(new ImageRequirementSuccessfulEvent());
							}
						}
					}).AddTo(this.disposables);
					KarmaMessageBus.OnEvent<MissionActionButtonClicked>().Subscribe(ev =>
					{
						isSkipped = ev.IsClicked;
					}).AddTo(disposables);
				}
			}
		}
        public override void OnTick()
        {
			if(isSkipped)
            {
				return;
            }

			if ((ImageRequirements.Count > 0) && (Duration - Timer < 3) && (Duration) < 120)
			{
				KarmaMessageBus.Publish(new MissionActionButtonEvent() { State = true });
				KarmaMessageBus.Publish(new LectureUpdateForMission()); ;
            }

			if(gameState.Scenario.Duration <= KarmaTime.Time)
            {
				OnComplete();
            }
			
            base.OnTick();
        }
        public override void OnComplete()
		{ 
			_record.FinishTime = (int)KarmaTime.Time;
			KarmaMessageBus.Publish(_record);
		}
	}

	public class LectureStartedEvent : KarmaBaseUnityEvent
	{
		public TeacherLecture Lecture { get; set; }
	}

	public class ImageRequirementEvent : KarmaBaseUnityEvent
	{
		public string Title { get; set; }

		public string Text { get; set; }
	}

	public class ImageRequirementTryEvent : KarmaBaseUnityEvent
	{
		public string ImageName { get; set; }
	}

	public class ImageRequirementSuccessfulEvent : KarmaBaseUnityEvent
	{
	}

	[Serializable]
	public class LectureImageRequirement
	{
		[XmlAttribute]
		public string TechType { get; set; }

		[XmlAttribute]
		public string Folder { get; set; }

		[XmlAttribute]
		public string Name { get; set; }

		public LectureImageRequirement()
		{
		}

		public LectureImageRequirement(string tech, string folder, string filename)
		{
			TechType = tech;
			Folder = folder;
			Name = filename;
		}

		public override string ToString()
		{
			return TechType + "." + Folder + "." + Name;
		}
	}
}
