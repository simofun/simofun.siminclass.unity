﻿using KarmaFramework.ActionCore;
using Sinifta.Actors;
using Sinifta.Environment;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;
using Simofun.SimInClass.Unity.Utility;
namespace Sinifta.Actions.TeacherActions
{
    public class TeacherSendOutOfClass : TeacherAction
    {
        Classroom _classroom;
        StudentFacade targetActor;
        CalculateNearWaypoint _nearWaypoint;
       
        List<Transform> _target;
        List<Vector3> _shortestPath;

        bool IsArrivedToPoint = false;

        [Inject]
        void Construct(GameState gameState)
        {
            this._classroom = gameState.Environment as Classroom;
        }

        public TeacherSendOutOfClass()
        {
            Duration = int.MaxValue;   
        }

        public TeacherSendOutOfClass(TeacherSendOutOfClass other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherSendOutOfClass(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            
            targetActor = Targets.FirstOrDefault() as StudentFacade;

            if (targetActor != null)
            {
                targetActor.SetAnimBool("Sit", false);
            }
            var destinationPoint = GameObject.Find("CorridorSlot").transform;
            _classroom._waypoints.TryGetValue(_classroom.CurrentState, out _target);
            _nearWaypoint = new CalculateNearWaypoint(_target);
            _shortestPath = _nearWaypoint.GetPath(targetActor.Transform.position, destinationPoint.position);
        }
        public override void OnTick()
        {
            
            if(targetActor.SitStandState != SitStandState.Standing)
            {
                return;
            }

            if (_shortestPath.Count <= 0)
            {
                OnComplete();
                targetActor.Transform.gameObject.SetActive(false);
                return;
            }

            if (!IsArrivedToPoint && _shortestPath.Count != 0 )
            {
                IsArrivedToPoint = targetActor.Move(_shortestPath[0]);
            }
            else
            {
               
                _shortestPath.RemoveAt(0);
                IsArrivedToPoint = false;
            }
    
            base.OnTick();
        }
    }
}
