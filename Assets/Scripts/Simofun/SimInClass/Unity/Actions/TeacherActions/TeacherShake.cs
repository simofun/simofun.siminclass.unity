﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherShake : TeacherAction
    {
        public TeacherShake()
        {
        }

        public TeacherShake(TeacherShake other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherShake(this);
        }
    }
}
