﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherLecturePastEvents : TeacherAction
    {
        public TeacherLecturePastEvents()
        {
        }

        public TeacherLecturePastEvents(TeacherLecturePastEvents other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherLecturePastEvents(this);
        }
    }
}
