﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherContactWithTheParents : TeacherAction
    {
        public TeacherContactWithTheParents()
        {
        }

        public TeacherContactWithTheParents(TeacherContactWithTheParents other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherContactWithTheParents(this);
        }
    }
}
