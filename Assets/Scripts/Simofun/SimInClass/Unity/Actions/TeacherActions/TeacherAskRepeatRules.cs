﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherAskRepeatRules : TeacherAction
    {

        // TODO : implement
        //public override void Starting()
        //{
        //    base.Starting();
        //    GazeManager.IncreaseTargetAttractivenessTemporarily(Target.GetComponent<GazeBehaviour>().attachedGazeTarget, 2f, 5f);
        //}
        //
        //public override void Completing()
        //{
        //    base.Completing();
        //
        //    // Only if either the kid keepin' it cool or is dealt with, s/he's gonna repeat rules
        //    if (TargetExecutableAction == null ||
        //        (TargetExecutableAction.Action as StudentAction).DealedState == ActionDealedState.DealedCorrectly)
        //    {
        //        var repeatExec = KarmaManagers.ActionFactory.CreateAction(typeof(RepeatClassRules), Target, TeacherActor);
        //		KarmaManagers.EventManager.Register(null, repeatExec);
        //
        //    }
        //}
        public TeacherAskRepeatRules()
        {
        }

        public TeacherAskRepeatRules(TeacherAskRepeatRules other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherAskRepeatRules(this);
        }
    }
}
