﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherGivePaper : TeacherAction
    {
        public TeacherGivePaper()
        {
        }

        public TeacherGivePaper(TeacherGivePaper other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherGivePaper(this);
        }

        public override void OnStart()
        {
            base.OnStart();

            //(Targets[0] as Student).PaperAppear("ActivityPaper"); Set Activity Paper Appears
        }
    }
}