﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherRecognize : TeacherAction
    {
        public TeacherRecognize()
        {
        }

        public TeacherRecognize(TeacherRecognize other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherRecognize(this);
        }
    }
}