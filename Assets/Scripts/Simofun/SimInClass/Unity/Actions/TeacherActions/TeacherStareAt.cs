﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherStareAt : TeacherAction
    {
        public TeacherStareAt()
        {
        }

        public TeacherStareAt(TeacherStareAt other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherStareAt(this);
        }
    }
}