﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class OpenBoard : TeacherAction
    {
        public OpenBoard()
        {
        }

        public OpenBoard(OpenBoard other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new OpenBoard(this);
        }
    }
}