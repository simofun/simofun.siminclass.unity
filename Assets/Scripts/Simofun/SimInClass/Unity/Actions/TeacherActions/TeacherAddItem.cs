﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherAddItem : TeacherAction
    {
        public TeacherAddItem()
        {
        }

        public TeacherAddItem(TeacherAddItem other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherAddItem(this);
        }
        public override void OnStart()
        {
            //TeacherActor.Teacher.Inventory.AddItem(ActionAs<TeacherRemoveItem>().Item);
            base.OnStart();
        }

    }
}
