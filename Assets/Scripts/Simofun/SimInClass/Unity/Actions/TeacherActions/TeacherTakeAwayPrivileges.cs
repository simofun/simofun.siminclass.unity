﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherTakeAwayPrivileges : TeacherAction
    {
        public TeacherTakeAwayPrivileges()
        {
        }

        public TeacherTakeAwayPrivileges(TeacherTakeAwayPrivileges other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherTakeAwayPrivileges(this);
        }
    }
}
