﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherHumor : TeacherAction, IInterfaceUpdater
    {

        // TODO: implement
        //public override void Starting()
        //{
        //    base.Starting();
        //
        //    GazeTarget gazeTarget = Target.GetComponent<GazeBehaviour>().attachedGazeTarget;
        //    GazeManager.IncreaseTargetAttractivenessTemporarily(gazeTarget, 3f, 10f);
        //    gazeTarget.attractiveness += 0.2f;
        //}
        public TeacherHumor()
        {
        }

        public TeacherHumor(TeacherHumor other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherHumor(this);
        }
    }
}
