﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherVerbalRedirection : TeacherAction
    {
        public TeacherVerbalRedirection()
        {
        }

        public TeacherVerbalRedirection(TeacherVerbalRedirection other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherVerbalRedirection(this);
        }
    }
}
