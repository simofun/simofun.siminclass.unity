﻿using KarmaFramework.ActionCore;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherSayName : TeacherAction
    {
        public TeacherSayName()
        {
        }

        public TeacherSayName(TeacherSayName other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherSayName(this);
        }
    }
}