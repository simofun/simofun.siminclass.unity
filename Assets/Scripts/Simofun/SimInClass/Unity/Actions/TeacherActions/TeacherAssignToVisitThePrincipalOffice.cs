﻿using KarmaFramework.ActionCore;
using Sinifta.Actors;
using System.Linq;

namespace Sinifta.Actions.TeacherActions
{
    public class TeacherAssignToVisitThePrincipalOffice : TeacherAction
    {
        public TeacherAssignToVisitThePrincipalOffice()
        {
        }

        public TeacherAssignToVisitThePrincipalOffice(TeacherAssignToVisitThePrincipalOffice other) : base(other)
        {
        }

        public override BaseAction Clone()
        {
            return new TeacherAssignToVisitThePrincipalOffice(this);
        }

        public override void OnStart()
        {
            base.OnStart();
            var target = Targets.FirstOrDefault() as StudentFacade;
            if (target != null)
            {
                target.SetAnimBool("Sit", false);
                //TODO seda: yürüme animasyonu ve navmesh StandIdleNode.Cs den çağırıldı 
            }
        }

    }
}