﻿namespace Sinifta.Actions.Interfaces
{
    public interface ILecture
    {
        void Suspend();
        void Resume();
    }
}
