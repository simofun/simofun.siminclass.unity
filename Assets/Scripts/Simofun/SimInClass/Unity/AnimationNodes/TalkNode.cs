﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class TalkNode : StateMachineBehaviour
{
    private SkinnedMeshRenderer meshRenderer;
    private float time = 1;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        meshRenderer = animator.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
    }
    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (time >= 0.1)
        {
            int randomLetter = Random.Range(1, 3);
            int randomValue = Random.Range(0, 80);

            if (randomLetter < meshRenderer.sharedMesh.blendShapeCount)
            {
                meshRenderer.SetBlendShapeWeight(randomLetter, randomValue);
            }
            
            time = 0;
        }
        time += Time.deltaTime;
    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        for (int i = 0; i < meshRenderer.sharedMesh.blendShapeCount; i++)
        {
            meshRenderer.SetBlendShapeWeight(i, 0);
        }
    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
