﻿using Assets.Scripts;
using UnityEngine;
using System.Collections;
using KarmaFramework.CharacterCore;
using Sinifta.Actors;

public class SitIdleNode : StateMachineBehaviour
{

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var avatar = animator.GetComponent<MonoFacade>();
        if (avatar)
        {
            var sf = avatar.Actor as StudentFacade;
            if(sf != null)
            {
                sf.SitStandState = SitStandState.Sitting;
            }
        }

        animator.SetLayerWeight(1, 1);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetLayerWeight(1, 0);
    }
}
