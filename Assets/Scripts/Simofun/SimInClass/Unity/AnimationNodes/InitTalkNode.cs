﻿using UnityEngine;
using System.Collections;

public class InitTalkNode : StateMachineBehaviour
{
    //private bool _isProcessingIdleAnim = true;
    //private float _timeCounter = 0;
    private float _waitingTime;
    private const int subAnimCount = 3;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger("AnimationSelector", UnityEngine.Random.Range(1, (subAnimCount + 1)));
    }
}
