﻿using Assets.Scripts;
using UnityEngine;
using System.Collections;
using KarmaFramework.CharacterCore;
using Sinifta.Actors;

public class StandNode : StateMachineBehaviour
{
    private StudentFacade _avatar;
    private Transform _chair;
    private Transform _chairTargetPos;
    //private Transform _avatarTargetPos;
    private float animationTime = 0;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
     
        if (animator.gameObject.GetComponent<MonoFacade>() != null)
        {
            _avatar = animator.GetComponent<MonoFacade>().Actor as StudentFacade;
            _chair = _avatar.Chair;
            _chairTargetPos = _avatar.DeskStandPosSlot;
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (animator.GetComponent<MonoFacade>() != null)
        {
          
            if (Vector3.Distance(_chair.transform.position, _chairTargetPos.position) > 0.01f)
            {
                _chair.transform.position = Vector3.Lerp(_chair.transform.position, _chairTargetPos.position, animationTime);
                // _avatar.NavMeshWarp(_chair.transform.position);
            }
            animationTime += Time.deltaTime * 0.05f;
        }
    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetComponent<MonoFacade>() != null)
        {
           
            _chair.transform.position = _chairTargetPos.position;
            _chair.transform.rotation = _chairTargetPos.rotation;
           
        }

    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
