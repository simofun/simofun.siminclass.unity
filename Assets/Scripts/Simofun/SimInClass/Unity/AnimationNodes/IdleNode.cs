﻿using Assets.Scripts;
using UnityEngine;
using System.Collections;
using UniRx;
using System;

public class IdleNode : StateMachineBehaviour
{
    private float _waitingTime;
    private IDisposable _d;
    private const int subAnimCount = 7;
    private int currentAnimIndex;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        currentAnimIndex = animator.GetInteger("AnimationSelector");
        _waitingTime = UnityEngine.Random.Range(40, 60);
        _d?.Dispose();
        _d = Observable.Timer(TimeSpan.FromSeconds(_waitingTime)).Subscribe(ev => 
        {
            WaitAnimation(animator);
        });
    }

    private void WaitAnimation(Animator animator)
    {
        int nextAnimIndex;
        do
        {
            nextAnimIndex = UnityEngine.Random.Range(1, (subAnimCount + 1));
        } while (currentAnimIndex == nextAnimIndex);

        if (animator)
        {
            animator.SetInteger("AnimationSelector", nextAnimIndex);
        }
    }
}
