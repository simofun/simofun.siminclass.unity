﻿using Assets.Scripts;
using UnityEngine;
using System.Collections;

public class CryNode : StateMachineBehaviour
{
    private SkinnedMeshRenderer meshRenderer;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        meshRenderer = animator.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        meshRenderer.SetBlendShapeWeight(6, 100);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        meshRenderer.SetBlendShapeWeight(6, 0);
    }
}
