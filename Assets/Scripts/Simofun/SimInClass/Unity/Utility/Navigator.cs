﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;

namespace Sinifta.Utility
{
	public interface INavigator
	{
		List<Vector3> GetPath(Vector3 origin, Vector3 target);
	}

	public class Navigator : INavigator
	{
		private class Node
		{
			public string Name { get; private set; }
			public Vector3 Position { get; private set; }
			public HashSet<Node> Neighbors { get; private set; }

			public Node(string name, Vector3 pos)
			{
				Name = name;
				Position = pos;
				Neighbors = new HashSet<Node>();
			}

			#region Equality
			private bool Equals(Node other)
			{
				return string.Equals(Name, other.Name) && Position.Equals(other.Position) && Equals(Neighbors, other.Neighbors);
			}

			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				if (ReferenceEquals(this, obj)) return true;
				if (obj.GetType() != this.GetType()) return false;
				return Equals((Node)obj);
			}

			public override int GetHashCode()
			{
				unchecked
				{
					int hashCode = (Name != null ? Name.GetHashCode() : 0);
					hashCode = (hashCode * 397) ^ Position.GetHashCode();
					hashCode = (hashCode * 397) ^ (Neighbors != null ? Neighbors.GetHashCode() : 0);
					return hashCode;
				}
			}

			public static bool operator ==(Node a, Node b)
			{
				// If both are null, or both are same instance, return true.
				if (ReferenceEquals(a, b))
				{
					return true;
				}

				// If one is null, but not both, return false.
				if (((object)a == null) || ((object)b == null))
				{
					return false;
				}

				return a.Name == b.Name;
			}

			public static bool operator !=(Node a, Node b)
			{
				return !(a == b);
			}
			#endregion
		}

		private class Graph
		{
			public HashSet<Node> Nodes { get; private set; }

			public Graph()
			{
				Nodes = new HashSet<Node>();
			}

			public Node GetNode(string name)
			{
				var retVal = Nodes.FirstOrDefault(n => n.Name == name);

				if (retVal == null)
				{
					Debug.LogError("Node not found with name : " + name);
				}

				return retVal;
			}
		}

		private readonly Graph _graph;

		public Navigator(List<Transform> waypointGOs)
		{
			_graph = new Graph();

			// map : nodeName -> that node's neighbors
			var neighborNames = new Dictionary<string, string>();

			// Instantiate node from GO and store the neighbors' names
			foreach (var wpGO in waypointGOs)
			{
				var node = new Node(wpGO.name.Split('_')[0], wpGO.transform.position);
				_graph.Nodes.Add(node);
				neighborNames.Add(node.Name, wpGO.name.Split('_')[1]);
			}

			// TODO ATIL: Duplicate names check

			// Parse neighbors and assign them for each node
			foreach (var node in _graph.Nodes)
			{
				foreach (var neighborName in neighborNames[node.Name].Split('#'))
				{
					var other = _graph.GetNode(neighborName);
					node.Neighbors.Add(other);
					other.Neighbors.Add(node);
					//Debug.DrawLine(node.Position + Vector3.up, other.Position + Vector3.up, Color.red, 100f);
				}
			}
		}

		public List<Vector3> GetPath(Vector3 origin, Vector3 target)
		{
			Node startNode = null, targetNode = null;

			// Find closest nodes to origin 
			var minDist = float.MaxValue;
			foreach (var node in _graph.Nodes)
			{
				if (Vector3.Distance(node.Position, origin) < minDist)
				{
					startNode = node;
					minDist = Vector3.Distance(node.Position, origin);
				}
			}

			// Find closest nodes to target 
			minDist = float.MaxValue;
			foreach (var node in _graph.Nodes)
			{
				if (Vector3.Distance(node.Position, target) < minDist)
				{
					targetNode = node;
					minDist = Vector3.Distance(node.Position, target);
				}
			}

			// Sanity check
			if (startNode == null || targetNode == null)
			{
				Debug.LogError("Pathfind error. Either start or end is undefined");
				return new List<Vector3>();
			}

			// Empty path if start and end are the same
			if (startNode == targetNode)
			{
				return new List<Vector3>();
			}

			var visited = new HashSet<Node>(); // Hold visited nodes
			var parents = new Dictionary<Node, Node>(); // map : node -> node's parent in path

			var q = new Queue<Node>();
			q.Enqueue(startNode);

			// Perform BFS
			while (q.Count != 0)
			{
				var node = q.Dequeue();

				if (node == targetNode) // Found
				{
					// Backtrack parents
					var path = new List<Node>() { targetNode };
					while (path.Last() != startNode)
					{
						path.Add(parents[path.Last()]);
					}

					// It'll be in reverse order here
					path.Reverse();

					// Return vec3 array
					var retVal = new List<Vector3>();
					retVal.Add(origin);
					path.ForEach(n => retVal.Add(n.Position));
					//retVal.Add(target);

					return retVal;
				}

				// Enqueue unvisited neighbors
				foreach (var neighbor in node.Neighbors)
				{
					if (!visited.Contains(neighbor))
					{
						visited.Add(neighbor);
						q.Enqueue(neighbor);
						parents.Add(neighbor, node);
					}
				}
			}

			// No path found
			// If you see this message, check waypoint connectivity
			// Probably you forgot to connect to waypoints
			Debug.LogError("No path found btwn : " + startNode.Name + " - " + targetNode.Name);
			return new List<Vector3>();
		}

	}
}
