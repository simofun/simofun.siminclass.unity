﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.Utility.UI
{
    public class Console : MonoBehaviour
    {
        public delegate void OnConsoleLog(string msg, string color);
        private static event OnConsoleLog ConsoleLog;

        public static void Log(string msg)
        {
            if (ConsoleLog != null)
            {
                ConsoleLog(msg, "black");
            }
        }

        public static void Log(string msg, string color)
        {
            if (ConsoleLog != null)
            {
                ConsoleLog(msg, color);
            }
        }

        private enum Movement : byte
        {
            StayUp,
            StayDown,
            MoveDown,
            MoveUp
        }

        private Text _consoleText;
        private Text _dateText;
        private Movement _movement;
        private RectTransform _thisTransform;
        private const float MoveSpd = 8f;

        void Awake()
        {
            DontDestroyOnLoad(transform.root.gameObject);
            ConsoleLog += OnLog;
            _thisTransform = gameObject.GetComponent<RectTransform>();
            _consoleText = GameObject.Find("ConsoleText").GetComponent<Text>();
            _dateText = transform.Find("DateText").GetComponent<Text>();

            _consoleText.text = string.Empty;

            var v2 = _thisTransform.anchoredPosition;
            v2.y = Screen.height;
            _thisTransform.anchoredPosition = v2;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                if (_movement == Movement.StayUp || _movement == Movement.MoveUp)
                {
                    _movement = Movement.MoveDown;
                }
                else if (_movement == Movement.StayDown || _movement == Movement.MoveDown)
                {
                    _movement = Movement.MoveUp;
                }
            }

            if (_movement == Movement.MoveDown)
            {
                var v2 = _thisTransform.anchoredPosition;
                v2.y = Mathf.Lerp(v2.y, Screen.height / 2f, Time.deltaTime * MoveSpd);
                _thisTransform.anchoredPosition = v2;

                if (Mathf.Abs(v2.y - Screen.height / 2f) < 0.1f)
                {
                    _movement = Movement.StayDown;
                }
            }
            else if (_movement == Movement.MoveUp)
            {
                var v2 = _thisTransform.anchoredPosition;
                v2.y = Mathf.Lerp(v2.y, Screen.height, Time.deltaTime * MoveSpd);
                _thisTransform.anchoredPosition = v2;

                if (Mathf.Abs(v2.y - Screen.height) < 0.1f)
                {
                    _movement = Movement.StayUp;
                }
            }
            _dateText.text = DateTime.Now.ToLongDateString() + " // " + DateTime.Now.ToLongTimeString();
        }


        public void OnLog(string msg, string color)
        {
            var lines = msg.Split('\n');
            foreach (var line in lines)
            {
                _consoleText.text += "[" + DateTime.Now.ToLongTimeString() + "]: <color=" + color + ">" + line + "</color>\n\n";
            }
        }
    }
}
