﻿using System.Collections;
using UnityEngine;

namespace Sinifta.Utility
{
    public class WaitForSecondsUnscaled : CustomYieldInstruction
    {
        private float waitTime;
        private float waitUntil;

        public WaitForSecondsUnscaled(float time)
        {
            waitTime = time;
            waitUntil = Time.realtimeSinceStartup + waitTime;
        }

        public override bool keepWaiting
        {
            get
            {
                return Time.realtimeSinceStartup < waitUntil;
            }
        }

        public new void Reset()
        {
            waitUntil = Time.realtimeSinceStartup + waitTime;
        }
    }

    public static class CoroutineStarter
    {
        private static readonly MonoBehaviour coroutineStarter;
        public static Coroutine StartCoroutine(IEnumerator function)
        {
            return coroutineStarter.StartCoroutine(function);
        }

        public static void StopCoroutine(IEnumerator function)
        {
            if (function != null)
            {
                coroutineStarter.StopCoroutine(function);
            }
        }

        public static void StopCoroutine(Coroutine function)
        {
            if (function != null)
            {
                coroutineStarter.StopCoroutine(function);
            }
        }

        public static void StopAllCoroutines()
        {
            coroutineStarter.StopAllCoroutines();
        }

        static CoroutineStarter()
        {
            GameObject go = new GameObject("CoroutineStarter");
            coroutineStarter = go.AddComponent<MonoBehaviourScript>();
            Object.DontDestroyOnLoad(coroutineStarter.gameObject);
        }
    }
}
