﻿#if UNITY_EDITOR || UNITY_STANDALONE
#define EDITOR_OR_STANDALONE
#endif

using KarmaFramework;
using KarmaFramework.API;
using KarmaFramework.ScenarioCore;
using LitJson;
using Simofun.Karma.Unity.Web.Api;
using Simofun.Karma.Web.Api.Model;
using Sinifta.Actions.TeacherActions;
using Sinifta.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class SiniftaUtility
{
	#region Public Fields
	public static readonly HashSet<Type> ValidIntroLessons =
		new HashSet<Type>
		{
			typeof(TeacherIntroDailyLife),
			typeof(TeacherIntroQuestion),
			typeof(TeacherIntroHistory),
			typeof(TeacherIntroDailyLife),
			typeof(TeacherIntroQuestion),
			typeof(TeacherIntroHistory)
		};

	public static readonly HashSet<Type> ValidMidLessons = new HashSet<Type>
	{
		typeof(TeacherMidEbook),
		typeof(TeacherMidBook),
		typeof(TeacherMidConcrete),
		typeof(TeacherMidAbstract),
		typeof(TeacherMidEbook),
		typeof(TeacherMidBook),
		typeof(TeacherMidConcrete),
		typeof(TeacherMidAbstract)
	};

	public static Dictionary<Scenario, Texture2D> CachedEnvironmentIcons = new Dictionary<Scenario, Texture2D>();

	public static readonly HashSet<Type> ValidOutroLessons = new HashSet<Type>
	{
		typeof(TeacherOutroHomework),
		typeof(TeacherOutroQuiz),
		typeof(TeacherOutroHomework),
		typeof(TeacherOutroQuiz)
	};

	public static INavigator navigator;

	public static List<Scenario> CachedEnvironments = new List<Scenario>();
	#endregion

	#region Properties
	public static string ReportsSaveDir
	{
		get
		{
#if EDITOR_OR_STANDALONE
			return Path.Combine(
				Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
				"EndGameReports",
				InterLevelData.AppConfig.UserInfo.Email);
#else
			return Path.Combine(Application.persistentDataPath, "EndGameReports");
#endif
		}
	}
	#endregion

	#region Public Methods
	#region Navigation
	public static void InitNavigation(List<Transform> wps) => navigator = new Navigator(wps);

	public static List<Vector3> GetPath(Vector3 origin, Vector3 target) => navigator.GetPath(origin, target);
	#endregion

	public static IEnumerator GetUserProgress(
		IKarmaWebApiRestClient webApiClient,
		UserInfo info,
		Action onAfter = null)
	{
		var wwwProgress = webApiClient.GetUserProgress(info.Id);

		yield return wwwProgress;

		InterLevelData.AppConfig.LevelInfos = JsonMapper.ToObject<KarmaLevelInfos>(
			JsonMapper.ToObject(wwwProgress.text).ToJson());

		onAfter?.Invoke();
	}
	#endregion
}
