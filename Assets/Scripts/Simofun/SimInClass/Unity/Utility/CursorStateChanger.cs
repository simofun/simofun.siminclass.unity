﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace Simofun.SimInClass.Unity.Utility
{
    public enum ChangeControlTypes
    {
        None, Interactable, TargetType
    }

    public class CursorStateChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public ChangeControlTypes ControlType;
        public KarmaCursorMode TargetType;

        private bool _onHover;
        private Button _button;
        private bool _canCheckCursor;
        private KarmaCursorMode _prevCursorMode;

        private const string NoButtonError = "CursorStateChanger-ButtonComponentRequired!";

        // Use this for initialization
        private void Start()
        {
            ControlCursorCheck();
        }

        private void ControlCursorCheck()
        {
            _canCheckCursor = true;
            _button = transform.GetComponent<Button>();

            switch (ControlType)
            {
                case ChangeControlTypes.None:
                    _canCheckCursor = false;
                    break;
                case ChangeControlTypes.TargetType:
                case ChangeControlTypes.Interactable:
                    if (_button == null)
                    {
                        Debug.LogWarning(NoButtonError);
                        _canCheckCursor = false;
                    }
                    break;
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!_canCheckCursor) return;
            _prevCursorMode = KarmaCursor.Mode;
            _onHover = true;
            StartCoroutine(CursorStateControl());
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!_canCheckCursor) return;
            _onHover = false;
            KarmaCursor.Mode = _prevCursorMode;
        }

        private void ChangeState(KarmaCursorMode targetMode)
        {
            if (KarmaCursor.Mode == targetMode)
			{
                return;
            }

            KarmaCursor.Mode = targetMode;
        }

        private IEnumerator CursorStateControl()
        {
            while (_onHover)
            {
                switch (ControlType)
                {
                    case ChangeControlTypes.Interactable:
                        ChangeState((_button.interactable) ? _prevCursorMode : KarmaCursorMode.Disabled);

                        break;
                    case ChangeControlTypes.TargetType:
                        if (KarmaCursor.Mode != TargetType)
                            ChangeState(TargetType);
                        break;
                }
                yield return null;
            }
        }
    }
}