using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Simofun.SimInClass.Unity.Utility
{
	public class CalculateNearWaypoint 
	{
		private readonly Graph _graph;
		public CalculateNearWaypoint(List<Transform> waypointGOs)
		{
			_graph = new Graph();

			// map : nodeName -> that node's neighbors
			var neighborNames = new Dictionary<string, string>();

			// Instantiate node from GO and store the neighbors' names
			foreach (var wpGO in waypointGOs)
			{
				var node = new Node(wpGO.name.Split('_')[0], wpGO.transform.position);
				_graph.Nodes.Add(node);
				neighborNames.Add(node.Name, wpGO.name.Split('_')[1]);
			}

			// TODO ATIL: Duplicate names check

			// Parse neighbors and assign them for each node
			foreach (var node in _graph.Nodes)
			{
				foreach (var neighborName in neighborNames[node.Name].Split('#'))
				{
					var other = _graph.GetNode(neighborName);
					node.Neighbors.Add(other);
					other.Neighbors.Add(node);
					//Debug.DrawLine(node.Position + Vector3.up, other.Position + Vector3.up, Color.red, 100f);
				}
			}
		}
		public List<Vector3> GetPath(Vector3 origin, Vector3 target)
		{
			Node startNode = null, targetNode = null;

			// Find closest nodes to origin 
			var minDist = float.MaxValue;
			foreach (var node in _graph.Nodes)
			{
				if (Vector3.Distance(node.Position, origin) < minDist)
				{
					startNode = node;
					minDist = Vector3.Distance(node.Position, origin);
				}
			}

			// Find closest nodes to target 
			minDist = float.MaxValue;
			foreach (var node in _graph.Nodes)
			{
				if (Vector3.Distance(node.Position, target) < minDist)
				{
					targetNode = node;
					minDist = Vector3.Distance(node.Position, target);
				}
			}

			// Sanity check
			if (startNode == null || targetNode == null)
			{
				Debug.LogError("Pathfind error. Either start or end is undefined");
				return new List<Vector3>();
			}

			// Empty path if start and end are the same
			if (startNode == targetNode)
			{
				return new List<Vector3>();
			}

			var visited = new HashSet<Node>(); // Hold visited nodes
			var parents = new Dictionary<Node, Node>(); // map : node -> node's parent in path

			var q = new Queue<Node>();
			q.Enqueue(startNode);

			// Perform BFS
			while (q.Count != 0)
			{
				var node = q.Dequeue();

				if (node == targetNode) // Found
				{
					// Backtrack parents
					var path = new List<Node>() { targetNode };
					while (path.Last() != startNode)
					{
						path.Add(parents[path.Last()]);
					}

					// It'll be in reverse order here
					path.Reverse();

					// Return vec3 array
					var retVal = new List<Vector3>();
					retVal.Add(origin);
					path.ForEach(n => retVal.Add(n.Position));
					//retVal.Add(target);

					return retVal;
				}

				// Enqueue unvisited neighbors
				foreach (var neighbor in node.Neighbors)
				{
					if (!visited.Contains(neighbor))
					{
						visited.Add(neighbor);
						q.Enqueue(neighbor);
						parents.Add(neighbor, node);
					}
				}
			}

			// No path found
			// If you see this message, check waypoint connectivity
			// Probably you forgot to connect to waypoints
			Debug.LogError("No path found btwn : " + startNode.Name + " - " + targetNode.Name);
			return new List<Vector3>();
		}

	}
}
