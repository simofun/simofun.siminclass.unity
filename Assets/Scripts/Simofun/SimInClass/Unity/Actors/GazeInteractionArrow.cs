using Simofun.Karma.UniRx.Unity;
using Simofun.SimInClass.Unity.Tutorial.Actions;
using UniRx;
using UnityEngine;
namespace Simofun.SimInClass.Unity.Tutorial
{

    public class GazeInteractionArrow : MonoBehaviour
    {
        [SerializeField]
        GameObject Arrow;

        CompositeDisposable disposables = new CompositeDisposable();
        public void Start()
        {
            KarmaMessageBus.OnEvent<TutorialGazeArrowEvent>().Subscribe(ev =>
            {
                if (ev.State)
                {
                    Arrow.SetActive(true);
                    return;
                }
                Arrow.SetActive(false);
            }).AddTo(disposables);

            Arrow.SetActive(false);
        }
        private void OnDestroy() => disposables.Dispose();
    }
}