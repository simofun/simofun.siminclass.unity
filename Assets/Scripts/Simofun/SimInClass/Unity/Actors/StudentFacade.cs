﻿using KarmaFramework;
using KarmaFramework.ActionCore;
using KarmaFramework.CharacterCore;
using Simofun.Karma.UniRx.Unity;
using Sinifta.Actions;
using Sinifta.AI;
using Sinifta.Interactables.TabletApps;
using Sinifta.Views;
using System.Linq;
using UniRx;
using UnityEngine;
using Zenject;

namespace Sinifta.Actors
{
	public enum SitStandState
	{
		Sitting,
		Standing
	}

	public class StudentFacade : NPCFacade
	{
		#region Public Fields
		public SitStandState SitStandState;

		public StudentTooltipView StudentTooltipView;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		GameObject _tablet;
	    private const float RotateSpd = 6.5f;
		private const float WalkSpd = 1f;
		#endregion

		public TextMesh OverlayText { get; set; }
		public Transform Desk { get; set; }
		public Transform Chair { get; set; }
		public Transform ChairSlot { get; set; } // Student will be snapped here when sitting
		public Transform TableTop { get; set; } 
		public Transform TeacherFocus { get; set; }
		public Transform DeskStandPosSlot { get; set; }

		public StudentFacade(Student actor,
			StudentView view,
			Transform transform,
			IAnimatorController animator,
			ActorRegistry actorRegistry,
			BaseAction.Factory factory) : base(actor, view, transform, animator, actorRegistry, factory)
		{
			FacadeId = actorRegistry.GetNPCs().Count;

			KarmaMessageBus.OnEvent<ActionMindUpdateEvent>().Subscribe(ev => 
			{
				if(ev.Targets.Count == 0 || ev.Targets.Contains(this))
				{
					UpdateMind(ev.Action);
				}
			}).AddTo(this.disposables);

			KarmaMessageBus.OnEvent<AdjustMindParamEvent>().Subscribe(ev =>
			{
				var sp = GetMindSnapshot();
				OverrideMindWithSnapshot(sp + ev.Delta);
			}).AddTo(this.disposables);

			KarmaMessageBus.OnEvent<SendImageToTabletEvent>().Subscribe(ev =>
			{
				if(_tablet == null)
					_tablet = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/Tablet/Tablet"), TableTop);
				_tablet.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = ev.Image;
			}).AddTo(this.disposables);
		}

		~StudentFacade()
		{
			Dispose();
		}
        public override void Dispose()
        {
			this.disposables.Dispose();
			base.Dispose();
        }
        public override bool Move(Vector3 destination)
        {
			if ((Transform.position - destination).sqrMagnitude > 0.04f)
			{
				var toTargetHorz = Vector3.ProjectOnPlane(destination - Transform.position, Vector3.up);
				Transform.position += (toTargetHorz.normalized * WalkSpd * KarmaTime.DeltaTime);

				if (Vector3.Angle(toTargetHorz, Transform.forward) > 1f)
				{
					var targetRot = Quaternion.LookRotation(toTargetHorz, Vector3.up);
					Transform.rotation = Quaternion.Slerp(Transform.rotation, targetRot,
						KarmaTime.DeltaTime * RotateSpd);
				}
				Debug.DrawLine(Transform.position, destination, Color.red);
				return false;
			}
			return true;
			//if (Vector3.Distance(Transform.position, destination) <= NavMeshAgent.stoppingDistance)
   //         {
			//	return true;
   //         }
			//return false;
		}
        public void Bind(StudentTooltipView studentTooltipView)
		{
			StudentTooltipView = studentTooltipView;
			((StudentView)this.View).BindFacade(this);
		  
		}

		public Student GetActor()
		{
			return (Student)this.Actor;
		}

		public MindSnapshot UpdateMind(BaseAction action)
		{
			var targetAction = action.Targets.FirstOrDefault()?.GetActions().FirstOrDefault(a => (a as NPCAction).IsMisbehaviour);
			var repeatCount = 0;
			var lectureOvertime = 0f;
			if (action is TeacherAction)
			{
				repeatCount = 1;
			}

			var prms = new StudentMindUpdateParams()
			{
				AppliedAction = action,
				TargetAction = targetAction,
				RepeatCount = repeatCount,
				LectureOvertime = lectureOvertime,
				DistanceToAction = Vector3.Distance(action.Actor.Transform.position, Transform.position),
				DeltaTime = KarmaTime.DeltaTime / 6
			};
			return (this.Actor as Student).UpdateMind(prms);
		}

		public class Factory : PlaceholderFactory<string, Student, StudentFacade>
		{
		}
	}
}
