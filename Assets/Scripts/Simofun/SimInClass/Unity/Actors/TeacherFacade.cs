﻿using KarmaFramework.ActionCore;
using KarmaFramework.CharacterCore;
using Sinifta.Actors;
using Sinifta.Views;
using UnityEngine;
using Zenject;

public class TeacherFacade : PlayerFacade
{

    //public TeacherFacade(
    //TeacherView view,
    //Transform transform,
    //IAnimatorController animator,
    //ActorRegistry actorRegistry,
    //BaseAction.Factory factory) : base(null, view, transform, animator, actorRegistry, factory)
    //{
    //}

    public TeacherFacade(Teacher actor,
        TeacherView view,
        Transform transform,
        IAnimatorController animator,
        ActorRegistry actorRegistry,
        BaseAction.Factory factory) : base(actor, view, transform, animator, actorRegistry, factory)
    {
    }

    public class Factory : PlaceholderFactory<Teacher, TeacherFacade>
    {
        
    }
}
