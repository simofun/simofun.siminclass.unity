﻿using KarmaFramework;
using KarmaFramework.ActionCore;
using KarmaFramework.CharacterCore;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Domain;
using Sinifta.Actions.TeacherActions;
using Sinifta.AI;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using UniRx;
using UnityEngine;
using Zenject;

namespace Sinifta.Actors
{
	public class Student : NPC
	{
		[XmlIgnore]
		public bool CanMisbehav { get; private set; }

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		float _1minuteCheck = 6;

		float _10minuteCheck = 60;

		float _misbehavTimer;

		TeacherLecture _currentLecture;
		#endregion

		public Student()
		{
			CanMisbehav = true;

			KarmaMessageBus.OnEvent<LectureStartedEvent>()
				.Subscribe(ev => _currentLecture = ev.Lecture)
				.AddTo(this.disposables);
			_misbehavTimer = Random.Range(10, 30);
		}

		~Student()
		{
			this.disposables.Dispose();
		}

		protected override void OnTick()
		{
			base.OnTick();
			if(CanMisbehav)
			{
				var decidedAction = Decide(new StudentReasonerDecideParameters() 
				{
					distanceToTeacher = Vector3.Distance(this.Registry.GetPlayer().Transform.position, this.Registry.GetNPC(Name).Transform.position)
				});
				CanMisbehav = false;
				if(decidedAction != null && !this.Actions.Any(a => a.Name == decidedAction))
				{
					ExecuteAction(decidedAction, new List<IActor>() { });
				}
			}
			else if(!this.Actions.Any(a => (a as NPCAction).IsMisbehaviour) && _misbehavTimer <= 0)
			{
				_misbehavTimer = Random.Range(10, 30);
				CanMisbehav = true;
			}

			if(_10minuteCheck <= 0)
			{
				_10minuteCheck = 60;
				var sp = GetMindSnapshot();
				if(_currentLecture != null)
				{
					sp.Params["Concentration"] += 2;
					sp.Params["Enjoyment"] += 2;
				}
				else
				{
					sp.Params["Concentration"] -= 5;
					sp.Params["Enjoyment"] -= 5;
				}
				OverrideMindWithSnapshot(sp);
			}
			if (_1minuteCheck <= 0)
			{
				_1minuteCheck = 6;
				if(_currentLecture != null)
				{
					var sp = GetMindSnapshot();
					if(KarmaTime.Time < 60)
					{
						sp.Params["Knowledge"] += 1;
					}
					else if(KarmaTime.Time < 180)
					{
						sp.Params["Knowledge"] += 2;
					}
					OverrideMindWithSnapshot(sp);
				}
			}

			_10minuteCheck -= KarmaTime.DeltaTime;
			_1minuteCheck -= KarmaTime.DeltaTime;
			_misbehavTimer -= KarmaTime.DeltaTime;
		}

		public new class Factory : PlaceholderFactory<string, XElement, Student>
		{
		}
	}
}
