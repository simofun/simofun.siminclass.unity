﻿using KarmaFramework.CharacterCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Sinifta.Actors
{
    public class MonoFacade : MonoBehaviour
    {

        public ActorFacade Actor;

        public void BindFacade(StudentFacade studentFacade)
        {
            Actor = studentFacade;
        }


        // [Inject]
        //public void Construct([InjectOptional] StudentFacade npc, [InjectOptional] TeacherFacade player) // OH GOD THIS IS A TERRIBLE HACK
        //{
        //    if (npc == null)
        //        Actor = player;
        //    else
        //        Actor = npc;
        //}
    }
}
