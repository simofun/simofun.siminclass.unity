//-----------------------------------------------------------------------
// <copyright file="LevelAnalyticsService.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut</author>
//-----------------------------------------------------------------------

#if SIM_GAME_ANALYTICS
using UniRx;

namespace Simofun.HyperCasual.Unity.Analytics.Level
{
	using KarmaFramework.Views;
	using Simofun.GameAnalytics.Unity;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Simofun.Unity.Analytics;
	using Simofun.Unity.DesignPatterns.Singleton;
	using UnityEngine;
	using GA = GameAnalyticsSDK.GameAnalytics;

	public class LevelAnalyticsService : SimGlobalSingletonBase<LevelAnalyticsService>
	{
		#region Fields
		string currentLevelName;
		#endregion

		#region Protected Properties
		protected ISimAnalytics Analytics { get; set; }
		#endregion

		#region Construct
		/// <inheritdoc />
		public virtual void Construct(params object[] deps)
		{
			this.Analytics = (ISimAnalytics)deps[0];
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start()
		{
			GA.SetCustomId(System.Guid.NewGuid().ToString());

			this.Construct(SimGameAnalytics.Instance);

			this.RegisterEventHandlers();
		}

		/// <inheritdoc />
		protected override void OnDestroy()
		{
			base.OnDestroy();

			this.UnRegisterEventHandlers();
		}
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			KarmaMessageBus.OnEvent<StartGameEvent>()
				.Subscribe(ev =>
				{
					this.currentLevelName = "Level-" + ev.GameState.Scenario.Name;

					this.Analytics.LevelStart(this.currentLevelName);
				})
				.AddTo(this);
			KarmaMessageBus.OnEvent<EndGameEvent>()
				.Subscribe(ev => this.Analytics.LevelSuccess(this.currentLevelName))
				.AddTo(this);
		}

		protected virtual void UnRegisterEventHandlers()
		{
		}
		#endregion
		#endregion
		#endregion
	}
}
#endif
