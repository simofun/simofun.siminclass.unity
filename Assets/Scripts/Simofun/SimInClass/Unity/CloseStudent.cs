﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseStudent : MonoBehaviour
{
    /// <summary>
    /// TODO:SEDA Bu script öğrenci sınıftan gönderildiğinde öğrencinin kapatılıp sınıfa yeniden spawnlanmaması için
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.SetActive(false);
    }
}
