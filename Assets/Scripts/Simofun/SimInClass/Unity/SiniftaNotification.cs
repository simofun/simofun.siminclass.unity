﻿namespace Simofun.SimInClass.Unity.UI
{
    using KarmaFramework.Notification;
    using DG.Tweening;
    using UnityEngine;
    using KarmaFramework.Localization;
    using UnityEngine.UI;
    public class SiniftaNotification : KarmaNotification
    {
        [SerializeField]
        RectTransform NotificationPanel;
        [SerializeField]
        Image Bar;
        public enum NotificationType
        {
            Tablet,
            SmartBoard
        }
        public NotificationType Type;
        public override void ShowAnimation()
        {
            Bar.fillAmount = 0;
            NotificationPanel.DOAnchorPosX(-NotificationPanel.rect.width/2 ,Duration).OnComplete(()=> 
            {
                Bar.DOFillAmount(1,Duration).OnComplete(()=> 
                {
                    Hide();
                });
            });
        }
        public override void HideAnimation()
        {
            NotificationPanel.DOAnchorPosX(NotificationPanel.rect.width / 2, Duration);
        }
        public override void Show(string Message, string notificationType)
        {
            Message = Localizer.Instance.GetString("Notification" + notificationType) + ": " + Message;
            base.Show(Message);
        }
    }
}