﻿using Simofun.Karma.UniRx.Unity;
using System;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.ScenarioEditor.ActionEditor
{
	public class CreateTeacherActionPanelView : MonoBehaviour
	{
		public InputField InputField_Name;
		public InputField InputField_Text;
		public InputField InputField_Duration;
		public InputField InputField_CooldownMin;
		public InputField InputField_CooldownMax;
		public InputField InputField_Label;
		
		public Button Button_Create;
		public Button Button_Cancel;

		private XNamespace _type = "xsi:";

		// Start is called before the first frame update
		void Start()
		{
			Button_Create.onClick.AddListener(CreateButtonClicked);
			Button_Cancel.onClick.AddListener(() =>
			{
				ResetValues();
				gameObject.SetActive(false);
			});
		}

		void CreateButtonClicked()
		{
			if (InputField_Name.text.Trim().Equals(""))
				return;

			XElement root = new XElement("BaseAction");
			root.Add(new XAttribute(_type + "type", InputField_Name.text));
			root.Add(new XAttribute(_type + "type", InputField_Text.text));
			root.Add(new XAttribute("Id", Guid.NewGuid()));
			root.Add(new XAttribute("Name", InputField_Name.text));
			root.Add(new XElement("ActorId", "-1"));
			root.Add(new XElement("IsMisbehaviour", "false"));

			if (!InputField_Duration.text.Trim().Equals(""))
				root.Add(new XElement("Duration", InputField_Duration.text));
			if (!InputField_CooldownMin.text.Trim().Equals(""))
				root.Add(new XElement("CooldownMin", InputField_CooldownMin.text));
			if (!InputField_CooldownMax.text.Trim().Equals(""))
				root.Add(new XElement("CooldownMax", InputField_CooldownMax.text));
			if (!InputField_Label.text.Trim().Equals(""))
			{
				root.Add(new XElement("Label", InputField_Label.text));
				root.Add(new XElement("ExecuteImmediately", "false"));
				root.Add(new XElement("TickMin", "5"));
				root.Add(new XElement("TickMax", "0"));
			}

			KarmaMessageBus.Publish(new CreateTeacherActionEvent { element = root });
			gameObject.SetActive(false);
		}

		void ResetValues()
		{
			InputField_Name.text = "";
			InputField_Duration.text = "";
		}
	}
}
