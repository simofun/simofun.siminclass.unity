﻿using KarmaFramework.KarmaUtils;
using KarmaFramework.Localization;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TeacherActionEditorEntry : MonoBehaviour
{
    [SerializeField] private Text _name;
    [SerializeField] private Text _text;
    [SerializeField] private Text _duration;
    [SerializeField] private Text _cooldownMin;
    [SerializeField] private Text _cooldownMax;

    public void Bind(XElement element)
    {
        _name.text = Localizer.Instance.GetString(element.GetStrAttr("Name"));
        _text.text = Localizer.Instance.GetString(element.GetStrElem("Text"));
        _duration.text = Localizer.Instance.GetString("Duration") + " : " + element.GetStrElem("Duration");
        _cooldownMin.text = Localizer.Instance.GetString("CooldownMin") + " : " + element.GetStrElem("CooldownMin");
        _cooldownMax.text = Localizer.Instance.GetString("CooldownMax") + " : " + element.GetStrElem("CooldownMax");

        _duration.gameObject.SetActive(!element.GetStrElem("Duration").Trim().Equals(""));
        _cooldownMin.gameObject.SetActive(!element.GetStrElem("CooldownMin").Trim().Equals(""));
        _cooldownMax.gameObject.SetActive(!element.GetStrElem("CooldownMax").Trim().Equals(""));
    }
}
