﻿using Simofun.Karma.UniRx.Unity;
using System;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.ScenarioEditor.ActionEditor
{
	public class CreateStudentActionPanelView : MonoBehaviour
	{
		public InputField InputField_Name;
		public InputField InputField_Duration;

		public Toggle Toggle_Yes;
		public Toggle Toggle_No;

		public Button Button_Create;
		public Button Button_Cancel;

		private XNamespace _type = "xsi:";

		// Start is called before the first frame update
		void Start()
		{
			Button_Create.onClick.AddListener(CreateButtonClicked);
			Button_Cancel.onClick.AddListener(() =>
			{
				ResetValues();
				gameObject.SetActive(false);
			});
		}

		void CreateButtonClicked()
		{
			if (InputField_Name.text.Trim().Equals(""))
				return;

			XElement root = new XElement("BaseAction");
			root.Add(new XAttribute(_type + "type", InputField_Name.text));
			root.Add(new XAttribute("Id", Guid.NewGuid()));
			root.Add(new XAttribute("Name", InputField_Name.text));
			root.Add(new XElement("ActorId", "0"));
			root.Add(new XElement("IsMisbehaviour", Toggle_Yes.isOn ? "true" : "false"));
			if(!InputField_Duration.text.Trim().Equals(""))
			{
				root.Add(new XElement("Duration",InputField_Duration.text));
			}

			KarmaMessageBus.Publish(new CreateStudentActionEvent() { element = root });
			gameObject.SetActive(false);
		}

		void ResetValues()
		{
			InputField_Name.text = "";
			InputField_Duration.text = "";
		}
	}
}
