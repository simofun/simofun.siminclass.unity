﻿using KarmaFramework.KarmaUtils;
using KarmaFramework.Localization;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StudentActionEditorEntry : MonoBehaviour
{
    [SerializeField] private Text _name;
    [SerializeField] private Text _duration;
    [SerializeField] private Text _isMisbehaviour;

    public void Bind(XElement element)
    {
        _name.text = Localizer.Instance.GetString(element.GetStrAttr("Name"));
        _duration.text = Localizer.Instance.GetString("Duration") + " : "+ element.GetStrElem("Duration");
        _isMisbehaviour.text = Localizer.Instance.GetString("IsMisbehaviour") + " : " + element.GetStrElem("IsMisbehaviour");

        _duration.gameObject.SetActive(!element.GetStrElem("Duration").Trim().Equals(""));
        _isMisbehaviour.gameObject.SetActive(!element.GetStrElem("IsMisbehaviour").Trim().Equals(""));
    }
}
