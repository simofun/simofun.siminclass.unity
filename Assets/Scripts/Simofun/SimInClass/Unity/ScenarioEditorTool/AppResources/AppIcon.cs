﻿namespace Sinifta.ScenarioEditor.AppResources
{
    public class AppIcon
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string AppType { get; set; }
    }
}