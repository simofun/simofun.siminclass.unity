﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Sinifta.ScenarioEditor.AppResources
{
    public class Folder
    {
        [XmlAttribute]
        public string Name { get; set; }
        public List<ImageButton> Images { get; set; }

        public Folder()
        {
            Images = new List<ImageButton>();
        }
        public override string ToString() => Name;
    }
}