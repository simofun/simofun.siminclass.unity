﻿namespace Sinifta.ScenarioEditor.AppResources
{
    public class ImageButton : ResourceBase
    {
        public string Image { get; set; }
    }

    public class AudioButton : ResourceBase
    {
        public string Audio { get; set; }
    }

    public abstract class ResourceBase
    {
        public string Name { get; set; }

        public override string ToString() => Name;

    }
}