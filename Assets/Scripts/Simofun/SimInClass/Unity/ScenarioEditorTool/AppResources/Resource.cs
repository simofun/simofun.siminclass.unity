﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Sinifta.ScenarioEditor.AppResources
{
    [Serializable]
    [XmlInclude(typeof(ImageGalleryAppResources))]
    [XmlInclude(typeof(ImageGalleryToStudentAppResources))]
    [XmlInclude(typeof(TabletOperatingSystemAppResources))]
    [XmlInclude(typeof(BoardOperatingSystemAppResources))]
    [XmlInclude(typeof(DrawingAppResources))]
    public abstract class Resource
    {
        public string Name { get; set; }
        public abstract IEnumerable<string> GetResourceNames();
    }

}
