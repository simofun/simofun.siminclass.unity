﻿using System.Collections.Generic;
using System.Linq;

namespace Sinifta.ScenarioEditor.AppResources
{
    public class BoardOperatingSystemAppResources : Resource
    {
        public List<AppIcon> Apps { get; set; }

        public override IEnumerable<string> GetResourceNames()
        {
            return Apps.Select(x => x.Icon);
        }
    }
}