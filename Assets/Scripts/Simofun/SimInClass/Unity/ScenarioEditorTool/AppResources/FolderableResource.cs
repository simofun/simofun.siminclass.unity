﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sinifta.ScenarioEditor.AppResources
{
    [Serializable]
    public abstract class FolderableResource : Resource
    {
        public string Header { get; set; }
        public List<Folder> Folders { get; set; }

        public override IEnumerable<string> GetResourceNames()
        {
            return Folders.SelectMany(f => f.Images.Select(x => x.Image)).ToList();
        }
    }
}
