﻿using JetBrains.Annotations;
using KarmaFramework.ScenarioCore;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;
using Zenject;

namespace Sinifta.ScenarioEditor
{
    public static class GameEnvironmentHolder
    {
        public static string RootPath;
        public static string GameStatePath;
        public static string EnvironmentPath;

        public static string DefaultStudentName = "Name";
    }


    public class EditorViewModel : MonoBehaviour, INotifyPropertyChanged
    {
        private ActionConfig _actionConfig;
        private ActorConfig _actorConfig;
        private ScenarioConfig _scenarioConfig;
        private ReasonerConfig _reasonerConfig;


        private bool _quitConfirmationEnabled;
        public bool QuitConfirmationEnabled
        {
            get { return _quitConfirmationEnabled; }
            set
            {
                if (value.Equals(_quitConfirmationEnabled)) return;
                _quitConfirmationEnabled = value;
            }
        }

        private bool isFirstLaunch;
        private bool isNewScenario;

        //public static Dictionary<string, BitmapImage> ImageList;
        
        [Inject]
        public void Construct(ActionConfig actionConfig, ActorConfig actorConfig, ScenarioConfig scenarioConfig, ReasonerConfig reasonerConfig)
        {
            _actionConfig = actionConfig;
            _actorConfig = actorConfig;
            _scenarioConfig = scenarioConfig;
            _reasonerConfig = reasonerConfig;
        }

        private void Start()
        {
            QuitConfirmationEnabled = true;
            isFirstLaunch = true;
            isNewScenario = true;
        }

        public void NewFile()
        {

        }

        public void LoadFile()
        {

        }

        public void SaveFile()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public DirectoryInfo MakeUnique(string path)
        {
            var di = new DirectoryInfo(path);
            for (int i = 1; ; ++i)
            {
                if (!Directory.Exists(path))
                    return new DirectoryInfo(path);

                path = Path.Combine(di.Parent.FullName, di.Name + " " + i);
            }
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            var dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }
            else
            {
                // If the destination directory doesn't exist, create it. 
                if (!Directory.Exists(destDirName))
                {
                    Directory.CreateDirectory(destDirName);
                }

                // Get the files in the directory and copy them to the new location.
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string temppath = Path.Combine(destDirName, file.Name);
                    file.CopyTo(temppath, true);
                }

                // If copying subdirectories, copy them and their contents to new location. 
                if (copySubDirs)
                {
                    var dirs = dir.GetDirectories();

                    foreach (DirectoryInfo subdir in dirs)
                    {
                        string temppath = Path.Combine(destDirName, subdir.Name);
                        DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                    }
                }
            }

        }
    }

}