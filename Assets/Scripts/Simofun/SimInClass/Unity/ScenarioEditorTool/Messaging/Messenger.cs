﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Sinifta.ScenarioEditor.Messaging
{
    public class Messenger
    {
        static Hashtable htActions = new Hashtable();

        public static void Register<T>(Action<T> action)
        {
            AddToHash<T>(action);
        }

        public static void UnRegister<T>(Action<T> action)
        {
            List<Action<T>> actionList = GetActionList<T>();

            if (actionList == null)
            {
                return;
            }
            actionList.Remove(action);
        }

        private static void AddToHash<T>(Action<T> action)
        {
            var type = typeof(T);
            List<Action<T>> actionList = GetActionList<T>();

            if (actionList == null)
            {
                actionList = new List<Action<T>>();
                htActions.Add(type, actionList);
            }
            actionList.Add(action);
        }

        private static List<Action<T>> GetActionList<T>()
        {
            var type = typeof(T);
            if (htActions.ContainsKey(type))
            {
                return htActions[type] as List<Action<T>>;
            }
            return null;
        }

        public static void Send<T>(T value)
        {
            var list = GetActionList<T>();
            if (list != null)
            {
                list.ForEach(l => l?.Invoke(value));
            }
        }
    }
}
