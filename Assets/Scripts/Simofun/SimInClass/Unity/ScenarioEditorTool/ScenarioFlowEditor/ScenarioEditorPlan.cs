﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ScenarioEditorPlan : MonoBehaviour
{
    private GameState _gameState;

    [Inject]
    public void Construct(GameState gameState)
    {
        _gameState = gameState;
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach(var item in _gameState.Actions)
        {
            Debug.LogError(item.Name);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
