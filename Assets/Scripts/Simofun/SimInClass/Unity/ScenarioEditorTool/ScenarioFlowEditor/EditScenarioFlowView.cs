﻿using KarmaFramework.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class EditScenarioFlowView : MonoBehaviour
{
    public Button Button_Create;

    public InputField InputField_Name;
    public InputField InputField_Description;

    public Transform AvailableActionEntryHolder;
    [SerializeField] private AvailableActionEntry _availableActionEntry;

    private string _configPath;
    private readonly string newScenarioPath = Application.streamingAssetsPath + "/Scenarios";

    private XDocument _document;
    private XElement _actionsRoot;

    private readonly XNamespace xsi= "http://www.w3.org/2001/XMLSchema-instance";
    private readonly XNamespace xsd = "http://www.w3.org/2001/XMLSchema";

    private MenuNavigator _navigator;
    [Inject]
    public void Construct(MenuNavigator navigator)
    {
        _navigator = navigator;
    }

    // Start is called before the first frame update
    void Start()
    {
        _configPath = Application.persistentDataPath + "/Configs";

        Button_Create.onClick.AddListener(CreateScenario);

        if (Directory.Exists(_configPath) && File.Exists(_configPath + "/ActionConfig.xml"))
        {
            _document = XDocument.Load(_configPath + "/ActionConfig.xml");
        }
        else
        {
            if (!Directory.Exists(_configPath))
                Directory.CreateDirectory(_configPath);

            var ta = Resources.Load<TextAsset>("Configs/ActionConfig");
            _document = XDocument.Parse(ta.text);
            _document.Save(_configPath + "/ActionConfig.xml");
        }
        _actionsRoot = _document.Element("ActionsHolder").Element("Actions");


        _availableActionEntry.gameObject.SetActive(false);

        foreach (var child in AvailableActionEntryHolder.Cast<Transform>().Select(x => x.gameObject).Where(x => x.gameObject.activeSelf))
        {
            Destroy(child);
        }
        foreach (var action in _actionsRoot.Elements())
        {
            if (action.Elements("Label").Any())
            {
                var availableActionEntry = Instantiate(_availableActionEntry, AvailableActionEntryHolder, false);
                availableActionEntry.gameObject.SetActive(true);
                availableActionEntry.Bind(action);
            }
        }
    }

    void CreateScenario()
    {
        int _scenarioCount = Directory.GetFiles(newScenarioPath).Length + 1;

        XDocument newScenario = new XDocument();
        XElement root = new XElement("Scenario");
        root.Add(new XAttribute("Name", "1"));
        root.Add(new XAttribute("ReadableName", InputField_Name.text));
        root.Add(new XElement("EnvironmentId","1"));
        root.Add(new XElement("Duration", "240"));
        root.Add(new XElement("Description", InputField_Description.text));

        int _timeTrigger = 0;
        XElement flowElements = new XElement("FlowElements");
        for(int i = 0; i < AvailableActionEntryHolder.childCount; i++)
        {
            var fe = AvailableActionEntryHolder.GetChild(i).GetComponent<AvailableActionEntry>();
            if (!fe.isSelected)
                continue;

            XElement flowElement = new XElement("FlowElement");
            flowElement.Add(new XAttribute("Name",fe.Name));

            XElement timeTrigger = new XElement("Trigger");
            timeTrigger.Add(new XAttribute(xsi + "type","TimeTrigger"));
            timeTrigger.Add(new XElement("Time", _timeTrigger.ToString()));

            flowElement.Add(timeTrigger);
            flowElement.Add(fe.element);
            flowElements.Add(flowElement);
        }
        root.Add(flowElements);
        newScenario.Add(root);

        string newFolderPath = newScenarioPath + "/" + _scenarioCount;
        var folder = Directory.CreateDirectory(newFolderPath);
        StartCoroutine(WaitForFolderCreate(newScenario, _scenarioCount));
    }

    IEnumerator WaitForFolderCreate(XDocument newScenario, int scenarioCount)
    {
        string newFolderPath = newScenarioPath + "/" + scenarioCount;
        while (!Directory.Exists(newScenarioPath))
        {
            yield return new WaitForSeconds(1f);
        }

        newScenario.Save(newFolderPath + "/scenario.xml");
        _navigator.Proceed(true);
    }
}
