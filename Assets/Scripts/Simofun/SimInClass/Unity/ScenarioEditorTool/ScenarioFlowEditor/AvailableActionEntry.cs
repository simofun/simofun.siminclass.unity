﻿using KarmaFramework.KarmaUtils;
using KarmaFramework.Localization;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AvailableActionEntry : MonoBehaviour
{
    [SerializeField] private Button _selected;
    [SerializeField] private Image _background;
    [SerializeField] private Text _name;

    public string Name;
    public XElement element;

    public bool isSelected;

    // Start is called before the first frame update
    void Start()
    {
        isSelected = false;
        _selected.onClick.AddListener(SelectedButtonClicked);   
    }
    
    public void Bind(XElement element)
    {
        this.element = element;
        Name = element.GetStrAttr("Name");
        _name.text = Localizer.Instance.GetString(Name);
    }

    private void SelectedButtonClicked()
    {
        isSelected = !isSelected;
        _background.color = isSelected ? new Color(50f/255, 144f/255,  197f/255) : Color.gray;
    }
}
