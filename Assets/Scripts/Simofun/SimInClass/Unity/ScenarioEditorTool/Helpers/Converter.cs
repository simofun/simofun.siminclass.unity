﻿using System;
using System.Globalization;
using System.IO;

namespace Sinifta.ScenarioEditor.Helpers
{
    public class FloatToHundredConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var f = (double)value;
            return (((int)(f * 100)) / 10) * 10;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var f = int.Parse((string)value);
            f = (f / 10) * 10;
            if (f == 0)
                return 0;
            return ((float)f) / 100;
        }
    }
    /*
    public class ImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var dir = Path.GetDirectoryName(VisualHolder.GameStatePath) + "\\img\\" + value as string;

            if (!File.Exists(dir))
                return null;

            var img = new BitmapImage();

            using (var fs = new FileStream(dir, FileMode.Open, FileAccess.Read))
            {
                img.BeginInit();
                img.StreamSource = fs;
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.EndInit();
            }

            img.Freeze();

            return img;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class VisualNameToFilePath : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            string name = value as string;
            var nn = VisualHolder.ScenarioManager.Visuals.ClassroomVisuals.FirstOrDefault(x => x.Name == name).PrefabName;
            var str = "../../Resources/" + nn + "/" + nn + ".JPG";
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = value as string;
            int x = 0;
            if (str == "")
                return -1;
            if (int.TryParse(str, out x) && x > 0)
                return x;
            return -1;
        }
    }

    public class ActionToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BaseAction act;
            if (value == null)
                return null;
            act = value as BaseAction;
            if (act == null)
                return null;
            return act.Name + " (" + act.ShortId + ")";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = value as string;
            int x = 0;
            if (str == "")
                return -1;
            if (int.TryParse(str, out x) && x > 0)
                return x;
            return -1;
        }
    }

    public class MissionToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TriggerMissionTuple act;
            if (value == null)
                return null;
            act = value as TriggerMissionTuple;
            if (act == null)
                return null;
            return act.Name + " (" + act.Mission.Id.ToString().Substring(0, 5) + ")";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = value as string;
            int x = 0;
            if (str == "")
                return -1;
            if (int.TryParse(str, out x) && x > 0)
                return x;
            return -1;
        }
    }

    public class GetFlowStudentActionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<TriggerActionsTuple> list;
            if (value == null)
                return null;
            list = value as List<TriggerActionsTuple>;
            if (list == null)
                return null;
            return list.SelectMany(x => x.Actions.Where(a => a is StudentAction));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = value as string;
            int x = 0;
            if (str == "")
                return -1;
            if (int.TryParse(str, out x) && x > 0)
                return x;
            return -1;
        }
    }

    public class GetFlowActionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<TriggerActionsTuple> list;
            if (value == null)
                return null;
            list = value as List<TriggerActionsTuple>;
            if (list == null)
                return null;
            return list.SelectMany(x => x.Actions);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = value as string;
            int x = 0;
            if (str == "")
                return -1;
            if (int.TryParse(str, out x) && x > 0)
                return x;
            return -1;
        }
    }

    public class ActionTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return "";
            return ((float)value) == -1 ? "" : (value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = value as string;
            int x = 0;
            if (str == "")
                return -1;
            if (int.TryParse(str, out x) && x > 0)
                return x;
            return -1;
        }
    }

    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RangeToTextboxConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            //return values[0];
            return values[0] + " - " + values[1];
            //throw new NotImplementedException("implement me UICurrencyPairConfigurationMultiConverter.Convert");
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value == null || (float.Parse((value as string).Split('-')[0]) > float.Parse((value as string).Split('-')[1])))
                {
                    return new object[] { };
                }
            }
            catch (Exception)
            {
                return new object[] { };
            }

            return new object[]
            {
                float.Parse((value as string).Split('-')[0]),
                float.Parse((value as string).Split('-')[1])
            };
        }
    }

    public class RangeToTextboxConverterInt : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            //return values[0];
            return values[0] + " - " + values[1];
            //throw new NotImplementedException("implement me UICurrencyPairConfigurationMultiConverter.Convert");
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value == null || (int.Parse((value as string).Split('-')[0]) > int.Parse((value as string).Split('-')[1])))
                {
                    return new object[] { };
                }
            }
            catch (Exception)
            {
                return new object[] { };
            }

            return new object[]
            {
                int.Parse((value as string).Split('-')[0]),
                int.Parse((value as string).Split('-')[1])
            };
        }
    }

    public class ResourceImageNameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values[0];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[] { value, value };
        }
    }

    public class TypeReadabilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return "";
            if (!(value is Type))
                return value;
            return (value as Type).Name.SplitCamelCase();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("MethodToValueConverter can only be used for one way conversion.");
        }
    }

    public class TeacherActionToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //if (value is TeacherAction)
            //    return Visibility.Hidden;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("MethodToValueConverter can only be used for one way conversion.");
        }
    }

    public class ActionTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ActionEffect)
                return Visibility.Hidden;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("MethodToValueConverter can only be used for one way conversion.");
        }
    }

    public sealed class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class StudentDataToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value as Student) == null ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class StudentPersonalityToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Student s = value as Student;
            if (s == null)
                return Visibility.Hidden;

            if (s.PersonalityType == PersonalityType.Custom)
                return Visibility.Visible;

            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class VisualToIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var gender = VisualHolder.ScenarioManager.Visuals.StudentVisuals.First(x => x.Name == (string)value).ModelGender;
                switch (gender)
                {
                    case GenderType.Male:
                        return Application.Current.FindResource("male_icon");
                    case GenderType.Female:
                        return Application.Current.FindResource("female_icon");
                    default:
                        return Application.Current.FindResource("male_icon");
                }
            }
            catch (Exception)
            {

            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("MethodToValueConverter can only be used for one way conversion.");
        }
    }

    public class LocalizerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string)(parameter ?? value.ToString());
            StringId id;
            if (Enum.TryParse(str, out id))
            {
                str = Localizer.Instance.GetString(id);
            }
            else
            {
                str = str.SplitCamelCase();
            }
            return str;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("MethodToValueConverter can only be used for one way conversion.");
        }
    }

    public class IsTeacherActionToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TeacherAction)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class IsTeacherLectureActionToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TeacherLecture)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TeacherResourceSelectionConverter : IValueConverter
    {
        TeacherActionResourceSelectionVm _prevVm;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var teacherAction = value as TeacherLecture;
            if (teacherAction == null)
            {
                return value;
            }
            if (_prevVm != null)
            {
                _prevVm.CleanUp();
            }
            return _prevVm = new TeacherActionResourceSelectionVm(teacherAction);

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class AudioResourceConverter : IValueConverter
    {
        static readonly string AudioResourceName = typeof(AudioSourceResource).Name;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (VisualHolder.GameState == null || VisualHolder.GameState.Scenario == null)
            {
                return null;
            }
            var audioResources = VisualHolder.GameState.Scenario.ResourcesHolder.Resources[AudioResourceName].GetResourceNames().ToList();
            audioResources.Insert(0, string.Empty);
            return audioResources;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }*/
}
