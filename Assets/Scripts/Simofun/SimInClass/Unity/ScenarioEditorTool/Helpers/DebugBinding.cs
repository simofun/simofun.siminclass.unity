﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace Sinifta.ScenarioEditor.Helpers
{
    public class DebugBinding : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Debugger.Break();
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


}
