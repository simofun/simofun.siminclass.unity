﻿using KarmaFramework.KarmaUtils;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StudentEditorEntry : MonoBehaviour
{
    [SerializeField] private Text _fullName;
    [SerializeField] private Text _bio;

    [SerializeField] private Image _image;

    public void Bind(XElement element)
    {
        _fullName.text = element.GetStrAttr("Name") + " " + element.GetStrAttr("SurName");
        _bio.text = element.Element("Bio").Value;
        
        _image.sprite = Resources.Load<Sprite>("Icons/Npc/" + element.GetStrAttr("Icon"));
    }
}
