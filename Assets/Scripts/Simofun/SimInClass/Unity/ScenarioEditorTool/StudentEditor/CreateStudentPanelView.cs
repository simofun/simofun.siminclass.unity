﻿using KarmaFramework.Localization;
using Simofun.Karma.UniRx.Unity;
using Sirenix.Utilities;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.ScenarioEditor.StudentEditor
{
	public class CreateStudentPanelView : MonoBehaviour
	{
		public InputField InputField_Name;
		public InputField InputField_SurName;
		public InputField InputField_Bio;

		public Toggle Toggle_GenderMale;
		public Toggle Toggle_GenderFemale;

		public Button Button_Create;
		public Button Button_Cancel;

		private XNamespace _type = "xsi:";
		private int _studentCount;

		// Start is called before the first frame update
		void Start()
		{
			Button_Create.onClick.AddListener(CreateButtonClicked);
			Button_Cancel.onClick.AddListener(() =>
			{
				ResetValues();
				gameObject.SetActive(false);
			});
		}


		public void Bind(int studentCount)
		{
			gameObject.SetActive(true);
			_studentCount = studentCount + 1;

			ResetValues();
		}

		void CreateButtonClicked()
		{
			var gender = this.Toggle_GenderFemale.isOn ? "FemaleStudent" : "MaleStudent";
			var icon = Localizer.Instance.GetCurrentLanguageShortStringCode().ToTitleCase()
				+ (this.Toggle_GenderFemale.isOn ? "FemaleStudentIcon" : "MaleStudentIcon");

			int random = Toggle_GenderFemale.isOn ? Random.Range(0, 4) : Random.Range(0, 8);

			gender = random == 0 ? gender : gender + random.ToString();
			icon = random == 0 ? icon : icon + random.ToString();

			XElement root = new XElement("Actor");
			root.Add(new XAttribute(_type + "type", "Student"));
			root.Add(new XAttribute("id", _studentCount));
			root.Add(new XAttribute("Name", InputField_Name.text));
			root.Add(new XAttribute("SurName", InputField_SurName.text));
			root.Add(new XAttribute("VisualName", gender));
			root.Add(new XAttribute("Icon", icon));
			root.Add(new XAttribute("PlacementNumber", _studentCount));
			root.Add(new XElement("Bio", InputField_Bio.text));
			root.Add(new XElement("PersonalityType", "Average"));

			KarmaMessageBus.Publish(new CreateStudentEvent() { element = root });
			gameObject.SetActive(false);
		}

		void ResetValues()
		{
			InputField_Name.text = "";
			InputField_SurName.text = "";
			InputField_Bio.text = "";
		}
	}
}
