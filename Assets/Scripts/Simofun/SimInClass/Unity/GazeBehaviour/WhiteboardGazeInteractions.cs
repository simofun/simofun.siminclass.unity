﻿using UnityEngine;
using System.Collections;

public class WhiteboardGazeInteractions : MonoBehaviour
{
    private static WhiteboardGazeInteractions instance = null;

    [SerializeField]
    private GazeTarget gazeTarget;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (this != instance)
        {
            Debug.LogError("Multiple whiteboards in scene!");
            Destroy(this);
        }
    }

    public static void OnBoardClosed()
    {
        if (instance != null)
            GazeManager.IncreaseTargetAttractivenessTemporarily(instance.gazeTarget, 1.5f, 10f);
    }
}
