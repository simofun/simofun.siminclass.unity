﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts;
using Sinifta.Views;
using KarmaFramework;
using Sinifta.Actors;

public class GazeBehaviour : MonoBehaviour
{
	[System.Serializable]
	private class GazeTargetScoreMultiplier
	{
		public GazeTarget target;
		public float scoreMultiplier;

		public GazeTargetScoreMultiplier(GazeTarget target)
		{
			this.target = target;
			this.scoreMultiplier = 1f;
		}
	}

	[Range(0f, 360f)]
	[SerializeField]
	private float fieldOfView = 200f;

	private float _HalfFieldOfView;
	public float halfFieldOfView { get { return _HalfFieldOfView; } }

	private float orientationMultiplierForHumans;
	private float orientationMultiplierForProps;

	[Range(0f, 1f)]
	[SerializeField]
	private float shyness = 0.3f;

	[SerializeField]
	private float minimumGazeDuration = 3f;
	private float minimumGazeDurationWithMultiplier;

	[SerializeField]
	private float newTargetCheckFrequency = 0.2f;
	private float timeSinceNewTargetIsChecked = Mathf.Infinity;

	//[SerializeField]
	//private float maximumGazeDuration = 8f;

	private float timeSinceGazeStarted = Mathf.Infinity;

	[SerializeField]
	private Transform rootTransform;

	[SerializeField]
	private GazeTarget selfGazeTarget;
	public GazeTarget attachedGazeTarget { get { return selfGazeTarget; } }

	[SerializeField]
	private HeadMovement headController;

	[Header("Weights for saliency criteria (sum=1)")]
	[Header("- Closer targets are more interesting")]
	[Range(0f, 1f)]
	[SerializeField]
	private float proximityWeight = 0.25f;

	[Header("- Moving targets are more interesting")]
	[Range(0f, 1f)]
	[SerializeField]
	private float velocityWeight = 0.15f;

	[Header("- Targets looking at me are more interesting")]
	[Range(0f, 1f)]
	[SerializeField]
	private float orientationWeight = 0.3f;

	[Header("- More attractive targets are more interesting")]
	[Range(0f, 1f)]
	[SerializeField]
	private float attractivenessWeight = 0.3f;

	public Vector3 position { get { return rootTransform.position; } }
	public Vector3 forwardDirection { get { return rootTransform.forward; } }

	private List<GazeTarget> gazeTargets = new List<GazeTarget>();
	private GazeTarget currentlyActiveGazeTarget = null;

	public GazeTarget currentGazeTarget { get { return currentlyActiveGazeTarget; } }

	private Dictionary<GazeTarget, GazeTargetScoreMultiplier> gazeTargetsScoreMultipliers = new Dictionary<GazeTarget, GazeTargetScoreMultiplier>();
	private List<GazeTargetScoreMultiplier> scoreMultipliersList = new List<GazeTargetScoreMultiplier>();

	private Animator studentAnimator;

	private bool canLookAround
	{
		get
		{
			StudentAnim currAnim = (StudentAnim)studentAnimator.GetInteger("Animation");
			if (currAnim == StudentAnim.None || currAnim == StudentAnim.Idle || currAnim == StudentAnim.HaveQuestion)
				return true;

			return false;
		}
	}

	void Awake()
	{
		_HalfFieldOfView = fieldOfView * 0.5f;

		orientationMultiplierForHumans = 1f / (halfFieldOfView * 180f);
		orientationMultiplierForProps = 1f / halfFieldOfView;

		minimumGazeDuration += Random.Range(-1f, 1f);
		minimumGazeDurationWithMultiplier = minimumGazeDuration;

		shyness += Random.Range(-0.15f, 0.15f);

		headController.SetHeadMovementSpeed01(Random.Range(0.035f, 0.055f));

		studentAnimator = GetComponent<Animator>();
	}

	void Start()
	{
		GazeManager.AddGazeBehaviour(this);

		StudentFacade facade = GetComponent<MonoFacade>().Actor as StudentFacade;
		if(facade != null)
        {
			var studentAnim = facade.Animator as StudentAnimController;
			if (studentAnim != null)
			{
				studentAnim.onAnimationChanged += OnStudentAnimationChanged;
			}
		}
	}

	void OnDestroy()
	{
		GazeManager.RemoveGazeBehaviour(this);
	}

	void Update() //öğrencinin baktığı nesnenin süreye ve nesnenin anlık dikkat çekiciliğine göre belirlenmesi
	{
		if (timeSinceGazeStarted >= minimumGazeDurationWithMultiplier)
		{
			if (timeSinceNewTargetIsChecked >= newTargetCheckFrequency)
			{
				if (canLookAround)
				{
					GazeManager.FindGazeTargetsFor(this, selfGazeTarget, gazeTargets);
					UpdateScoreMultipliersList();

					float mostInterestingTargetScore;
					GazeTarget mostInterestingTarget = ScoreGazeTargets(out mostInterestingTargetScore);
					if (mostInterestingTarget == null || mostInterestingTargetScore < shyness)
					{
						if (currentlyActiveGazeTarget != null)
						{
							OnGazeTargetChanged(null);
						}
					}
					else if (mostInterestingTarget != currentlyActiveGazeTarget)
					{
						OnGazeTargetChanged(mostInterestingTarget);
					}
				}

				timeSinceNewTargetIsChecked = 0f;
			}
			else
			{
				timeSinceNewTargetIsChecked += KarmaTime.DeltaTime;
			}

			if (currentlyActiveGazeTarget != null && gazeTargetsScoreMultipliers.ContainsKey(currentlyActiveGazeTarget))
			{
				gazeTargetsScoreMultipliers[currentlyActiveGazeTarget].scoreMultiplier -= 0.12f * currentlyActiveGazeTarget.interestLoseMultiplier * Time.deltaTime;
			}
		}
		else
		{
			timeSinceGazeStarted += KarmaTime.DeltaTime;

			if (currentlyActiveGazeTarget != null && gazeTargetsScoreMultipliers.ContainsKey(currentlyActiveGazeTarget))
				gazeTargetsScoreMultipliers[currentlyActiveGazeTarget].scoreMultiplier -= 0.03f * currentlyActiveGazeTarget.interestLoseMultiplier * Time.deltaTime;
		}

		UpdateScoreMultipliers(KarmaTime.DeltaTime);
	}

	public void OnStudentAnimationChanged(string anim)
	{
		if (currentlyActiveGazeTarget == null)
			return;

		headController.SetLookAtWeight(canLookAround ? 1f : 0f);
	}

	public void ForceChangeGazeTarget(GazeTarget target)
	{
		OnGazeTargetChanged(target);

		if (target != null && gazeTargetsScoreMultipliers.ContainsKey(target))
			gazeTargetsScoreMultipliers[target].scoreMultiplier = 1f;
	}

	public void ForceUpdateGazeTarget()
	{
		minimumGazeDurationWithMultiplier = 0f;
	}

	private void OnGazeTargetChanged(GazeTarget newTarget) //bakılan nesnenin değiştirilmesi(ForceChangeGazeTarget'da çağrılıyor)
	{
		if (newTarget == null)
		{
			headController.SetLookAtWeight(0f);
			currentlyActiveGazeTarget = null;

			timeSinceGazeStarted = 0f;
			minimumGazeDurationWithMultiplier = minimumGazeDuration;
		}
		else
		{
			headController.SetLookAtWeight(canLookAround ? 1f : 0f);
			headController.SetTarget(newTarget.transform, newTarget.targetImportance);

			currentlyActiveGazeTarget = newTarget;

			timeSinceGazeStarted = 0f;
			minimumGazeDurationWithMultiplier = minimumGazeDuration * newTarget.gazeTimeMultiplier;
		}
	}

	private GazeTarget ScoreGazeTargets(out float score) //Bakılacak nesnelerin çekicilik değerlerinin hesaplanması ve en ilgi çekici nesnenin döndürülmesi
	{
		GazeTarget mostInterestingTarget = null;
		float mostInterestingTargetScore = Mathf.NegativeInfinity;

		for (int i = 0; i < gazeTargets.Count; i++)
		{
			GazeTarget thisTarget = gazeTargets[i];
			float targetScoreMultiplier = gazeTargetsScoreMultipliers[thisTarget].scoreMultiplier;

			Vector3 directionToGazeTarget = thisTarget.position - position;
			Vector3 gazeTargetForward = thisTarget.forwardDirection;

			float targetFinalScore;

			// 500f = distance from one corner of the classroom to the other corner
			// 0.002f = 1f / 500f
			float proximityScore = (20f - directionToGazeTarget.magnitude) * 0.05f;
			float velocityScore = (thisTarget.isMoving) ? 1f : 0f;
			float orientationScore;

			if (thisTarget.isHuman)
			{
				orientationScore = Mathf.Sqrt(Mathf.Max(0f, (_HalfFieldOfView - Vector3.Angle(forwardDirection, directionToGazeTarget)) *
								Vector3.Angle(forwardDirection, gazeTargetForward) * orientationMultiplierForHumans));
			}
			else
			{
				orientationScore = Mathf.Sqrt(Mathf.Max(0f, (_HalfFieldOfView - Vector3.Angle(forwardDirection, directionToGazeTarget)) *
								orientationMultiplierForProps));
			}

			targetFinalScore = targetScoreMultiplier * (proximityWeight * proximityScore + velocityWeight * velocityScore +
								orientationWeight * orientationScore + attractivenessWeight * thisTarget.attractiveness);

			if (targetFinalScore > mostInterestingTargetScore)
			{
				mostInterestingTarget = thisTarget;
				mostInterestingTargetScore = targetFinalScore;
			}
		}

		score = mostInterestingTargetScore;
		return mostInterestingTarget;
	}

	private void UpdateScoreMultipliersList()
	{
		for (int i = 0; i < gazeTargets.Count; i++)
		{
			GazeTarget target = gazeTargets[i];
			GazeTargetScoreMultiplier targetScoreMultiplier;
			if (!gazeTargetsScoreMultipliers.TryGetValue(target, out targetScoreMultiplier))
			{
				targetScoreMultiplier = new GazeTargetScoreMultiplier(target);

				gazeTargetsScoreMultipliers.Add(target, targetScoreMultiplier);
				scoreMultipliersList.Add(targetScoreMultiplier);
			}
		}
	}

	private void UpdateScoreMultipliers(float deltaTime)
	{
		for (int i = 0; i < scoreMultipliersList.Count; i++)
		{
			GazeTargetScoreMultiplier targetScoreMultiplier = scoreMultipliersList[i];
			if (currentlyActiveGazeTarget != targetScoreMultiplier.target)
			{
				targetScoreMultiplier.scoreMultiplier += 0.01f * deltaTime;
				if (targetScoreMultiplier.scoreMultiplier > 1f) targetScoreMultiplier.scoreMultiplier = 1f;
			}
		}
	}
}