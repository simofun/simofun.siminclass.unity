﻿using KarmaFramework;
using Simofun.Unity.DesignPatterns.Singleton;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;

public class TeacherGazeInteractions : SimSceneSingletonBase<TeacherGazeInteractions>
{
	#region Unity Fields
	[Title(nameof(TeacherGazeInteractions), "References")]
	[SerializeField]
	GazeTarget gazeTarget;

	[SerializeField]
	UnityStandardAssets.Characters.FirstPerson.FirstPersonController characterController;
	#endregion

	#region Properties
	public static GazeTarget GazeTarget => Instance.gazeTarget;
	#endregion

	#region Fields
	readonly WaitForSeconds shoutEffectWaitTimer = new WaitForSeconds(10f);

	float movementResetTime = Mathf.Infinity;

	IEnumerator shoutCoroutine = null;
	#endregion

	#region Unity Methods
	///// <inheritdoc />
	//protected virtual void Start()
	//{
	//	this.characterController.onCharacterMoved += this.OnTeacherMoved;
	//}

	/// <inheritdoc />
	protected virtual void Update()
	{
		if (KarmaTime.Time >= this.movementResetTime)
		{
			this.gazeTarget.isMoving = false;
			this.movementResetTime = Mathf.Infinity;
		}
	}
	#endregion

	#region Public Static Methods
	public static void OnShout()
	{
		var instance = Instance;
		if (instance.shoutCoroutine != null)
		{
			return;
		}

		instance.shoutCoroutine = instance.ShoutCoroutine();
		instance.StartCoroutine(instance.shoutCoroutine);
	}
	#endregion

	#region Public Methods
	public void OnTeacherMoved()
	{
		this.movementResetTime = KarmaTime.Time + 2f;
		this.gazeTarget.isMoving = true;
	}
	#endregion

	#region Methods
	private IEnumerator ShoutCoroutine()
	{
		this.gazeTarget.attractiveness += 10f;
		GazeManager.ForceChangeGazeTargetForBehaviours(this.gazeTarget);

		yield return shoutEffectWaitTimer;

		this.gazeTarget.attractiveness -= 10f;

		this.shoutCoroutine = null;
	}
	#endregion
}
