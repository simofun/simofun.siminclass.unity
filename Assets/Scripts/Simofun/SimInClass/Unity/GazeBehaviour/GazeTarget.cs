﻿using UnityEngine;

[DisallowMultipleComponent]
public class GazeTarget : MonoBehaviour
{
	public enum Importance { Low = 0, Normal = 1, High = 2 };

	[Header("In range [0,1] in normal circumstances")]
	public float attractiveness = 1f;

	[Header("Affects minimum gaze duration of behaviours")]
	public float gazeTimeMultiplier = 1f;

	[Header("How fast will behaviours lose interest on this target")]
	public float interestLoseMultiplier = 1f;

	[Header("Affects behaviours' head rotation speed")]
	public Importance targetImportance = Importance.Normal;

	[Header("Other settings")]
	[SerializeField]
	private bool isHumanTarget = true;
	public bool isHuman { get { return isHumanTarget; } }

	[System.NonSerialized]
	public bool isMoving = false;

	[SerializeField]
	private Transform rootTransform;

	public Vector3 position { get { return rootTransform.position; } }

	private Vector3 _ForwardDirection;
	public Vector3 forwardDirection { get { return _ForwardDirection; } }

	void Start()
	{
		GazeManager.AddGazeTarget(this);
	}

	void Update()
	{
		_ForwardDirection = rootTransform.forward;
	}

	void OnDestroy()
	{
		GazeManager.RemoveGazeTarget(this);
	}
}