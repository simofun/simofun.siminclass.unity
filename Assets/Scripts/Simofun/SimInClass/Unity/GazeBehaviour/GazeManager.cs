﻿using Simofun.Unity.DesignPatterns.Singleton;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeManager : SimGlobalSingletonBase<GazeManager>
{
	#region Fields
	readonly List<GazeBehaviour> gazeBehavioursInScene = new();

	readonly List<GazeTarget> gazeTargetsInScene = new();
	#endregion

	#region Public Static Methods
	#region GazeBehaviour Methods
	/// <summary>
	/// Nesneye bakabilecek birimin(genellikle öğrenci) eklenmesi 
	/// </summary>
	public static void AddGazeBehaviour(GazeBehaviour behaviour) => Instance.gazeBehavioursInScene.Add(behaviour);

	/// <summary>
	/// Nesneye bakabilecek birimin(genellikle öğrenci) çıkarılması
	/// </summary>
	public static void RemoveGazeBehaviour(GazeBehaviour behaviour)
	{
		// Destroyed when 'OnApplicationQuit' is invoked
		if (Instance == null)
		{
			return;
		}

		Instance.gazeBehavioursInScene.Remove(behaviour);
	}
	#endregion

	#region GazeTarget Methods
	/// <summary>
	/// Nesnenin sahnedeki hefeler listesine eklenmesi
	/// </summary>
	public static void AddGazeTarget(GazeTarget target) => Instance.gazeTargetsInScene.Add(target);

	public static void ForceChangeGazeTargetForBehaviours(GazeTarget newTarget)
	{
		for (var i = 0; i < Instance.gazeBehavioursInScene.Count; i++)
		{
			Instance.gazeBehavioursInScene[i].ForceChangeGazeTarget(newTarget);
		}
	}

	/// <summary>
	/// Nesneye bakmanın durdurulması
	/// </summary>
	public static void ForceStopLookingAtGazeTargetForBehaviours(GazeTarget gazeTarget)
	{
		for (var i = 0; i < Instance.gazeBehavioursInScene.Count; i++)
		{
			var behaviour = Instance.gazeBehavioursInScene[i];
			if (behaviour.currentGazeTarget == gazeTarget)
			{
				behaviour.ForceUpdateGazeTarget();
			}
		}
	}

	/// <summary>
	/// Nesnenin ilgi çekiciliğinin arttırılması
	/// </summary>
	public static void IncreaseTargetAttractivenessTemporarily(GazeTarget target, float increaseAmount, float time)
	{
		if (Instance == null)
		{
			return;
		}
		
		Instance.StartCoroutine(Instance.IncreaseTargetAttractiveness(target, increaseAmount, time));
	}

	/// <summary>
	/// Nesnenin sahnedeki hefeler listesinden çıkarılması
	/// </summary>
	public static void RemoveGazeTarget(GazeTarget target)
	{
		// Destroyed when 'OnApplicationQuit' is invoked
		if (Instance == null)
		{
			return;
		}

		Instance.gazeTargetsInScene.Remove(target);
	}
	#endregion

	/// <summary>
	/// Bakılabilecek nesnelerin liste haline getirilmesi
	/// </summary>
	public static void FindGazeTargetsFor(GazeBehaviour agent, GazeTarget except, List<GazeTarget> result)
	{
		result.Clear();

		var agentPosition = agent.position;
		var agentForwardDirection = agent.forwardDirection;
		for (var i = 0; i < Instance.gazeTargetsInScene.Count; i++)
		{
			var thisTarget = Instance.gazeTargetsInScene[i];
			if (thisTarget != except)
			{
				var direction = thisTarget.position - agentPosition;
				if (Vector3.Angle(agentForwardDirection, direction) <= agent.halfFieldOfView)
				{
					result.Add(thisTarget);
				}
			}
		}
	}
	#endregion

	#region Methods
	IEnumerator IncreaseTargetAttractiveness(GazeTarget target, float amount, float time)
	{
		target.attractiveness += amount;

		yield return new WaitForSeconds(time);

		target.attractiveness -= amount;
	}
	#endregion
}
