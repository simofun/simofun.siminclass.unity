﻿using Assets.Scripts;
using KarmaFramework;
using UnityEngine;

public class HeadMovement : MonoBehaviour
{
	[SerializeField]
	private Transform headBoneTransform;

	[SerializeField]
	private Transform rootTransform;

	[SerializeField]
	private float headRotationYLimit = 50f;

	[SerializeField]
	private Vector3 additionalHeadRotation = Vector3.zero;
	private Quaternion additionalRotation;
	private Quaternion localToWorldMultiplier;

	private Quaternion smoothRotation = Quaternion.identity;
	private float smoothRotationModifier = 1.7f;
	private float smoothRotationModifierWithMultiplier;

	private float lookAtWeight = 0f;

	private Transform target;

	void Awake()
	{
		additionalRotation = Quaternion.Euler(additionalHeadRotation);
		localToWorldMultiplier = rootTransform.rotation * Quaternion.Inverse(headBoneTransform.rotation);

		smoothRotationModifierWithMultiplier = smoothRotationModifier;
	}

	void LateUpdate()
	{
		if (lookAtWeight <= 0f)
		{
			smoothRotation = Quaternion.Lerp(smoothRotation, Quaternion.identity, smoothRotationModifierWithMultiplier * KarmaTime.DeltaTime);
		}
		else
		{
			if (target != null)
			{
				Vector3 eulerAngles = (Quaternion.LookRotation(rootTransform.InverseTransformDirection(target.position - headBoneTransform.position)) * additionalRotation).eulerAngles;

				//while( eulerAngles.x > 180f ) eulerAngles.x -= 360f;
				while (eulerAngles.y > 180f) eulerAngles.y -= 360f;
				//while( eulerAngles.y < -180f ) eulerAngles.y += 360f;

				eulerAngles *= lookAtWeight;
				eulerAngles.y = Mathf.Clamp(eulerAngles.y, -headRotationYLimit, headRotationYLimit);

				float angle;
				Vector3 axis;
				Quaternion.Euler(eulerAngles).ToAngleAxis(out angle, out axis);
				axis = localToWorldMultiplier * axis;

				smoothRotation = Quaternion.Lerp(smoothRotation, Quaternion.AngleAxis(angle, axis), smoothRotationModifierWithMultiplier * KarmaTime.DeltaTime);
			}
		}

		headBoneTransform.localRotation *= smoothRotation;
	}

	public void SetTarget(Transform target, GazeTarget.Importance targetImportance = GazeTarget.Importance.Normal)
	{
		this.target = target;

		if (targetImportance == GazeTarget.Importance.Low)
			smoothRotationModifierWithMultiplier = smoothRotationModifier * 0.66f;
		else if (targetImportance == GazeTarget.Importance.Normal)
			smoothRotationModifierWithMultiplier = smoothRotationModifier;
		else
			smoothRotationModifierWithMultiplier = smoothRotationModifier * 1.33f;
	}

	public void SetLookAtWeight(float weight)
	{
		weight = Mathf.Clamp01(weight);
		lookAtWeight = weight;
	}

	public void SetHeadMovementSpeed01(float movementSpeed)
	{
		smoothRotationModifier = movementSpeed * 35f;
	}

	public void SetAdditionalHeadRotation(Vector3 localEulerAngles)
	{
		additionalRotation = Quaternion.Euler(localEulerAngles);
	}

	[ContextMenu("Test")]
	void Asd()
	{
		Debug.Log((rootTransform.rotation * Quaternion.Inverse(headBoneTransform.rotation)).eulerAngles);
	}
}