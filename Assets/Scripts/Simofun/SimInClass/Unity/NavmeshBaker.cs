﻿namespace Sinifta.AI
{
	using Simofun.Unity.DesignPatterns.Singleton;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using Unity.AI.Navigation;
	using UnityEngine;

	public class NavmeshBaker : SimSceneSingletonBase<NavmeshBaker>
	{
		#region Fields
		List<NavMeshSurface> navMeshSurface;
		#endregion

		#region Public Methods
		public void BakeNavmesh(float latency)
		{
			this.navMeshSurface = FindObjectsOfType<NavMeshSurface>().ToList();
			this.StartCoroutine(this.Bake(latency));
		}

		public void ClearNavmesh()
		{
			this.navMeshSurface = FindObjectsOfType<NavMeshSurface>().ToList();
			this.navMeshSurface.ForEach(s => s.RemoveData());
		}
		#endregion

		#region Methods
		IEnumerator Bake(float latency)
		{
			yield return new WaitForSeconds(latency);

			this.navMeshSurface.ForEach(s => s.BuildNavMesh());
		}
		#endregion
	}
}
