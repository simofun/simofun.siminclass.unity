﻿namespace Simofun.SimInClass.Unity.Events
{
	using Simofun.Karma.Unity.Events;
    using System;

    public class SmartboardGameEvent : KarmaBaseUnityEvent
	{
		public Action LookToSmartBoard;
	}
}
