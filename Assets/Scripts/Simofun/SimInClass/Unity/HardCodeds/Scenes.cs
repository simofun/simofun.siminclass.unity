﻿#if UNITY_ANDROID || UNITY_IOS
#define MOBILE
#endif

namespace Simofun.SimInClass.Unity.HardCodeds
{
	public static class Scenes
	{
		public const int Boot = 0;

#if MOBILE
		public const int Login = -1; // LoginMobile => Disabled login for mobile

		public const int MainMenu = 3; // MainMenuMobile
#else
		public const int Login = 1;

		public const int MainMenu = 2;
#endif
		public const int Game = 4;

		public const int TestDetailedReportView = 5;

		// Not implemented yets. TODO: Convert to 'int' constant value instead 'string'
		public const string MultiplayerLobby = "MultiplayerLobby";

		public const string MultiplayerMenu = "MultiplayerMenuScene";

		public const string ScenarioEdit = "ScenarioEditScene";

		public const string TutorialSelection = "TutorialSelectionScene";

		public const string ViewReports = "ViewReportsScene";
	}
}
