using KarmaFramework.Views;
using Simofun.Karma.UniRx.Unity;
using UnityEngine;
using UnityEngine.UI;
namespace Simofun.SimInClass.Unity.Tutorial
{
    public class TutorialMetricView : MonoBehaviour
    {
        [SerializeField]
        TutorialGameItem []items;

        [SerializeField]
        GameObject infoPanel;

        [SerializeField]
        Button quit;

        TutorialStepway tutorialStepway;
        void Start()
        {
            infoPanel.SetActive(false);
            tutorialStepway = GetComponentInParent<TutorialStepway>();
            quit.onClick.AddListener(() => { KarmaMessageBus.Publish(new EndGameEvent()); });
            SetTutorialGameItemState();

        }
        void SetTutorialGameItemState()
        {
            for(int i = 0; i < tutorialStepway.MetricCounter; i++ )
            {
                items[i].ItemState(true);
            }
            for(int i = tutorialStepway.MetricCounter; i < items.Length; i++ )
            {
                items[i].ItemState(false);
            }
        }
    }
}