using UniRx;
using Sinifta.Views;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Events;

namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialGazeInteraction : TutorialActionBase
    {
        public override void RunAction()
        {
            KarmaMessageBus.Publish(new TutorialGazeArrowEvent() { State = true });
            KarmaMessageBus.OnEvent<LookAtStudentEvent>().Subscribe(ev =>
            {
                if (RunActionHandler != null)
                {
                    RunActionHandler();
                    RunActionHandler = null;
                }
            }).AddTo(disposables);
            base.RunAction();
        }
    }
    public class TutorialGazeArrowEvent : KarmaBaseUnityEvent
    {
        public bool State;
    }
}