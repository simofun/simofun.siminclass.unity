using UniRx;
using System;
using UnityEngine;
namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialActionBase : MonoBehaviour
    {
        public Action RunActionHandler;
        public CompositeDisposable disposables = new CompositeDisposable();
        public virtual void RunAction()
        {

        }
        public virtual void OnDestroy() => disposables.Dispose();
        
    }
}