using UniRx;
using Simofun.Karma.UniRx.Unity;
using KarmaFramework.Interactables.Views;

namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialActivityAction : TutorialActionBase
    {   
        public override void RunAction()
        {
            KarmaMessageBus.Publish(new TutorialGazeArrowEvent() { State = false });
            KarmaMessageBus.OnEvent<OpenInteractableEvent>().Subscribe(ev => 
            {
                if(ev.Id != "Activity")
                {
                    return;
                }
                if (RunActionHandler != null)
                {
                    RunActionHandler();
                    RunActionHandler = null;
                }
            }).AddTo(disposables);
            base.RunAction();
        }
    }
}