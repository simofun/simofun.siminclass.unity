using UniRx;
using Simofun.SimInClass.Unity.UI;
using Simofun.Karma.UniRx.Unity;
namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialSettingsAction : TutorialActionBase
    {
        public override void RunAction()
        {
            KarmaMessageBus.OnEvent<TutorialSettingsButtonEvent>().Subscribe(ev =>
            {
                if (RunActionHandler != null)
                {
                    RunActionHandler();
                    RunActionHandler = null;
                }
            }).AddTo(disposables);
            base.RunAction();
        }
    }
}