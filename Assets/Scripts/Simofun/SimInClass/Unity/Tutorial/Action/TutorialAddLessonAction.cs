using UniRx;
using UnityEngine;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Events;

namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialAddLessonAction : TutorialActionBase
    {
        [SerializeField]
        RectTransform targetSize;

        [SerializeField]
        RectTransform infoPanel;

        Vector2 initialAnchoredPosition;
        Vector2 initialSizeDelta;
        public override void RunAction()
        {
            initialSizeDelta = infoPanel.sizeDelta;
            infoPanel.sizeDelta = targetSize.sizeDelta;
            initialAnchoredPosition = infoPanel.anchoredPosition;
            infoPanel.anchoredPosition = targetSize.anchoredPosition;
            KarmaMessageBus.OnEvent<TutorialAddLessonEvent>().Subscribe(ev => 
            {
                if (RunActionHandler != null)
                {
                    RunActionHandler();
                    RunActionHandler = null;
                    infoPanel.sizeDelta = initialSizeDelta;
                    infoPanel.anchoredPosition = initialAnchoredPosition;
                }
            }).AddTo(disposables);
            base.RunAction();
        }
    }
    public class TutorialAddLessonEvent : KarmaBaseUnityEvent
    {
    }
}