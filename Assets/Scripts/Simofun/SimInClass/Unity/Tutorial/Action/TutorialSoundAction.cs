using UniRx;
using UnityEngine;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Events;

namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialSoundAction : TutorialActionBase
    {
        public override void RunAction()
        {
            KarmaMessageBus.OnEvent<KarmaAudioOnOffEvent>().Subscribe(ev =>
            {
                if (RunActionHandler != null)
                {
                    RunActionHandler();
                    RunActionHandler = null;
                }
            }).AddTo(disposables);
            base.RunAction();
        }
    }
}