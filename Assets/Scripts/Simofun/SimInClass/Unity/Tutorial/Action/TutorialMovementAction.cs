using UniRx;
using Simofun.Karma.UniRx.Unity;
using Simofun.SimInClass.Unity.Events;
namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialMovementAction : TutorialActionBase
    {
        public override void RunAction()
        {
            KarmaMessageBus.OnEvent<MoveTeacherGameEvent>().Subscribe(ev =>
            {
                if (RunActionHandler != null)
                {
                    RunActionHandler();
                    RunActionHandler = null;
                }
            }).AddTo(disposables);
            base.RunAction();
        }
    }
}
