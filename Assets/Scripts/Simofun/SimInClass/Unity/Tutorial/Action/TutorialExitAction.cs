using Simofun.Karma.UniRx.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using KarmaFramework.Views;
using Simofun.Karma.Unity.Events;

namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialExitAction : TutorialActionBase
    {
        [SerializeField]
        GameObject QuitConfirmationView;
        [SerializeField]
        GameObject TutorialMetricView;
        public override void RunAction()
        {
            KarmaMessageBus.OnEvent<TutorialExitEvent>().Subscribe(ev =>
            {
                QuitConfirmationView.SetActive(false);
                TutorialMetricView.SetActive(true);
                if (RunActionHandler != null)
                {
                    RunActionHandler();
                    RunActionHandler = null;
                }
            }).AddTo(disposables);
            base.RunAction();
        }
    }
    public class TutorialExitEvent : KarmaBaseUnityEvent
    {

    }
}