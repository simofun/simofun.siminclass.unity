using UniRx;
using Zenject;
using System.Linq;
using Sinifta.Actors;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Domain;
using System.Collections.Generic;
using static NPCMenu;

namespace Simofun.SimInClass.Unity.Tutorial.Actions
{
    public class TutorialMisbehaviourAction : TutorialActionBase
    {
        Student student;
        GameState gameState;

        [Inject]
        void Init(GameState gameState)
        {
            this.gameState = gameState;
        }
        private void Start()
        {
            var students = this.gameState.NPCs.OfType<Student>().ToList();
            foreach (var studentItem in students)
            { 
                if(studentItem.Id == 0)
                {
                    student = studentItem;
                }
            }
        }
        public override void RunAction()
        {
            student.FireAction("Sleep", new List<IActor> {});
            KarmaMessageBus.OnEvent<TutorialMisbehaviourEvent>().Subscribe(ev =>
            {
                if (RunActionHandler != null)
                {
                    RunActionHandler();
                    RunActionHandler = null;
                }
            }).AddTo(disposables);
            base.RunAction();
        }
    }
}
