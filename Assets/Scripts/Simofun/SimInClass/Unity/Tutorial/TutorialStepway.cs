namespace Simofun.SimInClass.Unity.Tutorial
{
	using KarmaFramework.Localization;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;
	using static Simofun.SimInClass.Unity.Tutorial.TutorialStep;

	public class TutorialStepway : MonoBehaviour
	{
		[SerializeField]
		Text info;

		[SerializeField]
		Text header;

		[SerializeField]
		Button okButton;

		[SerializeField]
		Transform arrow;

		[SerializeField]
		Transform infoPanel;

		[SerializeField]
		List<TutorialStep> steps;

		int index;
		PlatformType currentPlatform;
		public int MetricCounter;
		private void Awake()
		{
			CheckPlatform();
		}
		private void Start()
		{
			index = 0;
			MetricCounter = 0;
			okButton.onClick.AddListener(() => { Iteration(); });
			Iteration();
		}
		public void Iteration()
		{
			if(index >= steps.Count)
			{
				return;
			}
			arrow.gameObject.SetActive(steps[index].IsArrowActive);
			infoPanel.gameObject.SetActive(steps[index].IsInfoActive);

			if(steps[index].IsInfoActive)
			{
				header.text = Localizer.Instance.GetString(steps[index].Header.ToString());
				if(currentPlatform == PlatformType.Desktop)
				{
					info.text = Localizer.Instance.GetString("Tutorial_" + steps[index].DesktopText);
				}
				else
				{
					info.text = Localizer.Instance.GetString("Tutorial_" + steps[index].MobileText);
				}
				SetInfoPosition();
			}
			if(steps[index].IsArrowActive)
			{
				SetArrowPosition();
			}
			if(steps[index].IsActionActive)
			{
				steps[index].TutorialAction.RunActionHandler += Iteration;
				steps[index].TutorialAction.RunAction();
			}
			if(steps[index].IsMetricCompleted)
			{
				MetricCounter++;
			}
			okButton.gameObject.SetActive(steps[index].IsShownOkey);
			index++;
		}
		void SetArrowPosition()
		{
			arrow.SetParent(steps[index].ArrowPos);
			arrow.localPosition = Vector3.zero;
			arrow.transform.localEulerAngles = Vector3.zero;
		}
		void SetInfoPosition()
		{
			infoPanel.SetParent(steps[index].InfoPos);
			infoPanel.localPosition = Vector3.zero;
			infoPanel.transform.localEulerAngles = Vector3.zero;
		}
		void CheckPlatform()
		{
		#if UNITY_ANDROID || UNITY_UNITY_IOS
			currentPlatform = PlatformType.Mobile;
		#else
			currentPlatform = PlatformType.Desktop;
		#endif
		}
	}
}
