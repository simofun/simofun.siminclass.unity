using UnityEngine;
using Sirenix.OdinInspector;
using Simofun.SimInClass.Unity.Tutorial.Actions;
namespace Simofun.SimInClass.Unity.Tutorial
{
    public class TutorialStep : MonoBehaviour
    {
        #region Arrow
       
        [BoxGroup("Arrow")]
        public bool IsArrowActive;

        [BoxGroup("Arrow")]
        [ShowIf("IsArrowActive")]
        public Transform ArrowPos;

        #endregion
        #region Info
        public enum PlatformType
        {
            Desktop,
            Mobile
        }
        public enum HeaderType
        {
            Welcome,
            Information,
            Task
        }

        [BoxGroup("Info")]
        public bool IsInfoActive;

        [BoxGroup("Info")]
        [ShowIf("IsInfoActive")]
        public Transform InfoPos;

        [BoxGroup("Info")]
        [ShowIf("IsInfoActive")]
        [Header("Info Text")]

        [BoxGroup("Info")]
        [ShowIf("IsInfoActive")]
        public HeaderType Header;

        [BoxGroup("Info")]
        [ShowIf("IsInfoActive")]
        [EnumToggleButtons]
        public PlatformType Platform;

        [BoxGroup("Info")]
        [ShowIf("@IsInfoActive && Platform == PlatformType.Desktop")]
        public int DesktopText;

        [BoxGroup("Info")]
        [ShowIf("@IsInfoActive && Platform == PlatformType.Mobile")]
        public int MobileText;

        #endregion
        #region Action
        [BoxGroup("Action")]
        public bool IsActionActive;

        [BoxGroup("Action")]
        [ShowIf("IsActionActive")]
        public TutorialActionBase TutorialAction;
        #endregion
        #region OkeyButton
        [BoxGroup("Okey")]
        public bool IsShownOkey;
        #endregion
        #region Metric
        [BoxGroup("Metric")]
        public bool IsMetricCompleted;
        #endregion
    }
}