﻿using KarmaFramework.ScenarioCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class OpenTutorialInterface : MonoBehaviour
{
    [SerializeField]
    private Button InterfaceButton;
    [SerializeField]
    private GameObject tutorialPanel;

    private GameState gameState;
    private ScenarioConfig scenarioConfig;
    private Scenario tutorial;
    [Inject]
    public void Construct(GameState gameState, ScenarioConfig scenarioConfig)
    {
        this.scenarioConfig = scenarioConfig;
        this.gameState = gameState;
    }
    void Start()
    {
        InterfaceButton.onClick.AddListener(OpenInfoPanel);
        tutorial = this.scenarioConfig.GetScenario(1061);
    }

    void OpenInfoPanel()
    {
        tutorialPanel.SetActive(true);
        gameState.Scenario = tutorial;
    }
}
