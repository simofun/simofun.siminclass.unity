using KarmaFramework.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Simofun.SimInClass.Unity.Tutorial
{
    public class TutorialGameItem : MonoBehaviour
    {
        [SerializeField]
        Text completedText;

        [SerializeField]
        Image checkBoxImage;
        
        public void ItemState(bool state)
        {
            if(state)
            {
                completedText.text = Localizer.Instance.GetString("Completed");
                checkBoxImage.enabled = true;
            }
            else
            {
                completedText.text = Localizer.Instance.GetString("Incompleted");
            }
        }
    }
}