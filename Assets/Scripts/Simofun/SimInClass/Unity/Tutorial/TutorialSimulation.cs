﻿using KarmaFramework.EnvironmentCore;
using KarmaFramework.ScenarioCore;
using KarmaFramework.UI;
using Simofun.Networking.Core;
using Simofun.SimInClass.Unity.HardCodeds;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class TutorialSimulation : MonoBehaviour
{
    public UnityEngine.UI.Button Simulation;
    [SerializeField]
    private GameObject _loadingScreen;
    private EnvironmentConfig _environments;
    private MenuNavigator _navigator;
    private GameState _gameState;
    private ScenarioConfig _scenarios;

    [Inject]
    public void Construct(ScenarioConfig scenarioConfig,
            EnvironmentConfig environmentConfig,
            MenuNavigator navigator,
            GameState gameState)
    {
        _scenarios = scenarioConfig;
        _environments = environmentConfig;
        _navigator = navigator;
        _gameState = gameState;
    }

    private void Start()
    {
        var infos = _scenarios.GetScenarioInfos();
        var scnInfo = infos[0];
        Simulation.onClick.AddListener(()=>OnScenarioButtonClicked(scnInfo));
    }

    private void OnScenarioButtonClicked( ScenarioConfig.Info scenarioInfo)
    {  
        Scenario scenario = _scenarios.GetScenario(scenarioInfo.ScenarioId);
        _gameState.Scenario = scenario;
        GameEnvironment environment = _environments.GetEnvironment(scenario.EnvironmentId);
        _gameState.Environment = environment;
         //_navigator.SetCustomNextAction(StartGame);
        // _navigator.Proceed(false,MenuType.None, StartGame);
        StartCoroutine(WaitForDisconnect());
    }

    IEnumerator WaitForDisconnect()
    {
        SimNetwork.Instance.Disconnect();
        yield return new WaitForSeconds(0.1f);
        StartGame();
    }

    void StartGame()
    {
        _loadingScreen.gameObject.SetActive(true);
        _gameState.IsTutorial = true;
        SceneManager.LoadScene(Scenes.Game);
    }
}
