using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Simofun.SimInClass.Unity.Tutorial
{
    public class TutorialInfoPanelDrag : MonoBehaviour, IDragHandler
    {
        [SerializeField]
        RectTransform infoPanel;
        Canvas canvas;
        void Start()
        {
            canvas = GetComponentInParent<Canvas>();
        }
        public void OnDrag(PointerEventData eventData)
        {
            infoPanel.anchoredPosition += eventData.delta / canvas.scaleFactor;
        }
    }
}