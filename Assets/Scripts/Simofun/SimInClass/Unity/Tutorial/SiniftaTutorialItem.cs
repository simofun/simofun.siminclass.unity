﻿namespace Simofun.SimInClass.Unity.Tutorial
{
	using KarmaFramework.Localization;
	using Simofun.Unity.Tutorial;
	using UnityEngine;

	public class SiniftaTutorialItem : SimTutorialItem
	{
		#region Unity Fields
		[Header("Settings")]
		[SerializeField]
		string textLocalizationKey;
		#endregion

		#region Protected Methods
		protected override void InvokeOnStarted()
		{
			this.Text = Localizer.Instance.GetString(this.textLocalizationKey);

			base.InvokeOnStarted();
		}
		#endregion
	}
}
