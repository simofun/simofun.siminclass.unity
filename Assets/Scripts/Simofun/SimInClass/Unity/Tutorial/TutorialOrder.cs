﻿namespace Sinifta.Tutorial
{
	using KarmaFramework.Localization;
	using KarmaFramework.Views;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
    using System;
    using UniRx;
	using UnityEngine;
	using UnityEngine.UI;

	public class TutorialOrder : MonoBehaviour
	{
		public enum InfoType
		{
			Welcome,
			Information,
			Task
		}

		[SerializeField]
		private string[] trainerInstructionTurkish;
		[SerializeField]
		private string[] trainerInstructionEnglish;
		public InfoType[] trainerType;
		[SerializeField]
		private Text Info;
		[SerializeField]
		private Text Header;
		[SerializeField]
		private Button OkeyButton;
		[SerializeField]
		private Button QuitButton;
		[SerializeField]
		private GameObject InfoPanel;
		[SerializeField]
		private Transform[] arrowPos;
		[SerializeField]
		private Transform Arrow;

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		bool MovementLock = true;

		bool RotateLock = true; //Bu bool rorate eventi sürekli publish ettiği için tek seferlik çağırma için oluşturuldu.daha iyi bi çözümün bulunması gerekli

		int i = 0;

		string[] trainerInstruction;
		#endregion

		#region Unity Methods
		protected virtual void Start()
		{
			if (Localizer.Instance.CurrentLanguage == LanguageType.Turkish)
			{
#if !UNITY_ANDROID && !UNITY_IOS
				trainerInstructionTurkish[4] = Localizer.Instance.GetString("TutorialRotatePC");
				trainerInstructionTurkish[5] = Localizer.Instance.GetString("TutorialMovePC");
#endif
				trainerInstruction = trainerInstructionTurkish;
			}
			else
			{
#if !UNITY_ANDROID && !UNITY_IOS
				trainerInstructionEnglish[4] = Localizer.Instance.GetString("TutorialRotatePC");
				trainerInstructionEnglish[5] = Localizer.Instance.GetString("TutorialMovePC");
#endif
				trainerInstruction = trainerInstructionEnglish;
			}
			Info.text = trainerInstruction[0];
			Header.text = Localizer.Instance.GetString(trainerType[0].ToString());
			OkeyButton.onClick.AddListener(NextInstruction);
			QuitButton.onClick.AddListener(() => { gameObject.SetActive(false); });
		}

		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion

		void NextInstruction()
		{
			if (i < trainerInstruction.Length)
			{
				Header.text = Localizer.Instance.GetString(trainerType[i].ToString());
				Info.text = trainerInstruction[i];
				ControlInstruction();
			}

		}
		void ManageInstruction(Transform arrowPos = null, bool isArrowActive = false, bool isInfoPanelActive = false, Action action = null)
        {
			if(arrowPos != null)
            {
				ArrowPosition(arrowPos);
			}
			Arrow.gameObject.SetActive(isArrowActive);
			InfoPanel.gameObject.SetActive(isArrowActive);
			if(action != null)
            {
				action();
            }
        }

		void ControlInstruction()
		{
			switch (i)
			{
				case 0:
					i++;
					NextInstruction();
					break;
				case 1:
					i++;
					ArrowPosition(arrowPos[0]);
					break;
				case 2:
					ArrowPosition(arrowPos[1]);
					i++;
					break;
				case 3:
					ArrowPosition(arrowPos[2]);
					i++;
					break;
				case 4:
					Arrow.gameObject.SetActive(false);
					i++;
					break;
				case 5:
					InfoPanel.SetActive(false);
					i++;
					KarmaMessageBus.OnEvent<RotateTeacherGameEvent>().Subscribe(ev =>
					{
						if (RotateLock)
						{
							InfoPanel.gameObject.SetActive(true);
							RotateLock = false;
						}
					});
					break;
				case 6:
					InfoPanel.SetActive(false);
					i++;
					KarmaMessageBus.OnEvent<MoveTeacherGameEvent>().Subscribe(ev =>
					{
						if (MovementLock)
						{
							InfoPanel.gameObject.SetActive(true);
							MovementLock = false;
						}
					});
					break;
				case 7:
					//öğrenci oku buraya gelecek
					InfoPanel.SetActive(false);
					i++;
					KarmaMessageBus.OnEvent<DealResultEvent>().Subscribe(ev =>
					{
						InfoPanel.gameObject.SetActive(true);
						ArrowPosition(arrowPos[3]);
					});
					break;
				case 8:
					ArrowPosition(arrowPos[4]);
					i++;
					break;
				case 9:
					Arrow.gameObject.SetActive(false);
					KarmaMessageBus.Publish(new EndGameEvent());
					break;
			}

		}

		void ArrowPosition(Transform arrowPos)
		{
			Arrow.gameObject.SetActive(true);
			Arrow.SetParent(arrowPos);
			Arrow.localPosition = Vector3.zero;
			Arrow.transform.localEulerAngles = Vector3.zero;
		}
	}
}
