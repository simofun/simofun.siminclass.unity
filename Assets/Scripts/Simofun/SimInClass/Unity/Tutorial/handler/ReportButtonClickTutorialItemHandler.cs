﻿namespace Simofun.SimInClass.Unity.Tutorial
{
	using KarmaFramework.UI;
	using Simofun.Unity.Tutorial;
	using Zenject;

	public class ReportButtonClickTutorialItemHandler : SimTutorialItemHandlerBase
	{
		#region Fields
		MenuNavigator menuNavigator;
		#endregion

		#region Construct
		[Inject]
		public void Construct(MenuNavigator menuNavigator)
		{
			this.menuNavigator = menuNavigator;
		}
		#endregion

		#region Protected Methods
		protected override void WhenStarted()
		{
			base.WhenStarted();

			this.menuNavigator.Proceed(true);
		}
		#endregion
	}
}
