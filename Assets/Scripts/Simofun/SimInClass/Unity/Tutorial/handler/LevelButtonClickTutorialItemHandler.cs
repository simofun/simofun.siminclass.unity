﻿using UniRx;

namespace Simofun.SimInClass.Unity.Tutorial
{
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Simofun.Unity.Tutorial;

	public class LevelButtonClickTutorialItemHandler : SimTutorialItemHandlerBase
	{
		#region Fields
		CompositeDisposable disposables = new CompositeDisposable();
		SimTutorialController tutorialController;
        #endregion
        protected override void Start()
        {
			tutorialController = GetComponentInParent<SimTutorialController>();
            base.Start();
        }

        #region Protected Methods
        protected override void WhenStarted()
		{
			base.WhenStarted();

			this.disposables?.Dispose();
			this.disposables = new CompositeDisposable();

			KarmaMessageBus.OnEvent<LevelButtonClickGameEvent>()
				.Subscribe(_ => this.TutorialItem.Complete())
				.AddTo(this.disposables);
			KarmaMessageBus.OnEvent<BackButtonClickGameEvent>().Subscribe(ev =>
			{
				tutorialController.Close();
				disposables.Dispose();
			}).AddTo(disposables);
		}

		protected override void WhenCompleted()
		{
			base.WhenCompleted();

			this.disposables.Dispose();
			this.disposables = null;
		}
		#endregion
	}
}
