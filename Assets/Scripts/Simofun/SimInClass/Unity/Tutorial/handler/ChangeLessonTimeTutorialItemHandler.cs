﻿using UniRx;

namespace Simofun.SimInClass.Unity.Tutorial
{
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using Simofun.Unity.Tutorial;

	public class ChangeLessonTimeTutorialItemHandler : SimTutorialItemHandlerBase
	{
		#region Fields
		CompositeDisposable disposables = new CompositeDisposable();
		SimTutorialController tutorialController;
		#endregion

		#region Protected Methods
		protected override void Start()
		{
			tutorialController = GetComponentInParent<SimTutorialController>();
			base.Start();
		}
		protected override void WhenStarted()
		{
			base.WhenStarted();

			this.disposables?.Dispose();
			this.disposables = new CompositeDisposable();

			KarmaMessageBus.OnEvent<ChangeLessonTimeGameEvent>()
				.Subscribe(_ => this.TutorialItem.Complete())
				.AddTo(this.disposables);
			KarmaMessageBus.OnEvent<ConfirmExitGameEvent>().Subscribe(ev =>
			{
				tutorialController.Close();
				disposables.Dispose();
			}).AddTo(disposables);
		}

		protected override void WhenCompleted()
		{
			base.WhenCompleted();

			this.disposables.Dispose();
			this.disposables = null;
		}
		#endregion
	}
}
