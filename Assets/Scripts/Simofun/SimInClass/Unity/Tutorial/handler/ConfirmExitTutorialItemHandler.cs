﻿using UniRx;

namespace Simofun.SimInClass.Unity.Tutorial
{
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using Simofun.Unity.Tutorial;

	public class ConfirmExitTutorialItemHandler : SimTutorialItemHandlerBase
	{
		#region Fields
		CompositeDisposable disposables = new CompositeDisposable();
		#endregion

		#region Protected Methods
		protected override void WhenStarted()
		{
			base.WhenStarted();

			this.disposables.Dispose();
			this.disposables = new CompositeDisposable();

			KarmaMessageBus.OnEvent<ConfirmExitGameEvent>()
				.Subscribe(_ => this.TutorialItem.Complete())
				.AddTo(this.disposables);
		}

		protected override void WhenCompleted()
		{
			base.WhenCompleted();

			this.disposables.Dispose();
			this.disposables = null;
		}
		#endregion
	}
}
