﻿using UniRx;

namespace Simofun.SimInClass.Unity.Tutorial
{
	using KarmaFramework.Localization;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Simofun.Unity.Tutorial;
	using UnityEngine;

	public class SiniftaTutorialController : MonoBehaviour
	{
		#region Unity Fields
		[Header("References")]
		[Tooltip("If not set, try to find by 'GetComponent'")]
		[SerializeField]
		SimTutorialController tutorialController;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();
		#endregion

		#region Protected Properties
		protected SimTutorialController TutorialController =>
			this.tutorialController ?? (this.tutorialController = this.GetComponent<SimTutorialController>());
		#endregion

		#region Unity Methods
		protected virtual void OnEnable() => this.SetPanelInfo();

		protected virtual void Start() =>
			KarmaMessageBus.OnEvent<StartGameEvent>()
				.Subscribe(_ => this.TutorialController.Close())
				.AddTo(this.disposables);

		protected virtual void OnDestroy() => this.disposables.Clear();
		#endregion

		#region Methods
		void SetPanelInfo()
		{
			var localizer = Localizer.Instance;
			this.TutorialController.SetInitialInfoText(localizer.GetString("StartTutorialItem"));
			this.TutorialController.SetLastInfoText(localizer.GetString("LastTutorialItem"));
			this.TutorialController.SetTitleText(localizer.GetString("StartTutorialItemTitle"));
		}
		#endregion
	}
}
