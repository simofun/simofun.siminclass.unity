﻿using System.Linq;
using UnityEngine;
using Zenject;

namespace Sinifta.UI
{
    public class ScenarioSummaryPanel : ScenarioDetailsPanel
    {
        private GameState _gameState;

        [Inject]
        public void Construct(GameState gameState)
        {
            _gameState = gameState;
        }

        public override void SetPanelInformation()
        {
            base.SetPanelInformation();
            var dcption = _gameState.Scenario.Description;
            var sections = dcption.Split('_');
            for (int i = 1; i < 3; i++)
            {
                this.Description.text =
                    i % 2 == 1
                        ? FirstCharToUpper(sections[i].TrimStart('\n').ToLowerInvariant())
                        : sections[i].Replace("\t", "");
            }
            Description.text = "<b>" + _gameState.Scenario.ReadableName + "</b>" + Description.text;
            Title.text = "<b>" + _gameState.Scenario.ReadableName + "</b>";
        }
        private string FirstCharToUpper(string input)
        {
            if (string.IsNullOrEmpty(input))
                return input;

            return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}