﻿using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using KarmaFramework.UI;
using Sinifta.Environment;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class StudentTab : MonoBehaviour
{
	public StudentTabView StudentTabView;
	public ScenarioEnvironmentButton ScenarioEnvironmentPrefab;
	public RectTransform ScenarioEnvironmentsParent;
	public Button ExitButton;
	public Button OkButton;
	private List<ScenarioEnvironmentButton> environmentButtons;
	private ScenarioEnvironmentButton selectedEnvironment;

	private MenuNavigator _menuNavigator;
	private EnvironmentConfig _environments;

	[Inject]
	void Construct(EnvironmentConfig environments, MenuNavigator menuNavigator)
    {
		_environments = environments;
		_menuNavigator = menuNavigator;
	}

	void Awake()
	{
		StudentTabView.Init(_environments.GetEnvironment(1) as Classroom);
		environmentButtons = new List<ScenarioEnvironmentButton>();

		for (int i = 0; i < SiniftaUtility.CachedEnvironments.Count; i++)
		{
			/*if (SiniftaUtility.CachedEnvironments[i].School.Name == "Tutorial")
				continue;
			*/

			ScenarioEnvironmentButton envButton = Instantiate(ScenarioEnvironmentPrefab, ScenarioEnvironmentsParent, false);
			envButton.Initialize();
			envButton.Icon.GetComponent<Button>().onClick.AddListener(() =>
			{
				if (selectedEnvironment != envButton)
				{
					//OnEnvironmentSelected(envButton);
					StudentTabView.Init(_environments.GetEnvironment(1) as Classroom);
				}
			});

			environmentButtons.Add(envButton);
		}
		ExitButton.onClick.AddListener(() =>
		{
			_menuNavigator.Proceed(true, MenuType.ScenarioDetails);
		});
		OkButton.onClick.AddListener(() =>
		{ 

		});
	}
	/*
	public void OnTabOpened()
	{
		TabDescriptionText.text = Localizer.Instance.GetString("ScenarioStudentTabPrompt");
		StudentTabView.Init(InterLevelData.ScenarioEnvironment.Classroom);

		for (int i = 0; i < environmentButtons.Count; i++)
		{
			if (InterLevelData.ScenarioEnvironment == environmentButtons[i].Environment)
			{
				OnEnvironmentSelected(environmentButtons[i]);
				break;
			}
		}
	}

	public ScenarioEnvironmentButton GetEnvironmentButtonFor(ScenarioEnvironment environment)
	{
		for (int i = 0; i < environmentButtons.Count; i++)
		{
			if (environmentButtons[i].Environment == environment)
				return environmentButtons[i];
		}

		return null;
	}

	private void OnEnvironmentSelected(ScenarioEnvironmentButton env)
	{
		if (selectedEnvironment != env)
		{
			if (selectedEnvironment != null)
				selectedEnvironment.IsSelected = false;

			env.IsSelected = true;
			selectedEnvironment = env;

			env.Environment.Classroom.InitialState = InterLevelData.ScenarioEnvironment.Classroom.InitialState;
			InterLevelData.ScenarioEnvironment = env.Environment;
		}
	}*/
}
