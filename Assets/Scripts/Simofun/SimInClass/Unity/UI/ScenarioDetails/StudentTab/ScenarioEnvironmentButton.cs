﻿using DG.Tweening;
using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ScenarioEnvironmentButton : MonoBehaviour
{
	public RawImage Icon;
	public Texture2D EnvironmentTexture;

	public Text Name;
	public Text Description;

	public LayoutElement InfoHolder;
	public RectTransform InfoHolderReference;

	private Scenario _scenario;
	private GameState _gameState;

	private bool _isSelected;
	public bool IsSelected
	{
		get { return _isSelected; }
		set
		{
			if (_isSelected == value)
				return;

			_isSelected = value;
			InfoHolder.DOPreferredSize(_isSelected ? InfoHolderReference.sizeDelta : Vector2.zero, 0.5f);
		}
	}

	[Inject]
	void Construct(GameState gameState, Scenario scenario)
    {
		_gameState = gameState;
		_scenario = scenario;
    }

	public void Initialize()
	{
		IsSelected = false;

		Name.text = _gameState.Environment.Name;
		Description.text = Localizer.Instance.GetString("ClassroomInfo", _gameState.NPCs.Count);

		float aspect = (float)EnvironmentTexture.width / EnvironmentTexture.height;
		Icon.texture = EnvironmentTexture;
		Icon.GetComponent<AspectRatioFitter>().aspectRatio = aspect;

		LayoutRebuilder.ForceRebuildLayoutImmediate(InfoHolderReference);
	}
}
