﻿using KarmaFramework.ScenarioCore;
using Sinifta.Environment;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Zenject;

namespace Sinifta.UI
{
    public class ClassLayoutPanel : ScenarioDetailsPanel
    {
        private GameState _gameState;
        private EnvironmentConfig _environments;

        [Inject]
        public void Construct(GameState gameState, EnvironmentConfig environments)
        {
            _gameState = gameState;
            _environments = environments;
        }
        private void OnEnable() => SetPanelInformation();
        
        public override void SetPanelInformation()
        {
            base.SetPanelInformation();
            var firstOrDefault = _environments.GetEnvironment(_gameState.Scenario.EnvironmentId) as Classroom;
            if (firstOrDefault != null)
            {
                var classroomState = firstOrDefault.ClassRoomStates[_gameState.Scenario.EnvironmentState];
                var txts = classroomState.Description.Trim().Split('_');
                //Title.text += txts[0].Trim();
                string formatted = Regex.Replace(txts[1], @"[\r\n]{2,}", "\r\n");
                this.Description.text = "<b>" + txts[0].Trim() + "</b>" + formatted;



                string sprite;
                switch (_gameState.Scenario.EnvironmentState) // wowowowowow haram, never do this kids...
                {
                    case 1:
                        sprite = "ClassroomLayoutGroupWork";
                        break;
                    case 2:
                        sprite = "ClassroomLayoutUStyle";
                        break;
                    case 3:
                        sprite = "ClassroomLayoutOStyle";
                        break;
                    default:
                        sprite = "ClassroomLayoutClassic";
                        break;
                }
                Image.sprite = Resources.Load<Sprite>("WebPlayerResources/" + sprite);
            }
        }
    }
}