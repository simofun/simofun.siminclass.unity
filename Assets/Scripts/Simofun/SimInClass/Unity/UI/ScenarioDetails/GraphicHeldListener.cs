﻿using UnityEngine;
using UnityEngine.EventSystems;

public class GraphicHeldListener : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	private bool m_isBeingHeld = false;
	public bool IsBeingHeld
	{
		get
		{
			return m_isBeingHeld;
		}
		private set
		{
			if (m_isBeingHeld != value)
			{
				m_isBeingHeld = value;

				if (OnHeldStatusChanged != null)
					OnHeldStatusChanged(value);
			}
		}
	}

	public event System.Action<bool> OnHeldStatusChanged;

	void OnDisable()
	{
		IsBeingHeld = false;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		IsBeingHeld = true;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		IsBeingHeld = false;
	}

	public void OnDrag(PointerEventData eventData)
	{
		IsBeingHeld = true;
	}
}