﻿using KarmaFramework.Localization;
using KarmaFramework.UI;
using System.Linq;
using Zenject;

namespace Sinifta.UI
{
    public class CourseFlowPanel : ScenarioDetailsPanel
    {
        private GameState _gameState;
        [Inject]

        public void Construct(GameState gameState)
        {
            _gameState = gameState;
        }
        public override void SetPanelInformation()
        {
            base.SetPanelInformation();
            int i = 0;
            Description.text = "";
            _gameState.Scenario.FlowElements.Select(s => s.Action).ToList().ForEach(triggerTuple =>
            {
                Description.text += ++i + "-" + Localizer.Instance.GetString(triggerTuple.Name).Trim() + System.Environment.NewLine;
            });

        }
        private string FirstCharToUpper(string input)
        {
            if (string.IsNullOrEmpty(input))
                return input;

            return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}