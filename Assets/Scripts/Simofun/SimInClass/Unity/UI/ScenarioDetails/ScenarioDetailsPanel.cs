﻿using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI
{
    public class ScenarioDetailsPanel : MonoBehaviour
    {
        public Text Title;
        public Text Description;
        public Image Image;

        public virtual void SetPanelInformation()
        {
            Debug.Log("I am ScenarioDetails ");
        }
    }
}