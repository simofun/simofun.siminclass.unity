﻿using KarmaFramework.Localization;
using Zenject;

namespace Sinifta.UI
{
    public class StudentDetailsPanel : ScenarioDetailsPanel
    {
        private GameState _gameState;

        [Inject]
        void Construct(GameState gameState)
        {
            _gameState = gameState;
        }

        public override void SetPanelInformation()
        {
            base.SetPanelInformation();
            //Title.text = "<b>ÖGRENCİLER</b>";
            var des = Localizer.Instance.GetString("Student");
            this.Description.text = _gameState.NPCs.Count + " " + des;
        }
    }
}