﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class VerticalList : MonoBehaviour
{
    private int _childCount;

    void Start()
    {
        _childCount = 0;
    }

    void Update()
    {
        if (_childCount != transform.childCount)
        {
            _childCount = transform.childCount;
            Sort();
        }
    }

    public float CalculateHeight()
    {
        return Sort() + 25;
    }

    public float Sort()
    {
        var top = 0f;
        foreach (Transform t in transform)
        {
            var vl = t.GetComponentInChildren<VerticalList>();
            if (vl != null)
            {
                var rekt = t as RectTransform;
                rekt.anchoredPosition = new Vector2(0, top);
                rekt.offsetMin = new Vector2(0, rekt.offsetMin.y);
                rekt.offsetMax = new Vector2(0, rekt.offsetMax.y);
                top -= vl.CalculateHeight();
            }
            else
            {
                var rekt = t as RectTransform;
                rekt.anchoredPosition = new Vector2(0, top);
                rekt.offsetMin = new Vector2(0, rekt.offsetMin.y);
                rekt.offsetMax = new Vector2(0, rekt.offsetMax.y);
                top -= rekt.sizeDelta.y;
            }
        }
        var shrekt = (transform as RectTransform);
        shrekt.sizeDelta = new Vector2(shrekt.sizeDelta.x, -top);

        return -top;
    }
}
