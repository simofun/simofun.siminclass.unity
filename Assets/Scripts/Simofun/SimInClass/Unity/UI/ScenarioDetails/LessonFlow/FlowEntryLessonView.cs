﻿using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Events;
using Simofun.SimInClass.Unity.Events;
using Sinifta.Actions.TeacherActions;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FlowEntryLessonView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	private float _baseHeight = 60;
	private LayoutElement _layoutElement;
	private float _time;

	private string _title;
	private string _description;

	///
	private GameObject _holderGO;
	private GameObject _backgroundGO;
	public GameObject TimerButton;
	public GameObject SliderPanel;
	public Slider Slider;
	public Text Description;

	private Button _closeButton;
	private Text _timeText;
	private Image _timeHighlight;
	private Image _icon;
	private Text _titleText;

	private GraphicHeldListener _sliderHeldListener;

	public FlowElement FlowEntryLesson { get; set; }
	public float Duration { get; set; }
	public float FinishTime { get; set; }
	public float Time
	{
		get { return _time; }
		set
		{
			_time = value;
			(FlowEntryLesson.Trigger as TimeTrigger).Time = value;
		}
	}
	public Button CloseButton
	{
		get { return _closeButton; }
	}

	public delegate void ChangedHandler();
	public event ChangedHandler Changed;

	private bool _isHover = false;
	private bool _isTimerEnabled = false;

	void Awake()
	{
		_sliderHeldListener = Slider.GetComponent<GraphicHeldListener>();
	}

	public void Init(FlowElement flowEntryLesson)
	{
		FlowEntryLesson = flowEntryLesson;
		_layoutElement = GetComponent<LayoutElement>();
		_holderGO = transform.Find("holder").gameObject;
		_backgroundGO = transform.Find("background").gameObject;
		_closeButton = transform.Find("CloseButton").GetComponent<Button>();
		_timeText = _holderGO.transform.Find("Time").GetComponent<Text>();
		_titleText = _holderGO.transform.Find("Title").GetComponent<Text>();
		//Description = _holderGO.transform.FindChild("Description").GetComponent<Text>();
		_timeHighlight = _holderGO.transform.Find("Time/Highlight").GetComponent<Image>();
		_icon = _holderGO.transform.Find("iconHolder/icon").GetComponent<Image>();

		_time = (FlowEntryLesson.Trigger as TimeTrigger).Time;
		_title = Localizer.Instance.GetString(FlowEntryLesson.Action.Name);
		//_description =
		//    FlowEntryLesson.BaseAction.Text.Length > 100
		//        ? FlowEntryLesson.BaseAction.Text.Substring(0,
		//            Math.Min(100, FlowEntryLesson.BaseAction.Text.Length)) + "..."
		//        : FlowEntryLesson.BaseAction.Text;
		_description = (FlowEntryLesson.Action as TeacherLecture).Text; // All text appears
		Duration = FlowEntryLesson.Action.Duration;
		/*if (!string.IsNullOrEmpty((FlowEntryLesson.BaseAction as TeacherLecture).Icon))
			_icon.sprite = KarmaResources.Load<Sprite>(((PrefabType)Enum.Parse(typeof(PrefabType), (FlowEntryLesson.BaseAction as TeacherLecture).Icon)));*/

		transform.localScale = Vector3.one;

		if (TimerButton)
		{
			TimerButton.GetComponent<Button>().onClick.AddListener(() =>
			{
				_isTimerEnabled = !_isTimerEnabled;
				Slider.value = Duration / 6;

				//#if !UNITY_EDITOR && !UNITY_STANDALONE
				//SliderPanel.SetActive( !SliderPanel.activeSelf );
				//#endif
			});
			Slider.value = Duration / 6;

			Slider.onValueChanged.AddListener((s) =>
			{
				FlowEntryLesson.Action.Duration = Slider.value * 6;
				Duration = Slider.value * 6;

				if (Changed != null)
					Changed();
				KarmaMessageBus.Publish(new ChangeLessonTimeGameEvent());
			});

#if !UNITY_EDITOR && !UNITY_STANDALONE
			TimerButton.SetActive( true );
#endif
		}
	}

	public void RefreshDuration()
	{
		Duration = FlowEntryLesson.Action.Duration;
	}

	public void SetViewElement()
	{
		SetTime(_time, Duration);

		SetTitle(_title);
		SetDesc(_description);
	}

	private void SetTime(float time, float duration)
	{
		var timeRange = Localizer.Instance.GetString("TimeRangeMinutes", (int)(time / 6), (int)((time + duration) / 6));
		if(_timeText != null)
		_timeText.text = timeRange;
		FinishTime = (time + duration);

		//Debug.Log("time: " + time);
		(FlowEntryLesson.Trigger as TimeTrigger).Time = time;
		_layoutElement.preferredHeight = Math.Max(_baseHeight, duration / 6 * 12);
	}

	private void SetTitle(string title)
	{
		_titleText.text = title;
	}

	private void SetDesc(string desc)
	{
		Description.text = desc;
	}

	public void ShowTimeFailed()
	{
		_backgroundGO.GetComponent<Image>().color = new Color(Color.red.r, Color.red.g, Color.red.b, 0.3f);
		_timeHighlight.color = new Color(Color.red.r, Color.red.g, Color.red.b, 0.5f);
	}

	public void ShowTimeEnable()
	{
		_backgroundGO.GetComponent<Image>().color = new Color(Color.white.r, Color.white.g, Color.white.b, 1f);
		_timeHighlight.color = new Color(Color.white.r, Color.white.g, Color.white.b, 0f);
	}

	public void Update()
	{
		if (TimerButton != null && SliderPanel != null)
		{
#if UNITY_EDITOR || UNITY_STANDALONE
			EnableTimerButton(_isHover);
#endif

			SliderPanel.SetActive(_sliderHeldListener.IsBeingHeld || (_isTimerEnabled && _isHover));
		}
	}

	private void EnableTimerButton(bool isHover)
	{
		TimerButton.SetActive(isHover);
		KarmaMessageBus.Publish(new ChangeLevelDetailsButtonGameEvent());
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		_isHover = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		_isHover = false;
		_isTimerEnabled = false;
	}
}
