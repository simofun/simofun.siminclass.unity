﻿using KarmaFramework.ActionCore;
using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Events;
using Sinifta.Actions.TeacherActions;
using Sinifta.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class LessonPlanView : MonoBehaviour, IDropHandler
{
	[Serializable]
	public struct DragSlot
	{
		public LessonPlanSlot slot;

		public RectTransform dragZone;
	}

	public GameObject LessonFlowElementPrefab;

	public LessonPlanOverviewView planOverview;

	public Text MainFlowTitle;
	public Text EmptyFlowText;
	public Text TotalLessonTimeText;

	public Color TotalLessonTimeNormalColor;
	public Color TotalLessonTimeOverflowColor;

	public GameObject _scrollViewContent;
	public Text _timeHeader;
	public Text _subjectHeader;

	public FlowEntryTimelineView timelineItem;
	public Transform timelineItemsParent;

	public InputField standardsText;
	public Text gainsText;
	public InputField materialsText;
	public Text lessonPlanActivitiesText;

	public DragSlot[] dragSlots;
	public Text[] dragZoneTimeTexts;

	#region Fields
	readonly CompositeDisposable disposables = new CompositeDisposable();

	GameState _gameState;

	int dragZoneIndex = 0;

	List<FlowEntryTimelineView> flowEntryTimelineItemsList;

	List<FlowEntryLessonView> flowEntryViewList;
	#endregion

	#region Construct
	[Inject]
	public void Construct(GameState gameState)
	{
		_gameState = gameState;
	}
	#endregion

	#region Unity Methods
	protected virtual void Awake()
	{
		MainFlowTitle.text = Localizer.Instance.GetString("SetupViewMainFlowTitle");
		EmptyFlowText.text = Localizer.Instance.GetString("FlowEmptyInfoText");
		KarmaMessageBus.OnEvent<InsertFlowElement>()
			.Subscribe(ev => this.SaveNewFlowElement())
			.AddTo(this.disposables);
	}

	protected virtual void OnDestroy() => this.disposables.Dispose();
	#endregion

	public void Init()
	{
		flowEntryViewList = new List<FlowEntryLessonView>();
		flowEntryTimelineItemsList = new List<FlowEntryTimelineView>();
		foreach (var dragSlot in dragSlots)
		{
			foreach (Transform child in dragSlot.dragZone)
			{
				Destroy(child.gameObject);
			}
		}

		foreach (Transform child in timelineItemsParent)
		{
			Destroy(child.gameObject);
		}

		_timeHeader.text = Localizer.Instance.GetString("Time");
		_subjectHeader.text = Localizer.Instance.GetString("Subject");
		//_contentHeader.text = Localizer.Instance.GetString(StringId.Content);
	}

	public void GetAllScenarioLessons()
	{
		if (_gameState == null)
		{
			return;
		}

		Init();

		List<FlowElement> flowEntryList = new List<FlowElement>();

		//get deafult scenario lecture flow entry
		foreach (var lectureAction in _gameState.Scenario.FlowElements)
		{
			var entry = new FlowElement(lectureAction.Action, lectureAction.Trigger);
			flowEntryList.Add(entry);
		}
		flowEntryList.OrderBy(entry => (entry.Trigger as TimeTrigger).Time);

		//Create default scenario lecture flow entry
		for (var i = 0; i < flowEntryList.Count; i++)
		{
			LessonPlanSlot teacherActionSlot = ((TeacherLecture)flowEntryList[i].Action).LessonPlanSlot;

			DragSlot? containerSlot = null;
			foreach (var dragSlot in dragSlots)
			{
				if (dragSlot.slot == teacherActionSlot)
				{
					containerSlot = dragSlot;
					break;
				}
			}

			if (containerSlot == null)
			{
				Debug.LogError("No slot (var dragSlots) assigned to " + teacherActionSlot);
				continue;
			}

			CreateFlowEntryLessonView(flowEntryList[i], i, containerSlot.Value.dragZone.childCount, containerSlot.Value.dragZone);
		}

		// Empty flow makes emptyText appear
		if (flowEntryList.Count == 0)
			EmptyFlowText.gameObject.SetActive(true);
		else
			EmptyFlowText.gameObject.SetActive(false);

		RefreshList();
	}

	public void ShowLessonPlanOverview()
	{
		BaseAction[][] lectureActivities = new BaseAction[dragSlots.Length][];
		for (int i = 0; i < lectureActivities.Length; i++)
		{
			lectureActivities[i] = new BaseAction[dragSlots[i].dragZone.childCount];

			for (int j = 0; j < lectureActivities[i].Length; j++)
				lectureActivities[i][j] = dragSlots[i].dragZone.GetChild(j).GetComponent<FlowEntryLessonView>().FlowEntryLesson.Action;
		}

		planOverview.ShowOverview(standardsText.text, lessonPlanActivitiesText.text, gainsText.text, materialsText.text, lectureActivities);
	}
	//<Orbay> Burda Sorun  DropZone nun null olmaması lazım?
	public void OnDrop(PointerEventData eventData)
	{
		var draggedGameObject = DragHandler.ItemBeingDragged;
		if (draggedGameObject)
		{
			GameObject hoveredFlowEntry = eventData.hovered.FirstOrDefault(hoverItem => hoverItem.GetComponent<FlowEntryLessonView>() != null);
			FlowEntryLessonView hoveredFlowEntryView = null;
			if (hoveredFlowEntry != null)
			{
				hoveredFlowEntryView = hoveredFlowEntry.GetComponent<FlowEntryLessonView>();
			}

			int indexToInsert = 0;
			int siblingIndex = 0;
			DragSlot? dropZone = null;
			if (hoveredFlowEntryView != null)
			{
				siblingIndex = hoveredFlowEntry.transform.GetSiblingIndex();
				indexToInsert = flowEntryViewList.IndexOf(hoveredFlowEntryView);

				Transform dropZoneTR = hoveredFlowEntry.transform.parent;
				foreach (var dragSlot in dragSlots)
				{
					if (dragSlot.dragZone == dropZoneTR)
					{
						dropZone = dragSlot;
						break;
					}
				}
			}
			else
			{
				for (int i = 0; i < dragSlots.Length; i++)
				{
					Transform dragZone = dragSlots[i].dragZone;
					if (RectTransformUtility.RectangleContainsScreenPoint(dragZone.parent.GetComponent<RectTransform>(), eventData.position))
					{
						dropZone = dragSlots[i];
						dragZoneIndex = i;
						break;
					}
					else
					{
						indexToInsert += dragSlots[i].dragZone.childCount;
					}
				}

				if (dropZone == null)
					return;
			}

			//Drag drop from method panel
			if (draggedGameObject.GetComponent<MethodParameterButtonView>())
			{
				//Create FlowEntry
				var entry = CreateFlowEntryLesson(
					draggedGameObject.GetComponent<MethodParameterButtonView>().FlowEntryLesson.Action, indexToInsert, siblingIndex, dropZone.Value);

				//var TriggerActionTuple = CreateTriggerActionsTuple(entry);
				//Create Lecture Trigger
				((TeacherLecture)entry.Action).LessonPlanSlot = dropZone.Value.slot;
				if(hoveredFlowEntryView == null)
                {
					return;
                }
				hoveredFlowEntryView.FlowEntryLesson = entry;		
				flowEntryViewList.Insert(indexToInsert,hoveredFlowEntryView);
			}
			//Drag and drop same panel(Lesson Plan Panel)
			else if (draggedGameObject.GetComponent<FlowEntryLessonView>())
			{
				var draggedItemView = draggedGameObject.GetComponent<FlowEntryLessonView>();
				((TeacherLecture)draggedItemView.FlowEntryLesson.Action).LessonPlanSlot = dropZone.Value.slot;

				int oldIndex = flowEntryViewList.IndexOf(draggedItemView);
				if (hoveredFlowEntryView != null && draggedGameObject.transform.parent == hoveredFlowEntryView.transform.parent && indexToInsert > oldIndex)
					indexToInsert++;

				flowEntryViewList.Insert(indexToInsert, draggedItemView);

				if (oldIndex >= indexToInsert)
					oldIndex++;

				flowEntryViewList.RemoveAt(oldIndex);

				draggedGameObject.transform.SetParent(dropZone.Value.dragZone);
				draggedGameObject.transform.SetSiblingIndex(siblingIndex);

				_gameState.Scenario.FlowElements.Insert(indexToInsert, draggedItemView.FlowEntryLesson);
				_gameState.Scenario.FlowElements.RemoveAt(oldIndex);

				RefreshList();
			}

			SetSegmentTimeText(dragZoneIndex);
			//PlayButtonSound.PlaySound1Static();

			EmptyFlowText.gameObject.SetActive(false);
		}
	}

	public void SetSegmentTimeText(int dragZoneIndex)
	{
		dragZoneTimeTexts[dragZoneIndex].text = Localizer.Instance.GetString("XMinutesAbbreviated", CalculateSegmentTime(dragSlots[dragZoneIndex].dragZone).ToString());
	}

	public FlowEntryLessonView GetFirstActionOfType<T>() where T : BaseAction
	{
		for (var i = 0; i < flowEntryViewList.Count; i++)
		{
			if (flowEntryViewList[i].FlowEntryLesson.Action.GetType() == typeof(T))
				return flowEntryViewList[i];
		}

		return null;
	}

	private FlowElement CreateFlowEntryLesson(BaseAction action, int indexToInsert, int siblingIndex, DragSlot droppedSlot)
	{
		var trigger = new TimeTrigger();

		// Create a copy of action which came with DragHandler
		var actionCopy = new TeacherLecture(action as TeacherLecture);
		actionCopy.LessonPlanSlot = droppedSlot.slot;

		var entry = new FlowElement(action, trigger);
		CreateFlowEntryLessonView(entry, indexToInsert, siblingIndex, droppedSlot.dragZone);

		return entry;
	}

	private void CreateFlowEntryLessonView(FlowElement entry, int indexToInsert, int siblingIndex, Transform parent)
	{
		var flowLesson = Instantiate(LessonFlowElementPrefab, parent);

		var view = flowLesson.GetComponent<FlowEntryLessonView>();
		view.Changed += RefreshList;

		view.Init(entry);
		view.SetViewElement();
		view.CloseButton.onClick.AddListener(() =>
		{
			if (view.FlowEntryLesson.TriggerActionsTuple != null)
			{
				//_gameState.Scenario.FlowElements.Remove(view.FlowEntryLesson);
			}
			flowEntryViewList.Remove(view);
			var children = _scrollViewContent.GetComponentsInChildren<RectTransform>();

			foreach (var child in children)
			{
				if (child == view.transform)
				{
					view.Changed -= RefreshList;
					Destroy(child.gameObject);
				}
			}

			view.transform.SetParent(null);
			//SetSegmentTimeText(indexToInsert);

			if (flowEntryViewList.Count == 0)
				EmptyFlowText.gameObject.SetActive(true);
			RefreshList();
		});

		flowLesson.transform.SetSiblingIndex(siblingIndex);
		flowEntryViewList.Insert(indexToInsert, view);
		RefreshList();
	}

	private TriggerActionsTuple CreateTriggerActionsTuple(FlowElement entry)
	{
		var _triggerActionTuple = new TriggerActionsTuple();
		_triggerActionTuple.Actions.Add(entry.Action);
		_triggerActionTuple.Triggers.Add(entry.Trigger);
		_triggerActionTuple.Name = entry.Action.Name;
		entry.TriggerActionsTuple = _triggerActionTuple;

		return _triggerActionTuple;
	}

	private void RefreshList()
	{
		for (int i = 0; i < dragSlots.Length; i++)
			SetSegmentTimeText(i);

		if (flowEntryTimelineItemsList.Count < flowEntryViewList.Count)
		{
			for (int i = flowEntryTimelineItemsList.Count; i < flowEntryViewList.Count; i++)
			{
				FlowEntryTimelineView _timelineIt = Instantiate(timelineItem, timelineItemsParent, false);
				flowEntryTimelineItemsList.Add(_timelineIt);
			}
		}
		else if (flowEntryTimelineItemsList.Count > flowEntryViewList.Count)
		{
			for (int i = flowEntryTimelineItemsList.Count - 1; i >= flowEntryViewList.Count; i--)
			{
				Destroy(flowEntryTimelineItemsList[i].gameObject);
				flowEntryTimelineItemsList.RemoveAt(i);
			}
		}

		int totalTime = 0;
		for (var i = 0; i < flowEntryViewList.Count; i++)
		{
			if (i == 0)
				flowEntryViewList[i].Time = 0;
			else
				flowEntryViewList[i].Time = flowEntryViewList[i - 1].Time + flowEntryViewList[i - 1].Duration;

			flowEntryViewList[i].SetViewElement();
			//( (TeacherLecture) flowEntryViewList[i].FlowEntryLesson.BaseAction ).SetColor( TimelineView.ItemColors[i % TimelineView.ItemColors.Count] );
			CheckLessonEnoughTime(flowEntryViewList[i]);

			float anchorMinX = flowEntryViewList[i].Time / 240f;
			float anchorMaxX = anchorMinX + flowEntryViewList[i].Duration / 240f;
			if (anchorMinX >= 1f)
				flowEntryTimelineItemsList[i].gameObject.SetActive(false);
			else
			{
				flowEntryTimelineItemsList[i].gameObject.SetActive(true);
				flowEntryTimelineItemsList[i].Resize(anchorMinX, Mathf.Min(1f, anchorMaxX));
				flowEntryTimelineItemsList[i].SetContent((TeacherLecture)flowEntryViewList[i].FlowEntryLesson.Action);
			}

			totalTime += (int)(flowEntryViewList[i].Duration / 6);
		}

		if (totalTime > 40)
			TotalLessonTimeText.color = TotalLessonTimeOverflowColor;
		else
			TotalLessonTimeText.color = TotalLessonTimeNormalColor;

		TotalLessonTimeText.text = Localizer.Instance.GetString("UsedLessonTimeMinutes", totalTime, 40);

		string activitiesText = string.Empty;
		for (int i = 0; i < flowEntryViewList.Count; i++)
			activitiesText += (i + 1) + ". " + Localizer.Instance.GetString(flowEntryViewList[i].FlowEntryLesson.Action.Name) + "\n";

		lessonPlanActivitiesText.text = activitiesText;
	}

	private void CheckLessonEnoughTime(FlowEntryLessonView lessonView)
	{
		if (((int)lessonView.FinishTime) > 240)
			lessonView.ShowTimeFailed();
		else
			lessonView.ShowTimeEnable();
	}

	private float CalculateSegmentTime(RectTransform dragZone)
	{
		float time = 0f;
		for (int i = 0; i < dragZone.childCount; i++)
			time += dragZone.GetChild(i).GetComponent<FlowEntryLessonView>().Duration / 6;

		return time;
	}

	#region LessonPlan section new

	public void SaveLessonPlan() //Editor Debug Button Callback 
	{
		string path = null;
		var predefinedPath = "Predefined";
		var folderPath = Path.Combine(predefinedPath, _gameState.Scenario.Name);
		if (!Directory.Exists(predefinedPath))
			Directory.CreateDirectory(predefinedPath);
		if (!Directory.Exists(folderPath))
			Directory.CreateDirectory(folderPath);
		int i;
		for (i = 0; i < 50; i++)
		{
			path = Path.Combine(folderPath, i + ".xml");
			if (!File.Exists(path))
				break;
		}
		//KarmaUtils.SaveLessonPlan(InterLevelData.EditorGameState, path);
	}

	#endregion
	private void SaveNewFlowElement()
	{
		_gameState.Scenario.FlowElements.Clear();
		for (int i = 0; i < flowEntryViewList.Count; i++)
		{
			_gameState.Scenario.FlowElements.Add(flowEntryViewList[i].FlowEntryLesson);
		}
	}
}

public class InsertFlowElement : KarmaBaseUnityEvent
{
}
