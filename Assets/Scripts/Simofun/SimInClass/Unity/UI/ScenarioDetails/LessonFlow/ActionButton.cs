﻿using KarmaFramework.ActionCore;
using System;
using UnityEngine;

namespace Sinifta.UI
{
    public class ActionButton
    {
        public BaseAction Action { get; set; }
        public string Name { get; set; }
        public Func<bool> Check { get; set; }
        public Action OnSelected { get; set; }
        public Sprite Icon { get; set; }

        public ActionButton()
        {
        }

        public ActionButton(BaseAction action, string name, Func<bool> check, Action onSelected)
        {
            Action = action;
            Name = name;
            Check = check;
            OnSelected = onSelected;
        }
    }
}
