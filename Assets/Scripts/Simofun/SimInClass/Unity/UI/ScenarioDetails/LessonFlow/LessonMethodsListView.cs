﻿using KarmaFramework.ScenarioCore;
using Sinifta.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Sinifta.UI
{
    public class LessonMethodsListView : MonoBehaviour
    {
        //private bool _isDefaultSelected = false;

        public MethodButtonView MethodButtonViewPrefab;

        private List<FlowElement> _flowEntryList;
        private List<MethodButtonView> _methodButtons = new List<MethodButtonView>();
        private GameObject _lessonMethodContent;
        public GameObject MethodHolder;
        public ImageDetailPanelView ImageDetailPanel;

        private GameState _gameState;

        [Inject]
        public void Construct(GameState gameState)
        {
            _gameState = gameState;
        }

        void Awake()
        {
            _lessonMethodContent = FindContentGO(gameObject);
        }

        private void Start()
        {
            //ImageDetailPanel = FindObjectOfType<ImageDetailPanelView>();
            //.GetComponent<ImageDetailPanelView>();
        }

        public GameObject FindContentGO(GameObject GO)
        {
            foreach (var item in GO.GetComponentsInChildren<RectTransform>())
            {
                if (item.name == "Content")
                {
                    return item.gameObject;
                }
            }
            return null;
        }

        public MethodButtonView GetMethodButton(Type lectureType)
        {
            foreach (var button in _methodButtons)
            {
                if (button.Lecturetype == lectureType)
                    return button;
            }

            return null;
        }

        public void GetAllTeachingMethods()
        {
            if(_lessonMethodContent == null)
            {
                _lessonMethodContent = FindContentGO(gameObject);
            }
            foreach (Transform child in _lessonMethodContent.transform)
            {
                Destroy(child.gameObject);
            }

            _methodButtons.Clear();
            _flowEntryList = new List<FlowElement>();
            foreach (var action in _gameState.Scenario.AvailableActions.GroupBy(a => a.GetType()).Select(g => g.First()).ToList()) //high iq move
            {
                _flowEntryList.Add(new FlowElement(action));
            }

            foreach (var entry in _flowEntryList)
            {
                var flowLesson = Instantiate(MethodButtonViewPrefab, _lessonMethodContent.transform);
                var view = flowLesson.GetComponent<MethodButtonView>();
                _methodButtons.Add(view);

                view.Init(entry,_gameState);

                flowLesson.GetComponent<Button>().onClick.AddListener(() => SelectMethod(view));
            }

            foreach (var mb in _methodButtons)
            {
                mb.Deselect();
            }
        }

        private void SelectMethod(MethodButtonView view)
        {
            if (view.gameObject != null)
            {
                var pointer = new PointerEventData(EventSystem.current);
                ExecuteEvents.Execute(view.gameObject, pointer, ExecuteEvents.pointerEnterHandler);
            }

            foreach (var mb in _methodButtons.Where(x => x != view))
            {
                mb.Deselect();
                mb.MethodImagePressed -= OnMethodImagePressed;
            }
            view.Select();
            view.MethodImagePressed += OnMethodImagePressed;

            MethodHolder.GetComponent<VerticalList>().Sort();
        }

        private void OnMethodImagePressed(MethodParameterButtonView script)
        {
            OpenImageDetail(script);
        }
        public void OpenImageDetail(MethodParameterButtonView script)
        {
            ImageDetailPanel.ShowImageDetail(script);
        }
    }
}
