﻿using KarmaFramework.Localization;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Sinifta.UI
{
    public class MethodListView : MonoBehaviour
    {
        public Text MethodsTitle;
        private LessonMethodsListView _lessonMethodsListPanel;


        private GameState _gameState;

        [Inject]
        public void Construct(GameState gameState)
        {
            _gameState = gameState;
        }

        void Awake()
        {
            _lessonMethodsListPanel = GetComponentInChildren<LessonMethodsListView>();
            MethodsTitle.text = Localizer.Instance.GetString("SetupViewMethodsTitle");
        }

        public void GetAllTeachingMethods()
        {
            if (_gameState == null)
                return;
            if(_lessonMethodsListPanel == null)
            {
                _lessonMethodsListPanel = GetComponentInChildren<LessonMethodsListView>();
            }
            _lessonMethodsListPanel.GetAllTeachingMethods();
        }
    }
}
