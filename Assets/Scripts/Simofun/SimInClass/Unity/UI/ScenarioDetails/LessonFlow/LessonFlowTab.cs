﻿namespace Sinifta.UI
{
	using KarmaFramework.UI;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.UI;
	using Zenject;

	public class LessonFlowTab : MonoBehaviour
	{
		public LessonPlanView lessonPlanPanel;
		public MethodListView methodListPanel;
		public GameObject TextsArea;
		public GameObject HiddenTextsArea;
		public Button HideTextsAreaButton;
		public Button ExitButton;
		public Button OkButton;

		private string _serializedFlow; //for saving initial data

		private MenuNavigator _navigator;
		private GameState _gameState;

		[Inject]
		public void Construct(MenuNavigator navigator, GameState gameState)
		{
			_navigator = navigator;
			_gameState = gameState;
		}

		void Awake()
		{
			HideTextsAreaButton.onClick.AddListener(() => ToggleTextsAreaVisible());
			ExitButton.onClick.AddListener(() =>
			{
				/*InterLevelData.EditorGameState.Scenario.Flow = ScenarioComponent<LectureFlow>.Deserialize<LectureFlow>(_serializedFlow);
				sview.PopUpPanel(3, false);*/
				StartCoroutine(Discard());
				KarmaMessageBus.Publish(new ConfirmExitGameEvent());
			});

			OkButton.onClick.AddListener(() =>
			{
				KarmaMessageBus.Publish(new InsertFlowElement());
				_navigator.Proceed(true);
				KarmaMessageBus.Publish(new ConfirmExitGameEvent());
			   
				//sview.PopUpPanel(3, false);
			});
		}
		private IEnumerator Discard()
		{
			_navigator.SetLoaderState(true);
			yield return null;
			//InterLevelData.EditorGameState.Scenario.Flow = ScenarioComponent<LectureFlow>.Deserialize<LectureFlow>(_serializedFlow);
			_navigator.Proceed(true);
			_navigator.SetLoaderState(false);
		}
		private void Start()
		{
			OnTabOpened();
		}
		void OnEnable()
		{
			OnTabOpened();
		}


		public void OnTabOpened()
		{
			//Uncomment
			//TabDescriptionText.text = Localizer.Instance.GetString( StringId.ScenarioFlowTabPrompt );
			methodListPanel.GetAllTeachingMethods();
			lessonPlanPanel.GetAllScenarioLessons();
			
			//_serializedFlow = InterLevelData.EditorGameState.Scenario.Flow.Serialize();
		}

		public void ToggleTextsAreaVisible()
		{
			TextsArea.SetActive(!TextsArea.activeSelf);
			HiddenTextsArea.SetActive(!HiddenTextsArea.activeSelf);

			HideTextsAreaButton.transform.localScale = new Vector3(1f, -HideTextsAreaButton.transform.localScale.y, 1f);
		}
	}
}
