﻿using KarmaFramework.ActionCore;
using KarmaFramework.API;
using KarmaFramework.Localization;
using Sinifta.Actions.TeacherActions;
using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class LessonPlanOverviewView : MonoBehaviour
{
	[System.Serializable]
	public class Segment
	{
		public LessonPlanSlot slot;
		public Text durationText;
		public Text titleText;
		public Text contentsText;
	}

	public struct SegmentInput
	{
		public readonly float startTime;
		public readonly float endTime;
		public readonly BaseAction lecture;

		public SegmentInput(float startTime, float endTime, BaseAction lecture)
		{
			this.startTime = startTime;
			this.endTime = endTime;
			this.lecture = lecture;
		}
	}

	[SerializeField]
	private Text teacherNameText;
	[SerializeField]
	private Text dateText;
	[SerializeField]
	private Text topicText;
	[SerializeField]
	private Text uniteAdiText;

	[SerializeField]
	private Text standardsText;
	[SerializeField]
	private Text lessonPlanActivitiesText;
	[SerializeField]
	private Text gainsText;
	[SerializeField]
	private Text materialsText;

	[SerializeField]
	private Segment[] segments;

	[SerializeField]
	private Button _okButton;
	[SerializeField]
	private Button _exitButton;
	[SerializeField]
	private Button _closeButton;


	private GameState _gameState;
	[Inject]
	void Construct(GameState gameState)
    {
		_gameState = gameState;
    }

	public void ShowOverview(string standards, string lessonPlanActivities, string gains, string materials, BaseAction[][] lecturesSlotted)
	{
		gameObject.SetActive(true);

		teacherNameText.text = "";// InterLevelData.AppConfig.UserInfo.Name + " " + InterLevelData.AppConfig.UserInfo.Surname;
		dateText.text = DateTime.Now.ToShortDateString();
		
		topicText.text = _gameState.Scenario.ReadableName;
		uniteAdiText.text = _gameState.Scenario.ReadableName;

		standardsText.text = standards;
		lessonPlanActivitiesText.text = lessonPlanActivities;
		gainsText.text = gains;
		materialsText.text = materials;

		float startTime = 0f;
		for (int i = 0; i < lecturesSlotted.Length; i++)
		{
			BaseAction[] lectures = lecturesSlotted[i];

			float duration = 0f;
			string lecturesText = string.Empty;
			for (int j = 0; j < lectures.Length; j++)
			{
				duration += lectures[j].Duration / 6;
				lecturesText += Localizer.Instance.GetString(lectures[j].Name) + " - " + Localizer.Instance.GetString("XMinutesAbbreviated", lectures[j].Duration / 6) + "\n";
			}

			segments[i].contentsText.text = lecturesText;
			segments[i].durationText.text = Localizer.Instance.GetString("TimeRangeMinutes", startTime, startTime + duration);

			startTime += duration;
		}

		_okButton.gameObject.SetActive(false);
		_exitButton.gameObject.SetActive(false);

		_closeButton.onClick.AddListener(()=>{
			HideOverview();
		});
	}

	public void HideOverview()
	{
		_okButton.gameObject.SetActive(true);
		_exitButton.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}
}