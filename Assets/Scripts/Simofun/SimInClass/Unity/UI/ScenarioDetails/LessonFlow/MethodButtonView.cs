﻿using KarmaFramework.ActionCore;
using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using Sinifta.Actions.TeacherActions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI
{
    public class MethodButtonView : MonoBehaviour
    {
        public Transform OptionsHolder;
        public Image Icon;
        public GameObject MethodDetailPrefab;

        public string Title { get; set; }
        public string Desc { get; set; }
        public FlowElement Lesson { get; set; }
        public Type Lecturetype { get; set; }

        public event Action<MethodParameterButtonView> MethodImagePressed;

        private Animator _animator;
        private Text _titleText;

        public bool IsExpanded { get { return _animator.GetBool("Open"); } }

        public void Select()
        {
            if (OptionsHolder && OptionsHolder.childCount > 0)
            {
                if (_animator.GetBool("Open"))
                {
                    Deselect();
                }
                else
                {
                    OptionsHolder.gameObject.SetActive(true);
                    _animator.SetBool("Open", true);
                }
            }
        }

        public void Deselect()
        {
            if (OptionsHolder)
            {
                OptionsHolder.gameObject.SetActive(false);
                _animator.SetBool("Open", false);
            }
        }

        public void Init(FlowElement entryLesson, GameState gameState)
        {
            Lesson = entryLesson;
            Lecturetype = entryLesson.Action.GetType();
            Title = Localizer.Instance.GetString(entryLesson.Action.Name);
            Desc = (entryLesson.Action as TeacherLecture).Text;

            /*var prefabType = Util.GetEnum<PrefabType>(((TeacherLecture)entryLesson.BaseAction).TimelineIcon);
            Icon.sprite = KarmaResources.Load<Sprite>(prefabType);*/
            Icon.sprite = Resources.Load<Sprite>("Icons/Lecture/" + entryLesson.Action.Icon);

            _animator = GetComponent<Animator>();
            _titleText = transform.Find("header/Title").GetComponent<Text>();
            _titleText.text = Title;

            var actions = new List<BaseAction>();
            foreach (var action in gameState.Scenario.AvailableActions)
            {
                if (action.GetType() == entryLesson.Action.GetType())
                    actions.Add(action);
            }

            ListLessonMethodContentParameters(actions, Lesson.Action.GetType());
            transform.localScale = Vector3.one;

            Deselect();
        }

        public void Init(Type lectureType, Type resource)
        {
            /*var strId = Util.GetEnum<StringId>(lectureType.ToString().Split('.')[2]);
            Title = Localizer.Instance.GetString(strId);

            var prefabType = Util.GetEnum<PrefabType>(((TeacherLecture)Activator.CreateInstance(lectureType)).TimelineIcon);
            Icon.sprite = KarmaResources.Load<Sprite>(prefabType);

            _animator = GetComponent<Animator>();
            _titleText = transform.Find("header/Title").GetComponent<Text>();
            Lecturetype = lectureType;
            _titleText.text = Title;

            var names = new List<string>();
            var docResources = InterLevelData.EditorGameState.Scenario.ResourcesHolder.Resources.Values.Where(x => x.GetType() == resource).ToList();

            foreach (var dRes in docResources)
            {
                var imgNames = dRes.GetResourceNames();
                names.AddRange(imgNames);
            }

            ListLessonMethodImagesParameters(names, lectureType);
            transform.localScale = Vector3.one;*/
        }

        public void ListLessonMethodImagesParameters(List<string> imageNameList, Type lectureType)
        {
            /*foreach (var imageName in imageNameList)
            {
                var methodParameter = Instantiate(KarmaResources.Load<GameObject>(PrefabType.MethodDetailButton), OptionsHolder.transform);
                methodParameter.AddComponent<DragHandler>();
                var view = methodParameter.GetComponent<MethodParameterButtonView>();

                var action = InterLevelData.EditorGameState.Scenario.RewardingSettings.Actions.First(x => x.GetType() == lectureType).Clone();
                //(action as TeacherLecture).ImageName = imageName;

                view.Init(new FlowElement(action));
                view.SetContentLesson(imageName.SplitCamelCase());
                view.SetImageParameter(lectureType, imageName);

                var texture = InterLevelData.UnityGameState.Load<Texture2D>(imageName);
                if (texture != null)
                {
                    var rect = new Rect(0, 0, texture.width, texture.height);
                    var pivot = new Vector2(0.5f, 0.5f);
                    view.BackgroundImage.sprite = Sprite.Create(texture, rect, pivot);
                    view.ImageButtonPressed += ParameterButtonViewImageButtonPressed;
                }
            }*/
        }
        public void ListLessonMethodContentParameters(List<BaseAction> actionsForLessonType, Type lessonType)
        {
            foreach (Transform child in OptionsHolder.transform)
            {
                Destroy(child.gameObject);
            }

            foreach (var action in actionsForLessonType)
            {
                var methodParameter = Instantiate(MethodDetailPrefab, OptionsHolder.transform);
                //methodParameter.AddComponent<DragHandler>();
                var view = methodParameter.GetComponent<MethodParameterButtonView>();

                view.Init(new FlowElement(action));
                view.SetContentLesson((action as TeacherLecture).Text);

                methodParameter.GetComponent<Button>().onClick.AddListener(() => view.SetContentLesson(view.ContentText));
            }
        }

        private void ParameterButtonViewImageButtonPressed(MethodParameterButtonView script)
        {
            if (MethodImagePressed != null)
                MethodImagePressed(script);
        }

        private void Start()
        {
            Select();
        }
    }
}
