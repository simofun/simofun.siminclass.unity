﻿using KarmaFramework.Localization;
using Sinifta.Actions.TeacherActions;
using UnityEngine;
using UnityEngine.UI;

public class FlowEntryTimelineView : MonoBehaviour
{
	private const int MIN_ALLOWED_FONT_SIZE = 8;

	private RectTransform tr;
	private Color textDefaultColor;

	public Text TeacherActionName;
	public Image TeacherActionIcon;
	public Image ActionColorBackground;

	private int checkTextFontSize = -1;

	void Awake()
	{
		tr = (RectTransform)transform;
		textDefaultColor = TeacherActionName.color;
	}

	public void Resize(float anchorXMin, float anchorXMax)
	{
		tr.anchorMin = new Vector2(anchorXMin, tr.anchorMin.y);
		tr.anchorMax = new Vector2(anchorXMax, tr.anchorMax.y);

		checkTextFontSize = 2;
	}

	public void SetContent(TeacherLecture lectureAction)
	{
		TeacherActionName.text = Localizer.Instance.GetString(lectureAction.Name);
		ActionColorBackground.color = lectureAction.BackgroundColor;
		TeacherActionIcon.sprite = Resources.Load<Sprite>("Icons/Lecture/" + lectureAction.Icon);

		checkTextFontSize = 2;
	}

	void Update()
	{
		if (checkTextFontSize > 0)
		{
			checkTextFontSize--;

			if (checkTextFontSize == 0)
			{
				if (TeacherActionName.cachedTextGenerator.fontSizeUsedForBestFit >= MIN_ALLOWED_FONT_SIZE)
					TeacherActionName.color = textDefaultColor;
				else
					TeacherActionName.color = Color.clear;
			}
		}
	}
}