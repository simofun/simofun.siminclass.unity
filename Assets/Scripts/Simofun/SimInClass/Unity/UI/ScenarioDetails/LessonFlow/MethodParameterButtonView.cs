﻿namespace Sinifta.UI
{
	using KarmaFramework.ScenarioCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using Sinifta.Actions.TeacherActions;
	using System;
	using UnityEngine;
	using UnityEngine.UI;

	public enum MethodDetailType
	{
		None,
		MethodDetail, // Buttons in flow edit scene
		ActivityDetail, // Buttons in activity view
	}

	public class MethodParameterButtonView : MonoBehaviour
	{
		public MethodDetailType DetailType;

		public GameObject imageRequiredButton;

		private Text _contentText;
		private Button _seeMoreButton;
		private LayoutElement _layoutElement;
		private FlowElement _flowEntryLesson;

		private float _textPadding = 10;
		private float _defaultHeight;
		private float _currentHeight;
		//private bool _isInitialized = false;
		private bool _isDetailsOpen = false;
		private bool _isSeeMoreButtonDisabled = false;
		private Button _imageButton;
		private Button _button;

		public Image BackgroundImage { get; set; }

		public bool IsDetailsOpen
		{
			get { return _isDetailsOpen; }
			set
			{
				if (_isDetailsOpen && !value)
				{
					//Close Details
					_currentHeight = _defaultHeight;
				}
				else if (!_isDetailsOpen && value)
				{
					//Open Details
					_currentHeight = _contentText.preferredHeight + _textPadding;

				}
				_isDetailsOpen = value;

				_layoutElement.preferredHeight = _currentHeight;
			}
		}

		public string ContentText
		{
			get { return _contentText.text; }
			set { _contentText.text = value; }
		}

		public bool IsSeeMoreButtonDisabled
		{
			get { return _isSeeMoreButtonDisabled; }
			set
			{
				_isSeeMoreButtonDisabled = value;
				if (DetailType == MethodDetailType.MethodDetail)
				{
					_seeMoreButton.gameObject.SetActive(!_isSeeMoreButtonDisabled);
				}
			}
		}

		public FlowElement FlowEntryLesson
		{
			get { return _flowEntryLesson; }
			set { _flowEntryLesson = value; }
		}

		public event Action<MethodParameterButtonView> ImageButtonPressed;

		public void OnDisable()
		{
			if (_isDetailsOpen)
			{
				IsDetailsOpen = false;
			}
		}

		public void SetImageParameter(Type type, string imageName)
		{
			BackgroundImage.gameObject.SetActive(true);
			IsSeeMoreButtonDisabled = true;
			if (type == typeof(TeacherShowImageOnBoard))
			{
				SetShowImageOnBoard(imageName);
			}
			else if (type == typeof(TeacherSendImage))
			{
				SetSendImage(imageName);
			}
		}

		private void SetSendImage(string name)
		{
			//(_flowEntryLesson.Action as TeacherSendImage). = name;
		}

		private void SetShowImageOnBoard(string name)
		{
			//(_flowEntryLesson.Action as TeacherShowImageOnBoard).ImageName = name;
		}

		public void SetContentLesson(string content)
		{
			(_flowEntryLesson.Action as TeacherLecture).Text = content;
			if (!string.IsNullOrEmpty(content))
			{
				//int lenght = content.Length > 100 ? 100 : content.Length;
				//ContenText = content.Replace("\t", "").Replace("\r\n", "").Substring(0, lenght) + "...";
				ContentText = content.Replace("\t", "").Replace("\r\n", "");

				//ContenText = text.Length > 100
				//    ? text.Substring(0,
				//        Math.Min(100, _flowEntryLesson.BaseAction.Text.Length)) + "..."
				//    : text;
			}
		}

		public void Init(FlowElement flowEntryLesson)
		{
			BackgroundImage = transform.Find("Image").GetComponent<Image>();
			BackgroundImage.gameObject.SetActive(false);

			_contentText = GetComponentInChildren<Text>();
			_layoutElement = GetComponent<LayoutElement>();
			_seeMoreButton = _contentText.GetComponentInChildren<Button>();
			_imageButton = BackgroundImage.GetComponent<Button>();
			_flowEntryLesson = flowEntryLesson;
			_button = transform.GetComponent<Button>();

			if (DetailType == MethodDetailType.MethodDetail)
			{
				_defaultHeight = _layoutElement.preferredHeight;
				_button.onClick.AddListener(OnSeeMoreButtonPressed);
				_seeMoreButton.onClick.AddListener(OnSeeMoreButtonPressed);
				_imageButton.onClick.AddListener(OnImageButtonPressed);
				_imageButton.enabled = (DetailType == MethodDetailType.MethodDetail);
			}

			_isDetailsOpen = false;
			//_isInitialized = true;

			var requiredImages = (_flowEntryLesson.Action as TeacherLecture).ImageRequirements;
			if (requiredImages != null && requiredImages.Count > 0)
				imageRequiredButton.SetActive(true);
		}

		public void InitializeButton(Transform parenTransform, ActionButton actionButton)
		{
			_button.transform.SetParent(parenTransform);
			_button.transform.localScale = Vector3.one;

			_button.onClick.AddListener(() => ButtonPressed(actionButton));
		}

		private void ButtonPressed(ActionButton actionButton)
		{
			//if(BackgroundImage.sprite != null)
			//    (actionButton.Action as TeacherLecture).ImageName = BackgroundImage.sprite.name;
			actionButton.OnSelected();
		}

		private void OnImageButtonPressed()
		{
			if (ImageButtonPressed != null) ImageButtonPressed(this);
		}
		private void OnSeeMoreButtonPressed()
		{
			IsDetailsOpen = !IsDetailsOpen;
			KarmaMessageBus.Publish(new MethodButtonGameEvent());
		}
	}
}
