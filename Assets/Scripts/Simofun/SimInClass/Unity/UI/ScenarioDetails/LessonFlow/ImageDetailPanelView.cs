﻿using Sinifta.UI;
using UnityEngine;
using UnityEngine.UI;

public class ImageDetailPanelView : MonoBehaviour
{
    public Image ImageData;
    public Text Title;
    public GameObject Container;

    private void Awake()
    {
        Container.SetActive(false);
    }

    public void ShowImageDetail(MethodParameterButtonView script)
    {
        if (script.BackgroundImage.sprite == null) return;
        ImageData.sprite = script.BackgroundImage.sprite;
        Title.text = script.BackgroundImage.sprite.texture.name;
        Container.SetActive(true);
    }
    public void HideImageDetail()
    {
        Container.SetActive(false);
    }
}
