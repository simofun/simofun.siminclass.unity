﻿#if UNITY_ANDROID || UNITY_IOS
#define MOBILE
#endif

namespace Sinifta.UI
{
	using KarmaFramework.UI;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Simofun.Networking.Core;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;
	using Zenject;

	public class ScenarioDetailsTab : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		List<ScenarioDetailsPanel> Panels;

		[SerializeField]
		Button EditButton;

		[SerializeField]
		Button StudentDetails;

		[SerializeField]
		Button StartButton;

		[SerializeField]
		GameObject LoadingLogo;
		#endregion

		#region Fields
		GameState gameState;

		MenuNavigator menuNavigator;
		#endregion

		#region Construct
		[Inject]
		public void Construct(MenuNavigator menuNavigator, GameState gameState)
		{
			this.menuNavigator = menuNavigator;
			this.gameState = gameState;
		}
		#endregion

		#region Unity Methods
		protected virtual void OnEnable() => this.RefreshPanels();

		protected virtual void Start()
		{
			KarmaCursor.Mode = KarmaCursorMode.Normal;

			this.StartButton = this.transform.Find("anim/ScenarioDetails/StartGame").GetComponent<Button>();
			this.StartButton.onClick.AddListener(this.StartGame);
			this.EditButton.onClick.AddListener(() =>
			{
#if MOBILE
				KarmaMessageBus.Publish(new ChangeLevelDetailsButtonGameEvent());
#else
				KarmaMessageBus.Publish(new LevelDetailsButtonGameEvent());
#endif
				this.menuNavigator.Proceed(false, MenuType.ScenarioFlow);
			});
			this.StudentDetails.onClick.AddListener(() => this.menuNavigator.Proceed(false, MenuType.StudentTab));
		}
		#endregion

		#region Public Methods
		public void RefreshPanels() =>
			this.Panels.ForEach(p => p.GetComponent<ScenarioDetailsPanel>().SetPanelInformation());
		#endregion

		#region Methods
		IEnumerator WaitForDisconnect()
		{
			SimNetwork.Instance.Disconnect();

			yield return new WaitForSeconds(0.1f);

			KarmaMessageBus.Publish(new StartGameEvent { GameState = this.gameState });
		}

		/// <summary>
		/// TODO: It's like duplication of 'ScenarioDetails::StartGame'
		/// </summary>
		void StartGame()
		{
			this.LoadingLogo.SetActive(true);

			this.StartButton.interactable = false;

			if (!SimNetwork.Instance.IsConnected)
			{
				// TODO seda: öğrenci pozisyonları bazen null geldiği için böyle yapıldı
				this.StartCoroutine(this.WaitForDisconnect());

				return;
			}

			if (SimNetwork.Instance.IsMaster)
			{
				KarmaMessageBus.Publish(new StartScenarioEvent());
			}
		}
		#endregion
	}
}
