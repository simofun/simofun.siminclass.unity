﻿namespace Sinifta.UI
{
	using Simofun.Unity.DesignPatterns.Singleton;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	public enum PrefabType
	{
		ButtonSound1,

		ButtonSound2,

		MenuMusic0,

		MenuMusic1,

		MenuMusic2
	}

	public class SiniftaResources : SimGlobalSingletonBase<SiniftaResources>
	{
		#region Fields
		readonly Dictionary<PrefabType, string> PrefabPaths = new();
		#endregion

		#region Unity Methods
		protected override void Awake()
		{
			base.Awake();

			this.PrefabPaths.Add(PrefabType.ButtonSound1, "Audio/butonses1");
			this.PrefabPaths.Add(PrefabType.ButtonSound2, "Audio/butonses2");
			this.PrefabPaths.Add(PrefabType.MenuMusic0, "Audio/bensound-buddy");
			this.PrefabPaths.Add(PrefabType.MenuMusic1, "Audio/bensound-cute");
			this.PrefabPaths.Add(PrefabType.MenuMusic2, "Audio/bensound-ukulele");
		}
		#endregion

		#region Public Static Methods
		public static string GetPath(PrefabType type) //prefabın dizindeki konumunu getirme
		{
			return Instance.PrefabPaths[type];
		}

		public static T Load<T>(PrefabType type) where T : Object
		{
			var prefabPath = Instance.PrefabPaths[type];
			var prefab = Resources.Load<T>(prefabPath);
			if (prefab == null)
			{
				Log(type, prefabPath);
			}

			return prefab;
		}

		public static T Load<T>(PrefabType type, string arg0) where T : Object
		{
			var prefabPath = string.Format(Instance.PrefabPaths[type], arg0);
			var prefab = Resources.Load<T>(prefabPath);
			if (prefab == null)
			{
				Log(type, prefabPath);
			}

			return prefab;
		}

		public static T Load<T>(PrefabType type, string arg0, string arg1) where T : Object
		{
			var prefabPath = string.Format(Instance.PrefabPaths[type], arg0, arg1);
			var prefab = Resources.Load<T>(prefabPath);
			if (prefab == null)
			{
				Log(type, prefabPath);
			}

			return prefab;
		}

		public static List<T> LoadAll<T>(string path) where T : Object => Resources.LoadAll<T>(path).ToList();

		public static T[] LoadAll<T>(PrefabType type) where T : Object =>
			Resources.LoadAll<T>(Instance.PrefabPaths[type]);
		#endregion

		#region Methods
		static void Log(PrefabType type, string prefabPath) =>
			Debug.LogError("Prefab of type : [" + type + "] not found under path : [" + prefabPath + "]");
		#endregion
	}
}
