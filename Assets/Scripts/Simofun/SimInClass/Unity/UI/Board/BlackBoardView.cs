﻿using Simofun.Karma.UniRx.Unity;
using Sinifta.Interactables;
using UniRx;
using UnityEngine;

namespace Sinifta.UI.BoardUI
{
	public class BlackBoardView : BoardView
	{
		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();
		#endregion

		#region Unity Methods
		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion

		#region Public Methods
		public override void Bind(Board board)
		{
			base.Bind(board);

			var appView = Resources.Load<GameObject>("Prefabs/UIPrefabs/AppPrefabs/BlackBoardAppPrefab");
			Instantiate(appView, _appHolder);

			KarmaMessageBus.OnEvent<OpenBoardEvent>().Subscribe(ev =>
			{
				if (ev.BoardType == Board.BoardType.BlackBoard)
				{
					if (ev.State)
					{
						Show();
						return;
					}
					gameObject.SetActive(false);
				}
				
			}).AddTo(this.disposables);
		}
		#endregion
	}
}
