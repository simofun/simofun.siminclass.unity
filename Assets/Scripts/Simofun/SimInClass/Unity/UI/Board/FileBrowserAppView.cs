﻿using KarmaFramework.Interactables;
using KarmaFramework.Interactables.Apps;
using KarmaFramework.Localization;
using Sinifta.Interactables.BoardApps;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI.BoardUI
{
    public class FileBrowserAppView : BoardAppView
    {
        private Transform _folderHolder;
        private FileBrowserApp _viewModel;
        private int _currentView = -1;
        private int _nextView = -1;
        private Button _backButton, _forwardButton;

        private const string BackButtonPath = "Window/Forward_Back/Back";
        private const string ForwardButtonPath = "Window/Forward_Back/Forward";
        private const string FolderHolderPath = "Window/WindowMask/Scroll View/Viewport/FolderHolder";

        public Sprite backButtonActive, backButtonInactive;
        public Sprite forwardButtonActive, forwardButtonInactive;

        void Awake()
        {
            _folderHolder = transform.Find(FolderHolderPath);
            _backButton = transform.Find(BackButtonPath).GetComponent<Button>();
            _forwardButton = transform.Find(ForwardButtonPath).GetComponent<Button>();
        }

        public override void Draw(GadgetApplication peek)
        {
            base.Draw(peek);
            _viewModel = peek as FileBrowserApp;
            Refresh();
        }

        private void GoBack(object sender, EventArgs e)
        {
            if (_currentView != -1)
            {
                _nextView = _currentView;
                _currentView = -1;
                Refresh();
            }
            else
            {
                _viewModel.ForceQuit();
            }
        }

        public void BackButton()
        {
            GoBack(null, null);
        }

        public void ForwardButton()
        {
            if (_nextView != -1)
            {
                _currentView = _nextView;
                _nextView = -1;
                Refresh();
            }
        }

        private void Refresh()
        {
            foreach (Transform t in _folderHolder)
            {
                Destroy(t.gameObject);
            }
            if (_nextView == -1)
            {
                _forwardButton.image.sprite = forwardButtonInactive;
                _forwardButton.interactable = false;
            }
            else
            {
                _forwardButton.image.sprite = forwardButtonActive;
                _forwardButton.interactable = true;
            }

            if (_currentView == -1)
            {
                GadgetButton[] folderButtons = _viewModel.GetFolderButtons().ToArray();
                for (int i = 0; i < folderButtons.Length; i++)
                {
                    var i1 = i;
                    var button = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Board/FolderIcon"));
                    if (folderButtons[i].Name == "Images")
                        button.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("Images");
                    else
                        button.GetComponentInChildren<Text>().text = folderButtons[i].Name;
                    button.transform.SetParent(_folderHolder, false);
                    button.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        _currentView = i1;
                        _nextView = -1;
                        Refresh();
                    });
                }
                _backButton.image.sprite = backButtonInactive;
                _backButton.interactable = false;
            }
            else
            {
                var buttons = _viewModel.GetFolderContent(_currentView);
                foreach (var b in buttons)
                {
                    var button = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Board/ImageIcon"));
                    BoardIconView script = button.GetComponent<BoardIconView>();
                    script.Initialize(b.Icon, b.Name, _folderHolder, false, b.Action);
                    button.GetComponent<Button>().onClick.AddListener(() => script.DoubleClickControl());
                }
                _backButton.image.sprite = backButtonActive;
                _backButton.interactable = true;
            }
        }
    }
}