﻿using KarmaFramework.Interactables.Apps;
using KarmaFramework.Localization;
using Sinifta.Interactables.BoardApps;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Simofun.Karma.UniRx.Unity;

namespace Sinifta.UI.BoardUI
{
	public class BoardOperatingSystemView : BoardAppView
	{
		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		SmartBoardOperatingSystemApp viewModel;

		Transform appHolder;
		#endregion

		#region Unity Methods
		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion

		#region Public Methods
		public override void Draw(GadgetApplication peek)
		{
			base.Draw(peek);

			appHolder = transform.Find("BoardOsAppHolder");
			viewModel = peek as SmartBoardOperatingSystemApp;

			var dwrapper = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/BoardAppWrapper"), transform).GetComponent<BoardAppWrapperView>();
			var drawingView = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/AppPrefabs/DrawingAppPrefab"), dwrapper.AppHolder).GetComponent<DrawingAppView>();
			var drawing = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Board/BoardIcon"), appHolder);
			drawing.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("DrawingApp");
			drawing.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Icons/AppIcons/DrawingApp");
			drawing.GetComponent<Button>().onClick.AddListener(() =>
			{
				dwrapper.Draw(viewModel.DrawingApp);
				drawingView.Draw(viewModel.DrawingApp);
			});

			var bwrapper = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/BoardAppWrapper"), transform).GetComponent<BoardAppWrapperView>();
			var browserView = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/AppPrefabs/FileBrowserAppPrefab"), bwrapper.AppHolder).GetComponent<FileBrowserAppView>();
			var browser = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Board/BoardIcon"), appHolder);
			browser.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("FileBrowser");
			browser.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Icons/AppIcons/FileBrowser");
			browser.GetComponent<Button>().onClick.AddListener(() =>
			{
				bwrapper.Draw(viewModel.FileBrowserApp);
				browserView.Draw(viewModel.FileBrowserApp);
			});

			KarmaMessageBus.OnEvent<ShowImageOnBoardEvent>().Subscribe(ev =>
			{
				var app = viewModel.OpenImage(ev.ImageName);
				var iwrapper = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/BoardAppWrapper"), transform)
					.GetComponent<BoardAppWrapperView>();

				var _offset = new Vector2((Random.value - 0.5f) * 80f, (Random.value - 0.5f) * 80f);

				var iwrapperRect = iwrapper.GetComponent<RectTransform>();
				iwrapperRect.offsetMin = new Vector2(140, 100) + _offset;
				iwrapperRect.offsetMax = new Vector2(-140, -100) + _offset;

				var iview = Instantiate(
						Resources.Load<GameObject>(
							"Prefabs/UIPrefabs/AppPrefabs/ImageViewerAppPrefab"),
							iwrapper.AppHolder)
					.GetComponent<ImageViewerAppView>();
				iwrapper.Draw(app);
				iview.Draw(app);
			}).AddTo(this.disposables);
		}
		#endregion
	}
}
