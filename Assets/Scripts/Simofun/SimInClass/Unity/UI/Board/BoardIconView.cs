﻿using Simofun.Karma.UniRx.Unity;
using Simofun.SimInClass.Unity.UI;
using Sinifta.Actions.TeacherActions;
using System;
using UnityEngine;
using UnityEngine.UI;
using static Simofun.SimInClass.Unity.UI.SiniftaNotification;

public class BoardIconView : MonoBehaviour
{
	public Text Label;
	public Image IconImage;
	public Action IconAction;
	
	public readonly Vector3 ScaleVector = new Vector3(0.15f, 0.15f, 1f);

	public void Initialize(Sprite sprite,string text, Transform parent, bool worldPositionStays,Action action)
	{
		Label.text = text;
		IconAction = action;
		IconImage.sprite = sprite;
		transform.SetParent(parent, worldPositionStays);
	}

	public void DoubleClickControl()
	{
		KarmaMessageBus.Publish(new ImageRequirementTryEvent() { ImageName = Label.text });
		IconAction.Invoke();
		SiniftaNotification.Instance.Show(Label.text, NotificationType.SmartBoard.ToString());
	}
}
