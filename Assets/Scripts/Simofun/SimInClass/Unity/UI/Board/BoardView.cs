﻿using Sinifta.Interactables;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI.BoardUI
{
    public class BoardView : MonoBehaviour
    {
        [SerializeField]
        protected Button _close;
        [SerializeField]
        protected RectTransform _appHolder;

        public virtual void Bind(Board board)
        {
            gameObject.SetActive(false);
            board.AppHolder = _appHolder;

            _close.onClick.AddListener(() =>
            {
                board.Close();
            });
        }

        public void Show()
        {
            gameObject.SetActive(true);
            transform.SetAsLastSibling();
        }
    }
}