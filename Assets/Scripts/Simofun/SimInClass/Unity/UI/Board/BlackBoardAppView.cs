﻿using System;
using System.Collections.Generic;
using KarmaFramework.Interactables.Apps;
using Sinifta.Interactables.BoardApps;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI.BoardUI
{
    public class BlackBoardAppView : BoardAppView
    {
        private DrawingApp _viewModel;
        private Transform _lineOptions;
        private Transform _brushOptions;
        private Transform _eraserOptions;

        private Image[] brushToolsImg;

        //private Transform _ownerWindow;

        public Texture2D SourceBaseTex;
        private Vector3 _drawingTopLeft;
        private RectTransform _drawingSurface;
        private Texture2D _baseTex;
        private Vector3 _scale;

        private Vector2 _dragStart;
        private Vector2 _dragEnd;
        private enum ToolType
        {
            None,
            Line,
            Brush,
            Eraser,
            Vector
        }

        private const Drawing.Samples AntiAlias = Drawing.Samples.Samples4;
        private Vector2 preDrag;
        private ToolType Tool { get; set; }

        private Texture2D _colorCircle;
        private Color _color = Color.red;
        private readonly Color _color2 = Color.red;
        private readonly LineTool _lineTool = new LineTool();
        private readonly BrushTool _brush = new BrushTool();
        private readonly EraserTool _eraser = new EraserTool();
        private readonly Stroke _stroke = new Stroke();
        private const int _zoom = 1;
        List<Drawing.BezierPoint> _bezierPoints;

        public override void Draw(GadgetApplication peek)
        {
            _viewModel = peek as DrawingApp;

            base.Draw(peek);
        }

        private void GoBack(object sender, EventArgs e)
        {
            print("board close");
            _viewModel.ForceQuit();
        }

        void Awake()
        {
            _drawingSurface = transform.Find("BBDrawingAppSurface").GetComponent<RectTransform>();
            _drawingTopLeft = _drawingSurface.position;

            brushToolsImg = new Image[]
            {
            transform.Find( "BBDrawingAppLeftMenu/DrawingAppToolMenu/DrawingAppBlueBrushTool" ).GetComponent<Image>(),
            transform.Find( "BBDrawingAppLeftMenu/DrawingAppToolMenu/DrawingAppRedBrushTool" ).GetComponent<Image>(),
            transform.Find( "BBDrawingAppLeftMenu/DrawingAppToolMenu/DrawingAppBlackBrushTool" ).GetComponent<Image>(),
            transform.Find( "BBDrawingAppLeftMenu/DrawingAppToolMenu/DrawingAppGreenBrushTool" ).GetComponent<Image>()
            };

            brushToolsImg[0].GetComponent<Button>().onClick.AddListener(() =>
            {
                Tool = ToolType.Brush;
                _color = Color.blue;

                for (int i = 0; i < brushToolsImg.Length; i++)
                {
                    Color c = brushToolsImg[i].color;

                    if (i == 0)
                        c.a = 1f;
                    else
                        c.a = 0f;

                    brushToolsImg[i].color = c;
                }
            });

            brushToolsImg[1].GetComponent<Button>().onClick.AddListener(() =>
            {
                Tool = ToolType.Brush;
                _color = Color.red;

                for (int i = 0; i < brushToolsImg.Length; i++)
                {
                    Color c = brushToolsImg[i].color;

                    if (i == 1)
                        c.a = 1f;
                    else
                        c.a = 0f;

                    brushToolsImg[i].color = c;
                }
            });

            brushToolsImg[2].GetComponent<Button>().onClick.AddListener(() =>
            {
                Tool = ToolType.Brush;
                _color = Color.black;

                for (int i = 0; i < brushToolsImg.Length; i++)
                {
                    Color c = brushToolsImg[i].color;

                    if (i == 2)
                        c.a = 1f;
                    else
                        c.a = 0f;

                    brushToolsImg[i].color = c;
                }
            });

            brushToolsImg[3].GetComponent<Button>().onClick.AddListener(() =>
            {
                Tool = ToolType.Brush;
                _color = Color.green;

                for (int i = 0; i < brushToolsImg.Length; i++)
                {
                    Color c = brushToolsImg[i].color;

                    if (i == 3)
                        c.a = 1f;
                    else
                        c.a = 0f;

                    brushToolsImg[i].color = c;
                }
            });

            transform.Find("BBDrawingAppLeftMenu/DrawingAppToolMenu/DrawingAppEraserTool").GetComponent<Button>().onClick.AddListener(() =>
            {
                Tool = ToolType.Eraser;

                for (int i = 0; i < brushToolsImg.Length; i++)
                {
                    Color c = brushToolsImg[i].color;
                    c.a = 0f;
                    brushToolsImg[i].color = c;
                }
            });

            _eraser.width = 20;
        }

        void Start()
        {
            _baseTex = new Texture2D((int)_drawingSurface.rect.width, (int)_drawingSurface.rect.height);
            for (int i = 0; i < _baseTex.width; i++)
            {
                for (int j = 0; j < _baseTex.height; j++)
                {
                    _baseTex.SetPixel(i, j, Color.white);
                }
            }
            _baseTex.Apply();
            _drawingSurface.GetComponent<RawImage>().texture = _baseTex;
            _bezierPoints = new List<Drawing.BezierPoint>();
            Tool = ToolType.Brush;
            _brush.hardness = 25;
            _brush.width = 5;
        }

        void Update()
        {
            _scale = transform.root.GetComponent<RectTransform>().localScale;
            _drawingTopLeft = _drawingSurface.position;
            Rect imgRect = new Rect(_drawingTopLeft.x, Screen.height - _drawingTopLeft.y, _baseTex.width * _zoom, _baseTex.height * _zoom);
            imgRect.x = imgRect.x * (1 / _scale.x);
            imgRect.y = imgRect.y * (1 / _scale.y);
            Vector2 mouse = Input.mousePosition;
            mouse.y = Screen.height - mouse.y;
            mouse.x = mouse.x * (1 / _scale.x);
            mouse.y = mouse.y * (1 / _scale.y);

            if (Input.GetKeyDown("mouse 0"))
            {
                if (imgRect.Contains(mouse))
                {
                    if (Tool == ToolType.Vector)
                    {
                        var m2 = mouse - new Vector2(imgRect.x, imgRect.y);
                        m2.y = imgRect.height - m2.y;

                        var bz = new List<Drawing.BezierPoint>(_bezierPoints);
                        bz.Add(new Drawing.BezierPoint(m2, m2 - new Vector2(50, 10), m2 + new Vector2(50, 10)));
                        _bezierPoints = bz;
                        Drawing.DrawBezier(_bezierPoints.ToArray(), _lineTool.width, _color, _baseTex);
                    }

                    _dragStart = mouse - new Vector2(imgRect.x, imgRect.y);
                    _dragStart.y = imgRect.height - _dragStart.y;
                    _dragStart.x = Mathf.Round(_dragStart.x / _zoom);
                    _dragStart.y = Mathf.Round(_dragStart.y / _zoom);
                    //LineStart (mouse - Vector2 (imgRect.x,imgRect.y));

                    _dragEnd = mouse - new Vector2(imgRect.x, imgRect.y);
                    _dragEnd.x = Mathf.Clamp(_dragEnd.x, 0, imgRect.width);
                    _dragEnd.y = imgRect.height - Mathf.Clamp(_dragEnd.y, 0, imgRect.height);
                    _dragEnd.x = Mathf.Round(_dragEnd.x / _zoom);
                    _dragEnd.y = Mathf.Round(_dragEnd.y / _zoom);
                }
                else
                {
                    _dragStart = Vector3.zero;
                }

            }
            if (Input.GetKey("mouse 0"))
            {
                if (_dragStart == Vector2.zero)
                {
                    return;
                }
                _dragEnd = mouse - new Vector2(imgRect.x, imgRect.y);
                _dragEnd.x = Mathf.Clamp(_dragEnd.x, 0, imgRect.width);
                _dragEnd.y = imgRect.height - Mathf.Clamp(_dragEnd.y, 0, imgRect.height);
                _dragEnd.x = Mathf.Round(_dragEnd.x / _zoom);
                _dragEnd.y = Mathf.Round(_dragEnd.y / _zoom);

                if (Tool == ToolType.Brush)
                {
                    Brush(_dragEnd, preDrag);
                }
                if (Tool == ToolType.Eraser)
                {
                    Eraser(_dragEnd, preDrag);
                }

            }
            if (Input.GetKeyUp("mouse 0") && _dragStart != Vector2.zero)
            {
                if (Tool == ToolType.Line)
                {
                    _dragEnd = mouse - new Vector2(imgRect.x, imgRect.y);
                    _dragEnd.x = Mathf.Clamp(_dragEnd.x, 0, imgRect.width);
                    _dragEnd.y = imgRect.height - Mathf.Clamp(_dragEnd.y, 0, imgRect.height);
                    _dragEnd.x = Mathf.Round(_dragEnd.x / _zoom);
                    _dragEnd.y = Mathf.Round(_dragEnd.y / _zoom);
                    Debug.Log("Draw Line");
                    Drawing.NumSamples = AntiAlias;
                    if (_stroke.enabled)
                    {

                        _baseTex = Drawing.DrawLine(_dragStart, _dragEnd, _lineTool.width, _color, _baseTex, true, _color2, _stroke.width);
                    }
                    else
                    {
                        _baseTex = Drawing.DrawLine(_dragStart, _dragEnd, _lineTool.width, _color, _baseTex);
                    }
                }
                _dragStart = Vector2.zero;
                _dragEnd = Vector2.zero;
            }
            preDrag = _dragEnd;
        }


        #region Tools
        public class LineTool
        {
            public float width = 1;
        }
        public class EraserTool
        {
            public float width = 1;
            public float hardness = 1;
        }

        void Eraser(Vector2 p1, Vector2 p2)
        {
            Drawing.NumSamples = AntiAlias;
            if (p2 == Vector2.zero)
            {
                p2 = p1;
            }
            Drawing.PaintLine(p1, p2, _eraser.width, Color.white, _eraser.hardness, _baseTex);
            _baseTex.Apply();
        }

        public class BrushTool
        {
            public float width = 1;
            public float hardness = 0;
            public float spacing = 10;
        }

        void Brush(Vector2 p1, Vector2 p2)
        {
            Drawing.NumSamples = AntiAlias;
            if (p2 == Vector2.zero)
            {
                p2 = p1;
            }
            Drawing.PaintLine(p1, p2, _brush.width, _color, _brush.hardness, _baseTex);
            _baseTex.Apply();
        }

        public class Stroke
        {
            public bool enabled = false;
            public float width = 1;
        }
        #endregion
    }
}