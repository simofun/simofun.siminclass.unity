﻿using KarmaFramework.Interactables;
using KarmaFramework.Interactables.Apps;
using UnityEngine;

namespace Sinifta.UI.BoardUI
{
    public class BoardAppView : MonoBehaviour
    {
        public virtual void Draw(GadgetApplication peek)
        {
            gameObject.SetActive(true);
        }
    }
}
