﻿using KarmaFramework.Interactables;
using KarmaFramework.Interactables.Apps;
using Sinifta.Interactables.BoardApps;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI.BoardUI
{
    public class ImageViewerAppView : BoardAppView
    {
        private ImageViewerApp _viewModel;
        [SerializeField] private Texture2D _image;
        private RectTransform _imageMask;
        private RectTransform _imageContainer;

        void Awake()
        {
            _imageMask = transform.Find("Window/ImageMask").GetComponent<RectTransform>();
            _imageContainer = transform.Find("Window/ImageMask/Image").GetComponent<RectTransform>();

        }

        public override void Draw(GadgetApplication peek)
        {
            base.Draw(peek);
            _viewModel = peek as ImageViewerApp;
            StartCoroutine(RedrawCoroutine());
        }

        // Coroutine ensures that UI components' rects are calculated 
        // before using those properties.
        IEnumerator RedrawCoroutine()
        {
            yield return new WaitForEndOfFrame();

            _image = _viewModel.Image;
            if (_image != null)
            {
                _imageContainer.GetComponent<Image>().sprite = Sprite.Create(_image,
                                new Rect(0, 0, _image.width, _image.height),
                                new Vector2(_image.width / 2, _image.height / 2));

                var aspect = (float)_image.width / (float)_image.height;
                var containerRect = _imageMask.rect;

                var h = Mathf.Min(_image.height, containerRect.height);
                var w = (h * aspect < containerRect.width) ? h * aspect : containerRect.width;
                if (w < h * aspect)
                {
                    h = w / aspect;
                }

                _imageContainer.sizeDelta = new Vector2(w, h);
            }
        }
    }
}