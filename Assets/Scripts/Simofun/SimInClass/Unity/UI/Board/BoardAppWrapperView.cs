﻿using KarmaFramework.Interactables.Apps;
using KarmaFramework.Localization;
using Sinifta.Interactables;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI.BoardUI
{
    public class BoardAppWrapperView : BoardAppView
    {
        public Transform AppHolder;
        private BoardApplication _app;
        private RectTransform _osRect;
        private Transform _closeButton;
        private Vector3 _mouseDown;

        public void Awake()
        {
            _closeButton = transform.Find("BoardApplicationWindow/WindowAppTopBar/TopBarRightSide/WindowAppCloseButton");
            _closeButton.GetComponent<Button>().onClick.AddListener(() =>
            {
                gameObject.SetActive(false);
            });
        }

        void Start()
        {
            _osRect = (RectTransform)transform.parent;
        }

        public override void Draw(GadgetApplication peek)
        {
            base.Draw(peek);
            _app = (BoardApplication)peek;
            SetHeader(_app.ApplicationName);
        }

        public void MouseDown()
        {
            _mouseDown = Input.mousePosition;
        }

        public void Drag()
        {
            var next = transform.position + Input.mousePosition - _mouseDown;
            if (RectTransformUtility.RectangleContainsScreenPoint(_osRect, Input.mousePosition))
            {
                transform.position = next;
                _mouseDown = Input.mousePosition;
            }

        }

        public void Focus()
        {
            transform.SetAsLastSibling();
            MoveInHierarchy(-1);
        }
        public void MoveInHierarchy(int delta)
        {
            int index = transform.GetSiblingIndex();
            transform.SetSiblingIndex(index + delta);
        }
        public void SetHeader(string applicationName)
        {
            var header = transform.Find("BoardApplicationWindow/WindowAppTopBar/TopBarLeftSide/WindowAppHeader");
            string headerText = applicationName;
            switch (applicationName)
            {
                case "FileBrowserApp":
                    headerText = Localizer.Instance.GetString("FileBrowser");
                    break;
                case "DrawingApp":
                    headerText = Localizer.Instance.GetString("DrawingApp");
                    break;
            }
            header.GetComponent<Text>().text = headerText;
        }
    }
}