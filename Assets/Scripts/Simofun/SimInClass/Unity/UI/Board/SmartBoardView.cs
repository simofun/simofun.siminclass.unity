﻿using Simofun.Karma.UniRx.Unity;
using Sinifta.Interactables;
using UniRx;
using UnityEngine;

namespace Sinifta.UI.BoardUI
{
	public class SmartBoardView : BoardView
	{
		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();
		#endregion

		#region Unity Methods
		protected virtual void Update() =>
			KarmaCursor.Mode = !this.gameObject.activeInHierarchy ? KarmaCursorMode.Hide : KarmaCursorMode.Normal;

		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion

		#region Public Methods
		public override void Bind(Board board)
		{
			base.Bind(board);

			var appView = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/AppPrefabs/SmartBoardOperatingSystemAppPrefab"), _appHolder);
			var os = appView.GetComponent<BoardOperatingSystemView>();
			os.Draw(board.App);

			KarmaMessageBus.OnEvent<OpenBoardEvent>().Subscribe(ev =>
			{
				if (ev.BoardType == Board.BoardType.SmartBoard)
				{
					if (ev.State)
					{
						Show();

						return;
					}
					
					gameObject.SetActive(false);
				}
			}).AddTo(this.disposables);
		}
		#endregion
	}
}
