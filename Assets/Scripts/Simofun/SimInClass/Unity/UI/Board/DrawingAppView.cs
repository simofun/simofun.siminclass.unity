﻿using KarmaFramework.Interactables;
using KarmaFramework.Interactables.Apps;
using Sinifta.Interactables.BoardApps;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI.BoardUI
{
    public class DrawingAppView : BoardAppView
    {
        private const float Delta = 10;
        private const int CanvasWidth = 800;
        private const int CanvasHeight = 600;

        private DrawingApp _viewModel;
        private Transform _lineOptions;
        private Transform _eraserOptions;

        private Transform _ownerWindow;

        public Texture2D SourceBaseTex;
        private Vector3 _drawingTopLeft;
        private RectTransform _drawingSurface;
        private RectTransform _alphaSurface;
        private RectTransform _drawingAppLeftMenu;
        private RectTransform _mask;
        private Texture2D _baseTex;
        private Texture2D _alphaTex;
        private Vector3 _scale;
        private Rect _imgRect;

        private Vector2 _dragStart;
        private Vector2 _dragEnd;
        private enum ToolType
        {
            Line,
            Eraser,
            Vector
        }

        private const Drawing.Samples AntiAlias = Drawing.Samples.Samples4;
        private Vector2 preDrag;
        private ToolType _tool;
        private ToolType Tool
        {
            get { return _tool; }
            set
            {
                _tool = value;
                _dragStart = _dragEnd = Vector2.zero;
                switch (_tool)
                {
                    case ToolType.Line:
                        {
                            _lineOptions.gameObject.SetActive(true);
                            _eraserOptions.gameObject.SetActive(false);
                            _colorPicker.gameObject.SetActive(true);
                            break;
                        }
                    case ToolType.Eraser:
                        {
                            _eraserOptions.gameObject.SetActive(true);
                            _lineOptions.gameObject.SetActive(false);
                            _colorPicker.gameObject.SetActive(false);
                            break;
                        }
                    default:
                        {
                            _eraserOptions.gameObject.SetActive(false);
                            _lineOptions.gameObject.SetActive(false);
                            _colorPicker.gameObject.SetActive(false);
                            break;
                        }
                }
            }
        }

        private Color _color = Color.red;
        private Color _color2 = Color.red;
        private LineTool _lineTool = new LineTool();
        private BrushTool _brush = new BrushTool();
        private EraserTool _eraser = new EraserTool();
        private Stroke _stroke = new Stroke();
        private int _zoom = 1;
        private float _width;
        private float _height;
        private float _xMultiplier;
        private float _yMultiplier;

        // Color Slider Backgrounds
        private Transform _colorPicker;
        private Image _colorPreview;
        private Image _redFill;
        private Image _greenFill;
        private Image _blueFill;


        public override void Draw(GadgetApplication peek)
        {
            _viewModel = peek as DrawingApp;
            base.Draw(peek);
        }

        private void GoBack(object sender, EventArgs e)
        {
            _viewModel.ForceQuit();
        }

        void Awake()
        {
            _brush.hardness = 25;
            _brush.width = 5;
            _drawingSurface = transform.Find("DrawingAppSurface").GetComponent<RectTransform>();
            _alphaSurface = transform.Find("AlphaSurface").GetComponent<RectTransform>();
            _colorPicker = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/ColorSliders");
            _drawingTopLeft = _drawingSurface.position;
            transform.Find("DrawingAppLeftMenu/DrawingAppToolMenu/DrawingAppLineTool").GetComponent<Button>().onClick.AddListener(() => { Tool = ToolType.Line; });
            transform.Find("DrawingAppLeftMenu/DrawingAppToolMenu/DrawingAppEraserTool").GetComponent<Button>().onClick.AddListener(() => { Tool = ToolType.Eraser; });

            _lineOptions = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/DrawingAppLineOptions");
            _lineOptions.gameObject.SetActive(false);
            _eraserOptions = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/DrawingAppEraserOptions");
            _eraserOptions.gameObject.SetActive(false);

            var ew = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/DrawingAppEraserOptions/DrawingAppEraserWidth").GetComponent<Slider>();
            ew.value = _eraser.width;
            ew.onValueChanged.AddListener((s) => { _eraser.width = s; });
            var eh = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/DrawingAppEraserOptions/DrawingAppEraserHardness").GetComponent<Slider>();
            eh.value = _eraser.hardness;
            eh.onValueChanged.AddListener((s) => { _eraser.hardness = s; });

            var lw = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/DrawingAppLineOptions/DrawingAppLineWidth");
            lw.GetComponent<Slider>().value = _lineTool.width;
            lw.GetComponent<Slider>().onValueChanged.AddListener((s) => { _lineTool.width = s; });

            InitializeColorSliders();
            Tool = ToolType.Line;
        }

        void ColorChanged(Color newColor)
        {
            _colorPreview.color = _color;
        }

        void InitializeColorSliders()
        {
            _redFill = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/ColorSliders/R/Fill Area/Fill").GetComponent<Image>();
            var rSlider = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/ColorSliders/R").GetComponent<Slider>();
            rSlider.value = _color.r;
            rSlider.onValueChanged.AddListener((v) => { _color.r = v; ColorChanged(_color); });
            _redFill.color = Color.red;

            _greenFill = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/ColorSliders/G/Fill Area/Fill").GetComponent<Image>();
            var gSlider = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/ColorSliders/G").GetComponent<Slider>();
            gSlider.value = _color.g;
            gSlider.onValueChanged.AddListener((v) => { _color.g = v; ColorChanged(_color); });
            _greenFill.color = Color.green;

            _blueFill = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/ColorSliders/B/Fill Area/Fill").GetComponent<Image>();
            var bSlider = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/ColorSliders/B").GetComponent<Slider>();
            bSlider.value = _color.b;
            bSlider.onValueChanged.AddListener((v) => { _color.b = v; ColorChanged(_color); });
            _blueFill.color = Color.blue;

            _colorPreview = transform.Find("DrawingAppLeftMenu/DrawingAppToolOptions/ColorSliders/Preview").GetComponent<Image>();
            _colorPreview.color = _color;
        }

        void Start()
        {
            _drawingSurface.sizeDelta = new Vector2(CanvasWidth, CanvasHeight);
            _alphaSurface.sizeDelta = new Vector2(CanvasWidth, CanvasHeight);

            _baseTex = new Texture2D(CanvasWidth, CanvasHeight);
            _alphaTex = new Texture2D(CanvasWidth, CanvasHeight);

            var colors = new Color32[CanvasWidth * CanvasHeight];
            for (int i = 0; i < colors.Length; i++) { colors[i] = Color.white; }
            _baseTex.SetPixels32(colors);
            _drawingSurface.GetComponent<RawImage>().texture = _baseTex;
            _alphaSurface.GetComponent<RawImage>().texture = _alphaTex;
            _alphaSurface.gameObject.SetActive(false);

            _ownerWindow = transform.parent.parent.parent;
            _mask = GetComponent<RectTransform>();
            _drawingAppLeftMenu = transform.Find("DrawingAppLeftMenu").GetComponent<RectTransform>();

            Drawing.Paint(Vector2.zero, 0, Color.white, 1, _baseTex);
            _baseTex.Apply();

            _scale = transform.root.GetComponent<RectTransform>().localScale;
            _drawingTopLeft = _drawingSurface.position;

            _width = _mask.rect.width - _drawingAppLeftMenu.rect.width;
            _height = _mask.rect.height;

            _imgRect = new Rect(_drawingTopLeft.x, Screen.height - _drawingTopLeft.y, _baseTex.width * _zoom, _baseTex.height * _zoom);
            _imgRect.x = _imgRect.x * (1 / _scale.x);
            _imgRect.y = _imgRect.y * (1 / _scale.y);

            _xMultiplier = (1 / _scale.x);
            _yMultiplier = (1 / _scale.y);
        }



        void Update()
        {
            // NOTE: INDEX CHECK ASSURES THAT DRAWING HAPPENS ONLY WHEN THE APP IS ON TOP
            // HARDCODED 2 IS BECAUSE OF THE CURSOR ALWAYS BEING THE TOP-MOST ITEM, SO THE TOP-MOST
            // APP INDEX BECOMES childCount - 2
            if (_ownerWindow.GetSiblingIndex() < _ownerWindow.parent.childCount - 2)
                return;

            Vector2 mouse = Input.mousePosition;
            mouse.y = Screen.height - mouse.y;
            mouse.x = mouse.x * _xMultiplier;
            mouse.y = mouse.y * _yMultiplier;

            if (Input.GetKeyDown("mouse 0"))
            {
                _drawingTopLeft = _drawingSurface.position;
                _imgRect = new Rect(_drawingTopLeft.x, Screen.height - _drawingTopLeft.y, _baseTex.width * _zoom, _baseTex.height * _zoom);
                _imgRect.x = _imgRect.x * (1 / _scale.x);
                _imgRect.y = _imgRect.y * (1 / _scale.y);

                if (_imgRect.Contains(mouse))
                {
                    _dragStart = mouse - new Vector2(_imgRect.x, _imgRect.y);
                    _dragStart.y = _imgRect.height - _dragStart.y;
                    _dragStart.x = Mathf.Round(_dragStart.x / _zoom);
                    _dragStart.y = Mathf.Round(_dragStart.y / _zoom);

                    _dragEnd = mouse - new Vector2(_imgRect.x, _imgRect.y);
                    _dragEnd.x = Mathf.Clamp(_dragEnd.x, 0, _width);
                    _dragEnd.y = _imgRect.height - Mathf.Clamp(_dragEnd.y, 0, _height);
                    _dragEnd.x = Mathf.Round(_dragEnd.x / _zoom);
                    _dragEnd.y = Mathf.Round(_dragEnd.y / _zoom);

                    if (Tool == ToolType.Line)
                    {
                        _alphaSurface.gameObject.SetActive(true);
                    }
                }
                else
                {
                    _dragStart = Vector3.zero;
                }

            }
            if (Input.GetKey("mouse 0"))
            {
                if (_dragStart == Vector2.zero)
                {
                    return;
                }
                _dragEnd = mouse - new Vector2(_imgRect.x, _imgRect.y);
                _dragEnd.x = Mathf.Clamp(_dragEnd.x, 0, _width);
                _dragEnd.y = _imgRect.height - Mathf.Clamp(_dragEnd.y, 0, _height);
                _dragEnd.x = Mathf.Round(_dragEnd.x / _zoom);
                _dragEnd.y = Mathf.Round(_dragEnd.y / _zoom);

                //Debug.Log(_dragEnd);
                switch (Tool)
                {
                    case ToolType.Eraser:
                        Eraser(_dragEnd, preDrag);
                        break;
                    case ToolType.Line:
                        Line(_dragStart, _dragEnd);
                        break;
                }

            }
            if (Input.GetKeyUp("mouse 0") && _dragStart != Vector2.zero)
            {
                if (Tool == ToolType.Line)
                {
                    _dragEnd = mouse - new Vector2(_imgRect.x, _imgRect.y);
                    _dragEnd.x = Mathf.Clamp(_dragEnd.x, 0, _width);
                    _dragEnd.y = _imgRect.height - Mathf.Clamp(_dragEnd.y, 0, _height);
                    _dragEnd.x = Mathf.Round(_dragEnd.x / _zoom);
                    _dragEnd.y = Mathf.Round(_dragEnd.y / _zoom);
                    _baseTex = DrawLine(_dragStart, _dragEnd, _lineTool.width, _color, _baseTex);
                    _baseTex.Apply();
                    _alphaSurface.gameObject.SetActive(false);
                }
                _dragStart = Vector2.zero;
                _dragEnd = Vector2.zero;
            }
            preDrag = _dragEnd;
        }

        public Texture2D DrawLine(Vector2 from, Vector2 to, float w, Color col, Texture2D tex)
        {
            return Drawing.DrawLine(_dragStart, _dragEnd, _lineTool.width, _color, _baseTex, true, _color2, 0);
        }

        #region Tools
        public class LineTool
        {
            public float width = 1;
        }
        public class EraserTool
        {
            public float width = 1;
            public float hardness = 1;
        }

        void Eraser(Vector2 p1, Vector2 p2)
        {
            Drawing.NumSamples = AntiAlias;
            if (p2 == Vector2.zero)
            {
                p2 = p1;
            }
            Drawing.PaintLine(p1, p2, _eraser.width, Color.white, _eraser.hardness, _baseTex);
            _baseTex.Apply();
        }

        void Line(Vector2 start, Vector2 end)
        {
            Drawing.NumSamples = Drawing.Samples.Samples4;
            Color32[] cleanTexture = new Color32[CanvasWidth * CanvasHeight];
            _alphaTex.SetPixels32(cleanTexture);
            if (end == Vector2.zero)
            {
                end = start;
            }
            Drawing.DrawLine(start, end, _lineTool.width, _color, _alphaTex);
            _alphaTex.Apply();
        }

        public class BrushTool
        {
            public float width = 1;
            public float hardness = 0;
            public float spacing = 10;
        }

        void Brush(Vector2 p1, Vector2 p2)
        {
            Drawing.NumSamples = AntiAlias;
            if (p2 == Vector2.zero)
            {
                p2 = p1;
            }
            Drawing.PaintLine(p1, p2, _brush.width, _color, _brush.hardness, _baseTex);
            _baseTex.Apply();
        }

        public class Stroke
        {
            public bool enabled = false;
            public float width = 1;
        }
        #endregion
    }
}