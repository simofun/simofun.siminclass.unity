﻿using KarmaFramework.Localization;
using KarmaFramework.Views;
using Simofun.Karma.UniRx.Unity;
using Simofun.SimInClass.Unity.HardCodeds;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuitConfirmationView : MonoBehaviour
{
    private GameObject _textQuitConfirmationLabel;
    private GameObject _confirmationText;
    private GameObject _yesButton;
    private GameObject _noButton;
    private GameObject _closeWindowButton;

    public bool showWarningInQuitConfirmation = true;

    void Awake()
    {
        _textQuitConfirmationLabel = GameObject.Find("Text_QuitConfirmation_Label");

        _confirmationText = GameObject.Find("Text_Confirmation");
        _yesButton = GameObject.Find("Button_Yes");
        _noButton = GameObject.Find("Button_No");
        _closeWindowButton = transform.Find("Close_QuitConfirmation").gameObject;

        if (showWarningInQuitConfirmation)
            SetConfirmationText(Localizer.Instance.GetString("QuitConfirmation"));
        else
            SetConfirmationText(Localizer.Instance.GetString("QuitConfirmationNoWarning"));

        SetYesButtonText(Localizer.Instance.GetString("Yes"));
        SetNoButtonText(Localizer.Instance.GetString("No"));
        SetQuitConfirmationLabel(Localizer.Instance.GetString("Quit"));

        _yesButton.GetComponent<Button>().onClick.AddListener(() => KarmaMessageBus.Publish(new EndGameEvent()));
        _noButton.GetComponent<Button>().onClick.AddListener(() => CloseWindow());
        _closeWindowButton.GetComponent<Button>().onClick.AddListener(() => CloseWindow());

        gameObject.SetActive(false);
    }

    private void CloseWindow()
    {
        TimeManager.Instance.ResetTimeScale();
        gameObject.SetActive(false);
    }

    private void SetQuitConfirmationLabel(string newText)
    {
        _textQuitConfirmationLabel.GetComponent<Text>().text = newText;
    }

    public void SetConfirmationText(string newText)
    {
        _confirmationText.GetComponent<Text>().text = newText;
    }

    public void SetYesButtonText(string newText)
    {
        _yesButton.GetComponentInChildren<Text>().text = newText;
    }

    public void SetNoButtonText(string newText)
    {
        _noButton.GetComponentInChildren<Text>().text = newText;
    }

    public void SetYesButtonAction(Action action)
    {
        _yesButton.GetComponent<Button>().onClick.AddListener(() => action());
    }
}
