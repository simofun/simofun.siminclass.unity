﻿using UnityEngine;
using UniRx;
using KarmaFramework.CharacterCore;
using Sinifta.Actions;
using System.Collections;
using UnityEngine.UI;
using Simofun.Karma.UniRx.Unity;

public class ActionFeedbackPopupView : MonoBehaviour
{
	#region Unity Fields
	[SerializeField]
	Text _concentrationDelta;

	[SerializeField]
	Text _enjoymentDelta;

	[SerializeField]
	Text _knowledgeDelta;
	#endregion

	#region Fields
	readonly CompositeDisposable disposables = new CompositeDisposable();

	MindSnapshot delta;
	#endregion

	#region Unity Methods
	protected virtual void Start()
	{
		KarmaMessageBus.OnEvent<ActionFeedbackEvent>().Subscribe(ev =>
		{
			delta = ev.Delta;
			if (gameObject.activeInHierarchy)
			{
				return;
			}

			gameObject.SetActive(true);
			StartCoroutine(ShowFeedbackCoroutine());
		}).AddTo(this.disposables);
		gameObject.SetActive(false);
	}

	protected virtual void OnDestroy() => this.disposables.Dispose();
	#endregion

	#region Methods
	IEnumerator ShowFeedbackCoroutine()
	{
		_concentrationDelta.text = delta.Params["Concentration"] > 0
			? "+ (" + delta.Params["Concentration"].ToString() + ")"
			: delta.Params["Concentration"].ToString();
		_enjoymentDelta.text = delta.Params["Enjoyment"] > 0
			? "+ (" + delta.Params["Enjoyment"].ToString() + ")"
			: delta.Params["Enjoyment"].ToString();
		_knowledgeDelta.text = delta.Params["Knowledge"] > 0
			? "+ (" + delta.Params["Knowledge"].ToString() + ")"
			: delta.Params["Knowledge"].ToString();

		yield return new WaitForSeconds(3f);

		gameObject.SetActive(false);
	}
	#endregion
}
