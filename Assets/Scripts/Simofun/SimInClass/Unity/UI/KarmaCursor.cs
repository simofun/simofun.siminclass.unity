﻿using Sinifta.Utility;
using System.Collections;
using UnityEngine;

public static class KarmaCursor
{
	#region Fields
	static readonly Texture2D CursorCircle;

	static readonly Texture2D CursorCircleHighlight;

	static readonly Texture2D CursorDisabled;

	static readonly Texture2D CursorHighlighted;

	static readonly Texture2D CursorNormal;

	static readonly Texture2D CursorStretch;

	static readonly Texture2D[] CursorLoading;

	static readonly Vector2 CrosshairCursorHotspot;

	static readonly Vector2 CrosshairHighlightHotspot;

	static readonly Vector2 CursorDefaultHotspot;

	static readonly Vector2 StretchCursorHotspot;

	static bool isForceHideCursor = false;

	static bool isLoading = false;

	static int cursorLoadingTextureIndex;

	static KarmaCursorMode currentMode;

	static WaitForSecondsUnscaled cursorLoadingWaitForTime = new WaitForSecondsUnscaled(0.06f);
	#endregion

	#region Constructors
	static KarmaCursor()
	{
		CursorNormal = Resources.Load<Texture2D>("Textures/Cursor/CursorNormal");
		CursorLoading = Resources.LoadAll<Texture2D>("Textures/Cursor/CursorLoading");
		CursorHighlighted = Resources.Load<Texture2D>("Textures/Cursor/CursorHighlighted");
		CursorDisabled = Resources.Load<Texture2D>("Textures/Cursor/CursorDisabled");
		CursorCircle = Resources.Load<Texture2D>("Textures/Cursor/CursorCircle");
		CursorCircleHighlight = Resources.Load<Texture2D>("Textures/Cursor/CursorCircleHighlight");
		CursorStretch = Resources.Load<Texture2D>("Textures/Cursor/CursorStretch");

		CursorDefaultHotspot = new Vector2(4f, 2f);
		CrosshairCursorHotspot = new Vector2(CursorCircle.width / 2f, CursorCircle.height / 2f);
		StretchCursorHotspot = new Vector2(CursorStretch.width / 2f, CursorStretch.height / 2f);
		CrosshairHighlightHotspot = new Vector2(CursorCircleHighlight.width / 2f, CursorCircleHighlight.height / 2f);
	}
	#endregion

	#region Properties
	public static bool IsForceHideCursor
	{
		get => isForceHideCursor;

		set
		{
			if (isForceHideCursor == value)
			{
				return;
			}

			isForceHideCursor = value;

			if (value)
			{
				Cursor.visible = false;

				return;
			}

			Mode = currentMode;
		}
	}

	public static bool IsLoadingMode
	{
		get => isLoading;

		set
		{
			if (value == isLoading)
			{
				return;
			}

			isLoading = value;

			if (!value)
			{
				Mode = currentMode;

				return;
			}

			cursorLoadingTextureIndex = 0;
			CoroutineStarter.StartCoroutine(LoadingCursorModeCoroutine());
		}
	}

	public static KarmaCursorMode Mode
	{
		get => currentMode;

		set
		{
			if (isLoading || isForceHideCursor)
			{
				currentMode = value;

				return;
			}

			var hotSpot = Vector2.zero;
			var isVisible = true;
			Texture2D texture = null;

			switch (value)
			{
				case KarmaCursorMode.None: // System cursor
					{
						break;
					}
				case KarmaCursorMode.Normal:
					{
						hotSpot = CursorDefaultHotspot;
						texture = CursorNormal;

						break;
					}
				case KarmaCursorMode.Loading:
					{
						isVisible = false;

						break;
					}
				case KarmaCursorMode.Highlighted:
					{
						hotSpot = CursorDefaultHotspot;
						texture = CursorHighlighted;

						break;
					}
				case KarmaCursorMode.Disabled:
					{
						hotSpot = CursorDefaultHotspot;
						texture = CursorDisabled;

						break;
					}
				case KarmaCursorMode.Crosshair:
					{
						hotSpot = CrosshairCursorHotspot;
						texture = CursorCircle;

						break;
					}
				case KarmaCursorMode.CrosshairHighlight:
					{
						hotSpot = CrosshairHighlightHotspot;
						texture = CursorCircleHighlight;

						break;
					}
				case KarmaCursorMode.Stretch:
					{
						hotSpot = StretchCursorHotspot;
						texture = CursorStretch;

						break;
					}
				case KarmaCursorMode.Hide:
					{
						isVisible = false;

						break;
					}
			}

			Cursor.visible = isVisible;
			if (isVisible)
			{
				Cursor.SetCursor(texture, hotSpot, CursorMode.ForceSoftware);
			}

			currentMode = value;
		}
	}
	#endregion

	#region Methods
	static IEnumerator LoadingCursorModeCoroutine()
	{
		while (isLoading)
		{
			cursorLoadingWaitForTime.Reset();

			Cursor.SetCursor(CursorLoading[cursorLoadingTextureIndex], Vector2.zero, CursorMode.ForceSoftware);

			cursorLoadingTextureIndex++;
			if (cursorLoadingTextureIndex >= CursorLoading.Length)
				cursorLoadingTextureIndex = 0;

			yield return cursorLoadingWaitForTime;
		}
	}
	#endregion
}

public enum KarmaCursorMode
{
	None,

	Normal,

	Loading,

	Highlighted,

	Disabled,

	Crosshair,

	CrosshairHighlight,

	Stretch,

	Hide
}
