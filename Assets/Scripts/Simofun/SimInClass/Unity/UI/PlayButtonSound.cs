﻿using Sinifta.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class PlayButtonSound : MonoBehaviour
    {
        [SerializeField]
        private AudioClip sound1;
        [SerializeField]
        private AudioClip sound2;

        private AudioSource audioSource;

        void Awake()
        {
            /*sound1 = SinifaResources.Load<AudioClip>(PrefabType.ButtonSound1);
            sound2 = SinifaResources.Load<AudioClip>(PrefabType.ButtonSound2);*/
            audioSource = GetComponent<AudioSource>();
        }

        public void PlaySound1()
        {
            if (sound1 != null && audioSource)
            {
                audioSource.PlayOneShot(sound1);
            }
        }

        public void PlaySound2()
        {
            audioSource.PlayOneShot(sound2);
        }

        /*public static void PlaySound1Static()
        {
            audioSource.PlayOneShot(sound1);
        }

        public static void PlaySound2Static()
        {
            audioSource.PlayOneShot(sound2);
        }*/
    }
}
