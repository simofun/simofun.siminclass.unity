﻿namespace Sinifta.UI
{
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using UnityEngine.EventSystems;

	public class LessonDragHandler : MobileDragHandler
	{
		private LessonFlow _lessonFlowView;
		private TimeHandler _mobileTimeHandler;

		private void Start()
		{
			_lessonFlowView = FindObjectOfType<LessonFlow>();
			_mobileTimeHandler = FindObjectOfType<TimeHandler>();
		}

		/// <summary>
		/// Actions maybe used for this purpose but this is less error prone i guess. 
		/// Otherwise we need to get track of all destroyed and instantiated flow elements which is better but requires more 
		/// time to implement and we don't have that amount of time.
		/// </summary>
		/// <param name="eventData"></param>
		public override void OnBeginDrag(PointerEventData eventData)
		{
			base.OnBeginDrag(eventData);
			SetInteractables(true);
			_mobileTimeHandler.SetCursorState(false);
			//MobileLessonPlanMessageBox.Instance.Close();
			KarmaMessageBus.Publish(new DragLessonButtonGameEvent());
			LessonPlanPanel.PlayButtonClick();
		}

		public override void OnEndDrag(PointerEventData eventData)
		{
			base.OnEndDrag(eventData);
			SetInteractables(false);

			var mobileEventView = FindObjectOfType<EventEntry>();
			if (mobileEventView != null)
			{
				mobileEventView.UpdateLayout();
			}
		}

		public void OnDestroy()
		{
			SetInteractables(false);
		}

		private void SetInteractables(bool state)
		{
			if (_lessonFlowView != null)
			{
				_lessonFlowView.SetDropHelpers(state);
				_lessonFlowView.TrashHandler.gameObject.SetActive(state);
				_lessonFlowView.DragTint.gameObject.SetActive(state);
			}
		}
	}
}
