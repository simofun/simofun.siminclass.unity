﻿using KarmaFramework.ActionCore;
using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using Sinifta.Actions.TeacherActions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI
{
    public class EventEntry : MonoBehaviour
    {
        public GameObject EventButtonPrefab;
        public Transform EventHolder;
        public Text DescriptionText;

        public List<Button> EventButtons = new List<Button>();

        public void UpdateEvents(List<BaseAction> actionsForLessonType)
        {
            foreach (Transform child in EventHolder.transform)
            {
                Destroy(child.gameObject);
            }
            EventButtons.Clear();
            if (actionsForLessonType.Any())
            {
                var teacherLecture = actionsForLessonType.First() as TeacherLecture;
                if (teacherLecture != null)
                {
                    var icons = Resources.LoadAll<Sprite>("Icons/Events/" + teacherLecture.Icon);
                    for (int i = 0; i < actionsForLessonType.Count; i++)
                    {
                        var action = actionsForLessonType[i];
                        var methodParameter = Instantiate(EventButtonPrefab, EventHolder.transform);
                        var view = methodParameter.GetComponent<EventButton>();
                        view.Init(new FlowElement(action));
                        view.Title.text = Localizer.Instance.GetString("Activity") + " " + (i + 1);
                        view.Icon.sprite = icons[i % icons.Count()];
                        EventButtons.Add(view.Button);
                    }
                }
            }
        }

        public void SetDescription(string text)
        {
            DescriptionText.text = text;
        }

        public void UpdateLayout()
        {
            var events = GetComponentsInChildren<EventButton>().OrderBy(b => b.Title.text).ToList();
            for (int i = 0; i < events.Count; i++)
            {
                var mobileEventButton = events[i];
                mobileEventButton.transform.SetSiblingIndex(i);
            }
        }
    }
}