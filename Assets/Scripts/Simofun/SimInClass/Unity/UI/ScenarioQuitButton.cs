﻿using UnityEngine;

public class ScenarioQuitButton : MonoBehaviour
{
	#region Unity Fields
	[SerializeField]
	GameObject _tooltip;
	#endregion

	#region Unity Methods
	#region OnMouseXXX Methods
	protected virtual void OnMouseEnter() => this.ShowTooltip();

	protected virtual void OnMouseExit() => this.HideTooltip();
	#endregion

	protected virtual void Start() => this.HideTooltip();
	#endregion

	#region Methods
	void HideTooltip() => this._tooltip.SetActive(false);

	void ShowTooltip() => this._tooltip.SetActive(true);
	#endregion
}
