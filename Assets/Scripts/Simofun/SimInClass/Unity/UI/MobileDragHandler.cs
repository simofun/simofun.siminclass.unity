﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Sinifta.UI
{
    public class MobileDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private Vector3 _dragStartPos;
        public static GameObject DragedGameObject;
        private Transform _startParent;

        public virtual void OnBeginDrag(PointerEventData eventData)
        {
            _dragStartPos = transform.position;
            DragedGameObject = gameObject;
            _startParent = transform.parent;
            gameObject.transform.SetParent(gameObject.GetComponentInParent<Canvas>().transform);
            foreach (var componentsInChild in transform.GetComponentsInChildren<Graphic>())
            {
                componentsInChild.raycastTarget = false;
            }
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
            transform.position = Input.mousePosition;
        }

        public virtual void OnEndDrag(PointerEventData eventData)
        {
            if (transform.parent == _startParent)
            {
                transform.position = _dragStartPos;
            }
            var graphic = transform.GetComponent<Graphic>();
            if (graphic != null)
            {
                graphic.raycastTarget = true;
            }
            if (DragedGameObject != null && !DragedGameObject.GetComponentInParent<DropHandler>())
            {
                transform.SetParent(_startParent);
            }
            DragedGameObject = null;
        }
    }
}
