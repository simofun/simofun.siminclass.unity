﻿using KarmaFramework.CharacterCore;
using KarmaFramework.Localization;
using Simofun.Karma.UniRx.Unity;
using Sinifta.Actions;
using Sinifta.Actors;
using Sinifta.UI;
using System.Linq;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class StudentTooltipView : UIElement
{
    public GameObject Background;

    public TMP_Text Name;
    public TMP_Text State;
    public Transform Bar1;
    public Transform Bar2;
    public Transform Bar3;
    public Transform Icon1;
    public Transform Icon2;
    public Transform Icon3;
    public Image StudentIcon;

    #region Fields
    readonly CompositeDisposable disposables = new CompositeDisposable();

    Sprite[] _conIcons;
    Sprite[] _enjIcons;
    Sprite[] _knoIcons;

    Image _bar1Image;
    Image _bar2Image;
    Image _bar3Image;

    Image _icon1Image;
    Image _icon2Image;
    Image _icon3Image;

    RectTransform _rightBorderRectTransform;
    RectTransform _leftBorderRectTransform;
    RectTransform _topBorderRectTransform;
    RectTransform _bottomBorderRectTransform;

    RectTransform _canvasRectTransform;

    Canvas _canvas;
    Camera _camera;
    NPCMenu _npcMenu;
    #endregion

    #region Unity Methods
    protected virtual void Awake()
    {
        Background.SetActive(false);
        
        _conIcons = new[]
        {
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/RedBarFill"),
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/YellowBarFill"),
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/GreenBarFill"),
        };
        _enjIcons = new[]
        {
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/RedBarFill"),
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/YellowBarFill"),
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/GreenBarFill"),
        };
        _knoIcons = new[]
        {
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/RedBarFill"),
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/YellowBarFill"),
            Resources.Load<Sprite>( "Textures/UI_Mobile/Stats/GreenBarFill"),
        };

        _bar1Image = Bar1.GetComponent<Image>();
        _bar2Image = Bar2.GetComponent<Image>();
        _bar3Image = Bar3.GetComponent<Image>();

        _icon1Image = Icon1.GetComponent<Image>();
        _icon2Image = Icon2.GetComponent<Image>();
        _icon3Image = Icon3.GetComponent<Image>();

        KarmaMessageBus.OnEvent<PlayerInitiatedEvent>().Subscribe(ev => {
            _camera = ev.camera;
#if UNITY_ANDROID || UNITY_IOS
        _npcMenu = _canvas.transform.Find("NPCMenu").GetComponent<NPCMenu>();
#elif UNITY_STANDALONE || UNITY_EDITOR || UNITY_EDITOR
        _npcMenu = _canvas.transform.Find("StudentMenuView").GetComponent<NPCMenu>();
#endif
        }).AddTo(this.disposables);
        _canvas = GameObject.Find("GameView").GetComponent<Canvas>();
    }

    protected virtual void LateUpdate()
    {
        if(_camera != null)
        {
            transform.LookAt(transform.position + _camera.transform.rotation * Vector3.back, _camera.transform.rotation * Vector3.up);
        }
    }

    protected virtual void OnDestroy() => this.disposables.Dispose();
    #endregion

    public void Show(StudentFacade sf)
    {
        if (sf == null || _npcMenu.gameObject.activeSelf)
            return;
        Background.SetActive(true);

        if (_canvasRectTransform == null)
            _canvasRectTransform = transform.GetComponentInParent<Canvas>().GetComponent<RectTransform>();

        StudentIcon.sprite = Resources.Load<Sprite>("Icons/Npc/" + sf.GetActor().Icon);

        RectTransform rectTransform = (RectTransform)transform;

        Vector2 size = rectTransform.sizeDelta * _canvasRectTransform.localScale.x;
        Vector3 screenPos = Camera.main.WorldToScreenPoint(sf.Transform.position) + new Vector3(0, 200, 0);


        float rightX = screenPos.x + size.x;
        float upY = screenPos.y + size.y;
        if (rightX >= Screen.width - 10f)
            screenPos.x -= rightX - Screen.width + 10f;
        if (upY >= Screen.height - 25f)
            screenPos.y -= upY - Screen.height + 25f;
        rectTransform.position = screenPos;

        Name.text = sf.Name;
        State.text = sf.GetActions().FirstOrDefault() == null ? "" : Localizer.Instance.GetString((sf.GetActions().FirstOrDefault() as StudentAction).Name);

        var mind = sf.GetMindSnapshot();

        // Set bar sizes
        Bar1.localScale = new Vector3(mind.Params["Concentration"] / 100f, 1, 1);
        Bar2.localScale = new Vector3(mind.Params["Enjoyment"] / 100f, 1, 1);
        Bar3.localScale = new Vector3(mind.Params["Knowledge"] / 100f, 1, 1);

        // Set bar colors
        _bar1Image.color = GetColor(mind.Params["Concentration"]);
        _bar2Image.color = GetColor(mind.Params["Enjoyment"]);
        _bar3Image.color = GetColor(mind.Params["Knowledge"]);
    }

    public void Close()
    {
        Background.SetActive(false);
    }

    private Color GetColor(float val)
    {
        var retVal = Color.red;
        if (val > 33)
        {
            retVal = Color.yellow;
        }
        if (val > 66)
        {
            retVal = Color.green;
        }
        return retVal;
    }
}
