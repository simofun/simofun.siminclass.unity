﻿namespace Sinifta.UI
{
	using KarmaFramework.ActionCore;
	using KarmaFramework.ScenarioCore;
	using KarmaFramework.UI;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.UI;
	using Zenject;

	public class LessonPlanPanel : MonoBehaviour
	{
		public EventEntry EventContentView;
		public LessonFlow LessonFlowView;
		public Transform LessonMethodContent;

		public List<FlowElement> FlowElements = new List<FlowElement>();

		[Header("Buttons")]
		public Button ConfirmButton;
		public Button DiscardButton;
		public GameObject TeachingMethodButtonPrefab;


		private string _serializedFlow; //for saving initial data
		private Button _firstMethodButton;

		private static AudioSource _audioSource;
		private MenuNavigator _navigator;
		private GameState _gameState;
		private static readonly AudioClip[] Clips = new AudioClip[2];

		[Inject]
		public void Construct(MenuNavigator navigator, GameState gameState)
		{
			_navigator = navigator;
			_gameState = gameState;
		}

		void Awake()
		{
			ConfirmButton.onClick.AddListener(() =>
			{
				_navigator.Proceed(true);
				KarmaMessageBus.Publish(new ConfirmExitGameEvent());
			});

			DiscardButton.onClick.AddListener(() =>
			{
				StartCoroutine(Discard());
				KarmaMessageBus.Publish(new ConfirmExitGameEvent());
			});

			InitializeSoundComponent();
		}

		private void InitializeSoundComponent()
		{
			_audioSource = gameObject.AddComponent<AudioSource>();
			Clips[0] = Resources.Load<AudioClip>("Audio/butonses1"); //Click sound
			Clips[1] = Resources.Load<AudioClip>("Audio/butonses2"); //Destroy sound
		}

		void OnEnable()
		{
			GetAllTeachingMethods();
			LessonFlowView.Init();
			StartCoroutine(SaveInitialState());
		}

		void OnDisable()
		{
			_firstMethodButton = null;
			_navigator.SetLoaderState(false);
		}

		public int GetFlowListIndex(FlowElement element)
		{
			for (int i = 0; i < FlowElements.Count; i++)
			{
				if (FlowElements[i].Action.Name == element.Name)
				{
					return i;
				}
			}
			return -1;
		}

		public void GetAllTeachingMethods()
		{
			FlowElements.Clear();

			foreach (Transform child in LessonMethodContent.transform)
			{
				Destroy(child.gameObject);
			}

			foreach (Transform child in EventContentView.transform)
			{
				Destroy(child.gameObject);
			}

			foreach (var action in _gameState.Scenario.AvailableActions.GroupBy(a => a.GetType()).Select(g => g.First()).ToList()) //high iq move
			{
				FlowElements.Add(new FlowElement(action));
			}

			foreach (var entry in FlowElements)
			{
				var flowLesson =  Instantiate(TeachingMethodButtonPrefab, LessonMethodContent.transform);
				var button = flowLesson.GetComponent<TeachingMethodButton>();

				if (_firstMethodButton == null)
				{
					_firstMethodButton = button.GetComponent<Button>();
				}

				button.Init(entry, _gameState);
				var currentEntry = entry;
				flowLesson.GetComponent<Button>().onClick.AddListener(() =>
				{
					var actions = new List<BaseAction>();
					foreach (var action in _gameState.Scenario.AvailableActions)
					{
						if (action.GetType() == currentEntry.Action.GetType())
							actions.Add(action);
					}

					EventContentView.UpdateEvents(actions);
				});
			}

			SetInitialState();
		}

		private void SetInitialState()
		{
			if (_firstMethodButton != null)
			{
				_firstMethodButton.onClick.Invoke();
				var eventView = EventContentView.GetComponentInChildren<EventEntry>();
				if (eventView != null && eventView.EventButtons.Any())
				{
					eventView.EventButtons.First().onClick.Invoke();
				}
			}
		}

		public static void PlayButtonClick(bool isClick = true)
		{
			_audioSource.clip = isClick ? Clips[0] : Clips[1];
			_audioSource.Play();
		}

		private IEnumerator Discard()
		{
			_navigator.SetLoaderState(true);
			yield return null;
			//InterLevelData.EditorGameState.Scenario.Flow = ScenarioComponent<LectureFlow>.Deserialize<LectureFlow>(_serializedFlow);
			_navigator.Proceed(true);
			_navigator.SetLoaderState(false);
		}

		private IEnumerator SaveInitialState()
		{
			_navigator.SetLoaderState(true);
			yield return null;
			//_serializedFlow = InterLevelData.EditorGameState.Scenario.Flow.Serialize();
			_navigator.SetLoaderState(false);
		}
	}
}
