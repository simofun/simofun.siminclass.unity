﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Sinifta.UI
{
    public class TimeHandler : MonoBehaviour, IPointerDownHandler
    {
        public GameObject TimeCursor;
        public static LessonFlowElement SelectedElement;

        private void OnEnable()
        {
            TimeCursor.SetActive(false);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            var go = eventData.pointerCurrentRaycast.gameObject;
            if (go.transform.parent.GetComponent<TimeCursorHandler>())
            {
                return;
            }

            var element = go.GetComponent<LessonFlowElement>();
            SelectElement(element);
        }

        public void SelectElement(LessonFlowElement element)
        {
            TimeCursor.SetActive(element != null);
            SelectedElement = element != null ? element : null;
            if (element != null)
            {
                SetCursorPosition();
            }
            LessonPlanPanel.PlayButtonClick();
        }

        private void SetCursorPosition()
        {
            var rect = SelectedElement.GetComponent<RectTransform>();
            var lessonFlowView = FindObjectOfType<LessonFlow>();
            if (lessonFlowView != null)
            {
                TimeCursor.transform.position = new Vector3(SelectedElement.Lesson.Action.Duration * lessonFlowView.UnitLength *
                    ((LessonFlow.ScaleFactor - 1) / 2f + 1) + rect.transform.position.x + rect.rect.x, transform.position.y, transform.position.z);
            }
        }

        public void SetCursorState(bool state)
        {
            TimeCursor.SetActive(state);
        }
    }
}