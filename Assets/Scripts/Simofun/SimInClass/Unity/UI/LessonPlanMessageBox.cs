﻿using Simofun.Networking.Core;
using UnityEngine;
using UnityEngine.UI;

public class LessonPlanMessageBox : Singleton<LessonPlanMessageBox>
{
    public GameObject MessageBoxGo;
    public Text MessageText;
    private float _timer;
    private float _duration;

    private void Update()
    {
        _timer += Time.deltaTime;
        if (_timer > _duration && MessageBoxGo.activeInHierarchy)
        {
            MessageBoxGo.SetActive(false);
        }
    }

    public void ShowMessage(string message, float duration = 3f)
    {
        MessageBoxGo.SetActive(true);
        MessageText.text = message;
        _duration = duration;
        _timer = 0f;
    }

    public void Close()
    {
        MessageBoxGo.SetActive(false);
    }

}
