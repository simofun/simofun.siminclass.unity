﻿namespace Simofun.Unity.Presentation
{
	using UnityEngine;

	public class BodyHighlighter
	{
		#region Fields
		const float highlightColorBoost = 1.45f;

		readonly Color highlightColorYellow = new Color(1, 1, 0, 0.3f);

		readonly Color highlightColorGreen = new Color(0, 1, 0, 0.3f);

		readonly int highlightWidth = Shader.PropertyToID("_Outline");

		readonly int highlightColor = Shader.PropertyToID("_OutlineColor");

		readonly int highlightColorBoostID;

		readonly Renderer renderer;

		readonly Shader highlightShader;

		readonly Shader standartShader;
		#endregion

		#region Constructors
		public BodyHighlighter(Renderer renderer, Shader highlightShader, Shader standardShader)
		{
			this.renderer = renderer;
			this.highlightShader = highlightShader;
			this.standartShader = standardShader;

			// To force the outline color to yellow initially
			this.IsHighlighted = true;
			this.SetStandardColor();
			this.IsHighlighted = false;
			this.highlightColorBoostID = Shader.PropertyToID("_ColorBoost");
		}
		#endregion

		#region Properties
		public bool IsHighlighted
		{
			get => this.renderer.material.shader == this.highlightShader;
			set
			{
				foreach (var material in this.renderer.materials)
				{
					material.shader = value ? this.highlightShader : this.standartShader;

					if (value)
					{
						material.SetFloat(this.highlightColorBoostID, highlightColorBoost);
					}
				}
			}
		}

		public float ThicknessPercent
		{
			get => this.renderer.material.GetFloat(this.highlightWidth);

			set
			{
				foreach (var material in this.renderer.materials)
				{
					material.SetFloat(this.highlightWidth, value);
				}
			}
		}
		#endregion

		#region Public Methods
		public void SetHighlightColor()
		{
			foreach (var material in this.renderer.materials)
			{
				material.SetColor(this.highlightColor, this.highlightColorGreen);
			}
		}

		public void SetStandardColor()
		{
			foreach (var material in this.renderer.materials)
			{
				material.SetColor(this.highlightColor, this.highlightColorYellow);
			}
		}
		#endregion
	}
}
