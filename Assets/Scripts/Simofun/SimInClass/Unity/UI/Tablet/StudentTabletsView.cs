﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.UI;
using Sinifta.Interactables.TabletApps;
using KarmaFramework.Interactables.Apps;
using Sinifta.Actions.StudentActions;
using Sinifta.Actors;
using Simofun.Karma.UniRx.Unity;
using UniRx;
namespace Sinifta.UI.TabletUI
{
    public class StudentTabletsView : TabletAppView
    {
        private StudentTabletsApp _viewModel;
        private Transform _maskWrapper;
        private Transform _imageHolder;
        private Transform _scrollBar;
        private CompositeDisposable disposables = new CompositeDisposable();
        private Texture2D _sentTabletImage;
        private int _currentView = -1;

        public List<Sprite> misbehaviorImages;
        public Sprite outOfBatteryImage;
        public Sprite normalImage;


        private int _rowCount, _minHeight, _maskHeight, _maskWidth, _deltaHeight;
        public void Awake()
        {
            _maskWrapper = transform.Find("Wrapper");
            _imageHolder = transform.Find("Wrapper/ImageHolder");
            _scrollBar = transform.Find("ImageGalleryScrollbar");
            KarmaMessageBus.OnEvent<SendImageToTabletEvent>().Subscribe(ev =>
            {
                _sentTabletImage = ev.Image;

            }).AddTo(this.disposables);
            gameObject.SetActive(false);
        }
        private void OnDestroy() => disposables.Dispose();
      
        public override void Draw(GadgetApplication peek)
        {
            base.Draw(peek);
            _viewModel = peek as StudentTabletsApp;
            Redraw();
        }

        public override void Back()
        {
            if (_currentView != -1)
            {
                _currentView = -1;
                Redraw();
            }
            else
            {
                Close();
            }
        }

        public void Redraw()
        {
            foreach (Transform t in _imageHolder)
            {
                Destroy(t.gameObject);
            }
            var dict = _viewModel.StudentBehaviour;
            var layout = _imageHolder.GetComponent<GridLayoutGroup>();
            _maskWidth = (int)_maskWrapper.GetComponent<RectTransform>().rect.width;
            _maskHeight = (int)_maskWrapper.GetComponent<RectTransform>().rect.height;
            _rowCount = Mathf.CeilToInt(dict.Count / layout.constraintCount);
            layout.cellSize = new Vector2(((7f * 1.3f) / 23f) * _maskWidth, ((5f * 1.3f) / 23f) * _maskWidth);
            layout.spacing = layout.spacing = new Vector2((1f * 1.3f / 23f) * _maskWidth, (1f * 1.3f / 23f) * _maskWidth);
            
            _minHeight = (int)(_rowCount * (layout.cellSize.y + layout.spacing.y));
            _deltaHeight = _minHeight - _maskHeight;
            _imageHolder.GetComponent<RectTransform>().offsetMax = Vector2.zero;
            _imageHolder.GetComponent<RectTransform>().offsetMin = Vector2.up * -_deltaHeight;
            
            
            _scrollBar.GetComponent<Scrollbar>().size = (float)_maskHeight / _minHeight;
            
            foreach (var pair in dict)
            {
                var preview = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Tablet/TabletPreview"));
            
                // Populate text etc.
                var textField = preview.transform.Find("Text").GetComponent<Text>();
                var tabletImage = preview.transform.Find("Frame/Content").GetComponent<Image>();
                var frame = preview.transform.Find("Frame");
                textField.text = pair.Key.Name;
                if (pair.Value.Any(c => c is IdleSurfOnInternet))
                {
                    tabletImage.sprite = misbehaviorImages[(int)(UnityEngine.Random.value * misbehaviorImages.Count)];
                }
                else
                {
                    Sprite sprite = null;
                    if(_sentTabletImage !=null)
                    {
                        var rect = new Rect(0, 0, _sentTabletImage.width, _sentTabletImage.height);
                        var pivot = new Vector2(0.5f, 0.5f);
                        sprite = Sprite.Create(_sentTabletImage, rect, pivot);
                    }
            
                    tabletImage.sprite = sprite ?? normalImage;
                }
            
                textField.GetComponent<RectTransform>().offsetMax = new Vector2(0, (0.25f * layout.cellSize.y));
                textField.GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
            
                frame.GetComponent<RectTransform>().offsetMax = (-0.04f * layout.cellSize);
                frame.GetComponent<RectTransform>().offsetMin = new Vector2((0.04f * layout.cellSize.x), (0.25f * layout.cellSize.y));
            
                preview.transform.SetParent(_imageHolder.transform);
                preview.transform.localScale = Vector3.one;
                LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)layout.transform);
            }
        }
    }
}