﻿namespace Sinifta.UI.TabletUI
{
	using KarmaFramework.Interactables.Apps;
	using KarmaFramework.Localization;
	using Simofun.SimInClass.Unity.UI;
	using Sinifta.Interactables.TabletApps;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.UI;
	using static Simofun.SimInClass.Unity.UI.SiniftaNotification;

	public class ImageGalleryToStudentAppView : TabletAppView
	{
		private Transform _imageHolder;
		private ImageGalleryToStudentApp _viewModel;
		private int _currentView = -1;

		public void Awake()
		{
			_imageHolder = transform.Find("Wrapper/ImageStudentHolder");
		}

		public override void Draw(GadgetApplication peek)
		{
			base.Draw(peek);
			_viewModel = peek as ImageGalleryToStudentApp;
			Redraw();
		}

		public override void Back()
		{
			if (_currentView != -1)
			{
				_currentView = -1;
				Redraw();
			}
			else
			{
				Close();
			}
		}

		public void Redraw()
		{
			foreach (Transform t in _imageHolder)
			{
				Destroy(t.gameObject);
			}
			if (_currentView == -1)
			{
				GadgetButton[] folderButtons = _viewModel.GetFolderButtons().ToArray();
				for (int i = 0; i < folderButtons.Length; i++)
				{
					int i1 = i;
					var folderContentButtons = _viewModel.GetFolderContent(i1);
					if (folderContentButtons.Any())
					{
						var button = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Tablet/FolderIcon"));
						if (folderButtons[i].Name == "Pictures")
							button.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("Images");
						else if (folderButtons[i].Name == "Documents")
							button.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("DocumentGallery");
						else
							button.GetComponentInChildren<Text>().text = folderButtons[i].Name;
						button.transform.SetParent(_imageHolder, false);
						button.GetComponent<Button>().onClick.AddListener(() =>
						{
							_currentView = i1;
							Redraw();
						});
					}
				}
			}
			else
			{
				var buttons = _viewModel.GetFolderContent(_currentView);
				foreach (var b in buttons)
				{
					var button = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Tablet/TabletIcon"));
					button.GetComponentInChildren<Image>().sprite = b.Icon;
					button.GetComponentInChildren<Image>().rectTransform.sizeDelta = Vector2.zero;
					button.GetComponentInChildren<Text>().text = b.Name;
					button.transform.SetParent(_imageHolder, false);
					var action = b.Action;
					button.GetComponent<Button>().onClick.AddListener(() => 
					{
						action();
						SiniftaNotification.Instance.Show(b.Name, NotificationType.Tablet.ToString());
					});
				}
			}
		}
	}
}
