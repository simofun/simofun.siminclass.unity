﻿using KarmaFramework.Interactables.Apps;
using UnityEngine;

namespace Sinifta.UI.TabletUI
{
    public class TabletAppView : MonoBehaviour
    {
        public virtual void Draw(GadgetApplication peek)
        {
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }

        public virtual void Back()
        {

        }
    }
}
