﻿using KarmaFramework.Interactables.Apps;
using KarmaFramework.Localization;
using Sinifta.Interactables.TabletApps;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI.TabletUI
{
    public class TabletOperatingSystemView : TabletAppView
    {
        private Transform _appHolder;
        private TabletOperatingSystemApp _viewModel;

        private TabletAppView _currentApp;

        public override void Back()
        {
            _currentApp?.Back();
        }

        public void Home()
        {
            _currentApp?.Close();
        }

        public override void Draw(GadgetApplication peek)
        {
            base.Draw(peek);
            _appHolder = transform.Find("TabletOsAppHolder");
            _viewModel = peek as TabletOperatingSystemApp;

            var galleryView = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/AppPrefabs/ImageGalleryToStudentAppPrefab"), transform).GetComponent<ImageGalleryToStudentAppView>();
            var gallery = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Tablet/TabletIcon"), _appHolder);
            gallery.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("StudentGallery");
            gallery.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Icons/AppIcons/FileBrowser");
            gallery.GetComponent<Button>().onClick.AddListener(() =>
            {
                _currentApp = galleryView;
                galleryView.Draw(_viewModel.ImageGalleryToStudentApp);
            });

            var stabletsView = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/AppPrefabs/StudentTabletsAppPrefab"), transform).GetComponent<StudentTabletsView>();
            var stablet = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Tablet/TabletIcon"), _appHolder);
            stablet.GetComponentInChildren<Text>().text = Localizer.Instance.GetString("StudentTablets");
            stablet.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Icons/AppIcons/Gallery");
            stablet.GetComponent<Button>().onClick.AddListener(() =>
            {
                _currentApp = stabletsView;
                stabletsView.Draw(_viewModel.StudentTabletsApp);
            });
        }
    }
}