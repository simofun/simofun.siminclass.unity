﻿using Simofun.Karma.UniRx.Unity;
using Sinifta.Interactables;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI.TabletUI
{
	public class TabletView : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		Transform _appHolder;

		[SerializeField]
		Button _close;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		Tablet tablet;

		Transform backButton;

		Transform homeButton;
		#endregion

		#region Unity Methods
		protected virtual void Awake()
		{
			_close.onClick.AddListener(() => gameObject.SetActive(false));
			backButton = transform.Find("TabletControlButtons/TabletBackButton");
			homeButton = transform.Find("TabletControlButtons/TabletHomeButton");
		}
		#endregion

		public void Bind(Tablet tablet)
		{
			gameObject.SetActive(false);
			this.tablet = tablet;

			var appView = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/AppPrefabs/TabletOperatingSystemAppPrefab"), _appHolder);
			var os = appView.GetComponent<TabletOperatingSystemView>();
			os.Draw(tablet.App);
			backButton.GetComponent<Button>().onClick.AddListener(() => os.Back());
			homeButton.GetComponent<Button>().onClick.AddListener(() => os.Home());
			
			KarmaMessageBus.OnEvent<OpenTabletEvent>()
				.Subscribe(ev => this.gameObject.SetActive(ev.State))
				.AddTo(this.disposables);
		}

        private void OnDestroy()
        {
			disposables.Dispose();
        }
    }
}
