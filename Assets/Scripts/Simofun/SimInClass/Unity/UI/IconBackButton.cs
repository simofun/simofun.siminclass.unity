using DG.Tweening;
using KarmaFramework.Localization;
using KarmaFramework.ViewHelpers;
using KarmaFramework.Views;
using Simofun.Karma.UniRx.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Simofun.SimInClass.Unity.UI
{

	public class IconBackButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerEnterHandler,IPointerExitHandler
	{
	   
		[SerializeField]
		RectTransform iconBack;

		[SerializeField]
		RectTransform removeButton;

		[SerializeField]
		GameObject tooltip;

		[SerializeField]
		TMP_Text tooltipHeader;

		[Header("Animation Settings")]

		[SerializeField]
		Vector3 rotationDirection;

		[SerializeField]
		float duration;

		Sequence sequence;
		EventTrigger removeEvent;
		FlowElementView flowElement;

		float time;
		float pressingTime = 1;
		bool buttonPressed = false;
	  
		#region Unity Methods
		private void Start()
		{
			time = pressingTime;
			flowElement = GetComponentInParent<FlowElementView>();
#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
			BindMobileRemoveEvent();
#endif
#if UNITY_STANDALONE ||  UNITY_EDITOR
			BindStandaloneRemoveEvent();
#endif
		}
		private void Update()
		{
			if (buttonPressed)
			{
				time -= Time.deltaTime;
			}
			if (time < 0.1)
			{
				ButtonRemoveableAnimation();
			}
		}
		#endregion

		#region Standalone
		void BindStandaloneRemoveEvent()
		{
			removeEvent = iconBack.GetComponent<EventTrigger>();
			var clickEntry = new EventTrigger.Entry();
			clickEntry.eventID = EventTriggerType.PointerDown;
			clickEntry.callback.AddListener((eventData) =>
			{
				if ((eventData as PointerEventData).button == PointerEventData.InputButton.Right)
				{
					tooltip.SetActive(false);
					KarmaMessageBus.Publish(new RemoveLectureEvent{ Element = flowElement.Element });
				}
			});
			removeEvent.triggers.Add(clickEntry);
			tooltipHeader.text = Localizer.Instance.GetString(flowElement.Element.Action.ActionStringIdString);
		}
		#endregion

		#region Mobile 
		void BindMobileRemoveEvent()
		{
			var removeBtn  = removeButton.GetComponent<Button>();
			removeBtn.onClick.AddListener(() =>
			{
				KarmaMessageBus.Publish(new RemoveLectureEvent { Element = flowElement.Element });
			});
		}
		void ButtonRemoveableAnimation()
		{
			if(sequence == null)
			{
				sequence = DG.Tweening.DOTween.Sequence();
			}
			if (!sequence.IsPlaying())
			{
				sequence = DG.Tweening.DOTween.Sequence();
				sequence.Append(iconBack.DOLocalRotate(-rotationDirection, duration, RotateMode.Fast));
				sequence.Append(iconBack.DOLocalRotate(rotationDirection, duration, RotateMode.Fast));
				sequence.SetLoops(5, LoopType.Restart);
				removeButton.DOSizeDelta(new Vector2(40, 40), duration);
			}
		}
		void ResetRemoveButton()
		{
			sequence.Kill();
			iconBack.eulerAngles = new Vector3(0, 0, 0);
			removeButton.DOSizeDelta(new Vector2(0, 0), duration);
		}
		public void OnPointerDown(PointerEventData eventData)
		{
			ResetRemoveButton();
			time = pressingTime;
			buttonPressed = true;
			
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			buttonPressed = false;  
		}
		public void OnPointerEnter(PointerEventData eventData)
		{
			tooltip.SetActive(true);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			tooltip.SetActive(false);
		}
		#endregion
	}
}
