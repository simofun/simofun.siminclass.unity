using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Simofun.SimInClass.Unity.UI
{
    public class InformationPanel : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField]
        Button CloseButton;

        private void Start()
        {
            CloseButton.onClick.AddListener(CloseInformation);
        }
        public void OpenInformation()
        {
            gameObject.SetActive(true);
        }

        void CloseInformation()
        {
            gameObject.SetActive(false);
        }
    }
}