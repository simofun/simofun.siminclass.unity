﻿using KarmaFramework.ScenarioCore;
using Sinifta.Actions.TeacherActions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Sinifta.UI
{
    [RequireComponent(typeof(ZenjectBinding))]
    public class LessonFlow : MonoBehaviour
    {
        public GameObject LessonFlowElementPrefab;
        public Transform LessonFlowElementHolder;
        public LessonDropTrashHandler TrashHandler;
        public GameObject DragTint;
        public List<Transform> SectionHolders = new List<Transform>();
        public List<DropHelper> DropHelpers = new List<DropHelper>();

        private bool _isHelpersEnabled;
        private float _timelineLength;

        public static float ScaleFactor;
        private GameState _gameState;
        public const float TimelineLength = 240;

        public float UnitLength
        {
            get
            {
                return _timelineLength / 240f; //why 240 ?
            }
        }

        [Inject]
        public void Construct(GameState gameState)
        {
            _gameState = gameState;
        }

        public void Start()
        {
            var canvasScaler = GetComponentInParent<CanvasScaler>();
            var r = LessonFlowElementHolder.GetComponent<RectTransform>();
            ScaleFactor = Screen.width / canvasScaler.referenceResolution.x;
            _timelineLength = r.rect.size.x - 24; //24 padding
            transform.GetComponent<RectTransform>().localScale = Vector3.one / ScaleFactor;
            UpdateFlow(); //gotta need to update flow elements
            r.localScale = Vector3.one;
        }

        public void Init()
        {
            GetAllScenarioLessons();
        }

        private void GetAllScenarioLessons()
        {
            var elements = GetComponentsInChildren<LessonFlowElement>().ToList();
            foreach (Transform t in elements.Select(x => x.transform))
            {
                Destroy(t.gameObject);
            }

            if (_gameState == null)
            {
                return;
            }

            List<FlowElement> flowEntryList = new List<FlowElement>();

            foreach (var lectureAction in _gameState.Scenario.FlowElements)
            {
                var entry = new FlowElement(lectureAction.Action, lectureAction.Trigger);
                flowEntryList.Add(entry);
            }

            var orderedlist = flowEntryList.OrderBy(entry => (entry.Trigger as TimeTrigger).Time);
            foreach (var flowEntryLesson in orderedlist)
            {
                CreateLessonFlowElement(flowEntryLesson);
            }

            UpdateFlow();
        }

        public void SetDropHelpers(bool state)
        {
            _isHelpersEnabled = state;
            UpdateHelpers();
        }

        private void UpdateHelpers()
        {
            for (int i = 0; i < SectionHolders.Count; i++)
            {
                DropHelpers[i].gameObject.SetActive(_isHelpersEnabled);
            }
        }

        public void UpdateFlow() //normalize element accordingto their time limits
        {
            foreach (var element in LessonFlowElementHolder.GetComponentsInChildren<LessonFlowElement>())
            {
                element.UpdateView(UnitLength);
                var index =
                    _gameState.Scenario.FlowElements.Select(x => x.Action)
                        .ToList()
                        .IndexOf(element.Lesson.Action);
                (element.Lesson.Trigger as TimeTrigger).Time = _gameState.Scenario.FlowElements.Take(index).Sum(tuple => tuple.Action.Duration);
            }
            UpdateHelpers();
        }

        public void CreateLessonFlowElement(FlowElement flowEntryLesson, int insertIndex = -1) //setPosition According to start mid or end Section - Ders akışı için ders yaratma(tamamlayıcı fonksiyon)
        {
            var go = Instantiate(LessonFlowElementPrefab);

            switch (((TeacherLecture)flowEntryLesson.Action).LessonPlanSlot) //logic update also
            {
                case LessonPlanSlot.None:
                    break;
                case LessonPlanSlot.Introduction:
                    go.transform.SetParent(SectionHolders[0]);
                    break;
                case LessonPlanSlot.Learning_TeachingProcess:
                    go.transform.SetParent(SectionHolders[1]);
                    break;
                case LessonPlanSlot.MeasurementAndEvaluation:
                    go.transform.SetParent(SectionHolders[2]);
                    break;
            }


            go.transform.SetSiblingIndex(insertIndex);

            var element = go.GetComponent<LessonFlowElement>();
            if (element != null)
            {
                element.Init(flowEntryLesson); //set icon and title, random color thing also 
            }
        }

        public void ClearFlow()
        {
            _gameState.Scenario.FlowElements.Clear();

            var elements = GetComponentsInChildren<LessonFlowElement>().ToList();
            foreach (Transform t in elements.Select(x => x.transform))
            {
                Destroy(t.gameObject);
            }
        }

    }
}
