﻿namespace Sinifta.UI
{
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using UnityEngine;
	using UnityEngine.EventSystems;

	public class DragHandler : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
	{
		public static GameObject ItemBeingDragged;
		private GameObject _fakeButton;
		private RectTransform canvas;

		public GameObject FlowEntry;
		public GameObject MethodDetailButton;

		public void OnBeginDrag(PointerEventData eventData)
		{
			ItemBeingDragged = gameObject;

			canvas = (RectTransform)GameObject.Find("MainMenuView").transform;

			// TODO CEMRE: Bumsen Sie geschafen machen
			if (ItemBeingDragged.GetComponent<MethodParameterButtonView>() != null)
			{
				_fakeButton = Instantiate(MethodDetailButton, canvas);

				var view = _fakeButton.GetComponent<MethodParameterButtonView>();
				view.Init(ItemBeingDragged.GetComponent<MethodParameterButtonView>().FlowEntryLesson);
				view.BackgroundImage = ItemBeingDragged.GetComponent<MethodParameterButtonView>().BackgroundImage;
				view.IsSeeMoreButtonDisabled = true;
				view.imageRequiredButton.SetActive(false);
				view.SetContentLesson(ItemBeingDragged.GetComponent<MethodParameterButtonView>().ContentText);
			}
			else if (ItemBeingDragged.GetComponent<FlowEntryLessonView>() != null)
			{
				_fakeButton = Instantiate(FlowEntry, canvas);

				var view = _fakeButton.GetComponent<FlowEntryLessonView>();
				view.Init(ItemBeingDragged.GetComponent<FlowEntryLessonView>().FlowEntryLesson);
				view.SetViewElement();
				ItemBeingDragged.transform.GetChild(1).gameObject.SetActive(false);

			}

			_fakeButton.GetComponent<RectTransform>().sizeDelta = ItemBeingDragged.GetComponent<RectTransform>().sizeDelta;
			_fakeButton.GetComponent<CanvasGroup>().blocksRaycasts = false;

		}
		public void OnDrag(PointerEventData eventData)
		{
			_fakeButton.transform.position = Input.mousePosition;
			/*Vector2 localPos;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas, Input.mousePosition, Camera.main, out localPos);
			_fakeButton.transform.localPosition = localPos;*/
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			ItemBeingDragged.transform.GetChild(1).gameObject.SetActive(true);
			Destroy(_fakeButton);
			ItemBeingDragged = null;
			KarmaMessageBus.Publish(new DragLessonButtonGameEvent());
		}
	}
}
