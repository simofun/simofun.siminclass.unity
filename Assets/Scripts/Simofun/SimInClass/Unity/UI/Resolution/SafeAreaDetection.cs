﻿using UnityEngine;
/// <summary>
/// TODO SEDA : Bu script iphone çentikli ekranlı cihazları için oluşturuldu
/// </summary>
namespace Sinifta.UI
{
    public class SafeAreaDetection : MonoBehaviour
    {
        public delegate void SafeAreaChanged(Rect safeArea);
        public static event SafeAreaChanged OnSafeAreaChanged;
        private Rect _safeArea;
        private ScreenOrientation _currentOrientation = ScreenOrientation.AutoRotation;
        void Awake()
        {
            _safeArea = Screen.safeArea;
            _currentOrientation = Screen.orientation;

        }
        void Update()
        {
            if (_safeArea != Screen.safeArea || _currentOrientation != Screen.orientation)
            {
                _safeArea = Screen.safeArea;
                _currentOrientation = Screen.orientation;
                OnSafeAreaChanged?.Invoke(_safeArea);
            }
        }
    }
}