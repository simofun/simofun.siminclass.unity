﻿namespace Sinifta.UI
{
	using KarmaFramework.Interactables.Views;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using UnityEngine;
	using UnityEngine.UI;

	public class InteractableOpenPanel : MonoBehaviour
	{
		[SerializeField]
		private Button _tablet;
		[SerializeField]
		private Button _board;
		[SerializeField]
		private Button _activity;

		void Start()
		{
			_tablet.onClick.AddListener(() =>
			{
				KarmaMessageBus.Publish(new OpenInteractableEvent { Id = "Tablet" });
				KarmaMessageBus.Publish(new TabletGameEvent());
			});
			_board.onClick.AddListener(() =>
			{
				KarmaMessageBus.Publish(new SmartboardGameEvent() 
				{
					LookToSmartBoard = () => { KarmaMessageBus.Publish(new OpenInteractableEvent { Id = "SmartBoard" }); }
			    });
			});
			_activity.onClick.AddListener(() => 
			{
				KarmaMessageBus.Publish(new OpenInteractableEvent { Id = "Activity" });
				//KarmaMessageBus.Publish(new ActivityGameEvent());
			});
		}
	}
}
