﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Sinifta.UI
{

    [RequireComponent(typeof(ZenjectBinding))]
    public class LessonDropTrashHandler : DropHandler {
        private GameState _gameState;

        [Inject]
        public void Construct(GameState gameState)
        {
            _gameState = gameState;
        }

        public override void OnDrop(PointerEventData eventData)
        {
            base.OnDrop(eventData);
            var lessonFlowElement = LessonDragHandler.DragedGameObject.GetComponent<LessonFlowElement>();
            if (lessonFlowElement != null)
            {
               TrashLessonFlowElement(lessonFlowElement);
            }
            LessonPlanPanel.PlayButtonClick(false);
        }

        private void TrashLessonFlowElement(LessonFlowElement lessonFlowElement)
        {
            Destroy(lessonFlowElement.gameObject);
            var action = _gameState.Scenario.FlowElements.First(x => x.Action.Id == lessonFlowElement.Lesson.Action.Id);
            if (action != null)
            {
                _gameState.Scenario.FlowElements.Remove(action);
            }
        }
    }
}