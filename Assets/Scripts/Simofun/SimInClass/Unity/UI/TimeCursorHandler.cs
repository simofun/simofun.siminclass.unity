﻿using Simofun.Karma.UniRx.Unity;
using Simofun.SimInClass.Unity.Events;
using Sinifta.Actions.TeacherActions;
using Sinifta.UI;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

[RequireComponent(typeof(ZenjectBinding))]
public class TimeCursorHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RectTransform _rect;
    private GameState _gameState;

    [Inject]
    public void Construct(GameState gameState)
    {
        _gameState = gameState;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (TimeHandler.SelectedElement != null)
        {
            _rect = TimeHandler.SelectedElement.GetComponent<RectTransform>();
        }

        KarmaMessageBus.Publish(new ChangeLessonTimeGameEvent());
    }

    public void OnDrag(PointerEventData eventData)
    {
       
        if (_rect == null)
        {
            return;
        }

        var a = ((TeacherLecture)TimeHandler.SelectedElement.Lesson.Action);
        var lessonFlowView = FindObjectOfType<LessonFlow>();
        if (lessonFlowView != null)
        {
            var newDuration = (eventData.position.x - (_rect.rect.x + _rect.transform.position.x)) / lessonFlowView.UnitLength;
           // Debug.LogError(a.Id);
            CalculateDuration(ref newDuration, a.Id);
            a.Duration = newDuration;
            lessonFlowView.UpdateFlow();
            transform.position = new Vector3(a.Duration * lessonFlowView.UnitLength * ((LessonFlow.ScaleFactor - 1) / 2f + 1) + _rect.transform.position.x + _rect.rect.x, transform.position.y, transform.position.z); //adaptive scaling for scanvas scale
        }
    }

    private void CalculateDuration(ref float newDuration, Guid id) //clamp to min 5 minutes don't allow drop in there is no time
    {
        var lessonPanel = FindObjectOfType<LessonPlanPanel>();
        if (lessonPanel != null)
        {
            var actions = _gameState.Scenario.FlowElements.Select(x => x.Action).ToList();
            //var actions = lessonPanel.FlowElements.Select(x => x.Action).ToList();
            //var actions = InterLevelData.EditorGameState.Scenario.Flow.LectureActions.SelectMany(x => x.Actions).ToList();
            var totalDuration = actions.Where(x => x.Id != id).Sum(x => x.Duration);

            if (totalDuration + newDuration > 240)
            {
                newDuration = 240 - totalDuration;
                //setCursorPos
            }
            else if (newDuration < 12) // min 2 minute which equals to 12 time
            {
                newDuration = 12;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }
}
