﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Sinifta.UI
{
    public class DropHelper : MonoBehaviour, IDropHandler
    {
        public LessonDropHandler RelatedHandler;

        public void Init(LessonDropHandler relatedHandler)
        {
            RelatedHandler = relatedHandler;
        }

        public void OnDrop(PointerEventData eventData)
        {
            RelatedHandler.OnDrop(eventData);
        }
    }
}
