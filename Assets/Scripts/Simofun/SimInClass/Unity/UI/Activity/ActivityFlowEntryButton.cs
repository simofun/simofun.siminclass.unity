using TMPro;

namespace Sinifta.UI.ActivityUI
{
	using KarmaFramework.Localization;
	using KarmaFramework.ScenarioCore;
	using Simofun.Karma.UniRx.Unity;
	using UnityEngine;
	using UnityEngine.UI;

	public class ActivityFlowEntryButton : MonoBehaviour
	{
		#region Unity Fields
		[Header("References")]
		[SerializeField]
		TMP_Text header;

		[SerializeField]
		Image icon;
		#endregion

		#region Fields
		FlowElement flowElement;
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start() =>
			this.GetComponent<Button>().onClick.AddListener(this.SetActivityDetailContent);
		#endregion

		#region Public Methods
		public void SetContent(FlowElement flowElement)
		{
			this.flowElement = flowElement;
			this.header.text = Localizer.Instance.GetString(flowElement.Action.Name);
			this.icon.sprite = Resources.Load<Sprite>("Icons/Lecture/" + flowElement.Action.Icon);
		}
		#endregion

		#region Methods
		void SetActivityDetailContent() =>
			KarmaMessageBus.Publish(new ActivityDetailButtonContent() { ActivityName = this.flowElement });
		#endregion
	}
}
