﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using KarmaFramework.ScenarioCore;
using Zenject;
using Sinifta.Interactables;
using System.Collections.Generic;
using System.Linq;
using KarmaFramework.Views;
using KarmaFramework.ActionCore;
using Simofun.Karma.UniRx.Unity;
using Simofun.SimInClass.Unity.Tutorial.Actions;

namespace Sinifta.UI.ActivityUI
{
    public class ActivityDetailButton : MonoBehaviour
    {
        [HideInInspector]
        public BaseAction ClonedAction;

        [SerializeField]
        TMP_Text text;

        Button button;
        FlowView flowView;
     

        private void Start()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(AddFlowElement);
            flowView = FindObjectOfType<FlowView>();
        }

        public void SetText()
        {
            text.text = ClonedAction.Text;
        }

        void AddFlowElement()
        {
            var time = new TimeTrigger();
            KarmaMessageBus.Publish(new TutorialAddLessonEvent());
            KarmaMessageBus.Publish(new OpenActivityEvent() { State = false });
            flowView.AddFlowElement(new FlowElement(ClonedAction, ClonedAction.Name, time));
        }
    }
}
