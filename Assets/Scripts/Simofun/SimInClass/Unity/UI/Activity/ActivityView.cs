﻿using UniRx;

namespace Sinifta.UI.ActivityUI
{
	using KarmaFramework.ActionCore;
	using KarmaFramework.Localization;
	using KarmaFramework.ScenarioCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Sinifta.Interactables;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.UI;
	using Zenject;

	public class ActivityView : MonoBehaviour
	{
		#region Unity Fields
		[Header("References")]
		[SerializeField]
		Activity activity;

		[SerializeField]
		Button CloseButton;

		[SerializeField]
		Text titleText;

		[SerializeField]
		RectTransform WrapperContent;

		[SerializeField]
		RectTransform WrapperContentRight;

		[SerializeField]
		Button ActivityDetailButton;

		[SerializeField]
		Button ActivityFlowEntryPrefab;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		readonly DiContainer container = new DiContainer();

		readonly List<FlowElement> allFlowEntryList = new List<FlowElement>();
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start()
		{
			this.CloseButton.onClick.AddListener(() =>
				KarmaMessageBus.Publish(new OpenActivityEvent() { State = false }));
			KarmaMessageBus.OnEvent<ActivityDetailButtonContent>().Subscribe(ev => 
			{
				this.ClearWrapperRightContent();
				this.SetActivityDetailContent(ev.ActivityName);
			}).AddTo(this.disposables);
		}

		/// <inheritdoc />
		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion

		#region Public Methods
		public void Bind(Activity activity)
		{
			KarmaMessageBus.OnEvent<OpenActivityEvent>()
				.Subscribe(ev => this.gameObject.SetActive(ev.State))
				.AddTo(this.disposables);

			this.activity = activity;
			this.gameObject.SetActive(false);
			this.SetActivityContent();
		}
		#endregion

		#region Methods
		void ClearWrapperRightContent()
		{
			var rectChildren = this.WrapperContentRight.GetComponentsInChildren<RectTransform>();
			for (var i = 1; i < rectChildren.Length; i++)
			{
				Destroy(rectChildren[i].gameObject);
			}
		}

		void SetActivityContent()
		{
			// TODO: [Seda] -> PanelActivity'de sol bölümde yer alan ana ders başlıkları
			this.allFlowEntryList.Clear();

			this.titleText.text = this.activity.gameState.Scenario.ReadableName
				+ " - "
				+ Localizer.Instance.GetString("Activity");

			// High iq move
			this.activity.gameState.Scenario.AvailableActions
				.GroupBy(a => a.GetType())
				.Select(g => g.First())
				.ToList()
				.ForEach(a => this.allFlowEntryList.Add(new FlowElement(a)));

			this.allFlowEntryList.ForEach(f =>
				Instantiate(this.ActivityFlowEntryPrefab, this.WrapperContent)
					.GetComponent<ActivityFlowEntryButton>()
					.SetContent(f));
		}

		void SetActivityDetailContent(FlowElement detailContent)
		{
			var actions = new List<BaseAction>();
			foreach (var action in this.activity.gameState.Scenario.AvailableActions)
			{
				if (action.GetType() == detailContent.Action.GetType())
				{
					actions.Add(action);
				}
			}

			foreach (var item in actions)
			{
				var detailBtnComp = this.container.InstantiatePrefabForComponent<ActivityDetailButton>(
					this.ActivityDetailButton,
					this.WrapperContentRight);
				detailBtnComp.ClonedAction = item.Clone();
				detailBtnComp.SetText();
			}
		}
		#endregion
	}

	public class ActivityDetailButtonContent : KarmaBaseUnityEvent
	{
		public FlowElement ActivityName;
	}
}
