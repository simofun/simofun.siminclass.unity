﻿using TMPro;
using UniRx;

namespace Sinifta.UI
{
	using DG.Tweening;
	using KarmaFramework.Reports;
	using KarmaFramework.Views;
	using Simofun.Karma.UniRx.Unity;
	using Sinifta.Actions.TeacherActions;
	using Sirenix.OdinInspector;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

	public class MissionPopupView : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField, Title(nameof(MissionPopupView), "References")]
		TMP_Text _popupText;

		[SerializeField]
		TMP_Text _popupTitle;

		[SerializeField]
		Button _actionButton;

		[SerializeField]
		Text _buttonLabel;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new();
		
		readonly List<Sequence> sequences = new();

		readonly List<Tween> tweens = new();

		Outline outline;

		Sequence sequence;

		Transform targetParent;
		#endregion

		#region Properties
		public string Text { get => this._popupText.text; set => this._popupText.text = value; }

		public string Title { get => this._popupTitle.text; set => this._popupTitle.text = value; }
		#endregion

		#region Protected Properties
		protected Outline Outline
		{
			get
			{
				if (this.outline != null)
				{
					return this.outline;
				}

				if (this.TryGetComponent(out this.outline))
				{
					this.outline.enabled = false;
				}

				return this.outline;
			}

			set => this.outline = value;
		}
		#endregion

		#region Unity Methods        
		/// <inheritdoc />
		protected virtual void Awake()
		{
			this.gameObject.SetActive(false);

			KarmaMessageBus.OnEvent<ImageRequirementEvent>().Subscribe(ev =>
			{
				this.Title = ev.Title;
				this.Text = ev.Text;
				this.Show();
			}).AddTo(this.disposables);
			KarmaMessageBus.OnEvent<ImageRequirementSuccessfulEvent>()
				.Subscribe(ev => this.CloseElement())
				.AddTo(this.disposables);
			KarmaMessageBus.OnEvent<MissionActionButtonEvent>().Subscribe(ev =>
			{
				if (ev.State)
				{
					this.ShowActionButton();

					return;
				}

				KarmaMessageBus.Publish(new MissionRecord { Success = false });
				// ev.isClicked = false;
				this.CloseElement();
				this.HideActionButton();
			}).AddTo(this.disposables);
			this._actionButton.onClick.AddListener(() =>
			{
				KarmaMessageBus.Publish(new MissionActionButtonEvent { State = false, isClicked = true });
				KarmaMessageBus.Publish(new MissionActionButtonClicked { IsClicked = true });
				KarmaMessageBus.Publish(new LectureUpdateForMission());
			});
		}

		/// <inheritdoc />
		protected virtual void OnDisable() => this.sequence?.Kill(true);

		/// <inheritdoc />
		protected virtual void OnDestroy()
		{
			this.sequences.ForEach(s => s.Kill());
			this.tweens.ForEach(t => t.Kill());
			this.disposables.Dispose();
		}
		#endregion

		#region Public Methods
		public void Animate() => this.ScaleUp();

		public void CloseElement()
		{
			// this._actionButton.onClick.RemoveAllListeners();
			this.gameObject.SetActive(false);
		}

		public void HideActionButton() => this._actionButton.gameObject.SetActive(false);

		public void SetTarget(Transform game) => this.targetParent = game;

		public void Show()
		{
			this.gameObject.SetActive(true);
			this.ScaleUp();
			this._actionButton.gameObject.SetActive(false);
		}

		public void ShowActionButton()
		{
			KarmaMessageBus.Publish(new MissionActionButtonClicked { IsClicked = false });
			this._actionButton.gameObject.SetActive(true);
		}
		#endregion

		#region Methods
		void Pan()
		{
			this.PlayOutlineEffect();
			
			if (this.targetParent == null || !this.TryGetComponent<RectTransform>(out var rectTransform))
			{
				return;
			}

			this.tweens.Add(
				rectTransform.transform.DOMoveX(this.targetParent.transform.position.x, 0.5f)
					.SetDelay(0.5f)
					.SetEase(Ease.Linear)
					.OnComplete(() =>
					{
						this.transform.SetParent(this.targetParent);
						this.PlayOutlineEffect();
					}));
		}

		void PlayOutlineEffect()
		{
			this.Outline.enabled = true;
			if (!this.Outline)
			{
				return;
			}

			this.sequence = DOTween.Sequence();
			this.sequence.AppendCallback(() =>
			{
				var scale = Vector2.zero;
				for (var i = 0; i < 2; ++i)
				{
					scale[i] = 3 * Mathf.Sin(Time.time * 3) + 1;
				}

				this.Outline.effectDistance = scale;
			});
			this.sequence.SetLoops(-1, LoopType.Restart);
			this.sequences.Add(this.sequence);
		}

		void ScaleUp()
		{
			this.transform.localScale = Vector3.zero;
			this.tweens.Add(this.transform.DOScale(1.0f, 1.5f).SetEase(Ease.OutBounce).OnComplete(() => this.Pan()));
		}
		#endregion
	}
}
