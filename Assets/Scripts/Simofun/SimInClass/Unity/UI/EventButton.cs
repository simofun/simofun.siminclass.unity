﻿namespace Sinifta.UI
{
	using KarmaFramework.ScenarioCore;
	using Sinifta.Actions.TeacherActions;
	using UnityEngine;
	using UnityEngine.EventSystems;
	using UnityEngine.UI;

	public class EventButton : MonoBehaviour, IBeginDragHandler
	{
		#region Unity Fields
		public Image Icon;

		public Text Title;

		public GameObject BoardEventGameObject;

		public GameObject TabletEventGameObject;

		public FlowElement EntryLesson;

		public Button Button;
		#endregion

		#region Fields
		Sprite highlightSprite;

		Sprite normalSprite;
		#endregion

		#region Public Methods
		#region Initialization
		public void Init(FlowElement entryLesson)
		{
			this.EntryLesson = entryLesson; // Handle later
			this.Button = this.GetComponent<Button>();

			var lecture = entryLesson.Action as TeacherLecture;
			//if (lecture != null)
			//{
			//    var techType = GetTechType(lecture.ImageRequirements);
			//    BoardEventGameObject.gameObject.SetActive(techType == TechType.SmartBoard || techType == TechType.Both);
			//    TabletEventGameObject.gameObject.SetActive(techType == TechType.Tablet || techType == TechType.Both);
			//}

			var mobileEventView = FindObjectOfType<EventEntry>();

			this.Button.onClick.AddListener(() =>
			{
				if (mobileEventView == null)
				{
					return;
				}

				mobileEventView.SetDescription((this.EntryLesson.Action as TeacherLecture).Text);
				this.SetSprites(this.Button);
			});

			this.normalSprite = this.Button.image.sprite;
			this.highlightSprite = this.Button.spriteState.highlightedSprite;
		}
		#endregion

		public void OnBeginDrag(PointerEventData eventData) => this.Select();

		public void Select() => this.Button.onClick.Invoke();
		#endregion

		#region Methods
		void SetSprites(Button selectedButton)
		{
			foreach (var button in FindObjectsOfType<EventButton>())
			{
				var btn = button.GetComponent<Button>();
				btn.image.sprite = selectedButton == btn ? highlightSprite : normalSprite;
			}
		}

		//// TODO: Move to utils
		//static TechType GetTechType(System.Collections.Generic.List<LectureImageRequirement> requirements)
		//{
		//	if (!requirements.Any())
		//	{
		//		return TechType.None;
		//	}

		//	if (requirements.All(x => x.TechType == "FileBrowserApp"))
		//	{
		//		return TechType.SmartBoard;
		//	}

		//	if (requirements.Any(x => x.TechType != "FileBrowserApp"))
		//	{
		//		return TechType.Tablet;
		//	}
		//	else
		//	{
		//		return TechType.Both;
		//	}
		//}
		#endregion
	}
}
