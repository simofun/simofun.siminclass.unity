﻿using KarmaFramework.ActionCore;
using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using Sinifta.Actions.TeacherActions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Sinifta.UI
{
    [RequireComponent(typeof(ZenjectBinding))]
    public class LessonDropHandler : DropHandler
    {
        public LessonPlanSlot LessonPlanSlot;
        private GameState _gameState;

        public int ElementCount
        {
            get
            {
                return GetComponentsInChildren<LessonFlowElement>().Length;
            }
        }

        [Inject]
        public void Construct(GameState gameState)
        {
            _gameState = gameState;
        }

        public override void OnDrop(PointerEventData eventData)
        {
            if (MobileDragHandler.DragedGameObject == null)
            {
                return;
            }

            var mobileEventButton = MobileDragHandler.DragedGameObject.GetComponent<EventButton>(); //Eger methodlardan atılırsa
            var insertIndex = GetInsertIndex(eventData);
            if (mobileEventButton != null)
            {
                if (CanAddNewEvent(mobileEventButton.EntryLesson.Action)) //check does any space available for new event
                {
                    var mainInsertIndex = GetMainInsertIndex(eventData);
                    var entry = CreateFlowElement(mobileEventButton.EntryLesson.Action, insertIndex);
                    entry.Action.Id = Guid.NewGuid();
                    (entry.Trigger as TimeTrigger).Time =  _gameState.Scenario.FlowElements.Take(mainInsertIndex).Sum(t => t.Action.Duration);
                    _gameState.Scenario.FlowElements.Insert(mainInsertIndex, entry);
                    var triggerActionTuple = CreateTriggerActionsTuple(entry);
                    if(_gameState.Scenario.LectureActions == null)
                    {
                        _gameState.Scenario.LectureActions = new List<TriggerActionsTuple>();
                    }
                    _gameState.Scenario.LectureActions.Insert(mainInsertIndex, triggerActionTuple);
                }
            }

            var lessonFlowElement = MobileDragHandler.DragedGameObject.GetComponent<LessonFlowElement>();
            if (lessonFlowElement != null)
            {
                ChangeLessonFlowSlot(lessonFlowElement, insertIndex);
            }
            SetScenarioFlow();
            LessonPlanPanel.PlayButtonClick();
        }

        private TriggerActionsTuple CreateTriggerActionsTuple(FlowElement entry) //Akışdaki lesson actionları için TriggerActionTuple yaratma
        {
            var _triggerActionTuple = new TriggerActionsTuple();
            _triggerActionTuple.Actions.Add(entry.Action);
            _triggerActionTuple.Triggers.Add(entry.Trigger);
            _triggerActionTuple.Name = entry.Action.Name;
            entry.TriggerActionsTuple = _triggerActionTuple;

            return _triggerActionTuple;
        }

        private void SetScenarioFlow() // Lesson Flow'u Scenario altında kaydetme
        {
            var timelineBase = GetComponentInParent<LessonFlow>().transform;
            var StartSection = timelineBase.Find("StartSection");
            var MidSection = timelineBase.Find("MidSection");
            var EndSection = timelineBase.Find("EndSection");
            var Sections = FindObjectsOfType<LessonDropHandler>();
            List<FlowElement> newList = new List<FlowElement>();
            newList.Clear();

            for (int y = 0; y < Sections.Length; y++)
            {
                if (y == 0)
                    for (int z = 0; z < StartSection.transform.childCount; z++)
                    {
                        if (StartSection.transform.GetChild(z).GetComponent<LessonFlowElement>() != null)
                            newList.Add(StartSection.transform.GetChild(z).GetComponent<LessonFlowElement>().Lesson);
                    }

                else if (y == 1)
                    for (int z = 0; z < MidSection.transform.childCount; z++)
                    {
                        if (MidSection.transform.GetChild(z).GetComponent<LessonFlowElement>() != null)
                            newList.Add(MidSection.transform.GetChild(z).GetComponent<LessonFlowElement>().Lesson);
                    }

                else if (y == 2)
                    for (int z = 0; z < EndSection.transform.childCount; z++)
                    {
                        if (EndSection.transform.GetChild(z).GetComponent<LessonFlowElement>() != null)
                            newList.Add(EndSection.transform.GetChild(z).GetComponent<LessonFlowElement>().Lesson);
                    }

            }

            _gameState.Scenario.FlowElements = newList;
            timelineBase.GetComponent<LessonFlow>().UpdateFlow();
        }


        private bool CanAddNewEvent(BaseAction baseAction)
        {
            var availableDuration = LessonFlow.TimelineLength - _gameState.Scenario.FlowElements.Select(x => x.Action).Sum(x => x.Duration);
            if (30 > availableDuration && availableDuration >= 12)
            {
                baseAction.Duration = availableDuration;
            }
            else if (availableDuration < 12)
            {
                LessonPlanMessageBox.Instance.ShowMessage(Localizer.Instance.GetString("LessonPlanOverFlow"));
                return false;
            }
            return true;
        }

        private void ChangeLessonFlowSlot(LessonFlowElement lessonFlowElement, int insertIndex) //ders akışının lesson plandaki görünümünü değiştirme
        {
            lessonFlowElement.transform.SetParent(transform); // setParent
            ((TeacherLecture)lessonFlowElement.Lesson.Action).LessonPlanSlot = LessonPlanSlot;
            var lessonFlowView = GetComponentInParent<LessonFlow>();
            if (lessonFlowView != null)
            {
                lessonFlowView.UpdateFlow();
            }
            if (insertIndex != -1)
            {
                lessonFlowElement.transform.SetSiblingIndex(insertIndex);
            }
            //logic update here
        }

        private FlowElement CreateFlowElement(BaseAction action, int indexToInsert) //ders akışı için yeni girdi oluşturma
        {
            var trigger = new TimeTrigger();

            // Create a copy of action which came with DragHandler
            var actionCopy = action.Clone();
            ((TeacherLecture)actionCopy).LessonPlanSlot = LessonPlanSlot;

            var entry = new FlowElement(actionCopy, trigger);

            CreateFlowElementView(entry, indexToInsert);

            return entry;
        }

        private void CreateFlowElementView(FlowElement entry, int indexToInsert) // handle view here - 
        {
            var lessonFlowView = FindObjectOfType<LessonFlow>();
            if (lessonFlowView != null)
            {
                lessonFlowView.CreateLessonFlowElement(entry, indexToInsert);
                lessonFlowView.UpdateFlow();
            }
        }


        private int GetInsertIndex(PointerEventData eventData) //ait olduğu sectiondaki indexini hesaplama
        {
            var flowElements = GetComponentsInChildren<LessonFlowElement>().ToList();
            for (int i = 0; i < flowElements.Count; i++)
            {
                if (flowElements[i].gameObject == eventData.pointerCurrentRaycast.gameObject)
                {
                    return i + 1;
                }
            }
            return 0;
        }

        public int GetMainInsertIndex(PointerEventData eventData) //tüm akışa göre indexini hesaplama
        {
            var insertIndex = GetInsertIndex(eventData);
            var mobileLessonDropHandlers = FindObjectsOfType<LessonDropHandler>().ToList();

            switch (LessonPlanSlot) //TODO: needs better implementation 
            {
                case LessonPlanSlot.Learning_TeachingProcess:
                    insertIndex += mobileLessonDropHandlers.First(x => x.LessonPlanSlot == LessonPlanSlot.Introduction).ElementCount;
                    break;
                case LessonPlanSlot.MeasurementAndEvaluation:
                    insertIndex += mobileLessonDropHandlers.Where(x => x.LessonPlanSlot != LessonPlanSlot).Sum(x => x.ElementCount);
                    break;
            }
            return insertIndex;
        }
    }
}