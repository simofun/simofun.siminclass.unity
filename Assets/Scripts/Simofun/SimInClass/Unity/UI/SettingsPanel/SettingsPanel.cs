using DG.Tweening;
using KarmaFramework.UI;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Domain.Audio.Model;
using Simofun.Karma.Unity.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Simofun.SimInClass.Unity.UI
{
    public class SettingsPanel : MonoBehaviour
    {
        [SerializeField]
        Sprite SoundOn;

        [SerializeField]
        Sprite SoundOff;

        [SerializeField]
        Button Sound;

        [SerializeField]
        Button Information;

        [SerializeField]
        Button Feedback;

        [SerializeField]
        Button Language;

        [SerializeField]
        RectTransform InformationPanel;

        [SerializeField]
        RectTransform FeedbackPanel;

        [SerializeField]
        RectTransform LanguagePanel;

        bool isSoundOn = true;
        InformationPanel info;
        FeedbackPanel feedback;
        private void Start()
        {
            Sound.onClick.AddListener(SoundEvent);

            Information.onClick.AddListener(InformationEvent);

            Feedback.onClick.AddListener(FeedbackEvent);

            Language.onClick.AddListener(LanguageEvent);

            feedback = FeedbackPanel.GetComponent<FeedbackPanel>();
            info = InformationPanel.GetComponent<InformationPanel>();
        }

        void SoundEvent()
        {
            if (isSoundOn)
            {
                isSoundOn = false;
            }
            else
            {
                isSoundOn = true;
            }

            KarmaMessageBus.Publish(new KarmaAudioOnOffEvent { IsOn = isSoundOn });
            this.Sound.image.sprite = isSoundOn ? this.SoundOn : this.SoundOff;
        }
        void InformationEvent()
        {
            info.OpenInformation();
        }
        void FeedbackEvent()
        {
            feedback.OpenFeedbackPanel();
        }

        void LanguageEvent()
        {
            LanguagePanel.gameObject.SetActive(true);
        }
    }
    public class TutorialSettingsButtonEvent : KarmaBaseUnityEvent
    {

    }
}