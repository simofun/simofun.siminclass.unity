﻿using UnityEngine;
using UnityEngine.UI;

public class ScorePanelActionEntry : MonoBehaviour
{
	[SerializeField]
	private RectTransform expandIconTR;
	[SerializeField]
	private Text timeText;
	[SerializeField]
	private Text titleText;
	[SerializeField]
	private Text contentText;

	[SerializeField]
	private GameObject actionResultHolder;
	[SerializeField]
	private Text actionResultDescription;

	public bool IsExpanded { get; private set; }

	public void Init(string time, string title, string content, string resultDescription)
	{
		timeText.text = time;
		titleText.text = title;
		contentText.text = content;
		actionResultDescription.text = resultDescription;

		IsExpanded = false;
	}

	public void ToggleExpand()
	{
		IsExpanded = !IsExpanded;

		if (IsExpanded)
		{
			expandIconTR.localEulerAngles = new Vector3(0f, 0f, 0f);
			actionResultHolder.SetActive(true);
		}
		else
		{
			expandIconTR.localEulerAngles = new Vector3(0f, 0f, 90f);
			actionResultHolder.SetActive(false);
		}
	}
}