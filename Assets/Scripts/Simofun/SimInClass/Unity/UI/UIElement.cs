﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI
{
    public enum PanelTypes
    {
        None,
        Score,
    }

    public class UIElement : MonoBehaviour
    {
        public bool IsWindow;
        public bool IsVisible { get; private set; }
        public PanelTypes Type { get; set; }
        public Transform CloseButton { get; set; }
        protected Canvas Canvas { get; private set; }

        public void Init(Canvas canvas)
        {
            Canvas = canvas;

            CloseButton = transform.Find("Close_" + Type);
            if (CloseButton != null)
            {
                CloseButton.GetComponent<Button>().onClick.AddListener(() =>
                {
                    CloseWindow();
                });
            }
        }

        public virtual void ShowElement()
        {
            IsVisible = true;
            gameObject.SetActive(true);
            transform.SetAsLastSibling();
        }

        public virtual void CloseElement()
        {
            IsVisible = false;
            gameObject.SetActive(false);
        }

        public void ShowWindow()
        {
            /*if (KarmaManagers.InterfaceManager != null)
                KarmaManagers.InterfaceManager.ShowWindow(Type, this);*/
        }

        public virtual void CloseWindow()
        {
            /*if (KarmaManagers.InterfaceManager != null)
                KarmaManagers.InterfaceManager.CloseWindow(Type, this);*/
        }

        public virtual void SetViewElement() { }

        public virtual void ToggleButtons(bool isEnabled, List<string> buttonExecs) { }
        /// <summary>
        /// Resizes the UI Element to given width and height values.
        /// </summary>
        /// <param name="width">Target width value, if given -1 this value will be calculated from the height parameter.</param>
        /// <param name="height">Target height value, if given -1 this value will be calculated from the width parameter.</param>
        /// <returns>Returns the original width and height values of the UIElement before the resize operation.</returns>
        public virtual Vector2 ResizeElement(float width, float height)
        {
            var r = GetComponent<RectTransform>();
            var rect = r.rect;
            var originalSize = new Vector2(rect.width, rect.height);

            if (width < 0)
            {
                width = (height / rect.height) * rect.width;
            }
            if (height < 0)
            {
                height = (width / rect.width) * rect.height;
            }

            r.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            r.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);

            return originalSize;
        }
        public virtual void Animate() { }
    }
}
