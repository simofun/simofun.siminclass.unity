﻿namespace Sinifta.UI
{
	using KarmaFramework.ActionCore;
	using KarmaFramework.Localization;
	using KarmaFramework.ScenarioCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.SimInClass.Unity.Events;
	using Sinifta.Actions.TeacherActions;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using UnityEngine.UI;

	public class TeachingMethodButton : MonoBehaviour
	{
		public Image Icon;

		public Text TitleText;

		public string Title { get; set; }
		public string Desc { get; set; }
		public FlowElement Lesson { get; set; }
		public Type Lecturetype { get; set; }

		private Color NormalColor;
		private Color HighlightColor;

		public void Init(FlowElement entryLesson, GameState gameState)
		{
			Lesson = entryLesson;
			Lecturetype = entryLesson.Action.GetType();
			Title = Localizer.Instance.GetString(entryLesson.Action.Name);
			Desc = (entryLesson.Action as TeacherLecture).Text;

			Icon.sprite = Resources.Load<Sprite>("Icons/Lecture/" + entryLesson.Action.Icon);

			TitleText = GetComponentInChildren<Text>();
			if (TitleText != null)
			{
				TitleText.text = Title;
			}

			var actions = new List<BaseAction>();
			foreach (var action in gameState.Scenario.AvailableActions)
			{
				if (action.GetType() == entryLesson.Action.GetType())
				{
					actions.Add(action);
				}
			}

			var button = GetComponent<Button>();
			if (button != null)
			{
				button.onClick.AddListener(() =>
				{
					var eventView = FindObjectOfType<EventEntry>();
					eventView.UpdateEvents(actions);
					SetButtonColors(button);
					eventView.DescriptionText.text = string.Empty;
					LessonPlanPanel.PlayButtonClick();
					KarmaMessageBus.Publish(new MethodButtonGameEvent());
				});

				var colorBlock = button.colors;
				NormalColor = colorBlock.normalColor;
				HighlightColor = colorBlock.highlightedColor;
			}
		}

		private void SetButtonColors(Button selectedButton)
		{
			var methodButtons = FindObjectsOfType<TeachingMethodButton>().ToList();
			foreach (var button in methodButtons)
			{
				var b = button.GetComponent<Button>();
				var colorblock = b.colors;
				colorblock.normalColor = selectedButton == b ? HighlightColor : NormalColor;
				b.colors = colorblock;
			}
		}
	}
}
