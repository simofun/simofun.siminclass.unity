﻿using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.UI
{
    public class LessonFlowElement : MonoBehaviour
    {

        public Image Icon;
        public Text Title;

        public FlowElement Lesson;
        public void Init(FlowElement lesson)
        {
            Lesson = lesson;
            if (Lesson != null)
            {
                Icon.sprite = Resources.Load<Sprite>("Icons/Lecture/" + lesson.Action.Icon);
                Title.text = Localizer.Instance.GetString(lesson.Action.Name);
                var lessonPanel = GetComponentInParent<LessonPlanPanel>();
                var image = GetComponent<Image>();
                if (image != null)
                {
                    var index = lessonPanel.GetFlowListIndex(Lesson) + 1;
                    var ratio = index / (float)lessonPanel.FlowElements.Count;
                    image.color = Color.HSVToRGB(ratio, 0.8f, 0.75f);
                }
            }

            var graphics = GetComponentsInChildren<Graphic>().Skip(1);
            foreach (var g in graphics)
            {
                g.raycastTarget = false;
            }
        }

        public void UpdateView(float unitLength)
        {
            var rectTransform = GetComponent<RectTransform>();
            if (rectTransform != null)
            {
                rectTransform.sizeDelta = new Vector2(Lesson.Action.Duration * unitLength, rectTransform.rect.size.y);
                rectTransform.localScale = Vector3.one;
                Title.text = (Lesson.Action.Duration / 6f).ToString("N0") + " " + Localizer.Instance.GetString("Minutes");
            }
        }
    }
}