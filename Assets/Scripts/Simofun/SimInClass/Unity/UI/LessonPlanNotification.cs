using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Events;
using UnityEngine;
using UniRx;
using TMPro;
using DG.Tweening;
using KarmaFramework.ScenarioCore;
using KarmaFramework.Localization;

namespace Simofun.SimInClass.Unity.UI
{
    public class LessonPlanNotification : MonoBehaviour
    {
        [SerializeField]
        TMP_Text body;

        [SerializeField]
        RectTransform notification;

        CompositeDisposable disposables = new CompositeDisposable();
        
        void Start()
        {
            KarmaMessageBus.OnEvent<ExecuteFlowElement>().Subscribe(ev => 
            {
                body.text = Localizer.Instance.GetString(ev.Action.ActionStringIdString);
                StartAnimation();
            }).AddTo(disposables);

            notification.anchoredPosition = new Vector2(0f, notification.sizeDelta.y);
        }
        void StartAnimation()
        {
            notification.DOAnchorPosY(0f, 1.5f).OnComplete(() => notification.DOAnchorPosY(notification.sizeDelta.y, 1.5f).SetDelay(2f));
        }
    }
}
