﻿#if !UNITY_ANDROID && !UNITY_IOS
#define ENABLE_WEB_API
#endif

using UniRx;

namespace Simofun.SimInClass.Unity.UI
{
	using KarmaFramework.API;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Domain.Model;
	using Simofun.Karma.Unity.Events;
	using Simofun.Karma.Unity.Web.Api;
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using Zenject;

	public class ScenarioSelect : MonoBehaviour
	{
		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		IScene scene;

		IKarmaWebApiRestClient webApiClient;
		#endregion

		#region Construct
		[Inject]
		public virtual void Construct(IScene scene, IKarmaWebApiRestClient webApiClient)
		{
			this.scene = scene;
			this.webApiClient = webApiClient;
		}
		#endregion

		#region Unity Methods
		protected virtual void Start() =>
			KarmaMessageBus.OnEvent<StartGameEvent>()
				.Subscribe(ev => this.StartCoroutine(this.StartGameCoroutine(ev.GameState)))
				.AddTo(this.disposables);

		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion

		#region Static Methods
		IEnumerator CreateGameCoroutine(GameState gameState)
		{
			// TODO: 'Web Api Client' must have enable/disable feature, then enable for PC and disable for mobile, or etc..
#if !ENABLE_WEB_API
			yield break;
#else
			var data = this.webApiClient.CreateGame(
				InterLevelData.AppConfig.UserInfo.Id,
				gameState.Scenario.Name,
				string.Empty,
				gameState.Scenario.ReadableName,
				gameState.Scenario.Description);

			yield return data;

			var createResult = this.webApiClient.ProcessCreateGame(data);
			if (createResult.IsSuccessful)
			{
				InterLevelData.GameId = createResult.GameInfo.GameInfoId;

				yield break;
			}

			//ErrorText.text = Localizer.Instance.GetString(StringId.CreateGameError) + ": " + createResult.Error;
			Debug.LogError("Creating game error : " + data.error);
#endif
		}

		IEnumerator CreateSessionCoroutine(string UserID, Action doAfter)
		{
			//yield return null;

			var sessionId = this.webApiClient.CreateSession(UserID);

			yield return sessionId;

			if (!string.IsNullOrEmpty(sessionId.error))
			{
				Debug.Log("WWW Error Getting ");

				yield break;
			}

			Debug.LogFormat("x{0}x ", sessionId.text);

			var isSuccess = int.TryParse(sessionId.text.ToString(), out int id);
			if (!isSuccess)
			{
				Debug.Log("Parsing Error");

				yield break;
			}

			InterLevelData.SessionId = id;
			if (id > 0)
			{
				Debug.Log("Successfull Creation of Session Id");
			}

			doAfter?.Invoke();
		}
		#endregion

		#region Methods
		IEnumerator StartGameCoroutine(GameState gameState)
		{
			var startTime = Time.realtimeSinceStartup;
			KarmaCursor.IsLoadingMode = true; // Show loading cursor while async operations

			yield return CreateGameCoroutine(gameState); // Create game on website

			while (Time.realtimeSinceStartup - startTime < 0.75f)
			{
				yield return null;
			}

			Debug.Log("Resource init for scenario : [" + gameState.Scenario.Name
				+ "] is done in : [" + (Time.realtimeSinceStartup - startTime) + "] ms");
			KarmaCursor.IsLoadingMode = false; // Hide loading cursor after async operations

			SceneManager.LoadSceneAsync(this.scene.Game); // Start the 'Game' scene
		}
		#endregion
	}
}
