﻿using KarmaFramework.Localization;
using Simofun.Karma.UniRx.Unity;
using UnityEngine;
using UnityEngine.UI;

public class LanguagePanel : MonoBehaviour
{
	#region Unity Fields
	[SerializeField]
	Text _selectedLanguage;

	[SerializeField]
	Button _nextLanguageButton;

	[SerializeField]
	Button _previousLanguageButton;

	[SerializeField]
	Button _acceptButton;

	[SerializeField]
	Button _cancelButton;
	#endregion

	#region Fields
	LanguageType currentLangInPanel;
	#endregion

	#region Unity Methods
	protected virtual void Start()
	{
		this._acceptButton.onClick.AddListener(() =>
		{
			KarmaMessageBus.Publish(new ChangeLanguageRequestEvent { type = this.currentLangInPanel });

			this.gameObject.SetActive(false);
		});
		this._cancelButton.onClick.AddListener(() => this.gameObject.SetActive(false));

		var localizer = Localizer.Instance;
		this.currentLangInPanel = localizer.CurrentLanguage;
		this._nextLanguageButton.onClick.AddListener(() =>
		{
			this.currentLangInPanel = localizer.GetNextLanguageOf(this.currentLangInPanel);
			this.UpdateCurrentLanguageText();
		});
		this._previousLanguageButton.onClick.AddListener(() =>
		{
			this.currentLangInPanel = localizer.GetPreviousLanguageOf(this.currentLangInPanel);
			this.UpdateCurrentLanguageText();
		});

		this.UpdateCurrentLanguageText();
	}
	#endregion

	#region Methods
	void UpdateCurrentLanguageText() => this._selectedLanguage.text = this.currentLangInPanel.ToString();
	#endregion
}
