﻿using DG.Tweening;
using KarmaFramework;
using Simofun.Karma.UniRx.Unity;
using Simofun.SimInClass.Unity.Tutorial.Actions;
using Simofun.SimInClass.Unity.UI;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class TimePanel : MonoBehaviour
{
	#region Unity Fields
	[Title(nameof(TimePanel), "Time References")]
	[SerializeField]
	Button Faster;

	[SerializeField]
	Button Slower;

	[SerializeField]
	Button Pause;

	[SerializeField]
	TMP_Text Speed;

	[SerializeField]
	TMP_Text TimeSeconds;

	[SerializeField]
	Sprite PlaySprite;

	[SerializeField]
	Sprite PauseSprite;

	[Title("", "Quit References")]
	[SerializeField]
	Button Quit;

	[SerializeField]
	Button Settings;

	[SerializeField]
	RectTransform SettingsPanel;

	[SerializeField]
	QuitConfirmationView QuitConfirmationView;
	#endregion

	#region Fields
	bool isPlay = true;

	bool isSettingOpen = false;

	float lastTimeSpeed;

	[Inject]
	GameState gameState;

	Image PauseButtonImage;
	#endregion

	#region Unity Methods
	/// <inheritdoc />
	protected virtual void Start()
	{
		this.Quit.onClick.AddListener(() =>
		{
			TimeManager.Instance.SetTimeScale(0f);
			this.QuitConfirmationView.gameObject.SetActive(true);
			KarmaMessageBus.Publish(new TutorialExitEvent());
		});
		this.Settings.onClick.AddListener(() =>
		{
			this.isSettingOpen = !this.isSettingOpen;

			this.SettingsPanel.DOAnchorPosY(this.isSettingOpen ? 0 : this.SettingsPanel.rect.width / 2, 1);

			KarmaMessageBus.Publish(new TutorialSettingsButtonEvent());
		});

		this.Faster.onClick.AddListener(this.FasterAction);
		this.Slower.onClick.AddListener(this.SlowerAction);
		this.Pause.onClick.AddListener(this.PauseAction);
		this.PauseButtonImage = this.Pause.GetComponent<Image>();
		this.Speed.text = Time.timeScale.ToString();
	}

	/// <inheritdoc />
	protected virtual void Update() => this.TimeCalculation();
	#endregion

	#region Methods
	void FasterAction()
	{
		if (Time.timeScale < 2f)
		{
			TimeManager.Instance.ChangeTimeScale(0.2f);
		}

		this.Speed.text = Time.timeScale.ToString("n1");
	}

	void PauseAction()
	{
		if (!this.isPlay)
		{
			TimeManager.Instance.SetTimeScale(this.lastTimeSpeed);
			this.PauseButtonImage.sprite = this.PlaySprite;
			this.isPlay = true;

			return;
		}

		this.lastTimeSpeed = Time.timeScale;
		TimeManager.Instance.SetTimeScale(0f);
		this.PauseButtonImage.sprite = this.PauseSprite;
		this.isPlay = false;
	}

	void SlowerAction()
	{
		if (Time.timeScale >= 0.4f)
		{
			TimeManager.Instance.ChangeTimeScale(-0.2f);
		}

		this.Speed.text = Time.timeScale.ToString("n1");
	}

	void TimeCalculation()
	{
		var lectureSecs = (KarmaTime.Time / (gameState.Scenario.Duration * .1f * 60))
			* (gameState.Scenario.Duration * 60);
		TimeSeconds.text = ((int)lectureSecs / 60).ToString();
	}
	#endregion
}
