﻿#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBGL
#define NOT_MOBILE_INPUT
#endif

#if UNITY_ANDROID || UNITY_IOS
#define MOBILE
#endif

#if MOBILE || UNITY_EDITOR
#define MOBILE_AND_EDITOR
#endif

using Assets.Scripts.Managers;
using DG.Tweening;
using KarmaFramework;
using KarmaFramework.Interactables.Views;
using KarmaFramework.Views;
using Simofun.Karma.UniRx.Unity;
using Simofun.SimInClass.Unity.Events;
using Sinifta.Views;
using System;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.Characters.FirstPerson;

public class TeacherController : MonoBehaviour
{
	#region Unity Fields
	[Header("Settings")]
	[SerializeField]
	float _mouseMoveThreshold;

	[SerializeField]
	float _moveSpeed;

	[SerializeField]
	float _panSpeed;

	[SerializeField]
	float stepInterval;

	[Header("References")]
	[SerializeField]
	Transform _cameraTransform;

	[SerializeField]
	Joystick joystick;

	[SerializeField]
	Transform joystickPos;

	[SerializeField]
	Trackball _trackball;

	[SerializeField]
	AudioClip[] footstepAudios; // An array of footstep sounds that will be randomly selected from.
	#endregion

	#region Fields
	const float mouseMoveThreshold = 27f;

	readonly CompositeDisposable disposables = new CompositeDisposable();

	AudioSource audioSource;

	bool isEyeContacted;

	bool isFocusing;

	bool isMouseClick;

	bool isMouseDown;

	bool isMouseMoved;

	bool isScenarioPlaying;

	CharacterController controller;

	FirstPersonController firstPersonController;

	float eyeContactCooldown = 1f;

	float nextStep;

	float pressTime;

	float stepCycle;

	KarmaCursorMode currentCursorMode;

	KarmaCursorMode newCursorMode;

	Vector3 lastPosition;

	Vector3 mouseDownPosition;
	#endregion

	#region Properties
	public float DistanceTraveled { get; set; } = 0f;
	#endregion

	#region Unity Methods
	/// <inheritdoc />
	protected virtual void Start()
	{
		this.audioSource = this.GetComponent<AudioSource>();
		this.firstPersonController = this.GetComponent<FirstPersonController>();
		this.firstPersonController.CanWalk = false;
		this.firstPersonController.CanLook = false;
		this.nextStep = this.stepCycle / 2f;

		this.controller = this.GetComponent<CharacterController>();
#if MOBILE_AND_EDITOR
		this.joystick.gameObject.SetActive(true);
#else
		this.joystick.gameObject.SetActive(false);
#endif

		this.isScenarioPlaying = true;
		KarmaMessageBus.OnEvent<ScenarioEndedEvent>()
			.Subscribe(ev => this.isScenarioPlaying = false)
			.AddTo(this.disposables);

		KarmaMessageBus.OnEvent<SmartboardGameEvent>()
			.Subscribe(ev => this.LookToSmartBoard(ev.LookToSmartBoard))
			.AddTo(this.disposables);

		this.isEyeContacted = false;
		this.lastPosition = this.transform.position;
	}

	/// <inheritdoc />
	protected virtual void Update()
	{
		if (!this.isScenarioPlaying)
		{
			return;
		}

		this.DistanceTraveled += Vector3.Distance(this.transform.position, this.lastPosition);
		this.lastPosition = this.transform.position;

		if (this.isEyeContacted)
		{
			this.eyeContactCooldown -= KarmaTime.DeltaTime;
			if (this.eyeContactCooldown <= 0)
			{
				this.eyeContactCooldown = 1f;
				this.isEyeContacted = false;
			}
		}

		var horizontalAxisValue = this.GetHorizontalAxisValue();
		var verticalAxisValue = this.GetVerticalAxisValue();
		this.firstPersonController.HorizontalAxisValue = horizontalAxisValue;
		this.firstPersonController.VerticalAxisValue = verticalAxisValue;

		var speed = this._moveSpeed * KarmaTime.DeltaTime;
		var verticalMovement = this.transform.forward * verticalAxisValue * speed;
		var horizontalMovement = this.transform.right * horizontalAxisValue * speed;

		if (horizontalMovement != Vector3.zero || verticalMovement != Vector3.zero)
		{
			KarmaMessageBus.Publish(new MoveTeacherGameEvent());
		}

		this.controller.Move(verticalMovement);
		this.controller.Move(horizontalMovement);
		this.firstPersonController.Velocity = (horizontalMovement + verticalMovement) / 15f;
		this.ProgressStepCycle(horizontalAxisValue, verticalAxisValue);

		this.newCursorMode = KarmaCursorMode.Normal;

		if (Input.GetMouseButtonUp(0))
		{
			if (this.isMouseDown && !this.isMouseMoved && (Time.time - this.pressTime) < 0.5f)
			{
				this.isMouseClick = true;
			}

			this._trackball.SetState(false);

			this.isMouseMoved = false;
			this.isMouseDown = false;
		}

		var delta = this._trackball.GetTrackballRotation();
		if (delta != Vector2.zero)
		{
			delta = this._panSpeed * delta;

			this.UpdateTeacherLookFromTrackball(delta.x, delta.y);
			KarmaMessageBus.Publish(new RotateTeacherGameEvent());
		}

		if (!this.HasFocus())
		{
			this.OnUpdateExit(); // Call before return calls inside 'Update'

			return;
		}

		if (this.HasTouch())
		{
			if (this.isFocusing)
			{
				this.isFocusing = false;
				KarmaMessageBus.Publish(new FocusEvent { State = false });
				KarmaMessageBus.Publish(new PauseEvent { State = false });

				this.OnUpdateExit(); // Call before return calls inside 'Update'

				return;
			}

			this.isMouseMoved = false;
			this.mouseDownPosition = Input.mousePosition;
			this.isMouseDown = true;
			this.pressTime = Time.time;
		}

		if (this.isMouseDown)
		{
			var mouseMoveAmount = Vector3.Distance(this.mouseDownPosition, Input.mousePosition);
			if (mouseMoveAmount > mouseMoveThreshold)
			{
				this.isMouseMoved = true;
			}
		}

		if (this.isMouseDown && this.isMouseMoved)
		{
			this._trackball.SetState(true);
		}
		else
		{
			this.MouseInteractionsOutsideUi();
		}

		this.isMouseClick = false;

		this.OnUpdateExit(); // Call before return calls inside 'Update'
	}

	/// <inheritdoc />
	protected virtual void OnDestroy() => this.disposables.Dispose();
	#endregion

	#region Methods
	bool HasFocus()
	{
#if NOT_MOBILE_INPUT
		if (EventSystem.current.IsPointerOverGameObject())
#else
		if (Input.touchCount < 1 || EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
#endif
		{
			return false;
		}

		return true;
	}

	bool HasTouch()
	{
#if NOT_MOBILE_INPUT
		return Input.GetMouseButtonDown(0);
#else
		return Input.touchCount > 0
			&& Input.GetTouch(0).phase == TouchPhase.Began
			&& !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
#endif
	}

	float GetHorizontalAxisValue()
	{
		var horizontal = this.joystick.Horizontal;

#if NOT_MOBILE_INPUT
		var standaloneHorizontal = Input.GetAxis("Horizontal");
		if (standaloneHorizontal != 0)
		{
			horizontal = standaloneHorizontal;
		}
#endif
		return horizontal;
	}

	float GetVerticalAxisValue()
	{
		var vertical = this.joystick.Vertical;

#if NOT_MOBILE_INPUT
		var standaloneVertical = Input.GetAxis("Vertical");
		if (standaloneVertical != 0)
		{
			vertical = standaloneVertical;
		}
#endif
		return vertical;
	}

	Quaternion ClampRotationAroundXAxis(Quaternion q, float minimumX, float maximumX)
	{
		q.x /= q.w;
		q.y /= q.w;
		q.z /= q.w;
		q.w = 1.0f;

		var angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
		angleX = Mathf.Clamp(angleX, minimumX, maximumX);

		q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

		return q;
	}

	void MouseInteractionsOutsideUi()
	{
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (!Physics.Raycast(ray, out RaycastHit hit, float.MaxValue))
		{
			return;
		}

		var sView = hit.transform.GetComponent<StudentView>();
		if (sView != null)
		{
			this.isEyeContacted = true;
		}

		var npcView = hit.collider.GetComponent<NPCView>();
		if (npcView == null)
		{
			this.newCursorMode = KarmaCursorMode.Crosshair;
		}
		else
		{
			this.newCursorMode = KarmaCursorMode.CrosshairHighlight;
			if (this.isMouseDown)
			{
				if (!this.isFocusing)
				{
					KarmaMessageBus.Publish(
						new FocusEvent
						{
							State = true,
							Target = npcView
						});
					KarmaMessageBus.Publish(new PauseEvent { State = true });
				}

				this.isFocusing = true;
			}
		}

		var interactableView = hit.collider.GetComponent<InteractableTrigger>();
		if (interactableView != null && this.isMouseDown)
		{
			var dist = Vector3.Distance(this.transform.position, hit.collider.transform.position);
			if (dist < interactableView.InteractableDistance) // Only interact if its close enough
			{
				KarmaMessageBus.Publish(new OpenInteractableEvent { Id = interactableView.InteractableId });
			}
		}
	}

	void OnUpdateExit()
	{
		if (this.currentCursorMode == this.newCursorMode)
		{
			return;
		}

		KarmaCursor.Mode = this.newCursorMode;
		this.currentCursorMode = this.newCursorMode;
	}

	void PlayFootStepAudio()
	{
		//if (!this.firstPersonController.isGrounded)
		//{
		//    return;
		//}

		// Pick & play a random footstep sound from the array,
		// Excluding sound at index 0
		int n = UnityEngine.Random.Range(1, this.footstepAudios.Length);
		this.audioSource.clip = this.footstepAudios[n];
		this.audioSource.PlayOneShot(this.audioSource.clip);

		// Move picked sound to index 0 so it's not picked next time
		this.footstepAudios[n] = this.footstepAudios[0];
		this.footstepAudios[0] = this.audioSource.clip;
	}

	void ProgressStepCycle(float horizontalAxisValue, float verticalAxisValue)
	{
		if (this.firstPersonController.Velocity.sqrMagnitude > 0
				&& (horizontalAxisValue != 0 || verticalAxisValue != 0))
		{
			this.stepCycle += KarmaTime.DeltaTime *
				(this.firstPersonController.Velocity.magnitude + this.firstPersonController.Speed);
		}

		if (this.stepCycle <= this.nextStep)
		{
			return;
		}

		this.nextStep = this.stepCycle + this.stepInterval;

		this.PlayFootStepAudio();
	}

	void UpdateTeacherLookFromTrackball(float vertRotate, float horzRotate)
	{
		const float rotSpd = 40f;
		this.transform.Rotate(Vector3.up, vertRotate * KarmaTime.DeltaTime * rotSpd, Space.World);
		this._cameraTransform.Rotate(Vector3.left, horzRotate * KarmaTime.DeltaTime * rotSpd, Space.Self);
		var q = this._cameraTransform.localRotation;
		q = this.ClampRotationAroundXAxis(q, -20, 40);
		this._cameraTransform.localRotation = q;
	}

	void LookToSmartBoard(Action lookToSmartBoardAction)
	{
		var smartBoard = GameObject.Find("SmartBoard");

		var rot = new Vector3(this.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y,
			-this.transform.rotation.eulerAngles.z);
		rot.Normalize();

		this.transform
			.DOLookAt(smartBoard.transform.position, 1, AxisConstraint.Y, this.transform.up)
			.OnComplete(() => lookToSmartBoardAction?.Invoke());
	}
	#endregion
}
