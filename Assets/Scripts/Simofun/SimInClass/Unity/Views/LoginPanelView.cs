﻿using KarmaFramework.Localization;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sinifta.Localization;
using KarmaFramework.API;
using Simofun.Karma.Web;

namespace Sinifta.Views
{
    public class LoginPanelView
    {
        public event Action<string, string> OnLoginClick;
        public event Action OnFacebookLogoutClick;
        public event Action OnFacebookLoginClick;
        public event Action OnQuitClick;

        public bool IsRememberMe
        {
            get { return _rememberToggle.isOn; }
        }

        readonly IKarmaWebUrlProvider webUrlProvider;

        private readonly Transform _panelTransform;
        private readonly Transform _bottomContainer;
        private readonly Transform _loadingTransform;
        private readonly Transform _bottomRightContainer;
        private readonly Transform _bottomMiddleContainer;

        private readonly Transform _userNameContainer;
        private readonly Transform _passwordContainer;

        private readonly InputField _userNameInputField;
        private readonly InputField _passwordInputField;

        private readonly Text _loginErrorText;
        private readonly Text _passwordErrorText;

        private readonly Toggle _rememberToggle;

        private readonly Button _loginButton;
        private readonly Button _quitButton;
        private readonly Button _facebookLoginButton;
        private readonly Button _forgotPasswordButton;
        private readonly Button _registerButton;

        private bool _isPasswordEmpty = true;
        private bool _isUserNameEmpty = true;

        private readonly Queue<InputField> _inputFields = new Queue<InputField>();

        public LoginPanelView(Transform panelTransform, IKarmaWebUrlProvider webUrlProvider)
        {
            _panelTransform = panelTransform;
            this.webUrlProvider = webUrlProvider;

            _loadingTransform = _panelTransform.parent.transform.Find("SiniftaLoadingLogo");
            _bottomContainer = panelTransform.Find("Container").Find("BottomContainer");
            _bottomMiddleContainer = _bottomContainer.Find("Middle");
            _bottomRightContainer = _bottomContainer.Find("Right");

            _userNameContainer = _bottomMiddleContainer.Find("UserName");
            _passwordContainer = _bottomMiddleContainer.Find("Password");

            // Localize labels
            var usernameLabel = _userNameContainer.Find("Title").GetComponent<Text>();
            var passwordLabel = _passwordContainer.Find("Title").GetComponent<Text>();
            usernameLabel.text = Localizer.Instance.GetString(StringId.EmailLabel.ToString());
            passwordLabel.text = Localizer.Instance.GetString(StringId.PasswordLabel.ToString());

            // Localize input fields
            _userNameInputField = _userNameContainer.Find("InputField").GetComponent<InputField>();
            _passwordInputField = _passwordContainer.Find("InputField").GetComponent<InputField>();

            _userNameInputField.onValueChanged.AddListener(OnUserNameChanged);
            _passwordInputField.onValueChanged.AddListener(OnPasswordChanged);

            _inputFields.Enqueue(_passwordInputField);
            _inputFields.Enqueue(_userNameInputField);
            (_userNameInputField.placeholder as Text).text = Localizer.Instance.GetString(StringId.EmailPlaceholder.ToString());
            (_passwordInputField.placeholder as Text).text = Localizer.Instance.GetString(StringId.PasswordPlaceholder.ToString());

            _rememberToggle = _bottomRightContainer.Find("RememberToggle").GetComponent<Toggle>();
            var rememberToggleLabel = _rememberToggle.transform.Find("Label").GetComponent<Text>();
            rememberToggleLabel.text = Localizer.Instance.GetString(StringId.RememberMe.ToString());

            _loginErrorText = _bottomMiddleContainer.Find("LoginErrorText").GetComponent<Text>();
            _passwordErrorText = _bottomMiddleContainer.Find("PasswordErrorText").GetComponent<Text>();

            // Refill input fields if a user is remembered
            var rememberedUserName = PlayerPrefs.GetString("username", string.Empty);
            var rememberedPassword = PlayerPrefs.GetString("password", string.Empty);
            if (!string.IsNullOrEmpty(rememberedUserName))
            {
                _isPasswordEmpty = false;
                _isUserNameEmpty = false;

                _userNameInputField.text = rememberedUserName;
                _passwordInputField.text = rememberedPassword;
            }
            else
            {
                _isPasswordEmpty = true;
                _isUserNameEmpty = true;
                // If there's no remembered user, clear rememberToggle
                _rememberToggle.isOn = false; //gameObject.SetActive(false);
                                              //_rememberToggle.isOn = false;
            }

            _loginButton = _bottomMiddleContainer.Find("Button_Login").GetComponent<Button>();
            _loginButton.onClick.AddListener(OnLoginButtonClicked);

            _facebookLoginButton = _bottomMiddleContainer.Find("LoginWithFacebook").GetComponent<Button>();
            _facebookLoginButton.onClick.AddListener(OnFacebookLoginButtonClicked);

            var loginButtonText = _loginButton.transform.Find("Text").GetComponent<Text>();
            loginButtonText.text = Localizer.Instance.GetString(StringId.Login.ToString());
            _loginErrorText.gameObject.SetActive(false);

            _forgotPasswordButton = _bottomRightContainer.Find("ForgotPasswordButton").GetComponent<Button>();
            _forgotPasswordButton.GetComponentInChildren<Text>().text =
                 Localizer.Instance.GetString(StringId.ForgotPassword.ToString());
            _forgotPasswordButton.onClick.AddListener(OnForgotPasswordButtonPressed);

            _registerButton = _bottomMiddleContainer.Find("RegisterButton").GetComponent<Button>();
            _registerButton.GetComponentInChildren<Text>().text =
                 Localizer.Instance.GetString(StringId.KarmaRegister.ToString());
            _registerButton.onClick.AddListener(OnRegisterButtonPressed);

            _quitButton = _bottomMiddleContainer.Find("Button_Quit").GetComponent<Button>();
            _quitButton.onClick.AddListener(() =>
            {
                _loginButton.interactable = false;
                _quitButton.interactable = false;
                _userNameInputField.interactable = false;
                _passwordInputField.interactable = false;

                //KarmaCursor.LoadingMode = true;


                _loginErrorText.text = string.Empty;

                if (OnQuitClick != null) OnQuitClick();
            });

            var quitButtonText = _quitButton.transform.Find("Text").GetComponent<Text>();
            quitButtonText.text = Localizer.Instance.GetString(StringId.Quit.ToString());
        }

        private void OnLoginButtonClicked()
        {
            if (/*!InterLevelData.AppConfig.IsolatedEnvironment && */string.IsNullOrEmpty(_passwordInputField.text.Trim()))
            {
                _passwordErrorText.gameObject.SetActive(true);
                return;
            }

            _passwordErrorText.gameObject.SetActive(false);

            _loginButton.interactable = false;
            _quitButton.interactable = false;
            _userNameInputField.interactable = false;
            _passwordInputField.interactable = false;

            //KarmaCursor.LoadingMode = true;

            _loginErrorText.text = string.Empty;

            string userName = _userNameInputField.text;
            string password = _passwordInputField.text;

            if (OnLoginClick != null) OnLoginClick(userName, password);
        }

        private void OnFacebookLoginButtonClicked()
        {
            _loadingTransform.gameObject.SetActive(true);

            _loginButton.interactable = false;
            _quitButton.interactable = false;
            _userNameInputField.interactable = false;
            _passwordInputField.interactable = false;

            //KarmaCursor.LoadingMode = true;

            _loginErrorText.text = string.Empty;

            if (OnFacebookLoginClick != null) OnFacebookLoginClick();
        }

        //public void OnFacebookLoginResultReceived(ILoginResult result, bool isLoggedIn)
        //{
        //    if (isLoggedIn)
        //    {
        //        //FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUserName);
        //        FB.API("/me?fields=email", HttpMethod.GET, CheckEmail);
        //    }
        //    else
        //    {
        //       InitializeDefaultLoginPanelView();
        //    }
        //}

        public void SetActive(bool isActive)
        {
            _panelTransform.gameObject.SetActive(isActive);
#if UNITY_ANDROID || UNITY_IOS
            if (isActive && IsRememberMe)
                _loginButton.onClick.Invoke();
#endif
        }

        public void OnLoginResult(bool succ, string message)
        {
            _loginButton.interactable = true;
            _quitButton.interactable = true;
            _userNameInputField.interactable = true;
            _passwordInputField.interactable = true;

            //KarmaCursor.LoadingMode = false;

            if (!string.IsNullOrEmpty(message))
            {
                if (message.Contains("404"))
                {
                    message = Localizer.Instance.GetString(StringId.InvalidUserNameOrPassword.ToString());
                }
                else if (message.Contains("Failed to connect"))
                {
                    message = Localizer.Instance.GetString(StringId.ConnectionError.ToString());
                }

                _loginErrorText.text = message;
                _loginErrorText.gameObject.SetActive(true);
            }
        }

        public void OnLoginFBResult(bool succ, string message)
        {
            _loginButton.interactable = true;
            _quitButton.interactable = true;
            _userNameInputField.interactable = true;
            _passwordInputField.interactable = true;

            //KarmaCursor.LoadingMode = false;

            _loadingTransform.gameObject.SetActive(false);

            if (!string.IsNullOrEmpty(message))
            {
                if (message.Contains("404"))
                {
                    message = Localizer.Instance.GetString(StringId.FacebookAccountNotFoundError.ToString());
                }
                else if (message.Contains("Failed to connect"))
                {
                    message = Localizer.Instance.GetString(StringId.ConnectionError.ToString());
                }

                _loginErrorText.text = message;
                _loginErrorText.gameObject.SetActive(true);
            }
        }

        public void OnForgotPasswordButtonPressed() => Application.OpenURL(this.webUrlProvider.ForgotPassword);

        public void OnRegisterButtonPressed() => Application.OpenURL(this.webUrlProvider.Register);

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return)
                && _loginButton.interactable
                && Time.timeScale > 0) // Feedback view should be inactive
            {
                OnLoginButtonClicked();
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                var nextInputField = _inputFields.Dequeue();
                nextInputField.ActivateInputField();
                nextInputField.Select();
                _inputFields.Enqueue(nextInputField);
            }

#if UNITY_EDITOR
            if (Input.GetKey(KeyCode.LeftAlt)
                && Input.GetKey(KeyCode.LeftControl)
                && Input.GetKey(KeyCode.LeftShift)
                && Input.GetKey(KeyCode.A))
            {
                _userNameInputField.text = "admin@simsoft.com.tr";
                _passwordInputField.text = "Karma123!";
            }
#endif
        }

        private void OnUserNameChanged(string arg0)
        {
            string userNameInput = _userNameInputField.text.Trim();
            if (string.IsNullOrEmpty(userNameInput))
            {
                _isUserNameEmpty = true;
                _rememberToggle.isOn = false; //gameObject.SetActive(false);
            }
            else
            {
                _isUserNameEmpty = false;
                if (!_isPasswordEmpty) _rememberToggle.isOn = true; //gameObject.SetActive(true);
            }
        }

        private void OnPasswordChanged(string arg0)
        {
            string passwordInput = _passwordInputField.text.Trim();
            if (string.IsNullOrEmpty(passwordInput))
            {
                _isPasswordEmpty = true;
                _rememberToggle.isOn = false; //gameObject.SetActive(false);
            }
            else
            {
                _isPasswordEmpty = false;
                if (!_isUserNameEmpty) _rememberToggle.isOn = true; //gameObject.SetActive(true);
            }
        }
    }
}