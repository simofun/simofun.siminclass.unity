﻿
namespace Sinifta.ScenarioEditor.StudentEditor
{
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using System.IO;
	using System.Linq;
	using System.Xml.Linq;
	using UniRx;
	using UnityEngine;
	using UnityEngine.UI;

	public class EditStudentView : MonoBehaviour
	{
		#region Unity Fields
		public Button CreateStudentButton;

		public Transform CreateStudentPanel;

		[SerializeField]
		StudentEditorEntry _studentEntryPrefab;

		[SerializeField]
		Transform _studentEntryHolder;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		int _studentCount;

		string _configPath;

		XDocument _document;

		XElement _actorsRoot;
		#endregion

		#region Unity Methods
		protected virtual void Start()
		{
			_configPath = Application.persistentDataPath + "/Configs";

			CreateStudentPanel.gameObject.SetActive(false);

			CreateStudentButton.onClick.AddListener(() => {
				CreateStudentPanel.gameObject.SetActive(true);
				_studentCount = 0;
				foreach (var child in _studentEntryHolder.Cast<Transform>().Select(x => x.gameObject).Where(x => x.gameObject.activeSelf))
					_studentCount++;

				CreateStudentPanel.GetComponent<CreateStudentPanelView>().Bind(_studentCount);
			});

			if (Directory.Exists(_configPath) && File.Exists(_configPath + "/ActionConfig.xml"))
			{
				_document = XDocument.Load(_configPath+ "/ActorConfig.xml");
			}
			else
			{
				if (!Directory.Exists(_configPath))
					Directory.CreateDirectory(_configPath);

				var ta = Resources.Load<TextAsset>("Configs/ActorConfig");
				_document = XDocument.Parse(ta.text);
				_document.Save(_configPath + "/ActorConfig.xml");
			}
			_actorsRoot = _document.Element("ActorsHolder").Element("Actors");

			_studentEntryPrefab.gameObject.SetActive(false);

			foreach (var child in _studentEntryHolder.Cast<Transform>().Select(x => x.gameObject).Where(x => x.gameObject.activeSelf))
			{
				Destroy(child);
			}
			foreach (var element in _actorsRoot.Elements())
			{
				var studentEntry = Instantiate(_studentEntryPrefab, _studentEntryHolder, false);
				studentEntry.gameObject.SetActive(true);
				studentEntry.Bind(element);
			}

			KarmaMessageBus.OnEvent<CreateStudentEvent>().Subscribe(ev =>
			{
				var studentEntry = Instantiate(_studentEntryPrefab, _studentEntryHolder, false);
				studentEntry.gameObject.SetActive(true);
				studentEntry.Bind(ev.element);

				_actorsRoot.Add(ev.element);
				_document.Save(_configPath + "/ActorConfig.xml");
			}).AddTo(this.disposables);
		}

		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion
	}

	public class CreateStudentEvent : KarmaBaseUnityEvent
	{
		public XElement element;
	}
}
