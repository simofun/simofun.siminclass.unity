﻿#if UNITY_EDITOR
#define DEV_REMEMBER_LOGIN_INFO
#endif

using UniRx;

namespace Sinifta.Views
{
	using DG.Tweening;
	using KarmaFramework;
	using KarmaFramework.API;
	using KarmaFramework.Localization;
	using LitJson;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Web;
	using Simofun.Karma.Unity.Web.Api;
	using Simofun.Karma.Web;
	using Simofun.Karma.Web.Api.Model;
	using Simofun.SimInClass.Unity.Domain;
	using Simofun.SimInClass.Unity.HardCodeds;
	using System;
	using System.Collections;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using UnityEngine.UI;
	using Zenject;

	public class LoginMenuView : MonoBehaviour
	{
		#region Unity Fields
		[Header("References")]
		[SerializeField]
		InputField _userNameField;

		[SerializeField]
		InputField _passwordField;

		[SerializeField]
		Button _loginButton;

		[SerializeField]
		Toggle _rememberMeToggle;

		[SerializeField]
		Button _forgotPasswordButton;

		[SerializeField]
		GameObject _blackScreen;

		[SerializeField]
		Button _quitButton;

		[Header("Warning Things")]
		[SerializeField]
		RectTransform _warning;

		[SerializeField]
		Text _warningText;

		[Header("Logo")]
		[SerializeField]
		Sprite EnglishLogo;

		[SerializeField]
		Sprite TurkishLogo;

		Image backgroundImage;

		#endregion

		#region Fields
		float _lastConfirmationMailSent;

		IKarmaWebApiRestClient webApiClient;

		IKarmaWebClient webClient;

		IKarmaWebUrlProvider webUrlProvider;

		ScreenOrientation _defaultOrientation;

		Sequence _warningSequence;

		UserInfo testUser;

		CompositeDisposable disposables = new CompositeDisposable();
		#endregion

		#region Properties
		/// <summary>
		/// Gets or set fake test user info.
		/// If initial scene is <see cref="Scenes.Boot"/>, use app config user info which is already loaded.
		/// If initial scene is <see cref="Scenes.MainMenu"/>, create new user info.
		/// </summary>
		UserInfo TestUser => this.testUser
			?? (this.testUser = InterLevelData.AppConfig.UserInfo ?? new FakeUserFactory().CreateTestUser());
		#endregion

		#region Construct
		[Inject]
		public virtual void Construct(
			IKarmaWebApiRestClient webApiClient, IKarmaWebClient webClient, IKarmaWebUrlProvider webUrlProvider)
		{
			this.webApiClient = webApiClient;
			this.webClient = webClient;
			this.webUrlProvider = webUrlProvider;
		}
		#endregion

		#region Unity Methods
		protected virtual void Start()
		{
			KarmaCursor.Mode = KarmaCursorMode.Normal;
			
			this._defaultOrientation = Screen.orientation;
			Screen.orientation = ScreenOrientation.Portrait;
			this._warning.gameObject.SetActive(false);
			this._blackScreen.SetActive(false);

			this._loginButton.onClick.AddListener(
				() => this.StartCoroutine(this.LoginCoroutine(this._userNameField.text, this._passwordField.text)));
			this._forgotPasswordButton.onClick.AddListener(
				() => Application.OpenURL(this.webUrlProvider.ForgotPassword));
			this._quitButton.onClick.AddListener(() => { Application.Quit(); });

			#region Remember Things
			var user = PlayerPrefs.GetString("username");
			var password = PlayerPrefs.GetString("password");
			if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(password))
			{
				this._userNameField.text = user;
				this._passwordField.text = password;
				this._rememberMeToggle.isOn = true;
				//this._loginButton.onClick.Invoke();
			}

			this._rememberMeToggle.onValueChanged.AddListener(toogle =>
			{
				this.StartCoroutine(this.GetUserProgressCoroutine(this.TestUser));
				if (!toogle)
				{
					PlayerPrefs.SetString("username", string.Empty);
					PlayerPrefs.SetString("password", string.Empty);
				}
			});
			#endregion

//#if DEV_REMEMBER_LOGIN_INFO
//			this._userNameField.text = "ogrenci@simsoft.com.tr";
//			this._passwordField.text = "ogrenci";
//			this._rememberMeToggle.isOn = false;
//#endif

			#region Logo
			backgroundImage = GetComponent<Image>();
			ChangeLogo();

			KarmaMessageBus.OnEvent<LanguageChangedEvent>().Subscribe(ev =>
			{
				ChangeLogo();
			}).AddTo(disposables);
			#endregion
		}
		private void OnDestroy() => disposables.Dispose();
		
		#endregion

		#region Methods
		IEnumerator CreateSessionCoroutine(string UserID, Action doAfter)
		{
			//yield return null;

			var sessionId = this.webApiClient.CreateSession(UserID);

			yield return sessionId;

			if (!string.IsNullOrEmpty(sessionId.error))
			{
				Debug.Log("WWW Error Getting ");

				yield break;
			}

			Debug.Log(string.Format(" x{0}x ", sessionId.text));
			int Id;
			var IsSuccess = int.TryParse(sessionId.text.ToString(), out Id);
			if (!IsSuccess)
			{
				Debug.Log("Parsing Error");

				yield break;
			}

			InterLevelData.SessionId = Id;
			if (Id > 0)
			{
				Debug.Log("Successfull Creation of Session Id");
			}

			doAfter?.Invoke();
		}

		IEnumerator ExternalLoginCoroutine(KarmaExternalLoginInfo info)
		{
			yield return this.webApiClient.ExternalLogin(info);

			this.StartCoroutine(this.LoginCoroutine(info.Email, string.Empty, true));
		}

		IEnumerator GetUserProgressCoroutine(UserInfo info, Action callback = null)
		{
			var wwwProgress = this.webApiClient.GetUserProgress(info.Id);

			yield return wwwProgress;

			var js = JsonMapper.ToObject(wwwProgress.text);
			var infos = JsonMapper.ToObject<KarmaLevelInfos>(js.ToJson());
			InterLevelData.AppConfig.LevelInfos = infos;
			callback?.Invoke();
		}

		IEnumerator LoginCoroutine(string userName, string password, bool isExternalLogin = false)
		{
			var appConfig = InterLevelData.AppConfig;
			var quickLogin = false;
			WWW wwwLogin = null;
			this._loginButton.interactable = false;

#if UNITY_EDITOR
			// Yasir Kula: increase debugging speed in the editor
			// by quickly logging in if CTRL is pressed
			if (Input.GetKey(KeyCode.LeftControl))
			{
				appConfig.IsolatedEnvironment = true;
			}

			// Yasir Kula: increase debugging speed in the editor
			// by loading scenarios from local if Shift is pressed while logging in
			if (Input.GetKey(KeyCode.LeftShift))
			{
				appConfig.OnlineMode = false;
			}
#endif

			if (appConfig.IsolatedEnvironment)
			{
				quickLogin = true;

				yield return null;
			}
			else
			{
				wwwLogin = isExternalLogin
					? this.webApiClient.GetExternalUserData(userName)
					: this.webApiClient.GetUserData(userName, password);

				yield return wwwLogin;
			}

			UserInfo userInfo = null;

			if (!appConfig.IsolatedEnvironment)
			{
				userInfo = JsonMapper.ToObject<UserInfo>(JsonMapper.ToObject(wwwLogin.text).ToJson());
			}

			if (!quickLogin)
			{
				this.OnLoginResult(userInfo.Status, userInfo.ResultMessage, userInfo);
				this.PlayerPrefsSave(userName, password);
				
				yield break;
			}
			
			// Debug
			if (userInfo == null)
			{
				userInfo = this.TestUser;
			}	
		}
		
		void ChangeLogo()
		{
			if (Localizer.Instance.CurrentLanguage == LanguageType.English)
			{
				backgroundImage.sprite = EnglishLogo;
			}
			else
			{
				backgroundImage.sprite = TurkishLogo;
			}
		}
		void PlayerPrefsSave(string userName, string password)
		{
			if (_rememberMeToggle.isOn)
			{
				PlayerPrefs.SetString("username", userName);
				PlayerPrefs.SetString("password", password);
			}
			else
			{
				PlayerPrefs.SetString("username", string.Empty);
				PlayerPrefs.SetString("password", string.Empty);
			}

			PlayerPrefs.Save();
		}
		void DoExternalLogin(ExternalLoginResult result)
		{
			// If successful
			if (result.Status)
			{
				// Api things, add new user if not exits or retrieve user info
				this.StartCoroutine(this.ExternalLoginCoroutine(result.Info));

				return;
			}

			this.ShowWarning(result.Message);
		}

		void OnLoginResult(bool success, string message, UserInfo info)
		{
			if (success)
			{
				Screen.orientation = this._defaultOrientation;
				InterLevelData.AppConfig.UserInfo = info;
				this.StartCoroutine(this.GetUserProgressCoroutine(info, () => SceneManager.LoadScene(Scenes.MainMenu)));
			}
			else
			{
				Debug.LogError(message);
				this.ShowWarning(message);
			}

			this._loginButton.interactable = true;
		}

		void ShowWarning(string message)
		{
			const float offset = 400f;
			this._warningSequence?.Kill();

			this._warningText.text = message;
			this._warning.gameObject.SetActive(true);
			LayoutRebuilder.ForceRebuildLayoutImmediate(this._warning);
			this._warningSequence = DOTween.Sequence();
			this._warning.anchoredPosition = Vector2.down * offset;
			this._warningSequence.Append(this._warning.DOAnchorPosY(0f, 1f));
			this._warningSequence.Append(this._warning.DOAnchorPosY(-offset, 1f).SetDelay(3f));
			this._warningSequence.OnComplete(() => this._warning.gameObject.SetActive(false));
		}
		#endregion
	}

	public class ExternalLoginResult
	{
		public bool Status { get; set; }

		public KarmaExternalLoginInfo Info { get; set; }

		public string Message { get; set; }
	}
}
