﻿using UniRx;

namespace Sinifta.Views
{
	using DG.Tweening;
	using KarmaFramework;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Unity.Utils;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text.RegularExpressions;
	using UnityEngine;
	using UnityEngine.UI;

	/// <summary>
	/// [V1 => PanelBotSubtextController, PanelMobileBotSubTextController]
	/// </summary>
	public class SpeechView : MonoBehaviour
	{
		#region Unity Fields
		[Header("References")]
		[SerializeField]
		GameObject content;

		[SerializeField]
		Text textView;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		readonly List<Sentence> sentences = new List<Sentence>();

		float startTimePercent;

		Sentence currentSentence;

		string fullText;
		#endregion

		#region Properties
		public bool IsActive { get => this.gameObject.activeSelf; set => this.gameObject.SetActive(value); }

		public bool IsVisible { get => this.content.activeSelf; set => this.content.SetActive(value); }

		public float Duration { get; set; }

		public int StartTime { get; set; }

		public string FullText
		{
			get => this.fullText;

			set
			{
				var newFullText = value.Trim();
				this.fullText = newFullText;
				this.sentences.Clear();
				var fullTextReadingTime = (float)ReadingTimeUtil.GetReadingTime(newFullText).TotalSeconds;
				foreach (var sentenceText in Regex.Split(newFullText, @"(?<=[\.!\?])\s+"))
				{
					var newSentenceText = sentenceText.Trim();
					var readingTime = ReadingTimeUtil.GetReadingTime(newSentenceText);
					this.sentences.Add(
						new Sentence
						{
							ReadingTime = readingTime,
							ReadingTimePercent = (float)readingTime.TotalSeconds / fullTextReadingTime,
							Text = newSentenceText
						});
				}

				this.SetTextPercentage(0);
			}
		}
		#endregion

		#region Protected Properties
		protected Text TextView
		{
			get => this.textView != null
						? this.textView
				: (this.textView = this.GetComponent<Text>());
			set => this.textView = value;
		}
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start()
		{
			KarmaMessageBus.OnEvent<ShowLectureEvent>().Subscribe(ev =>
			{
				if (!ev.State)
				{
					this.IsActive = false;
					this.Clear();

					return;
				}

				this.Duration = ev.Duration;
				this.StartTime = ev.StartTime;

				var newFullText = ev.speech.Trim();
				if (!string.IsNullOrEmpty(newFullText))
				{
					this.FullText = newFullText;
					this.IsActive = true;
				}
			}).AddTo(this.disposables);

			this.IsActive = false;
		}

		/// <inheritdoc />
		protected virtual void Update()
		{
			var passedTime = KarmaTime.Time - this.StartTime;
			if (this.currentSentence != null && passedTime >= this.Duration)
			{
				this.Clear();

				return;
			}
			else if (passedTime >= this.Duration)
			{
				return;
			}

			var passedTimePercent = passedTime / this.Duration;
			this.SetTextPercentage(passedTimePercent - this.startTimePercent);
		}

		/// <inheritdoc />
		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion

		#region Public Methods
		public void SetTextPercentage(float value)
		{
			if (this.currentSentence != null && value <= this.currentSentence.ReadingTimePercent)
			{
				return;
			}

			// Current sentence is null. Then, show initial animation.
			if (this.currentSentence == null)
			{
				this.transform.DOPunchScale(Vector3.one * 0.1f, 0.5f, 5).SetEase(Ease.InBack);
			}

			// Show next sentence.
			this.currentSentence = this.sentences.First();
			this.sentences.Remove(this.currentSentence);
			this.startTimePercent = (KarmaTime.Time - this.StartTime) / this.Duration;
			this.TextView.text = this.currentSentence.Text;
			this.IsVisible = !string.IsNullOrEmpty(this.currentSentence.Text);
		}
		#endregion

		#region Methods
		void Clear()
		{
			this.IsVisible = false;
			this.startTimePercent = 0;
			this.currentSentence = null;
			this.sentences.Clear();
		}
		#endregion

		#region Nested Types
		class Sentence
		{
			public float ReadingTimePercent { get; set; }

			public string Text { get; set; }

			public TimeSpan ReadingTime { get; set; }
		}
		#endregion
	}
}
