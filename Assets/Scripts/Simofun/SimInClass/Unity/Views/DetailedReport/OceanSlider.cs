﻿using UnityEngine;
using UnityEngine.UI;

public class OceanSlider : MonoBehaviour
{
	public Image[] Slots;

	public Color EmptySlotColor;
	public Color FilledSlotColor;

	public float Value
	{
		get
		{
			if (Slots.Length == 0)
				return 0f;

			for (int i = 0; i < Slots.Length; i++)
			{
				if (Slots[i].color == EmptySlotColor)
					return (float)i / Slots.Length;
			}

			return 1f;
		}
		set
		{
			int numberOfSlotsToFill = (int)(Mathf.Clamp01(value) * Slots.Length);

			for (int i = 0; i < numberOfSlotsToFill; i++)
				Slots[i].color = FilledSlotColor;

			for (int i = numberOfSlotsToFill; i < Slots.Length; i++)
				Slots[i].color = EmptySlotColor;
		}
	}
}