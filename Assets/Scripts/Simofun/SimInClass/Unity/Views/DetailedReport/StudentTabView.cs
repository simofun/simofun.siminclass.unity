﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Sinifta.Actors;
using Sinifta.Environment;
using KarmaFramework.API;
using KarmaFramework.ScenarioCore;
using Zenject;

public delegate void OnClickedStudent(Student student);

public class StudentTabView : MonoBehaviour
{
    public event OnClickedStudent OnClickedStudent;

    public SetupStudentView StudentInfoSetup;

    public StudentNameButton NameButtonPrefab;

    public GameObject DiagramHolder;
    public GameObject SeatHolder;
    public GameObject StudentNamesHolder;
    public GameObject DraggedSeatHolder;

    public ScrollRect StudentNamesScrollRect;
    public RectTransform StudentNamesContent;
    public RectTransform StudentNamesViewport;

    public Text DragInfoText;

    public bool DragDropSupported = true;

    //Drag Operations
    private bool _isDragInProcess;
    private Image _draggedSeatImage;
    private StudentSeat _draggedSeat = null;
    private StudentSeat _seatToBeDropped = null;

    private StudentSeat _selected;
    private StudentNameButton _selectedNameButton = null;
    private bool _isDefaultStudentSelected = false;
    private List<StudentSeat> _studentSeats;
    private List<StudentNameButton> _instantiatedButtons = new List<StudentNameButton>();

    private Classroom _currentClassroom;
    private int _currentState = -1;

    private GameState _gameState;
    private EnvironmentConfig _environments;

    [Inject]
    void Construct(GameState gameState, EnvironmentConfig environmentConfig)
    {
        _gameState = gameState;
        _environments = environmentConfig;
    }

    public void Awake()
    {
        _draggedSeatImage = DraggedSeatHolder.GetComponent<Image>();

        if (!DragDropSupported)
            DragInfoText.gameObject.SetActive(false);
    }

    public void Init(Classroom classroom)
    {
        if (_currentClassroom != classroom)
        {
            _currentState = -1;
            _isDefaultStudentSelected = false;
            _selectedNameButton = null;

            _currentClassroom = classroom;
        }

        if (_currentState != (int)classroom.InitialState)
        {
            _currentState = (int)classroom.InitialState;
            StartCoroutine(CoUpdate());
        }
    }

    private IEnumerator CoUpdate()
    {
        if (_studentSeats != null)
            _studentSeats.Clear();

        DraggedSeatHolder.SetActive(false);

        foreach (Transform l in SeatHolder.transform)
        {
            if (l.name.Equals("InfoText")) continue;
            Destroy(l.gameObject);
        }

        yield return null;

        
        if (_currentClassroom != null)
        {
            var cs = (_environments.GetEnvironment(_gameState.Scenario.EnvironmentId) as Classroom).ClassRoomStates[_gameState.Scenario.EnvironmentState]; //_currentClassroom.ClassRoomStates.FirstOrDefault(x => x.Id == _currentState) ?? _currentClassroom.ClassRoomStates.First();

            var diarekt = (DiagramHolder.transform as RectTransform);
            diarekt.sizeDelta = new Vector2(_currentClassroom.Width * 40, _currentClassroom.Height * 40);
            var rect = diarekt.rect;
            var ratio = rect.width / rect.height;

            var hr = (DiagramHolder.transform.parent.transform as RectTransform).rect;
            if (hr.width / hr.height > rect.width / rect.height)
            {
                diarekt.sizeDelta = new Vector2(hr.height * ratio, hr.height);
            }
            else
            {
                diarekt.sizeDelta = new Vector2(hr.width, hr.width * (1 / ratio));
            }

            var pw = diarekt.sizeDelta.x / _currentClassroom.Width;
            var ph = diarekt.sizeDelta.y / _currentClassroom.Height;

            int seatCount = cs.SeatLocations.Count;
            int condition = Math.Min(_gameState.NPCs.Count, seatCount);
            for (int i = 0; i < condition; i++)
            {
                var seatIndex = (_gameState.NPCs[i] as Student).Id;
                if (seatIndex < seatCount)
                {
                    var sl = cs.SeatLocations[seatIndex];
                    CoUpdate(sl, pw, ph, diarekt, i);
                    yield return null;
                }
            }

            InitializeStudentNameButtons();
            SelectStudentNameButton(_selected);
        }
    }

    private void CoUpdate(SeatLocation sl, float pw, float ph, RectTransform diarekt, int i)
    {
        if (i >= _gameState.NPCs.Count)
        {
            Debug.LogError("Too many students to fit the classroom!");
            return;
        }
        var seat = (RectTransform)Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/DetailedReportView/StudentSeat"), SeatHolder.transform).transform;
        seat.anchoredPosition = new Vector2(-sl.PosX * pw - 15, -sl.PosZ * ph);
        seat.rotation = Quaternion.Euler(0, 0, -1 * Convert.ToSingle(sl.Rotation.Split(',')[1]));
        seat.localScale = Vector3.one * (diarekt.sizeDelta.x * 0.1f) / seat.sizeDelta.x;

        StudentSeat seatScript = seat.GetComponent<StudentSeat>();

        Student student = _gameState.NPCs[i] as Student; //_currentClassroom.students[i].GetActor();
        seatScript.StudentInfo = student;

        seat.GetComponent<Button>().onClick.AddListener(() =>
        {
            SelectStudent(seatScript.StudentInfo);
            SelectStudentNameButton(seatScript);
        });

        if (DragDropSupported)
        {
            seatScript.SeatBeginDrag += OnSeatDragBegin;
            seatScript.SeatDrop += OnSeatDrop;
        }

        seatScript.PointerEnter += OnPointerEnter;
        seatScript.PointerExit += OnPointerExit;

        if (_studentSeats == null) _studentSeats = new List<StudentSeat>();
        _studentSeats.Add(seatScript);

        if (!_isDefaultStudentSelected)
        {
            _isDefaultStudentSelected = true;
            SelectStudent(seatScript.StudentInfo);
        }
    }

    private void ShowStudent(Student student)
    {
        StudentInfoSetup.Show(student);
    }

    private void InitializeStudentNameButtons()
    {
        int studentCount = _gameState.NPCs.Count;
        if (_instantiatedButtons.Count <= studentCount)
        {
            int necessaryButtonCount = studentCount - _instantiatedButtons.Count;
            for (int i = 0; i < studentCount; i++)
            {
                if (i < necessaryButtonCount)
                {
                    //Instantiate Buttons
                    StudentNameButton button = Instantiate(NameButtonPrefab, StudentNamesHolder.transform);
                    button.ButtonHeight = 30; //todo ceren : burayıotomatik alması lazım aslında
                    _instantiatedButtons.Add(button);
                    button.StudentButtonComponent.onClick.AddListener(() => StudentNameButtonClicked(button));
                }

                StudentNameButton script = _instantiatedButtons[i];
                script.Initialize(_gameState.NPCs[i] as Student/*_currentClassroom.students[i].GetActor()*/);
            }
        }
        else
        {
            int deactivateStartIndex = studentCount;
            for (int i = 0; i < _instantiatedButtons.Count; i++)
            {
                if (i < deactivateStartIndex)
                {
                    StudentNameButton script = _instantiatedButtons[i];
                    script.Initialize(_gameState.NPCs[i] as Student/*_currentClassroom.students[i].GetActor()*/);
                }
                else
                {
                    _instantiatedButtons[i].Deactivate();
                }
            }
        }
    }

    #region Student Seat Drag Drop Operations
    private IEnumerator DraggedStudentDropControl()
    {
        RectTransform seatHolderRectTransform = SeatHolder.GetComponent<RectTransform>();
        Vector3 minPos = new Vector3(seatHolderRectTransform.rect.xMin, seatHolderRectTransform.rect.yMin);
        Vector3 maxPos = new Vector3(seatHolderRectTransform.rect.xMax, seatHolderRectTransform.rect.yMax);

        while (_draggedSeat != null)
        {
            Vector2 newMousePos;
            //Vector3 newMousePos = SeatHolder.transform.InverseTransformPoint(Input.mousePosition);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(seatHolderRectTransform, Input.mousePosition, Camera.main, out newMousePos);
            float xPoS = Mathf.Clamp(newMousePos.x, minPos.x, maxPos.x);
            float yPoS = Mathf.Clamp(newMousePos.y, minPos.x, maxPos.y);
            DraggedSeatHolder.transform.localPosition = new Vector3(xPoS, yPoS, 1);
            yield return null;
        }
    }

    private void OnSeatDragBegin(StudentSeat seat)
    {
        if (_isDragInProcess) return;
        _isDragInProcess = true;
        seat.SeatEndDrag += OnSeatDragEnd;

        _draggedSeat = seat;
        _seatToBeDropped = null;
        _draggedSeatImage.sprite = seat.StudentImage.sprite;
        DraggedSeatHolder.SetActive(true);
        seat.StudentImage.enabled = false;

        StartCoroutine(DraggedStudentDropControl());
    }

    private void OnSeatDragEnd(StudentSeat seat)
    {
        seat.SeatEndDrag -= OnSeatDragEnd;
        seat.StudentImage.enabled = true;

        if (_seatToBeDropped != null)
        {
            int droppedSeatNumber = _seatToBeDropped.StudentInfo.Id;
            int draggedSeatNumber = _draggedSeat.StudentInfo.Id;
            bool updateStudentInfoPanel = (_draggedSeat == _selected);

            _seatToBeDropped.StudentInfo.Id = draggedSeatNumber;
            _draggedSeat.StudentInfo.Id = droppedSeatNumber;

            Student droppedStudent = _seatToBeDropped.StudentInfo;
            Student draggedStudent = _draggedSeat.StudentInfo;
            int droppedStudentPlacement = droppedStudent.PlacementNumber;
            int draggedStudentPlacement = draggedStudent.PlacementNumber;

            droppedStudent.PlacementNumber = draggedStudentPlacement;
            draggedStudent.PlacementNumber = droppedStudentPlacement;

            _seatToBeDropped.StudentInfo = draggedStudent;
            _draggedSeat.StudentInfo = droppedStudent;

            if (updateStudentInfoPanel)
            {
                _selected = _draggedSeat;
                ShowStudent(_selected.StudentInfo);
                SelectStudentNameButton(_selected);
            }
        }

        _draggedSeat = null;
        _seatToBeDropped = null;
        DraggedSeatHolder.SetActive(false);

        _isDragInProcess = false;
    }

    private void OnSeatDrop(StudentSeat seat)
    {
        if (!_isDragInProcess) return;
        _seatToBeDropped = seat;
        _seatToBeDropped.HoverHighlight.SetActive(false);
    }

    private void OnPointerEnter(StudentSeat seat)
    {
        if (!_isDragInProcess) return;
        seat.HoverHighlight.SetActive(true);
    }
    private void OnPointerExit(StudentSeat seat)
    {
        if (!_isDragInProcess) return;
        seat.HoverHighlight.SetActive(false);
    }

    #endregion

    private void SelectStudent(Student info)
    {
        if (_selected != null)
            _selected.SetActive(false);

        StudentSeat seat = _studentSeats.First(s => s.StudentInfo == info);
        _selected = seat;
        _selected.SetActive(true);
        ShowStudent(_selected.StudentInfo);

        if (OnClickedStudent != null)
            OnClickedStudent(info);
    }

    private void SelectStudentNameButton(StudentSeat seatScript, bool repositionSrollRect = true)
    {
        StudentNameButton button = _instantiatedButtons.FirstOrDefault(s => s.StudentInfo == seatScript.StudentInfo);
        if (_selectedNameButton != null && _selectedNameButton == button)
        {
            _selectedNameButton.IsSelected = true;
            return;
        }

        if (_selectedNameButton != null)
            _selectedNameButton.IsSelected = false;

        _selectedNameButton = button;

        if (button == null)
            return;

        _selectedNameButton.IsSelected = true;

        //Focus Scrollrect To Specified Content
        if (!repositionSrollRect) return;
        Canvas.ForceUpdateCanvases();
        float viewportYMin = StudentNamesViewport.rect.yMin;
        float buttonYmin =
            (((Vector2)StudentNamesViewport.transform.InverseTransformPoint(button.transform.position)).y -
             button.ButtonHeight);
        float viewportYMax = StudentNamesViewport.rect.yMax;
        float buttonYmax =
            (((Vector2)StudentNamesViewport.transform.InverseTransformPoint(button.transform.position)).y);

        if (buttonYmin < viewportYMin)
        {
            StudentNamesContent.anchoredPosition = new Vector2(StudentNamesContent.anchoredPosition.x, StudentNamesContent.anchoredPosition.y - (buttonYmin - viewportYMin));
        }
        else if (buttonYmax > viewportYMax)
        {
            StudentNamesContent.anchoredPosition = new Vector2(StudentNamesContent.anchoredPosition.x, StudentNamesContent.anchoredPosition.y - (buttonYmax - viewportYMax));
        }
    }

    private void SelectStudentNameButton(StudentNameButton button, bool repositionSrollRect = true)
    {
        if (_selectedNameButton != null && _selectedNameButton == button) return;
        if (_selectedNameButton != null) _selectedNameButton.IsSelected = false;
        _selectedNameButton = button;
        _selectedNameButton.IsSelected = true;

        //Focus Scrollrect To Specified Content
        if (!repositionSrollRect) return;
        Canvas.ForceUpdateCanvases();
        float viewportYMin = StudentNamesViewport.rect.yMin;
        float buttonYmin =
            (((Vector2)StudentNamesViewport.transform.InverseTransformPoint(button.transform.position)).y -
             button.ButtonHeight);
        float viewportYMax = StudentNamesViewport.rect.yMax;
        float buttonYmax =
            (((Vector2)StudentNamesViewport.transform.InverseTransformPoint(button.transform.position)).y);

        if (buttonYmin < viewportYMin)
        {
            StudentNamesContent.anchoredPosition = new Vector2(StudentNamesContent.anchoredPosition.x, StudentNamesContent.anchoredPosition.y - (buttonYmin - viewportYMin));
        }
        else if (buttonYmax > viewportYMax)
        {
            StudentNamesContent.anchoredPosition = new Vector2(StudentNamesContent.anchoredPosition.x, StudentNamesContent.anchoredPosition.y - (buttonYmax - viewportYMax));
        }
    }

    private void StudentNameButtonClicked(StudentNameButton button)
    {
        SelectStudentNameButton(button, false);
        SelectStudent(button.StudentInfo);
    }
}