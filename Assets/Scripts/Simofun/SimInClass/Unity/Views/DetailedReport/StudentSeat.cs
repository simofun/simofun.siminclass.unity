﻿using Sinifta.Actors;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StudentSeat : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Image StudentImage;
    public GameObject HoverHighlight;
    public GameObject ActiveHighlight;
    public CanvasGroup CanvasGroup;

    private Student _studentInfo;
    public Student StudentInfo
    {
        get { return _studentInfo; }
        set
        {
            if (_studentInfo == value) return;
            _studentInfo = value;
            StudentImage.sprite = Resources.Load<Sprite>("Icons/Npc/" + _studentInfo.Icon);
        }
    }

    public event Action<StudentSeat> SeatBeginDrag;
    public event Action<StudentSeat> SeatEndDrag;
    public event Action<StudentSeat> SeatDrop;
    public event Action<StudentSeat> PointerEnter;
    public event Action<StudentSeat> PointerExit;

    void Start()
    {
        StudentImage.transform.eulerAngles = Vector3.zero;
    }

    public void SetActive(bool s)
    {
        ActiveHighlight.SetActive(s);
    }

    #region Events
    public void OnBeginDrag(PointerEventData eventData)
    {
        CanvasGroup.blocksRaycasts = false;
        if (SeatBeginDrag != null) SeatBeginDrag(this);
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        CanvasGroup.blocksRaycasts = true;
        if (SeatEndDrag != null) SeatEndDrag(this);
    }
    public void OnDrag(PointerEventData eventData)
    {

    }
    public void OnDrop(PointerEventData eventData)
    {
        if (SeatDrop != null) SeatDrop(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (PointerEnter != null) PointerEnter(this);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        if (PointerExit != null) PointerExit(this);
    }
    #endregion
}
