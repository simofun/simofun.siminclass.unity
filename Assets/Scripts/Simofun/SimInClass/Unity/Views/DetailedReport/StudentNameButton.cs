﻿using Sinifta.Actors;
using UnityEngine;
using UnityEngine.UI;

public class StudentNameButton : MonoBehaviour
{
    public Text StudentName;
    public Image BackgroundImage;

    public Sprite NormalBackground;
    public Sprite SelectedBackground;

    public Button StudentButtonComponent;

    private bool _isSelected;
    public bool IsSelected
    {
        get { return _isSelected; }
        set
        {
            _isSelected = value;
            BackgroundImage.sprite = value ? SelectedBackground : NormalBackground;
        }
    }

    public float ButtonHeight { get; set; }
    public Student StudentInfo { get; private set; }

    public int FontSize
    {
        get { return StudentName.fontSize; }
        set { StudentName.fontSize = value; }
    }

    public void Initialize(Student studentInfo)
    {
        StudentInfo = studentInfo;
        StudentName.text = StudentInfo.Fullname;
        IsSelected = false;
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        StudentInfo = null;
        gameObject.SetActive(false);
    }
}
