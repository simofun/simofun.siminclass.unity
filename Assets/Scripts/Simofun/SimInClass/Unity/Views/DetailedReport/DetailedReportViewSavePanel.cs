﻿using UnityEngine.UI;
using UnityEngine;
using System.IO;
using Sinifta.Report;
using KarmaFramework.Localization;
using Zenject;
using System;
using System.Text;
using KarmaFramework.API;
using Newtonsoft.Json;

public class DetailedReportViewSavePanel : MonoBehaviour
{
	[SerializeField]
	private GameObject[] pages;

	[SerializeField]
	private InputField reportNameInputField;

	[SerializeField]
	private Text savedText;

	[SerializeField]
	private Text errorText;

	private EndGameReport lastSavedReport = null;
	private EndGameReport report;

	private Color reportNameInputFieldInvalidBg = new Color(1f, 0.3f, 0.25f, 1f);
	private Color reportNameInputFieldDefaultBg;

	private GameState _gameState;

	[Inject]
	void Construct(GameState gameState)
	{
		_gameState = gameState;
	}

	private void Awake()
	{
		reportNameInputFieldDefaultBg = reportNameInputField.targetGraphic.color;
	}

	public void Show(EndGameReport report)
	{
		this.report = report;
		gameObject.SetActive(true);

		if (lastSavedReport == report)
			Show(1);
		else
			Show(0);
	}

	private void Show(int pageIndex)
	{
		for (int i = 0; i < pages.Length; i++)
			pages[i].SetActive(i == pageIndex);
	}

	public void Save()
	{
		var filename = reportNameInputField.text;
		if (filename.Length == 0 || filename.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0 || filename.Trim().Length == 0)
		{
			reportNameInputField.targetGraphic.color = reportNameInputFieldInvalidBg;
			errorText.text = Localizer.Instance.GetString("ReportNameIsNotValid");
		}

		var reportSavePath = Path.Combine(SiniftaUtility.ReportsSaveDir, filename + ".csr");
		if (File.Exists(reportSavePath))
		{
			reportNameInputField.targetGraphic.color = reportNameInputFieldInvalidBg;
			errorText.text = Localizer.Instance.GetString("ReportAlreadyExists");

			return;
		}

		reportNameInputField.targetGraphic.color = reportNameInputFieldDefaultBg;
		errorText.text = string.Empty;

		if (SaveReport(report, reportSavePath))
		{
			savedText.text = Localizer.Instance.GetString("ScorePanelReportSavedTo", reportSavePath);
			lastSavedReport = report;
		}
		else
        {
			savedText.text = Localizer.Instance.GetString("ScorePanelReportCouldNotBeSaved");
        }

		Hide();
	}

	public static bool SaveReport(EndGameReport report, string path)
	{
		Directory.CreateDirectory(Path.GetDirectoryName(path));

        report.Username = InterLevelData.AppConfig.UserInfo.Name;
		ReportSummary summary = new ReportSummary(report);
		string summaryJson = JsonConvert.SerializeObject(summary);
		try
		{
			//StringBuilder sb = new StringBuilder();
			//writer.TypeHinting = true;
			//writer.PrettyPrint = true;
			string sb = JsonConvert.SerializeObject(report);
			File.WriteAllText(path, summaryJson.Length.ToString("D10") + summaryJson + sb);
		}
		catch (Exception e)
		{
			Debug.LogException(e);
			return false;
		}


		return true;
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}
}
