﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sinifta.Actors;
using KarmaFramework.AISettingsNamespace;

public class SetupStudentView : MonoBehaviour
{
	public Text Name;
	public Text Stuff;

	public Text O_Label;
	public Text C_Label;
	public Text E_Label;
	public Text A_Label;
	public Text N_Label;

	public OceanSlider O_Slider;
	public OceanSlider C_Slider;
	public OceanSlider E_Slider;
	public OceanSlider A_Slider;
	public OceanSlider N_Slider;

	public Image StudentImage;

	IEnumerator Start()
	{
		yield return null;

		O_Label.font.RequestCharactersInTexture(O_Label.text + C_Label.text + E_Label.text + A_Label.text + N_Label.text, O_Label.fontSize, FontStyle.Normal);

		float maxTextSize = 0f;
		maxTextSize = Mathf.Max(maxTextSize, CalculateStringLength(O_Label.font, O_Label.text, O_Label.fontSize));
		maxTextSize = Mathf.Max(maxTextSize, CalculateStringLength(C_Label.font, C_Label.text, O_Label.fontSize));
		maxTextSize = Mathf.Max(maxTextSize, CalculateStringLength(E_Label.font, E_Label.text, O_Label.fontSize));
		maxTextSize = Mathf.Max(maxTextSize, CalculateStringLength(A_Label.font, A_Label.text, O_Label.fontSize));
		maxTextSize = Mathf.Max(maxTextSize, CalculateStringLength(N_Label.font, N_Label.text, O_Label.fontSize));

		Vector2 elementSize = O_Label.rectTransform.sizeDelta;
		elementSize.x = maxTextSize;
		O_Label.rectTransform.sizeDelta = elementSize;
		C_Label.rectTransform.sizeDelta = elementSize;
		E_Label.rectTransform.sizeDelta = elementSize;
		A_Label.rectTransform.sizeDelta = elementSize;
		N_Label.rectTransform.sizeDelta = elementSize;

		Vector2 oceanSize = new Vector2(Mathf.Min(150f, ((RectTransform)O_Label.transform.parent).rect.width - maxTextSize - 10), 0f);
		((RectTransform)O_Slider.transform).anchoredPosition = new Vector2(maxTextSize + 10f, 0f);
		((RectTransform)C_Slider.transform).anchoredPosition = new Vector2(maxTextSize + 10f, 0f);
		((RectTransform)E_Slider.transform).anchoredPosition = new Vector2(maxTextSize + 10f, 0f);
		((RectTransform)A_Slider.transform).anchoredPosition = new Vector2(maxTextSize + 10f, 0f);
		((RectTransform)N_Slider.transform).anchoredPosition = new Vector2(maxTextSize + 10f, 0f);

		((RectTransform)O_Slider.transform).sizeDelta = oceanSize;
		((RectTransform)C_Slider.transform).sizeDelta = oceanSize;
		((RectTransform)E_Slider.transform).sizeDelta = oceanSize;
		((RectTransform)A_Slider.transform).sizeDelta = oceanSize;
		((RectTransform)N_Slider.transform).sizeDelta = oceanSize;
	}

	public int CalculateStringLength(Font font, string message, int fontSize)
	{
		int totalLength = 0;

		CharacterInfo characterInfo = new CharacterInfo();
		char[] arr = message.ToCharArray();
		foreach (char c in arr)
		{
			font.GetCharacterInfo(c, out characterInfo, fontSize);
			totalLength += characterInfo.advance;
		}

		return totalLength;
	}

	public void Show(Student student)
	{
		Name.text = student.Name;
		Stuff.text = (student.Bio ?? "").Replace("/t", "").Replace("\n", "\n\n") + "\n";

		OceanPersonality ocean = student.Personality;
		O_Slider.Value = ocean.O;
		C_Slider.Value = ocean.C;
		E_Slider.Value = ocean.E;
		A_Slider.Value = ocean.A;
		N_Slider.Value = ocean.N;

		StudentImage.sprite = Resources.Load<Sprite>("Icons/Npc/" + student.Icon);
	}
}
