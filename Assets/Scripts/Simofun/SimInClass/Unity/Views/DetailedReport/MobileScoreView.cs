﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using KarmaFramework.API;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using KarmaFramework.UI;
using Simofun.Karma.Unity.Web.Api;
using Zenject;
using Simofun.SimInClass.Unity.HardCodeds;
using KarmaFramework.Reports;
using KarmaFramework.Localization;
using Simofun.Karma.UniRx.Unity;
using KarmaFramework.Views;
using System.Collections;

public class MobileScoreView : MonoBehaviour
{
    public Button ScenariosButton;
    public Button SaveReportButton;

    [Header("Ribbon")]
    public GameObject RibbonGameObject; //disable when unsuccesfull

    public Text LevelInfoText;

    [Header("Stars")]
    public List<Image> Stars;
    public Sprite FilledStar;
    public Sprite EmptyStar;

    [Header("Teacher Score Icons")]
    public Image Concentration;
    public Text ConcentrationValue;
    public List<Sprite> ConcentrationIcons;
    [Space(5)]
    public Image Enjoyment;
    public Text EnjoymentValue;
    public List<Sprite> EnjoymentIcons;
    [Space(5)]
    public Image Knowledge;
    public Text KnowledgeValue;
    public List<Sprite> KnowledgeIcons;

    [Space(5)]
    public Text GeneralScoreValue;

    private EndGameReport _report;

    [SerializeField]
    DetailedReportView detailedReport;
    float passScore;

    GameState gameState;
    MenuNavigator _navigator;
    IKarmaWebApiRestClient webApiClient;

    [Inject]
    public virtual void Construct(
        IKarmaWebApiRestClient webApiClient, 
        GameState gameState)
    {
        this.gameState = gameState;
        this.webApiClient = webApiClient;
        this._report = gameState.Scenario.EndGameReport;
    }

    void Start()
    {
        //ScenariosButton.onClick.AddListener(() => StartCoroutine(ReturnToMenuCoroutine()));
        ScenariosButton.onClick.AddListener(() => KarmaMessageBus.Publish(new EndGameEvent()));
        SaveReportButton.onClick.AddListener(() => this.detailedReport.Show());
    }

    private void OnEnable()
    {
        var levelInfos = InterLevelData.AppConfig.LevelInfos;

       passScore =  levelInfos.Infos.Any(x => x.Level == _report.Level)
                    ? levelInfos.Infos.First(x => x.Level == _report.Level).Score
                    : 0;
       SetResults();
    }

    public void SetResults()
    {
        var success =  gameState.Scenario.EndGameReport.Score > passScore;
        RibbonGameObject.SetActive(success);
        LevelInfoText.text = Localizer.Instance.GetString(success ? "LevelPass" : "LevelFail");
        GeneralScoreValue.text = Localizer.Instance.GetString("ScorePanelInfoScore", true) + gameState.Scenario.EndGameReport.Score.ToString() + "/100";

        SetStars();
        SetScore();

    }

    /// <summary>
    /// Setup the scores of the teacher for display
    /// </summary>
    private void SetScore()
    {
        var teacherScores = gameState.Scenario.EndGameReport.TeacherSnapshots.Last();

        // teacherScores.x = Concentration
        // teacherScores.y = Enjoyment
        // teacherScores.z = Knowledge

        AssignScore(teacherScores.x, Concentration, ConcentrationValue, ConcentrationIcons);
        AssignScore(teacherScores.y, Enjoyment, EnjoymentValue, EnjoymentIcons);
        AssignScore(teacherScores.z, Knowledge, KnowledgeValue, KnowledgeIcons);
    }

    /// <summary>
    /// Change the sprite of the icon depending on score, and set the value
    /// </summary>
    /// <param name="score"></param>
    /// <param name="icon"></param>
    /// <param name="value"></param>
    /// <param name="sprites"></param>
    private void AssignScore(float score, Image icon, Text value, List<Sprite> sprites)
    {
        value.text = score + " / 100";

        // Sprite 0 = Green, Sprite 1 = Yellow, Sprite 2 = Red
        if (score < 33)
            icon.sprite = sprites[2];
        else if (score < 66)
            icon.sprite = sprites[1];
        else
            icon.sprite = sprites[0];
    }


    private void SetStars()
    {
        var count = _report.Score / 33;
        for (int i = 0; i < Stars.Count; i++)
        {
            var image = Stars[i];
            image.sprite = count > i ? FilledStar : EmptyStar;
        }
    }

    //IEnumerator ReturnToMenuCoroutine()
    //{
    //    SiniftaUtility.GetUserProgress(this.webApiClient, InterLevelData.AppConfig.UserInfo, () =>
    //    SceneManager.LoadSceneAsync(Scenes.MainMenu));
    //    yield return new WaitForSeconds(0.2f);
    //    KarmaMessageBus.Publish(new EndGameEvent());
    //}
}
