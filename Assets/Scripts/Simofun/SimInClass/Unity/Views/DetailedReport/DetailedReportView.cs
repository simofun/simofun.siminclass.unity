﻿using KarmaFramework.API;
using KarmaFramework.KarmaUtils;
using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using KarmaFramework.Views;
using LitJson;
using Newtonsoft.Json;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Web.Api;
using Sinifta.Actions.TeacherActions;
using Sinifta.Actors;
using Sinifta.AI;
using Sinifta.Report;
using Sinifta.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class DetailedReportView : UIElement
{
	#region Fields
	public enum SliderValueColors
	{
		Red, Yellow, Green
	}

	[Header("General Variables")]
	public ScrollRect ScrollView;
	public Transform PanelContainer;
	public RectTransform ScrollableAreaViewport;
	public Transform TutorialPanelContainer;

	public Sprite YellowSpriteFront;
	public Sprite YellowSpriteBack;
	public Sprite RedSpriteFront;
	public Sprite RedSpriteBack;
	public Sprite GreenSpriteFront;
	public Sprite GreenSpriteBack;

	public Button ScenariosButton;
	public Button MenuButton;

	[Header("Tutorial Variables")]
	public List<Transform> Panels;
	public List<Animator> CompletedImages;

	[Header("Header Variables")]
	public Text UserName;
	public Text DateTimeText;
	public Text ScenarioNameText;
	public Text ScoreText;
	public Text TeacherGreetingText;

	public Transform SaveReportButton;
	public DetailedReportViewSavePanel SavePanel;

	[Header("Classroom Parameters Variables")]
	public WMG_Axis_Graph ClassroomParamsGraph;
	public WMG_Series ClassroomParamsLessonMethods;
	public WMG_Series ClassroomParamsConcentration;
	public WMG_Series ClassroomParamsEnjoyment;
	public WMG_Series ClassroomParamsKnowledge;

	[Header("Tech Analysis Variables")]
	public WMG_Pie_Graph techAnalysisGraph;
	public Text noMissionsText;
	public GameObject piePanel;
	public Text completedMissionsText;
	public Text failedMissionsText;

	[Header("Student Parameters Variables")]
	public StudentTabView StudentTabView;
	public WMG_Axis_Graph StudentParamsGraph;
	public WMG_Series StudentParamsLessonMethods;
	public WMG_Series StudentParamsStudentActions;
	public WMG_Series StudentParamsConcentration;
	public WMG_Series StudentParamsEnjoyment;
	public WMG_Series StudentParamsKnowledge;

	[Header("Pedagogical Evaluation Variables")]
	public GameObject LessonActionPrefab;
	public RectTransform LessonActionsParent;

	[Header("Visualize")]
	public GameObject BlackPanel;

	private Student currentStudent;

	private TeacherActionResult[] teacherActions;
	private StudentActionResult[] studentActions;
	
	private Dictionary<int, List<SerializableVector3>> studentToSnapshots;
	private List<ActionResult> currentStudentActions = new List<ActionResult>();
	private SimpleReasonerData simpleReasonerData;
	private GameState _gameState;
	private ScenarioConfig scenarioConfig;
	
	private EndGameReport report;
	private GameObject objectsHolder;

	CanvasGroup content;

	IKarmaWebApiRestClient webApiClient;
	#endregion

	[Inject]
	public void Construct(
		GameState gameState,
		SimpleReasonerData simpleReasonerData,
		ScenarioConfig scenarioConfig,
		IKarmaWebApiRestClient webApiClient)
	{
		this.simpleReasonerData = simpleReasonerData;
		this._gameState = gameState;
		this.scenarioConfig = scenarioConfig;
		this.webApiClient = webApiClient;
	}

	void Awake()
	{
		 Type = PanelTypes.Score;
		this.objectsHolder = GameObject.Find("ObjectsHolder");
		this.objectsHolder.SetActive(false);
		BlackPanel.SetActive(false);
		this.content = this.GetComponentInChildren<CanvasGroup>();
		this.Hide();
		//EndGameReport rapor = LoadReport(@"C:\Users\akif.uslu\Documents\SiniftaReports\asdas.csr");
		//SetResults(rapor, false);
	}

	public void ExitButtonClicked() => this.Hide();

	public void Hide() => this.ChangeVisibility(false);

	public void Show() => this.ChangeVisibility(true);

	public void SetResults(EndGameReport report, bool showSaveButton)
	{
		objectsHolder.SetActive(true);
		BlackPanel.SetActive(true);
		this.report = report;	
		if (showSaveButton)
		{
			StartCoroutine(SendAllSnapshots(report));

			// Overwrite report user name except report area in 'MainMenu' scene
			report.Username = InterLevelData.AppConfig.UserInfo.Name;
		}

		SaveReportButton.gameObject.SetActive(showSaveButton);
		SaveReportButton.GetComponent<Button>().onClick.AddListener(() => SaveReport());
		teacherActions = report.LectureResult.TeacherResult.OfType<TeacherActionResult>().ToArray();
		studentActions = report.LectureResult.StudentResult.OfType<StudentActionResult>().ToArray();
		_gameState.Scenario = this.scenarioConfig.GetScenario(report.Level);
		_gameState.Scenario.EnvironmentState = report.EnvironmentState;
		_gameState.Scenario.EnvironmentId = report.EnvironmentId;
		_gameState.Scenario.EndGameReport.Score = report.LectureResult.Score;
		_gameState.Scenario.EndGameReport.TeacherSnapshots = report.LectureResult.TeacherSnapshots;
		report.ScenarioName = _gameState.Scenario.ReadableName;

		report.Classroom.Width = report.Width;
		report.Classroom.Height = report.Height;
		studentToSnapshots = new Dictionary<int, List<SerializableVector3>>();
		foreach (var studentSnapshot in report.LectureResult.StudentSnapshots)
			studentToSnapshots[studentSnapshot.StudentId] = studentSnapshot.Snapshots;

		PanelContainer.gameObject.SetActive(true);
		TutorialPanelContainer.gameObject.SetActive(false);

		FillHeaderInfo();
		FillClassroomParametersSection();
		FillTechAnalysisSection();
		FillStudentParametersSection();
		FillPedagogicalEvaluationSection();

		ScrollView.verticalNormalizedPosition = 1f;
	}

	public void SetResultsTutorial(int completedTutorialCount)
	{
		PanelContainer.gameObject.SetActive(false);
		TutorialPanelContainer.gameObject.SetActive(true);

		StartCoroutine(InitializeTutorialPanel(completedTutorialCount));
	}

	//Player Info
	private void FillHeaderInfo()
	{
		UserName.text = report.Username;
		DateTimeText.text = report.Date;
		ScenarioNameText.text = report.ScenarioName;
		ScoreText.text = report.LectureResult.Score.ToString() + "/100";
	}

	private void FillClassroomParametersSection()
	{
		InitializeMindParametersGraph(3, report.LectureResult.TeacherSnapshots,
				ClassroomParamsGraph, ClassroomParamsLessonMethods, null, ClassroomParamsConcentration, ClassroomParamsEnjoyment, ClassroomParamsKnowledge);

		ClassroomParamsGraph.yAxis.AxisTitleString = Localizer.Instance.GetString("ScorePanelGraphYAxis");
	}

	private void FillTechAnalysisSection()
	{
		techAnalysisGraph.Init();

		failedMissionsText.text = "\n";
		completedMissionsText.text = "\n";

		techAnalysisGraph.sliceLabels[0] = Localizer.Instance.GetString("Successful");
		techAnalysisGraph.sliceLabels[1] = Localizer.Instance.GetString("Failed");

		int successfulTechMissions = 0;
		int failedTechMissions = 0;

		foreach (TeacherLectureActionResult lectureResult in report.LectureResult.ActivityResults)
		{
			if (lectureResult.TechActionRequired)
			{
				string time = Localizer.Instance.GetString("TimeRangeMinutes", lectureResult.StartTime / 6, lectureResult.FinishTime / 6);
				string title = Localizer.Instance.GetString(lectureResult.Type);

				if (lectureResult.ActionResult == KarmaFramework.ActionCore.ActionResult.LectureFailed)
				{
					failedTechMissions++;
					failedMissionsText.text += time + " " + title + "\n";
				}
				/*else if (lectureResult.TechState == LectureTechState.CompletedBoard)
				{
					successfulTechMissions++;
					completedMissionsText.text += time + " " + title + Localizer.Instance.GetString(StringId.ScorePanelTPACKShowImageOnBoardSuccessful) + "\n";
				}
				else if (lectureResult.TechState == LectureTechState.CompletedTablet)
				{
					successfulTechMissions++;
					completedMissionsText.text += time + " " + title + Localizer.Instance.GetString(StringId.ScorePanelTPACKSendDocumentToStudentsSuccessful) + "\n";
				}*/
			}
		}

		if (failedTechMissions + successfulTechMissions > 0)
		{
			noMissionsText.gameObject.SetActive(false);
			piePanel.SetActive(true);

			List<float> myList = new List<float>(2);
			myList.Add(successfulTechMissions);
			myList.Add(failedTechMissions);
			techAnalysisGraph.sliceValues.SetList(myList);
			//techAnalysisGraph.sliceValues[0] = successfulTechMissions;
			//techAnalysisGraph.sliceValues[1] = failedTechMissions;
		}
		else
		{
			//techAnalysisGraph.gameObject.SetActive(false);
			noMissionsText.gameObject.SetActive(true);
			piePanel.SetActive(false);
		}
	}

	private void FillStudentParametersSection()
	{
		StudentTabView.Init(report.Classroom);

		// Prevents an ArgumentOutOfRangeException at View Reports scene
		StudentParamsLessonMethods.pointValues.Clear();
		StudentParamsLessonMethods.PointSpriteUpdated -= MindParametersGraphUpdateLessonMethods;

		StudentParamsStudentActions.pointValues.Clear();
		StudentParamsStudentActions.PointSpriteUpdated -= MindParametersGraphUpdateStudentActions;

		StudentTabView.OnClickedStudent += (student) =>
		{
			if (currentStudent == student)
				return;

			currentStudent = student;
			currentStudentActions.Clear();

			// Misbehaviours
			foreach (StudentActionResult studentAction in studentActions)
			{
				if (studentAction.StudentId == student.Id)
					currentStudentActions.Add(studentAction);
			}

			// Dealing methods
			foreach (TeacherActionResult teacherAction in teacherActions)
			{
				if (teacherAction.ActionResult == KarmaFramework.ActionCore.ActionResult.None)
					continue;

				if (teacherAction.TargetStudentIds.Contains(student.Id))
					currentStudentActions.Add(teacherAction);
			}

			InitializeMindParametersGraph(6, studentToSnapshots[student.Id],
				StudentParamsGraph, StudentParamsLessonMethods, StudentParamsStudentActions, StudentParamsConcentration, StudentParamsEnjoyment, StudentParamsKnowledge);
		};
	}

	private void FillPedagogicalEvaluationSection()
	{
		foreach (Transform child in LessonActionsParent)
			Destroy(child.gameObject);

		IEnumerable<TeacherActionResult> teacherDealActions = teacherActions.Where(s => s.ActionResult != KarmaFramework.ActionCore.ActionResult.None);
		IEnumerable<TeacherActionResult> allTeacherActions = report.LectureResult.ActivityResults.OfType<TeacherActionResult>().Concat(teacherDealActions).OrderBy(s => s.StartTime);
		foreach (TeacherActionResult action in allTeacherActions)
		{
			string time;
			string title;
			string content;
			string description;
			if (action.GetType() == typeof(TeacherActionResult))
			{
				//TOD:seda -> bu alan system.action.misbehaviour şeklinde gelen stringi misbehaviour olacak şekilde parse etmek için yazıldı
				#region ParseString
				var targetExecString = action.TargetActionType; 
				try
				{
					int typeIndex = targetExecString.LastIndexOf(".");
					if (typeIndex < 0)
						typeIndex = 0;
					else
						typeIndex++;
					targetExecString = Localizer.Instance.GetString(targetExecString.Substring(typeIndex));
				}
				catch
				{
				}
				#endregion

				// Deal method
				time = Localizer.Instance.GetString("XMinuteAbbreviated", (action.StartTime / 6).ToString());
				title = Localizer.Instance.GetString("ScorePanelDealingTitle") + ": " + targetExecString;
				content = Localizer.Instance.GetString("MisbehaviorDealtWith", targetExecString, Localizer.Instance.GetString(action.ActionResultString));

				if (action.ActionResult == KarmaFramework.ActionCore.ActionResult.DealedCorrectly)
					description = Localizer.Instance.GetString("MisbehaviorDealtCorrectly");
				else
				{
					if (action.ActionResult == KarmaFramework.ActionCore.ActionResult.DealedWrong)
						description = Localizer.Instance.GetString("MisbehaviorDealtWrong");
					else
						description = Localizer.Instance.GetString("MisbehaviorDealtIgnored");

					description += " " + Localizer.Instance.GetString("MisbehaviorExpectedDealingMethods") + "\n\n";

					SimpleReasonerDataObject misbehaviourProperties;

					if (simpleReasonerData.ActionDictionary.TryGetValue(System.Type.GetType(action.TargetActionType), out misbehaviourProperties))
					{
						foreach (var correctDeal in misbehaviourProperties.CorrectDealDictionary)
						{
							try
							{
								int typeNameStartIndex = correctDeal.Key.LastIndexOf('.');
								if (typeNameStartIndex < 0)
									typeNameStartIndex = 0;
								else
									typeNameStartIndex++;

								description += "- " + Localizer.Instance.GetString(correctDeal.Key.Substring(typeNameStartIndex)) + "\n";
							}
							catch
							{
							}
						}
					}
					else
					{
						description += "---";
					}
				}
			}
			else
			{
				TeacherLectureActionResult activity = (TeacherLectureActionResult)action;
				Type activityType = System.Type.GetType(activity.Type);

				// Teaching method
				time = Localizer.Instance.GetString("TimeRangeMinutes", action.StartTime / 6, action.FinishTime / 6);
				title = Localizer.Instance.GetString(action.ActionName);

				var scenarioFlow = this._gameState.Scenario.FlowElements
					.FirstOrDefault(e => e.Action.Name == action.ActionName);
				content = scenarioFlow?.Action.Text ?? activity.Text;

				if (typeof(TeacherIntroQuestion).IsAssignableFrom(activityType) || typeof(TeacherIntroDailyLife).IsAssignableFrom(activityType) || typeof(TeacherIntroHistory).IsAssignableFrom(activityType))
				{
					description = Localizer.Instance.GetString("ScorePanelPedagogicalTeacherIntroDescription");
				}
				else if (typeof(TeacherIntroNoWarmup).IsAssignableFrom(activityType))
				{
					description = Localizer.Instance.GetString("ScorePanelPedagogicalTeacherIntroNoWarmupDescription"); 
				}
				else if (typeof(TeacherMidEbook).IsAssignableFrom(activityType))
				{
					description = Localizer.Instance.GetString("ScorePanelPedagogicalMidEbookDescription"); 
				}
				else if (typeof(TeacherMidConcrete).IsAssignableFrom(activityType))
				{
					description = Localizer.Instance.GetString("ScorePanelPedagogicalMidConcreteDescription"); 
				}
				else if (typeof(TeacherMidAbstract).IsAssignableFrom(activityType))
				{
					description = Localizer.Instance.GetString("ScorePanelPedagogicalMidAbstractDescription"); 
				}
				else if (typeof(TeacherMidBook).IsAssignableFrom(activityType))
				{
					description = Localizer.Instance.GetString("ScorePanelPedagogicalMidBookDescription");
				}
				else if (typeof(TeacherOutroQuiz).IsAssignableFrom(activityType))
				{
					description = Localizer.Instance.GetString("ScorePanelPedagogicalOutroQuizDescription"); 
				}
				else if (typeof(TeacherOutroHomework).IsAssignableFrom(activityType))
				{
					description = Localizer.Instance.GetString("ScorePanelPedagogicalOutroHomeworkDescription"); 
				}
				else
				{
					description = "---"; 
				}

				if (activity.TechActionRequired)
				{
					if (activity.ActionResult == KarmaFramework.ActionCore.ActionResult.LectureFailed)
						description += "\n\n" + Localizer.Instance.GetString("ScorePanelPedagogicalTechFailed");
					/*else if (activity.TechState == LectureTechState.CompletedBoard)
						description += "\n\n" + Localizer.Instance.GetString("ScorePanelPedagogicalShowImageOnBoard");
					else if (activity.TechState == LectureTechState.CompletedTablet)
						description += "\n\n" + Localizer.Instance.GetString("ScorePanelPedagogicalSendDocumentToStudents");*/
				}
			}

			ScorePanelActionEntry entryGo = Instantiate(LessonActionPrefab, LessonActionsParent).GetComponent<ScorePanelActionEntry>();
			entryGo.Init(time, title, content, description);
		}
	}

	#region Helper Functions
	private void InitializeMindParametersGraph(int xAxisStepSize, List<SerializableVector3> mindPoints, WMG_Axis_Graph graph,
		WMG_Series lessonMethodsSeries, WMG_Series studentActionsSeries, WMG_Series concentrationSeries, WMG_Series enjoymentSeries, WMG_Series knowledgeSeries)
	{
		graph.Init();

		graph.xAxis.AxisTitleString = Localizer.Instance.GetString("ScorePanelGraphXAxis");
		//graph.yAxis.AxisTitleString = Localizer.Instance.GetString( StringId.ScorePanelGraphYAxis );
		concentrationSeries.seriesName = Localizer.Instance.GetString("Concentration");
		enjoymentSeries.seriesName = Localizer.Instance.GetString("Enjoyment");
		knowledgeSeries.seriesName = Localizer.Instance.GetString("Knowledge");

		List<SerializableVector3> originalPoints = mindPoints;

		int reducedPointsCount = originalPoints.Count / xAxisStepSize;
		List<Vector3> reducedPoints = new List<Vector3>(reducedPointsCount);
		for (int i = 0; i < reducedPointsCount; i++)
			reducedPoints.Add(originalPoints[i * xAxisStepSize]);

		List<Vector2> concentrationPoints = new List<Vector2>(reducedPoints.Count);
		List<Vector2> enjoymentPoints = new List<Vector2>(reducedPoints.Count);
		List<Vector2> knowledgePoints = new List<Vector2>(reducedPoints.Count);

		for (int i = 0; i < reducedPoints.Count; i++)
		{
			concentrationPoints.Add(new Vector2(i, reducedPoints[i].x));
			enjoymentPoints.Add(new Vector2(i, reducedPoints[i].y));
			knowledgePoints.Add(new Vector2(i, reducedPoints[i].z));
		}

		concentrationSeries.pointValues.Clear();
		concentrationSeries.pointValues.SetList(concentrationPoints);

		enjoymentSeries.pointValues.Clear();
		enjoymentSeries.pointValues.SetList(enjoymentPoints);

		knowledgeSeries.pointValues.Clear();
		knowledgeSeries.pointValues.SetList(knowledgePoints);

		List<Vector2> lessonMethodsPoints = new List<Vector2>(report.LectureResult.ActivityResults.Count);
		for (int i = 0; i < report.LectureResult.ActivityResults.Count; i++)
			lessonMethodsPoints.Add(new Vector2(i, i));

		lessonMethodsSeries.pointValues.Clear();
		lessonMethodsSeries.pointValues.SetList(lessonMethodsPoints);

		lessonMethodsSeries.PointSpriteUpdated -= MindParametersGraphUpdateLessonMethods;
		lessonMethodsSeries.PointSpriteUpdated += MindParametersGraphUpdateLessonMethods;

		if (studentActionsSeries != null)
		{
			List<Vector2> studentActionsPoints = new List<Vector2>(currentStudentActions.Count);
			for (int i = 0; i < currentStudentActions.Count; i++)
				studentActionsPoints.Add(new Vector2(i, i));

			studentActionsSeries.pointValues.Clear();
			studentActionsSeries.pointValues.SetList(studentActionsPoints);

			studentActionsSeries.PointSpriteUpdated -= MindParametersGraphUpdateStudentActions;
			studentActionsSeries.PointSpriteUpdated += MindParametersGraphUpdateStudentActions;
		}

		graph.tooltipDisplaySeriesName = false;
		graph.theTooltip.tooltipLabeler = (series, node) =>
		{
			if (series == lessonMethodsSeries)
			{
				int actionIndex = -1;
				for (int i = 0; i < series.pointValues.Count; i++)
				{
					if (series.getPoints()[i].GetComponent<WMG_Node>() == node)
					{
						actionIndex = i;
						break;
					}
				}

				TeacherLectureActionResult lectureActivity = report.LectureResult.ActivityResults[actionIndex];
				return Util.TimeFormat(lectureActivity.StartTime) + " - " + Util.TimeFormat(lectureActivity.FinishTime) + ": " + Localizer.Instance.GetString(lectureActivity.ActionName);
			}
			else if (series == studentActionsSeries)
			{
				int actionIndex = -1;
				for (int i = 0; i < series.pointValues.Count; i++)
				{
					if (series.getPoints()[i].GetComponent<WMG_Node>() == node)
					{
						actionIndex = i;
						break;
					}
				}

				if (actionIndex == -1)
					return string.Empty;

				ActionResult action = currentStudentActions[actionIndex];
				TeacherActionResult teacherAction = action as TeacherActionResult;
				if (teacherAction != null && !string.IsNullOrEmpty(teacherAction.TargetActionType))
				{
					// Misbehaviour
					string dealingResult;
					if (teacherAction.ActionResult == KarmaFramework.ActionCore.ActionResult.DealedCorrectly)
						dealingResult = Localizer.Instance.GetString("Successful");
					else if (teacherAction.ActionResult == KarmaFramework.ActionCore.ActionResult.DealedWrong)
						dealingResult = Localizer.Instance.GetString("Failed");
					else if (teacherAction.ActionResult == KarmaFramework.ActionCore.ActionResult.DealingIgnored)
						dealingResult = Localizer.Instance.GetString("Ignored");
					else
						dealingResult = "___";

					return Util.TimeFormat(action.StartTime) + ": " +
							Localizer.Instance.GetString("MisbehaviorDealtWith", Localizer.Instance.GetString("System.Type.GetType(teacherAction.TargetActionType)"),
							Localizer.Instance.GetString(teacherAction.ActionName)) + " (" + dealingResult + ")";
				}

				return Util.TimeFormat(action.StartTime) + ": " + Localizer.Instance.GetString(action.ActionName);
			}
			else
			{
				Vector2 nodeData = series.getNodeValue(node);
				return series.seriesName + " (" + Util.TimeFormat(((int)(nodeData.x * 240f / (reducedPointsCount - 1)))) + ", " + nodeData.y + ")";
			}
		};
	}

	private void MindParametersGraphUpdateLessonMethods(WMG_Series series, GameObject pointObject, int pointIndex)
	{
		TeacherLectureActionResult lectureActivity = report.LectureResult.ActivityResults[pointIndex];

		RectTransform tr = (RectTransform)pointObject.transform;
		Vector2 graphSize = ((RectTransform)series.theGraph.graphAreaBoundsParent.transform).sizeDelta;

		Vector2 sizeDelta = new Vector2(lectureActivity.StartTime / 240f, lectureActivity.FinishTime / 240f);
		Vector2 anchoredPosition = new Vector2(graphSize.x * 0.5f * (sizeDelta.y + sizeDelta.x), graphSize.y * 0.5f);
		sizeDelta = new Vector2(graphSize.x * (sizeDelta.y - sizeDelta.x), graphSize.y);

		tr.anchoredPosition = anchoredPosition;
		tr.sizeDelta = sizeDelta;

		Color color = lectureActivity.TimelineColor;
		color.a = 0.25f;
		tr.GetComponent<Image>().color = color;
	}

	private void MindParametersGraphUpdateStudentActions(WMG_Series series, GameObject pointObject, int pointIndex)
	{
		if (pointIndex >= currentStudentActions.Count)
			return;

		ActionResult studentAction = currentStudentActions[pointIndex];

		RectTransform tr = (RectTransform)pointObject.transform;
		Vector2 graphSize = ((RectTransform)series.theGraph.graphAreaBoundsParent.transform).sizeDelta;
		Vector2 anchoredPosition = new Vector2(graphSize.x * studentAction.StartTime / 240f, graphSize.y * 0.5f);
		Vector2 sizeDelta = new Vector2(3f, graphSize.y);

		tr.anchoredPosition = anchoredPosition;
		tr.sizeDelta = sizeDelta;

		Color color = new Color32(255, 100, 0, 255);
		color.a = 0.9f;
		tr.GetComponent<Image>().color = color;
	}

	public void SaveReport()
	{
		SavePanel.Show(report);
	}

	public void PrintReport()
	{
		StartCoroutine(_PrintReport());
	}

	public IEnumerator _PrintReport()
	{
		Texture2D ss;

		//KarmaCursor.ForceHideCursor = true;

		yield return new WaitForEndOfFrame();

		Vector3[] cornerPositions = new Vector3[4];
		GetComponent<RectTransform>().GetWorldCorners(cornerPositions);

		float xMin = Mathf.Infinity, xMax = Mathf.NegativeInfinity, yMin = Mathf.Infinity, yMax = Mathf.NegativeInfinity;
		for (int i = 0; i < 4; i++)
		{
			Vector3 v = cornerPositions[i];

			if (v.x < xMin) xMin = v.x;
			if (v.x > xMax) xMax = v.x;
			if (v.y < yMin) yMin = v.y;
			if (v.y > yMax) yMax = v.y;
		}

		int width = (int)(xMax - xMin);
		int height = (int)(yMax - yMin);

		ss = new Texture2D(width, height, TextureFormat.RGB24, false);

		ss.ReadPixels(new Rect(xMin, yMin, width, height), 0, 0, false);
		ss.Apply();

		//KarmaCursor.ForceHideCursor = false;
		//Print.PrintTexture(ss.EncodeToPNG(), 1,null);
	}

	#region TutorialPanel Methods
	private IEnumerator InitializeTutorialPanel(int completedTutorialParts)
	{
		MenuButton.gameObject.SetActive(false);
		ScenariosButton.gameObject.SetActive(false);

		int panelCount = Panels.Count;
		yield return new WaitForSeconds(0.1f);

		for (int i = 0; i < panelCount; i++)
		{
			bool isCompleted = (i <= completedTutorialParts - 1);
			Animator animator = CompletedImages[i];
			Image completedImage = animator.transform.GetComponent<Image>();
			Text completedText = animator.transform.parent.GetComponent<Text>();

			completedText.text = isCompleted ? Localizer.Instance.GetString("Completed") : Localizer.Instance.GetString("Incompleted");
			completedImage.enabled = isCompleted;

			Panels[i].gameObject.SetActive(true);
			animator.gameObject.SetActive(true);
			if (isCompleted) animator.Play("Appear");

			yield return new WaitForSeconds(0.25f);
		}

		yield return null;

		MenuButton.gameObject.SetActive(true);
		ScenariosButton.gameObject.SetActive(true);
	}
	#endregion
	#endregion

	#region For Testing Purposes
	public static EndGameReport LoadReport(string path)
	{
		if (!File.Exists(path))
			return null;

		int length = GetReportLength(path);
		if (length < 0)
		{
			Debug.LogWarning("Invalid report");
			return null;
		}

		string jsonText = File.ReadAllText(path);
		if (length >= jsonText.Length - 1)
		{
			Debug.LogWarning("Invalid report");
			return null;
		}

		LitJson.JsonReader reader = new LitJson.JsonReader(jsonText.Substring(length + 10));
		//reader.TypeHinting = true;
		EndGameReport rapor = JsonMapper.ToObject<EndGameReport>(reader);
		return rapor;
	}

	private static int GetReportLength(string path)
	{
		byte[] lengthBuffer = new byte[10];
		try
		{
			using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
			{
				fs.Read(lengthBuffer, 0, lengthBuffer.Length);
				fs.Close();
			}
		}
		catch (Exception e)
		{
			Debug.LogException(e);
			return -1;
		}

		int length;
		if (!int.TryParse(System.Text.Encoding.UTF8.GetString(lengthBuffer), out length))
			length = -1;

		return length;
	}
	#endregion

	private IEnumerator SendAllSnapshots(EndGameReport report)
	{
		var snapshot = new Snapshot(_gameState, report.LectureResult);
		var jsonTxt = string.Empty;
		try
		{
			jsonTxt = JsonMapper.ToJson(snapshot);
		}
		catch (Exception e)
		{
			Debug.LogError(
				"Snapshot couldn't be serialized. Error : [" + e.Message + "] Inner message : ["
					+ e.InnerException + "]");
		}

		if (string.IsNullOrEmpty(jsonTxt))
		{
			//Debug.LogError("empty");

			yield break;
		}

		var reportJson = string.Empty;
		try
		{
			var settings = new JsonSerializerSettings()
			{
				//TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
				TypeNameHandling = TypeNameHandling.Objects,
				Formatting = Formatting.Indented
			};

			const string apiNamespace = "ScenarioManagerLibrary";
			reportJson = JsonConvert.SerializeObject(report, settings);
			reportJson = Regex.Replace(reportJson, "Assembly-CSharp", apiNamespace);
			reportJson = Regex.Replace(reportJson, $"{nameof(Sinifta)}.{nameof(Sinifta.Report)}", apiNamespace);
			reportJson = Regex.Replace(
				reportJson,
				$"{nameof(Simofun)}.{nameof(Simofun.Karma)}.{nameof(Simofun.Karma.Unity)}",
				apiNamespace);
			reportJson = Regex.Replace(
				reportJson,
				$"{nameof(Simofun)}.{nameof(Simofun.SimInClass)}.{nameof(Simofun.SimInClass.Unity)}",
				apiNamespace);
			reportJson = Regex.Replace(
				reportJson,
				"KarmaFramework.KarmaUtils",
				apiNamespace);
		}
		catch (Exception e)
		{
			Debug.LogException(e);
		}

		//Debug.LogError(reportJson);
		//Debug.LogError(jsonTxt);

		yield return this.webApiClient.SendGameLogs(
			InterLevelData.GameId,
			InterLevelData.AppConfig.UserInfo.Id,
			new List<string>(),
			jsonTxt,
			reportJson);
	}

	void ChangeVisibility(bool isVisible)
	{
		this.content.alpha = isVisible ? 1f : 0f;
		this.content.interactable = isVisible;
		this.content.blocksRaycasts = isVisible;
		this.objectsHolder.SetActive(isVisible);
		this.BlackPanel.SetActive(isVisible);
	}
}
