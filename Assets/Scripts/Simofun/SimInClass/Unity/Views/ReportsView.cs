﻿using LitJson;
using Sinifta.Report;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Sinifta.Views
{
	public class ReportsView : MonoBehaviour
	{
		[SerializeField] private ScrollRect _elementsScroll;
		[SerializeField] private Color[] _colors;
		[Space(10)] [SerializeField] private GameObject _elementPrefab;

		[Header("Response Panel")]
		[SerializeField] private GameObject _sendPanel;
		[SerializeField] private Button _okButton;
		[SerializeField] private Text _saveResponseText;

		[Header("Score View")]
		[SerializeField] private DetailedReportView _detailedReportView;


		private GameState _gameState;
		[Inject]
		public void Construct(GameState gameState)
		{
			_gameState = gameState;
		}

		private void Awake()
		{
			_elementPrefab.SetActive(false);
			_okButton.onClick.AddListener(() =>
			{
				_sendPanel.SetActive(false);
			});
		}

		private void OnEnable()
		{
			foreach (Transform ele in _elementsScroll.content)
			{
				if (ele.gameObject.activeSelf)
				{
					Destroy(ele.gameObject);
				}
			}

			var cnt = 0;
			if (Directory.Exists(SiniftaUtility.ReportsSaveDir))
			{
				foreach (var file in Directory.GetFiles(SiniftaUtility.ReportsSaveDir, "*.csr",
					SearchOption.TopDirectoryOnly))
				{
					var summary = LoadReportSummary(file);

					var report = LoadReport(file);

					var newEle = Instantiate(_elementPrefab, _elementsScroll.content, false).transform;
					newEle.gameObject.SetActive(true);
					newEle.GetChild(0).GetComponentInChildren<Text>().text =
						Path.GetFileNameWithoutExtension(file);
					newEle.GetChild(1).GetComponentInChildren<Text>().text =
						summary.Date;
					var file1 = file;
					newEle.GetChild(2).GetComponentInChildren<Button>().onClick.AddListener(() =>
					{
						var s = File.ReadAllText(file1);
						//SendReportMail(s, Path.GetFileNameWithoutExtension(file1));
						//SceneManager.LoadScene(Simofun.SimInClass.Unity.HardCodeds.Scenes.TestDetailedReportView);
						_detailedReportView.SetResults(report, false);
						_detailedReportView.Show();
					});
					cnt++;
				}
			}

			//Generate dummies to fill page
			var prefabHeight = ((RectTransform)_elementPrefab.transform).sizeDelta.y;
			while (cnt * prefabHeight < _elementsScroll.viewport.rect.size.y)
			{
				var dummy = Instantiate(_elementPrefab, _elementsScroll.content, false).transform;
				dummy.gameObject.SetActive(true);
				dummy.GetComponentInChildren<Button>().gameObject.SetActive(false);
				cnt++;
			}

			//Color Elements
			for (var i = 0; i < _elementsScroll.content.childCount; i++)
			{
				_elementsScroll.content.GetChild(i).GetComponent<Image>().color = _colors[i % _colors.Length];
			}
		}

		private void OpenResponsePanel()
		{
			_sendPanel.SetActive(true);
			//_saveResponseText.text = Localizer.Instance.GetString(StringId.ReportSendMessage, InterLevelData.AppConfig.UserInfo.Email);
		}

		public static EndGameReport LoadReport(string path)
		{
			if (!File.Exists(path))
			{
				return null;
			}

			var length = GetReportLength(path);
			if (length < 0)
			{
				Debug.LogWarning("Invalid report");

				return null;
			}

			string jsonText = File.ReadAllText(path);
			if (length >= jsonText.Length - 1)
			{
				Debug.LogWarning("Invalid report");

				return null;
			}

			var reader = new JsonReader(jsonText.Substring(length + 10));
			//reader.TypeHinting = true;

			return JsonMapper.ToObject<EndGameReport>(reader);
		}

		private static int GetReportLength(string path)
		{
			byte[] lengthBuffer = new byte[10];
			try
			{
				using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
				{
					fs.Read(lengthBuffer, 0, lengthBuffer.Length);
					fs.Close();
				}
			}
			catch (Exception e)
			{
				Debug.LogException(e);
				return -1;
			}

			int length;
			if (!int.TryParse(System.Text.Encoding.UTF8.GetString(lengthBuffer), out length))
				length = -1;

			return length;
		}

		public static ReportSummary LoadReportSummary(string path)
		{
			if (!File.Exists(path))
				return null;

			int length = GetReportLength(path);
			if (length < 0)
			{
				Debug.LogWarning("Invalid report");
				return null;
			}

			char[] summary = new char[length];
			using (TextReader summaryReader = File.OpenText(path))
			{
				summaryReader.Read(summary, 0, 10); // Skip the summary length

				if (summaryReader.ReadBlock(summary, 0, length) < length)
				{
					Debug.LogWarning("Invalid report");

					return null;
				}

				summaryReader.Close();
			}

			return JsonMapper.ToObject<ReportSummary>(new string(summary));
		}
	}
}
