﻿namespace Sinifta.Views
{
	using KarmaFramework.CharacterCore;
	using KarmaFramework.Views;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Domain;
	using Simofun.Karma.Unity.Events;
	using Simofun.Networking.Core;
	using Sinifta.Actors;
	using System.Collections.Generic;
	using UniRx;
	using UnityEngine;

	public class TeacherView : PlayerView
	{
		#region Fields
		const float cameraFov = 60f;

		readonly CompositeDisposable disposables = new CompositeDisposable();

		bool isFocusing;

		Camera cam;

		float camLerpTime;

		StudentFacade lastEyeContactStudent;

		Transform focusTarget;
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			this.cam = this.GetComponentInChildren<Camera>();

			if (SimNetwork.Instance.IsConnected && !SimNetwork.Instance.IsMaster)
			{
				this.GetComponent<TeacherController>().enabled = false;
			}
		}

		/// <inheritdoc />
		protected virtual void Update()
		{
			this.DisplayMindParams();

			if (this.isFocusing)
			{
				var isZoomingIn = this.focusTarget != null;
				var targetFov = cameraFov;
				var camTransform = this.cam.transform;

				if (this.camLerpTime < 1f)
				{
					this.camLerpTime += Time.deltaTime * 0.7f;
					if (isZoomingIn)
					{
						targetFov = Mathf.Clamp(this.CalculateFov(this.focusTarget), 15, cameraFov);

						// Rotate player horizontally
						var toTargetHorz = Vector3.ProjectOnPlane(
							this.focusTarget.position - this.transform.position,
							Vector3.up);
						this.transform.rotation = Quaternion.Slerp(
							this.transform.rotation,
							Quaternion.LookRotation(toTargetHorz), this.camLerpTime);

						// Rotate camera vertically
						var toTargetVertical = Vector3.ProjectOnPlane(
							this.focusTarget.position - camTransform.position,
							-this.transform.right);
						camTransform.rotation = Quaternion.Slerp(
							camTransform.rotation,
							Quaternion.LookRotation(toTargetVertical), this.camLerpTime);
					}

					this.cam.fieldOfView = Mathf.Lerp(this.cam.fieldOfView, targetFov, this.camLerpTime);
				}

				this.isFocusing = this.camLerpTime < 1f;
			}
		}

		/// <inheritdoc />
		protected override void OnDestroy()
		{
			base.OnDestroy();

			this.disposables.Dispose();
		}
		#endregion

		#region Public Methods
		public override void Bind(IActor actor)
		{
			base.Bind(actor);

			KarmaMessageBus.OnEvent<FocusEvent>().Subscribe(ev =>
			{
				this.focusTarget = ev.Target?.PlayerFocusPoint;
				this.isFocusing = true;
				this.camLerpTime = 0;
				KarmaMessageBus.Publish(
					new NPCMenu.NPCMenuEvent
					{
						State = ev.State,
						Player = this.Actor as Player,
						Npc = ev.Target?.Actor as NPC
					});
			}).AddTo(this.disposables);
			KarmaMessageBus.OnEvent<LookAtStudentEvent>().Subscribe(ev =>
			{
				ev.TargetStudent.Transform.GetComponent<GazeBehaviour>()
					.ForceChangeGazeTarget(TeacherGazeInteractions.GazeTarget);

				if (this.lastEyeContactStudent != ev.TargetStudent)
				{
					actor.FireAction("TeacherLookAtStudent", new List<IActor> { ev.TargetStudent.GetActor() });
					this.lastEyeContactStudent = ev.TargetStudent;
				}
			}).AddTo(this.disposables);
		}
		#endregion
	}

	public class ShowLectureEvent : KarmaBaseUnityEvent
	{
		public bool State { get; set; }

		public float Duration { get; set; }

		public int StartTime { get; set; }

		public string speech { get; set; }
	}

	public class LookAtStudentEvent : KarmaBaseUnityEvent
	{
		public StudentFacade TargetStudent { get; set; }
	}
}
