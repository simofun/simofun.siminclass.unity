﻿using Simofun.SimInClass.Unity.HardCodeds;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FirstLoginView : MonoBehaviour
{
    public Text UserMail;
    public Text UserName;
    public Text UserSur;
    public Text ErrorText;
    public Toggle PrivacyPolicy;
    public Button SubmitButton;
    public GameObject LoadingScreen;

    void Start()
    {
        SubmitButton.onClick.RemoveAllListeners();
        SubmitButton.onClick.AddListener(() => SceneManager.LoadScene(Scenes.MainMenu));
    }

    /*private System.Collections.IEnumerator LoginCoroutine(string userName, string password, bool isExternalLogin = false)
    {
        WWW wwwLogin = null;
        var quickLogin = false;
        var appConfig = InterLevelData.AppConfig;

#if UNITY_EDITOR
        // Yasir Kula: increase debugging speed in the editor
        // by quickly logging in if CTRL is pressed
        if (Input.GetKey(KeyCode.LeftControl))
            appConfig.IsolatedEnvironment = true;

        // Yasir Kula: increase debugging speed in the editor
        // by loading scenarios from local if Shift is pressed while logging in
        if (Input.GetKey(KeyCode.LeftShift))
            appConfig.OnlineMode = false;
#endif

        if (appConfig.IsolatedEnvironment)
        {
            quickLogin = true;
            yield return null;
        }
        else
        {
            wwwLogin = isExternalLogin ? ApiManager.GetExternalUserData(userName) : ApiManager.GetUserData(userName, password);
            yield return wwwLogin;
        }

        if (quickLogin || string.IsNullOrEmpty(wwwLogin.error))
        {
            UserInfo userInfo = null;

            if (!appConfig.IsolatedEnvironment)
            {
                var js = JsonMapper.ToObject(wwwLogin.text);
                userInfo = JsonMapper.ToObject<UserInfo>(js.ToJson());
            }

            // Debug
            if (userInfo == null)
                userInfo = TestUser;

            if (userInfo.EmailConfirmed)
            {
                if (userInfo.TrialDaysLeft >= 0)
                {
                    Debug.Log("Demo time left: " + userInfo.TrialDaysLeft + " day(s)");
                    if (userInfo.TrialDaysLeft < 5)
                    {
                        KarmaUtils.ShowPopup(Localizer.Instance.GetString(StringId.Information),
                            Localizer.Instance.GetString(StringId.TrialDaysLeft, userInfo.TrialDaysLeft + 1),
                            Localizer.Instance.GetString(StringId.Ok));
                    }

                    // Login success
                    yield return null;
                    OnLoginResult(true, string.Empty, userInfo);
                    appConfig.UserInfo = userInfo;

                    Debug.Log("UserInfo set : " + appConfig.UserInfo.Name);

                    // Remember the player only if he successfully logs in
                    //if (_rememberMeToggle.isOn)
                    //{
                    //    PlayerPrefs.SetString("username", userName);
                    //    PlayerPrefs.SetString("password", password);
                    //}
                    //else
                    //{
                    //    PlayerPrefs.SetString("username", string.Empty);
                    //    PlayerPrefs.SetString("password", string.Empty);
                    //}
                    //
                    //PlayerPrefs.Save();
                    KarmaAnalytics.SetUserId(userInfo.Id);
                    StartCoroutine(KarmaUtils.CreateSession(appConfig.UserInfo.Id,
                        () => KarmaNetwork.LoadLevel(Scenes.MainMenu)));
                    //Debug.Log(i);
                }
                else
                {
                    // Trial is over
                    OnLoginResult(false, Localizer.Instance.GetString(StringId.TrialOver), null);
                    if (InterLevelData.ActiveExternalLoginManager != null)
                    {
                        InterLevelData.ActiveExternalLoginManager.SignOut();
                    }
                }
            }
            //else
            //{
            //    // Email not confirmed
            //    SendConfirmationMail(userInfo.Id);
            //    OnLoginResult(false, Localizer.Instance.GetString(StringId.UserLocked), null);
            //    if (InterLevelData.ActiveExternalLoginManager != null)
            //    {
            //        InterLevelData.ActiveExternalLoginManager.SignOut();
            //    }
            //}
        }
        else
        {
            // Login fail
            OnLoginResult(false, wwwLogin.error, null);
            if (InterLevelData.ActiveExternalLoginManager != null)
            {
                InterLevelData.ActiveExternalLoginManager.SignOut();
            }
        }
    }

    private void OnLoginResult(bool success, string message, UserInfo info)
    {
        if (success)
        {
            //Screen.orientation = _defaultOrientation;

            StartCoroutine(KarmaUtils.GetUserProgress(info, () => SceneManager.LoadScene(Scenes.MainMenu)));
        }
        else
        {
            if (message.Contains("404"))
            {
                message = Localizer.Instance.GetString(StringId.InvalidUserNameOrPassword);
            }
            else if (message.Contains("Failed to connect"))
            {
                message = Localizer.Instance.GetString(StringId.ConnectionError);
            }
            //ShowWarning(message);
        }
    }

    private System.Collections.IEnumerator ExternalLogin(ExternalLoginInfo info)
    {
        var login = ApiManager.ExternalLogin(info);
        yield return login;
        CoroutineStarter.StartCoroutine(LoginCoroutine(info.Email, string.Empty, true));
    }

    public void DoExternalLogin(ExternalLoginInfo info)
    {
        CoroutineStarter.StartCoroutine(ExternalLogin(info));
    }

    public void FirstLogin()
    {
        foreach (var item in new List<Text> { UserName, UserMail, UserSur })
        {
            if (item.text.Length < 2)
            {
                ErrorText.text = "Lutfen bilgilerinizi kontrol edin.";
                return;
            }
        }

        var info = new ExternalLoginInfo()
        {
            Email = UserMail.text,
            Name = UserName.text,
            Surname = UserSur.text
        };
        PlayerPrefs.SetString("Email", UserMail.text);
        PlayerPrefs.SetString("Name", UserName.text);
        PlayerPrefs.SetString("Surname", UserSur.text);
        PlayerPrefs.SetInt("FirstLogin", 0);
        PlayerPrefs.Save();

        LoadingScreen.SetActive(true);
        DoExternalLogin(info);

    }

    public void ToggleChanged(bool newValue)
    {
        SubmitButton.interactable = newValue;
    }*/
}
