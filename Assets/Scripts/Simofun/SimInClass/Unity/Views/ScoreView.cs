﻿using System.Collections;
using System.Collections.Generic;
using KarmaFramework.Localization;
using KarmaFramework.Reports;
using UnityEngine;
using UnityEngine.UI;

namespace Sinifta.Views
{
    public class ScoreView : MonoBehaviour
    {
        private GameObject _objectsHolder;

        private Text _missionReportText;
        private Text _actionReportText;

        private Button _exitButton;

        // Start is called before the first frame update
        void Start()
        {
            _objectsHolder = GameObject.Find("Parent");
            _missionReportText = GameObject.Find("Text_MissionReport").GetComponent<Text>();
            _actionReportText = GameObject.Find("Text_ActionReport").GetComponent<Text>();
            _exitButton = GameObject.Find("Button_Exit").GetComponent<Button>();

            _exitButton.onClick.RemoveAllListeners();
            _exitButton.onClick.AddListener(ExitButtonClicked);
            _objectsHolder.SetActive(false);
        }

        public void SetResults(ReportSummary report)
        {
            _objectsHolder.SetActive(true);

            _actionReportText.text = string.Empty;
            /*_actionReportText.text += Localizer.Instance.GetString("DealPercentage") + report.DealPercent + "\n\n";
            _actionReportText.text += Localizer.Instance.GetString("CorrectDealPercentage") + report.CorrectDealPercent + "\n\n";
            _actionReportText.text += Localizer.Instance.GetString("WrongDealPercentage") + report.WrongDealPercent + "\n\n";
            _actionReportText.text += Localizer.Instance.GetString("IgnoredDeals") + report.IgnoredDealCount + "\n\n";
            _actionReportText.text += Localizer.Instance.GetString("NotDeals") + report.NotDealedCount + "\n\n";

            _missionReportText.text = string.Empty;
            _missionReportText.text += Localizer.Instance.GetString("MissionPercentage") + report.MissionPercentage + "\n\n";
            _missionReportText.text += Localizer.Instance.GetString("CorrectMissions") + report.CorrectMissionCount + "\n\n";
            _missionReportText.text += Localizer.Instance.GetString("WrongMissions") + report.WrongMissionCount + "\n\n";*/

        }

        void ExitButtonClicked()
        {
            gameObject.SetActive(false);
        }
    }
}
