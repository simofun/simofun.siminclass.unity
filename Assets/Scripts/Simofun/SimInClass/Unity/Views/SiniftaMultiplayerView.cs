﻿#if UNITY_EDITOR
#define DEV_MULTIPLAYER
#endif

using KarmaFramework.Localization;
using Simofun.Networking.Core;
using Simofun.SimInClass.Unity.Net.Multiplayer;
using Simofun.Unity.DesignPatterns.Singleton;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SiniftaMultiplayerView : SimSceneSingletonBase<SiniftaMultiplayerView>
{
	#region Public Methods
	[Header("Lobby View")]
	[SerializeField]
	GameObject playerPrefab;

	[SerializeField]
	Transform playerListTransform;

	[Header("General View")]
	[SerializeField]
	Transform lobbyView;

	[SerializeField]
	Transform buttonGroup;

	[SerializeField]
	Text roomNotFoundText;
	#endregion

	#region Fields
	Button createRoomButton;

	Button joinRoomButton;

	Button startGameButton;

	InputField createRoomIF;

	InputField joinRoomIF;

	List<IPlayer> players;

	Text roomNameText;
	#endregion

	#region Unity Methods
	protected virtual void Awake()
	{
		this.joinRoomButton = GameObject.Find("Button_FindRoom").GetComponent<Button>();
		this.joinRoomButton.onClick.RemoveAllListeners();
		this.joinRoomButton.onClick.AddListener(this.JoinRoom);

		this.createRoomButton = GameObject.Find("Button_CreateRoom").GetComponent<Button>();
		this.createRoomButton.onClick.RemoveAllListeners();
		this.createRoomButton.onClick.AddListener(this.CreateRoom);

		this.startGameButton = GameObject.Find("Button_StartGame").GetComponent<Button>();
		this.startGameButton.onClick.RemoveAllListeners();
		this.startGameButton.onClick.AddListener(this.StartGame);

		this.startGameButton.gameObject.SetActive(false);

		this.joinRoomIF = GameObject.Find("IF_FindRoom").GetComponentInChildren<InputField>();
		this.createRoomIF = GameObject.Find("IF_CreateRoom").GetComponentInChildren<InputField>();

		this.roomNameText = GameObject.Find("TitleRoomName").GetComponent<Text>();

		for (var i = 0; i < this.playerListTransform.childCount; i++)
		{
			Destroy(this.playerListTransform.GetChild(i).gameObject);
		}

		//this.roomNotFoundText.gameObject.SetActive(false);
		this.lobbyView.gameObject.SetActive(false);
		this.buttonGroup.gameObject.SetActive(true);

#if DEV_MULTIPLAYER
		this.createRoomIF.text = System.Environment.UserName;
		this.joinRoomIF.text = System.Environment.UserName;
#endif
	}
	#endregion

	#region Public Methods
	public void OnJoinedRoom()
	{
		this.lobbyView.gameObject.SetActive(true);
		this.buttonGroup.gameObject.SetActive(false);

		this.startGameButton.gameObject.SetActive(SimNetwork.Instance.IsMaster);
		this.roomNameText.text = Localizer.Instance.GetString("Room") + " : " + SimNetwork.Instance.ActiveRoom.Name;
		this.StartCoroutine(this.ShowLobbyPlayersCoroutine());
	}

	public void OnJoinRoomFailed(string message)
	{
		//this.roomNotFoundText.gameObject.SetActive(true);
  //      this.roomNotFoundText.text = message;
	}
	#endregion

	#region Methods
	IEnumerator ShowLobbyPlayersCoroutine()
	{
		while (true)
		{
			for (var i = 0; i < this.playerListTransform.childCount; i++)
			{
				Destroy(this.playerListTransform.GetChild(i).gameObject);
			}

			this.players = SimNetwork.Instance.ActiveRoom.GetPlayers().ToList();

			foreach (var player in this.players)
			{
				Instantiate(this.playerPrefab, this.playerListTransform).GetComponent<Text>().text = player.NickName;
			}

			yield return new WaitForSeconds(10f);
		}
	}

	void CreateRoom() => SiniftaMultiplayer.Instance.CreateRoom(this.createRoomIF.text.Trim());

	void JoinRoom() => SiniftaMultiplayer.Instance.JoinRoom(this.joinRoomIF.text.Trim());

	void StartGame() => SiniftaMultiplayer.Instance.StartGame();
	#endregion
}
