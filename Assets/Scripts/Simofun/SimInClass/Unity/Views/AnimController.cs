﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KarmaFramework.CharacterCore;
using System;
using System.Linq;
using UniRx;
using Sinifta.Actors;
using System;
using Simofun.Karma.UniRx.Unity;
using KarmaFramework;

namespace Sinifta.Views
{
    public enum StudentAnim
    {
        None,
        Idle,

        Write,
        Reading,
        HaveQuestion,

        PlayGame = 21, // Nope
        Hurt = 22, // Nope
        SambaDancing = 23, // Nope
        Daydream = 24, // Yep
        CallOut = 25,
        CallOutFrequently = 26,
        ComeWithoutSupplies = 27,
        Crying = 28,
        DamageEquipment = 29,
        Texting = 30, // Yep
        Film = 31,
        PlayWithTools = 32, // Almost yep
        Sleep = 33, // Yep
        Talking = 34, // Yep
        MakeNoise = 35, // Yep
        Argue = 36,
        Laugh = 37, // Nope
        Punch = 38, // HELL NO
        Dodge = 39, // Uh-nuh
        Yell = 40, // Yeah
        Whisper = 41, //So-So
        Poke = 42, // //So-So
        PhysicalThreat = 43 ///So-So
    }

    public class StudentAnimController : IAnimatorController
    {

        private readonly Animator _anim;
        private readonly Dictionary<StudentAnim, int> AnimStateMapping = new Dictionary<StudentAnim, int>();
        private readonly CompositeDisposable disposables = new CompositeDisposable();

        public delegate void OnStudentAnimationChanged(string anim);
        public event OnStudentAnimationChanged onAnimationChanged;

        public StudentAnimController(Animator anim)
        {
            _anim = anim;

            foreach (var s in Enum.GetValues(typeof(StudentAnim)).Cast<StudentAnim>())
            {
                AnimStateMapping.Add(s, Animator.StringToHash(s.ToString("F")));
            }

            KarmaMessageBus.OnEvent<PauseEvent>().Subscribe(ev => 
            {
                if(ev.State)
                {
                    SetSpeed(0);
                    return;
                }
                SetSpeed(1.1f);
            }).AddTo(disposables);
        }
        ~StudentAnimController()
        {
            Dispose();
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            disposables.Dispose();
        }
        public void SetAnimState(string anim)
        {
            StudentAnim state;
            if(Enum.TryParse(anim, out state))
            {
                SetAnimState(state); 
                if (onAnimationChanged != null)
                    onAnimationChanged.Invoke(anim);
            }
        }

        public void SetAnimState(string anim, int delay)
        {
            Observable.Timer(TimeSpan.FromSeconds(delay)).Subscribe(ev =>
            {
                SetAnimState(anim);
            });
        }

        private void SetAnimState(StudentAnim anim, bool state = true)
        {
            if (_anim != null)
            {
                //foreach (var value in AnimStateMapping.Values) //clear old states
                //{
                //    _anim.SetBool(value, false);
                //}
                //
                //_anim.SetBool(AnimStateMapping[anim], state);
                _anim.SetInteger("Animation", (int)anim);
            }
        }
        public void SetSpeed(float value)
        {
            _anim.SetFloat("Speed", value);
            _anim.speed = value;
        }

        public void SetBool(string anim, bool condition)
        {
            _anim.SetBool(anim, condition);
        }
    }
}
