﻿using Simofun.SimInClass.Unity.HardCodeds;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    public Button Tutorial;
    public Button SinglePlayer;
    public Button MultiPlayer;
    public Button LoadReport;
    public Button Quit;
    public Button Information;

    public Button About;
    public GameObject AboutPanel;
    public Button AboutOK;

    public Text ErrorText;

    private void Start()
    {
        SinglePlayer.onClick.RemoveAllListeners();
        SinglePlayer.onClick.AddListener(()=> SceneManager.LoadScene(Scenes.Game));
        MultiPlayer.onClick.RemoveAllListeners();
        MultiPlayer.onClick.AddListener(() => SceneManager.LoadScene(Scenes.MultiplayerLobby));
    }

    /*public MainMenuInformationPanel InformationPanel;

    public GameObject NoMultiplayerImage;
    private ApplicationConfig _appConfig;
    private bool _multiplayerPermissionQueryFinished = false;
    private bool _hasMultiplayerPermission;
    private bool _wasMultiplayerPermissionRequested;

    void Start()
    {
        _appConfig = InterLevelData.AppConfig;

        ErrorText.text = string.Empty;

        #region Tutorial Button

        Tutorial.onClick.AddListener(() =>
        {
            InterLevelData.GameTier = "Tutorial";
            KarmaNetwork.LoadLevel("TutorialSelectionScene");
        });
        Tutorial.GetComponentInChildren<Text>().text = Localizer.Instance.GetString(StringId.MainMenuTutorialButton);

        #endregion

        #region Single Player Button

        SinglePlayer.onClick.AddListener(() =>
        {
            InterLevelData.GameTier = "Level 1";
            //Old Interface

            StartCoroutine(KarmaUtils.GetUserProgress(InterLevelData.AppConfig.UserInfo, () =>
            {
                KarmaNetwork.LoadLevel("ScenarioEditScene");
            }));
                
            //New Interface
            //KarmaNetwork.LoadLevel("SelectStages");

            //KarmaNetwork.LoadLevel("ScenarioEdit_TestUI");
        });
        SinglePlayer.GetComponentInChildren<Text>().text = Localizer.Instance.GetString(StringId.MainMenuSinglePlayerButton);

        #endregion

        #region Multiplayer Button

        MultiPlayer.onClick.AddListener(() =>
        {
            StartCoroutine(KarmaUtils.GetUserProgress(InterLevelData.AppConfig.UserInfo, () =>
            {
                InterLevelData.GameTier = "Level 1";
                KarmaNetwork.LoadLevel("MultiplayerMenuScene");
            }));
        });
        MultiPlayer.GetComponentInChildren<Text>().text = Localizer.Instance.GetString(StringId.MainMenuMultiplayerButton);

#if UNITY_ANDROID || UNITY_IOS
        MultiPlayer.gameObject.SetActive(false);
#endif

        #endregion

        #region Load Report Button

        LoadReport.onClick.AddListener(() =>
        {
            KarmaNetwork.LoadLevel("ViewReportsScene");
        });

        #endregion

        #region Quit Button

        Quit.onClick.AddListener(() =>
        {
            _appConfig.UserInfo = null;
            KarmaNetwork.LoadLevel("LoginMenuScene");
        });
        //if (Information)
        //{
        //    Information.onClick.AddListener(() => { About(); });
        //}
        if (About != null)
        {
            About.onClick.AddListener(() => { AboutPanel.SetActive(true); });
        }
        if (AboutOK != null)
            AboutOK.onClick.AddListener(() => { AboutPanel.SetActive(false); });

        #endregion

        _hasMultiplayerPermission = _appConfig.IsolatedEnvironment || (_appConfig.UserInfo.HasMultiplayerPermission);

        if (_hasMultiplayerPermission)
        {
            MultiPlayer.interactable = true;
            NoMultiplayerImage.SetActive(false);
        }
        else
        {
            // Appointment things
            StartCoroutine(MultiplayerRequestControl());
        }
    }

    private System.Collections.IEnumerator MultiplayerRequestControl()
    {
        var userId = _appConfig.UserInfo.Id;
        bool prevRequestBool = false;
        bool requestBool = false;

        while (!_hasMultiplayerPermission)
        {
            var www = ApiManager.GetMultiplayerPermissionInfo(userId);
            yield return www;
            bool.TryParse(www.text, out _hasMultiplayerPermission);

            if (_hasMultiplayerPermission)
            {
                MultiPlayer.interactable = true;
                NoMultiplayerImage.SetActive(false);
                yield break;
            }

            var wwwApp = ApiManager.GetMultiplayerRequestInfo(userId);
            yield return wwwApp;
            bool.TryParse(wwwApp.text, out requestBool);
            _multiplayerPermissionQueryFinished = true;

            if (prevRequestBool != requestBool)
            {
                _wasMultiplayerPermissionRequested = requestBool;
                prevRequestBool = requestBool;
            }

            yield return new WaitForSecondsRealtime(10f);
        }
    }
    //public void About()
    //{
    //    var buttonVisibility = MainMenuInformationPanel.ButtonState.Visible;
    //    InformationPanel.Show(StringId.CreditsHeader,
    //                          Localizer.Instance.GetString(StringId.Credits), buttonVisibility,
    //                          Localizer.Instance.GetString(StringId.Ok),
    //                          () =>
    //                          {

    //                              InformationPanel.Hide();
    //                          });

    //}
    public void MultiplayerButtonOnPointerEnter()
    {
        if (_multiplayerPermissionQueryFinished && !_hasMultiplayerPermission)
        {
            MainMenuInformationPanel.ButtonState buttonVisibility = _wasMultiplayerPermissionRequested ? MainMenuInformationPanel.ButtonState.NotInteractable : MainMenuInformationPanel.ButtonState.Visible;
            string buttonLabel = _wasMultiplayerPermissionRequested ? Localizer.Instance.GetString(StringId.MultiplayerRequestSent) : Localizer.Instance.GetString(StringId.RequestMultiplayer);
            InformationPanel.Show(Localizer.Instance.GetString(StringId.NoMultiplayerContactSimsoft), buttonVisibility, buttonLabel, () =>
            {
                _wasMultiplayerPermissionRequested = true;
                MultiplayerButtonOnPointerEnter();

                ApiManager.PostMultiplayerRequest(_appConfig.UserInfo.Email, string.Empty);
            });
        }
        else
            InformationPanel.Hide();
    }

    public void ShowReportOnPointerEnter()
    {
        InformationPanel.Show(Localizer.Instance.GetString(StringId.ViewReportButtonHint, KarmaUtils.ReportsSaveDir), MainMenuInformationPanel.ButtonState.Hidden);
    }

    public void SinglePlayerButtonsOnPointerEnter()
    {
        InformationPanel.Hide();
    }*/
}
