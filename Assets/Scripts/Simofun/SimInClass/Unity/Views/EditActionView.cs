﻿namespace Sinifta.ScenarioEditor.ActionEditor
{
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using System.IO;
	using System.Linq;
	using System.Xml.Linq;
	using UniRx;
	using UnityEngine;
	using UnityEngine.UI;

	public class EditActionView : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		StudentActionEditorEntry _studentActionEntryPrefab;

		[SerializeField]
		Transform _studentActionEntryHolder;

		[SerializeField]
		TeacherActionEditorEntry _teacherActionEntryPrefab;

		[SerializeField]
		Transform _teacherActionEntryHolder;

		[SerializeField]
		Button _createStudentActionButton;

		[SerializeField]
		Button _createTeacherActionButton;

		[SerializeField]
		Transform _createStudentPanel;

		[SerializeField]
		Transform _createTeacherPanel;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		string _configPath;

		XDocument _document;

		XElement _actionsRoot;
		#endregion

		#region Unity Methods
		protected virtual void Start()
		{
			_configPath = Application.persistentDataPath + "/Configs";

			_createStudentPanel.gameObject.SetActive(false);

			_createStudentActionButton.onClick.AddListener(() =>
			{
				_createStudentPanel.gameObject.SetActive(true);
			});

			_createTeacherPanel.gameObject.SetActive(false);

			_createTeacherActionButton.onClick.AddListener(() =>
			{
				_createTeacherPanel.gameObject.SetActive(true);
			});


			if (Directory.Exists(_configPath) && File.Exists(_configPath + "/ActionConfig.xml"))
			{
				_document = XDocument.Load(_configPath + "/ActionConfig.xml");
			}
			else
			{
				if (!Directory.Exists(_configPath))
					Directory.CreateDirectory(_configPath);

				var ta = Resources.Load<TextAsset>("Configs/ActionConfig");
				_document = XDocument.Parse(ta.text);
				_document.Save(_configPath + "/ActionConfig.xml");
			}
			_actionsRoot = _document.Element("ActionsHolder").Element("Actions");

			_studentActionEntryPrefab.gameObject.SetActive(false);
			_teacherActionEntryPrefab.gameObject.SetActive(false);


			foreach (var child in _studentActionEntryHolder.Cast<Transform>().Select(x => x.gameObject).Where(x => x.gameObject.activeSelf))
			{
				Destroy(child);
			}

			foreach (var child in _teacherActionEntryHolder.Cast<Transform>().Select(x => x.gameObject).Where(x => x.gameObject.activeSelf))
			{
				Destroy(child);
			}

			foreach (var element in _actionsRoot.Elements())
			{
				if (int.Parse(element.Element("ActorId").Value) == -1)
				{
					var teacherActionEntry = Instantiate(_teacherActionEntryPrefab, _teacherActionEntryHolder, false);
					teacherActionEntry.gameObject.SetActive(true);
					teacherActionEntry.Bind(element);
				}
				else
				{
					var studentActionEntry = Instantiate(_studentActionEntryPrefab, _studentActionEntryHolder, false);
					studentActionEntry.gameObject.SetActive(true);
					studentActionEntry.Bind(element);
				}
			}

			KarmaMessageBus.OnEvent<CreateStudentActionEvent>().Subscribe(ev =>
			{
				var studentEntry = Instantiate(_studentActionEntryPrefab, _studentActionEntryHolder, false);
				studentEntry.gameObject.SetActive(true);
				studentEntry.Bind(ev.element);

				_actionsRoot.Add(ev.element);
				_document.Save(_configPath + "/ActionConfig.xml");
			}).AddTo(this.disposables);
			KarmaMessageBus.OnEvent<CreateTeacherActionEvent>().Subscribe(ev =>
			{
				var studentEntry = Instantiate(_teacherActionEntryPrefab, _teacherActionEntryHolder, false);
				studentEntry.gameObject.SetActive(true);
				studentEntry.Bind(ev.element);

				_actionsRoot.Add(ev.element);
				_document.Save(_configPath + "/ActionConfig.xml");
			}).AddTo(this.disposables);
		}

		protected virtual void OnDestroy() => this.disposables.Dispose();
		#endregion
	}

	public class CreateStudentActionEvent : KarmaBaseUnityEvent
	{
		public XElement element;
	}

	public class CreateTeacherActionEvent : KarmaBaseUnityEvent
	{
		public XElement element;
	}
}
