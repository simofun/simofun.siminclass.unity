﻿namespace Sinifta.Views
{
	using KarmaFramework.Views;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Domain;
	using Simofun.Unity.Presentation;
	using Sinifta.Actors;
	using UnityEngine;

	public class StudentView : NPCView
	{
		#region Fields
		/// <summary>
		/// The body highlighter which is student body
		/// </summary>
		BodyHighlighter bodyHighlighter;

		MonoFacade monoFacade;
		#endregion

		#region Properties
		public StudentFacade StudentFacade { get; protected set; }
		#endregion

		#region Unity Methods
		protected virtual void OnMouseOver()
		{
			this.bodyHighlighter.IsHighlighted = true;
			if (this.bodyHighlighter.ThicknessPercent < 0.009f)
			{
				this.bodyHighlighter.ThicknessPercent += 0.00003f;
				this.bodyHighlighter.SetStandardColor();
			}
			else
			{
				this.bodyHighlighter.SetHighlightColor();
					
				KarmaMessageBus.Publish(new LookAtStudentEvent { TargetStudent = this.StudentFacade });
			}

			this.StudentFacade.StudentTooltipView.Show(this.StudentFacade);
		}

		protected virtual void OnMouseExit()
		{
			this.bodyHighlighter.SetStandardColor();
			this.bodyHighlighter.ThicknessPercent = 0f;
			this.StudentFacade.StudentTooltipView.Close();
		}
		#endregion

		#region Public Methods
		public override void Bind(IActor actor)
		{
			base.Bind(actor);

			this.bodyHighlighter = new BodyHighlighter(
				this.transform.GetComponentInChildren<SkinnedMeshRenderer>(),
				Shader.Find("Toon/Basic Outline"),
				Shader.Find("Mobile/VertexLit (Only Directional Lights)"));
			this.monoFacade = this.GetComponent<MonoFacade>();
		}

		public void BindFacade(StudentFacade studentFacade)
		{
			this.StudentFacade = studentFacade;
			this.monoFacade.BindFacade(studentFacade);
		}
		#endregion
	}
}
