﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
namespace Simofun.SimInClass.Unity.UI
{
    public class ClassroomStatePanel : MonoBehaviour
    {
        Image image;
        private void Start()
        {
            image = GetComponent<Image>();
        }
        private void OpenAnimation()
        {
            image.DOFade(1, 1).OnComplete(() => 
            {
                image.DOFade(0, 3).OnComplete(() => 
                {
                    CloseClassroomStatePanel();
                });    
            });
        }
        public void OpenClassroomStatePanel()
        {
            gameObject.SetActive(true);
            OpenAnimation();
        }
        public void CloseClassroomStatePanel()
        {
            gameObject.SetActive(false);
        }
      
    }
}