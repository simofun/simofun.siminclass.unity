using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Graph
{
    public HashSet<Node> Nodes { get; private set; }

    public Graph()
    {
        Nodes = new HashSet<Node>();
    }

    public Node GetNode(string name)
    {
        var retVal = Nodes.FirstOrDefault(n => n.Name == name);

        if (retVal == null)
        {
            Debug.LogError("Node not found with name : " + name);
        }

        return retVal;
    }
}

