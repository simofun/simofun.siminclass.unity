﻿namespace Sinifta.Networking
{
	using KarmaFramework.CharacterCore;
	using KarmaFramework.NetworkingCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Simofun.Karma.Unity.Net.Multiplayer.Events;
	using Simofun.Networking.Core;
	using Simofun.Networking.HLAPI;
	using Simofun.SimInClass.Unity.HardCodeds;
	using Simofun.SimInClass.Unity.Net.Multiplayer;
	using Sinifta.Actions;
	using System.Collections.Generic;
	using UniRx;
	using UnityEngine.SceneManagement;

	public class PlayerSyncer : GhostView
	{
		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		IMessageSender mpMessageSender;
		#endregion

		#region Unity Methods
		protected virtual void Awake()
		{
			this.mpMessageSender = MessageSenderFactory.CreateForMP();
			this.mpMessageSender.MessageRecieved += this.OnMessageReceived;

			KarmaMessageBus.OnEvent<ActionTriggeredEvent>()
				.Subscribe(ev => this.HandleActionTriggered(ev.Action))
				.AddTo(this.disposables);
			KarmaMessageBus.OnEvent<SelectScenarioEvent>()
				.Subscribe(ev => this.mpMessageSender.SendMessage(ev))
				.AddTo(this.disposables);
			KarmaMessageBus.OnEvent<StartScenarioEvent>()
				.Subscribe(ev => this.mpMessageSender.SendMessage(ev))
				.AddTo(this.disposables);
			KarmaMessageBus.OnEvent<StartTimerEvent>()
				.Subscribe(ev => this.mpMessageSender.SendMessage(ev))
				.AddTo(this.disposables);

			DontDestroyOnLoad(this.gameObject);
		}

		protected virtual void OnDestroy()
		{
			this.disposables.Dispose();
			this.mpMessageSender.MessageRecieved -= this.OnMessageReceived;
		}
		#endregion

		void HandleActionTriggered(BaseActionRpcState ta)
		{
			mpMessageSender.SendMessage(ta);
		}

		public void HandleStartGameEvent()
		{
			if (SimNetwork.Instance.IsConnected)
			{
				this._owner.Rpc(nameof(HandleStartGameEventRPC), RpcTargets.All);
			}
		}

		[SimRPC]
		private void HandleStartGameEventRPC()
		{
			SiniftaMultiplayer.Instance.NavigatorProceed();
		}
		
		private void OnMessageReceived(object o)
		{
			if (o is BaseActionRpcState)
			{
				var baseAction = o as BaseActionRpcState;
				ActorFacade af = SiniftaMultiplayer.GetActor(baseAction.ActorName);
				List<ActorFacade> _targets = new List<ActorFacade>();
				foreach(var name in baseAction.TargetActorNames)
				{
					_targets.Add(SiniftaMultiplayer.GetActor(name));
				}

				if (baseAction.Type == RpcActionType.Teacher)
				{
					TeacherAction ta = new TeacherAction()
					{
						Id = baseAction.Id,
						Name = baseAction.Name,
						Duration = baseAction.Duration,
						Icon = baseAction.Icon,
						BackgroundColor = baseAction.BackgroundColor,
						Actor = SiniftaMultiplayer.GetActor(baseAction.ActorName),
						Targets = _targets
					};
					af.AddAction(ta);
				}
				if (baseAction.Type == RpcActionType.Student)
				{
					StudentAction ta = new StudentAction()
					{
						Id = baseAction.Id,
						Name = baseAction.Name,
						Duration = baseAction.Duration,
						Icon = baseAction.Icon,
						BackgroundColor = baseAction.BackgroundColor,
						Actor = SiniftaMultiplayer.GetActor(baseAction.ActorName),
						Targets = _targets
					};
					af.AddAction(ta);
				}
			}
			if (o is SelectScenarioEvent)
			{
				var sse = o as SelectScenarioEvent;

				KarmaMessageBus.Publish(new SelectScenarioReceiveEvent() { info = sse.info });
			}
			if (o is StartScenarioEvent)
			{
				SceneManager.LoadScene(Scenes.Game);
			}

			if(o is StartTimerEvent)
			{
				KarmaMessageBus.Publish(new StartTimerReceiveEvent());
			}
		}
	}

	public class ActionTriggeredEvent : KarmaBaseUnityEvent
	{
		public BaseActionRpcState Action { get; set; }
	}
}
