﻿namespace Sinifta.Report
{
	using KarmaFramework;
	using KarmaFramework.ActionCore;
	using KarmaFramework.KarmaUtils;
	using KarmaFramework.MissionCore;
	using KarmaFramework.ScenarioCore;
	using Newtonsoft.Json;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
	using Sinifta.Actions;
	using Sinifta.Actions.TeacherActions;
	using Sinifta.Actors;
	using Sinifta.Environment;
	using Sinifta.Localization;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Xml.Serialization;
	using UniRx;
	using UnityEngine;

	public class ActionRecord : KarmaBaseUnityEvent
	{
		public int StartTime { get; set; }
		public int FinishTime { get; set; }
		public BaseAction ExecutableAction { get; set; }
		public List<Tuple<float, bool>> SuspendTimes { get; set; }

		public ActionRecord()
		{
			SuspendTimes = new List<Tuple<float, bool>>();
		}
	}
	public enum MissionState
	{
		Inactive,
		Active,
		Succesful,
		Failed
	}

	public class MissionRecord : KarmaBaseUnityEvent
	{
		public int StartTime { get; set; }
		public int FinishTime { get; set; }
		public bool Success { get; set; }
		public BaseMission BaseMission { get; set; }
		public MissionState MissionState { get; set; }

		public MissionRecord()
		{
		}
	}

	//<Orbay>
	public class TabDurationEntry
	{
		public string TabName { get; set; }

		public float ActiveTime { get; set; }
	}
	public class LectureRecord
	{
		#region Public Fields
		public const float SnapshotResolution = 5f;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		float _snapshotTimer = 5.0001f;
		#endregion

		public int Score { get; set; }
		public List<TabDurationEntry> TabDurations { get; set; }
		public List<ActionRecord> ActionRecords { get; set; }
		public List<MissionRecord> MissionRecords { get; set; }
		public List<SerializableVector3> TeacherPositions { get; set; }
		public Dictionary<int, List<SerializableVector3>> StudentToSnapshots { get; set; }
		public List<SerializableVector3> TeacherSnapshots { get; set; }
		public float TeacherTravelAmount { get; set; }

		public LectureResult Result
		{
			get
			{
				return new LectureResult(this);
			}
		}

		public LectureRecord()
		{
			this.ActionRecords = new List<ActionRecord>();
			this.MissionRecords = new List<MissionRecord>();
			this.StudentToSnapshots = new Dictionary<int, List<SerializableVector3>>();
			this.TeacherSnapshots = new List<SerializableVector3>();

			KarmaMessageBus.OnEvent<ActionRecord>()
				.Subscribe(ev => this.ActionRecords.Add(ev))
				.AddTo(this.disposables);
			KarmaMessageBus.OnEvent<MissionRecord>()
				.Subscribe(ev => this.MissionRecords.Add(ev))
				.AddTo(this.disposables);

			this.TabDurations = new List<TabDurationEntry>
			{
				new TabDurationEntry { TabName = "ScenarioTab",ActiveTime= 0.0f },
				new TabDurationEntry { TabName = "ClassroomLayoutTab",ActiveTime= 0.0f },
				new TabDurationEntry { TabName = "CourseFlowTab",ActiveTime= 0.0f },
				new TabDurationEntry { TabName = "DifficultyTab",ActiveTime= 0.0f }
			};
		}

		~LectureRecord()
		{
			this.disposables.Dispose();
		}

		public ActionRecord FindActionRecord(Guid baseActionId)
		{
			return ActionRecords.FirstOrDefault(x => x.ExecutableAction.Id == baseActionId);
		}

		/*<Orbay> 
			   [Every 5 frame]   Loads all students Concentration, Enjoyment and Knowledge data to "StudentToSnapshots"
			   [Every frame]     Loads average Concentration, Enjoyment and Knowledge data to "TeacherSnapshots"   
		*/
		public void Update(List<StudentFacade> students, TeacherController teacherController) //öğrenci ve öğretmen snapshot güncellenmesi
		{
			if (_snapshotTimer > SnapshotResolution)
			{
				float avgCon = 0;
				float avgEnj = 0;
				float avgKno = 0;
				foreach (var student in students)
				{
					var ss = student.GetMindSnapshot();
					avgCon += ss.Params["Concentration"];
					avgEnj += ss.Params["Enjoyment"];
					avgKno += ss.Params["Knowledge"];

					if (StudentToSnapshots.ContainsKey(student.GetActor().Id))
						StudentToSnapshots[student.GetActor().Id].Add(new SerializableVector3(ss.Params["Concentration"], ss.Params["Enjoyment"], ss.Params["Knowledge"]));
					else
						StudentToSnapshots.Add(student.GetActor().Id, new List<SerializableVector3>() { new SerializableVector3(ss.Params["Concentration"], ss.Params["Enjoyment"], ss.Params["Knowledge"]) });
				}

				TeacherSnapshots.Add(new SerializableVector3(avgCon / students.Count, avgEnj / students.Count, avgKno / students.Count));

				_snapshotTimer = 0f;
			}

			_snapshotTimer += KarmaTime.DeltaTime;
			TeacherTravelAmount = teacherController.DistanceTraveled;
		}
	}

	#region Statistics / JSON
	[Serializable]
	public class EndGameReport
	{
		public string Username { get; set; }
		public string Date { get; set; }
		public LectureResult LectureResult { get; set; }
		[JsonIgnore]
		public Classroom Classroom { get; set; }

		public string ScenarioName { get; set; }
		public string SavedVideoPath { get; set; }
		public int Level { get; set; }
		public int EnvironmentId { get; set; }
		public int EnvironmentState { get; set; }
		public float Width { get; set; }
		public float Height { get; set; }
		public EndGameReport()
		{
			LectureResult = new LectureResult();
			Classroom = new Classroom();
		}

		public EndGameReport(Scenario scenario, Classroom classroom, LectureResult lectureResult, SerializableVector3 teacherParams)
		{
			DateTime todayTime = DateTime.Today;
			Date = string.Format("{0}/{1}/{2}", todayTime.Day, todayTime.Month, todayTime.Year);

			LectureResult = lectureResult;
			Classroom = classroom;
			EnvironmentId = scenario.EnvironmentId;
			EnvironmentState = scenario.EnvironmentState;
			ScenarioName = scenario.ReadableName;
			Height = classroom.Height;
			Width = classroom.Width;
			CalculateScore(teacherParams);
			if (int.TryParse(scenario.Name, out int level))
			{
				Level = level;
			}
		}

		private void CalculateScore(SerializableVector3 teacherParams)
		{
			HashSet<Type> validIntroActivities = new HashSet<Type>();
			HashSet<Type> validMidActivities = new HashSet<Type>();
			HashSet<Type> validOutroActivities = new HashSet<Type>();
			bool successfulTechActivity = false;

			foreach (var activityResult in LectureResult.ActivityResults)
			{
				Type type = Type.GetType(activityResult.Type);

				if (activityResult.StartTime < 60f)
				{
					if (SiniftaUtility.ValidIntroLessons.Contains(type))
						validIntroActivities.Add(type);
				}
				else if (activityResult.StartTime < 180f)
				{
					if (SiniftaUtility.ValidMidLessons.Contains(type))
						validMidActivities.Add(type);
				}
				else
				{
					if (SiniftaUtility.ValidOutroLessons.Contains(type))
						validOutroActivities.Add(type);
				}

				/*if (activityResult.TechState == LectureTechState.CompletedBoard || activityResult.TechState == LectureTechState.CompletedTablet)
					successfulTechActivity = true;*/
			}

			int introScore = validIntroActivities.Count > 2 ? 30 : validIntroActivities.Count * 15;
			int midScore = validMidActivities.Count > 2 ? 30 : validMidActivities.Count * 15;
			int outroScore = validOutroActivities.Count > 2 ? 30 : validOutroActivities.Count * 15;

			int lessonPlanPoints = introScore + midScore + outroScore;
			if (successfulTechActivity)
				lessonPlanPoints += 10;

			float simulationPoints = (teacherParams.x + teacherParams.y + teacherParams.z) / 3f;

			LectureResult.Score = Mathf.Clamp((int)(lessonPlanPoints * 0.3f + simulationPoints * 0.7f), 0, 100);
		}
	}

	[Serializable]
	public class StudentSnapshotHolder
	{
		public int StudentId { get; set; }
		public List<SerializableVector3> Snapshots { get; set; }

		public StudentSnapshotHolder()
		{
			Snapshots = new List<SerializableVector3>();
		}

		public StudentSnapshotHolder(int studentId, List<SerializableVector3> snapshots)
		{
			StudentId = studentId;
			Snapshots = snapshots;
		}
	}

	[Serializable]
	public class LectureResult
	{
		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();
		#endregion

		public int Score { get; set; }

		public List<MissionResult> MissionResults { get; private set; }
		public List<StudentActionResult> MisbehaveResult { get; set; }
		public List<TeacherActionResult> TeacherResult { get; set; }
		public List<StudentActionResult> StudentResult { get; set; }
		public List<ActionResult> ActionResults { get; private set; }
		public List<TeacherLectureActionResult> ActivityResults { get; private set; }
		public List<StudentSnapshotHolder> StudentSnapshots { get; private set; }
		public List<TabDurationEntry> TabDurations { get; private set; }

		public decimal DealPercent { get; set; }
		public decimal CorrectDealPercent { get; set; }
		public decimal WrongDealPercent { get; set; }
		public int CorrectDealCount { get; set; }
		public int WrongDealCount { get; set; }

		public float TeacherTravelAmount { get; set; }
		public int FocusCount { get; set; }
		public int EyeContactCount { get; set; }
		public int BoardUseCount { get; set; }


		public List<SerializableVector3> TeacherSnapshots { get; set; }

		public Dictionary<string, int> TpackPositiveCounts;
		public Dictionary<string, int> TpackNegativeCounts;

		public LectureResult()
		{
			MissionResults = new List<MissionResult>();
			MisbehaveResult = new List<StudentActionResult>();
			ActionResults = new List<ActionResult>();
			TeacherResult = new List<TeacherActionResult>();
			StudentResult = new List<StudentActionResult>();
			ActivityResults = new List<TeacherLectureActionResult>();
			StudentSnapshots = new List<StudentSnapshotHolder>();
			TeacherSnapshots = new List<SerializableVector3>();

			TpackPositiveCounts = new Dictionary<string, int>();
			TpackNegativeCounts = new Dictionary<string, int>();

			TabDurations = new List<TabDurationEntry>();

			KarmaMessageBus.OnEvent<ActionResult>()
				.Subscribe(ev => this.ActionResults.Add(ev))
				.AddTo(this.disposables);
		}

		~LectureResult()
		{
			this.disposables.Dispose();
		}

		public LectureResult(LectureRecord lr) : this()
		{
			Score = lr.Score;

			StudentSnapshots = new List<StudentSnapshotHolder>();
			foreach (var kvPair in lr.StudentToSnapshots)
				StudentSnapshots.Add(new StudentSnapshotHolder(kvPair.Key, kvPair.Value));

			TeacherSnapshots = lr.TeacherSnapshots;
			TeacherTravelAmount = lr.TeacherTravelAmount;

			foreach (var mr in lr.MissionRecords)
			{
				MissionResults.Add(new MissionResult(mr));
			}

			var studentRecords = lr.ActionRecords.Where(rec => rec.ExecutableAction is StudentAction && ((StudentAction)rec.ExecutableAction).IsMisbehaviour);
			foreach (var sr in studentRecords)
			{
				MisbehaveResult.Add(new StudentActionResult(sr));
			}

			var studentActionRecords = lr.ActionRecords.Where(rec => rec.ExecutableAction is StudentAction);
			foreach (var sr in studentActionRecords)
			{
				StudentResult.Add(new StudentActionResult(sr));
			}

			var teacherRecords = lr.ActionRecords.Where(rec => rec.ExecutableAction is TeacherAction);
			foreach (var tr in teacherRecords)
			{
				TeacherResult.Add(new TeacherActionResult(tr));
			}

			foreach (var ar in lr.ActionRecords)
			{
				if ((ar.ExecutableAction is StudentAction)
						&& ((StudentAction)ar.ExecutableAction).IsMisbehaviour)
				{
					ActionResults.Add(new StudentActionResult(ar));
				}
				else if (ar.ExecutableAction is TeacherAction)
				{
					if (ar.ExecutableAction is TeacherLecture)
						ActivityResults.Add(new TeacherLectureActionResult(ar));
					else
						ActionResults.Add(new TeacherActionResult(ar));
				}
				else
				{
					ActionResults.Add(new ActionResult(ar));
				}
			}

			FocusCount = lr.ActionRecords.Count(x => x.ExecutableAction is TeacherFocus);
			EyeContactCount = lr.ActionRecords.Count(x => x.ExecutableAction is TeacherLookAtStudent);
			BoardUseCount = lr.ActionRecords.Count(x => x.ExecutableAction is OpenBoard);

			//<Orbay> // For learning Linq
			lr.TabDurations.ForEach(tabs =>
			{
				this.TabDurations.Add(tabs);
			});
		}


		private void AddTpacksTo(Dictionary<string, int> countDict, IEnumerable<string> tpackList)
		{
			foreach (var str in tpackList)
			{
				if (countDict.ContainsKey(str))
				{
					countDict[str]++;
				}
				else
				{
					countDict.Add(str, 1);
				}
			}
		}
	}

	[Serializable]
	public class MissionResult
	{
		public string MissionType { get; set; }
		public string MissionText { get; set; }
		public string StartTime { get; set; }
		public string FinishTime { get; set; }
		public List<string> Tpacks { get; set; }
		public bool Success { get; set; }

		public MissionResult()
		{
		}

		public MissionResult(MissionRecord m)
		{
			MissionType = m.BaseMission.GetType().ToString();
			MissionText = m.BaseMission.MissionText;
			StartTime = Util.TimeFormat(m.StartTime);
			FinishTime = Util.TimeFormat(m.FinishTime);
			Success = m.Success;
		}
	}

	[Serializable]
	public class ActionResult : KarmaBaseUnityEvent
	{
		public string ActionName;
		public string ActorName { get; set; }
		public string ActorId { get; set; }
		public int StartTime { get; set; }
		public int FinishTime { get; set; }

		public string ActionStringIdString { get; set; }

		[XmlIgnore]
		public StringId ActionStringId
		{
			get => (StringId)Enum.Parse(typeof(StringId), this.ActionStringIdString);
			set => this.ActionStringIdString = value.ToString();
		}

		public ActionResult()
		{
		}

		public ActionResult(ActionRecord ar)
		{
			ActionStringIdString = ar.ExecutableAction.ActionStringIdString;
			ActionName = ar.ExecutableAction.Name;
			ActorName = ar.ExecutableAction.Actor.Name;
			ActorId = ar.ExecutableAction.Actor.FacadeId.ToString();
			StartTime = ar.StartTime;
			FinishTime = ar.FinishTime;
		}
	}

	[Serializable]
	public class StudentActionResult : ActionResult
	{
		public int StudentId { get; set; }
		public string StateString { get; set; }

		[JsonIgnore, XmlIgnore]
		public ActionDealedState State
		{
			get { return (ActionDealedState)Enum.Parse(typeof(ActionDealedState), StateString); }
			set { StateString = value.ToString(); }
		}

		public StudentActionResult() { }

		public StudentActionResult(ActionRecord ar) : base(ar)
		{
			StudentId = ((StudentFacade)ar.ExecutableAction.Actor).FacadeId;
			State = (ar.ExecutableAction as StudentAction).DealedState.Value;
		}
	}

	[Serializable]
	public class TeacherActionResult : ActionResult
	{
		public string TargetActionType { get; set; }
		public List<int> TargetStudentIds { get; set; }
		public string ActionResultString { get; set; }

		[JsonIgnore, XmlIgnore]
		public KarmaFramework.ActionCore.ActionResult ActionResult
		{
			get { return (KarmaFramework.ActionCore.ActionResult)Enum.Parse(typeof(KarmaFramework.ActionCore.ActionResult), ActionResultString); }
			set { ActionResultString = value.ToString(); }
		}

		public TeacherActionResult()
		{
			TargetStudentIds = new List<int>();
		}

		public TeacherActionResult(ActionRecord ar) : base(ar)
		{
			TeacherAction teacherAction = (TeacherAction)ar.ExecutableAction;
			ActionResult = teacherAction.ActionResult;

			TargetStudentIds = new List<int>();
			StudentFacade sa = teacherAction.Targets[0] as StudentFacade;
			if (sa != null)
				TargetStudentIds.Add(sa.FacadeId);
			else if (teacherAction.Targets != null && teacherAction.Targets.Count > 0)
			{
				foreach (StudentFacade av in teacherAction.Targets)
				{
					if (av != null)
						TargetStudentIds.Add(sa.FacadeId);
				}
			}

			NPCAction targetAct = ar.ExecutableAction.Targets[0].GetAllActions().FirstOrDefault() as NPCAction;
			if (targetAct != null)
			{
				TargetActionType = targetAct.GetType().FullName;
			}
		}
	}

	[Serializable]
	public class TeacherLectureActionResult : TeacherActionResult
	{
		public string Type { get; set; }
		public string Text { get; set; }
		public bool TechActionRequired { get; set; }
		public SerializableColor TimelineColor { get; set; }
		public string TechStateString { get; set; }

		public TeacherLectureActionResult() { }

		public TeacherLectureActionResult(ActionRecord ar) : base(ar)
		{
			//if( ActionResult == TeacherExecutableActionResult.LectureFailed )
			//	TpackList = null;

			Type = ar.ExecutableAction.GetType().ToString();
			Text = ((TeacherAction)ar.ExecutableAction).Text;
			TechActionRequired = false;
			TimelineColor = ((TeacherAction)ar.ExecutableAction).BackgroundColor;
		}
	}
	[Serializable]
	public class ScenarioLog
	{
		string _name;
		int _nextTime;
		public string GetName()
		{
			return _name;
		}
		public string Name { get { return this._name; } set { this._name = value; } }
		public int Time { get { return this._nextTime; } set { this._nextTime = value; } }
	}

	[Serializable]
	public class ScenarioRecord
	{
		public class DeskArrangement
		{
			public string _name;
			public int _nextTime;
		}
		public class TeachingPlan
		{
			public Dictionary<string, int> _CourseFlow = new Dictionary<string, int>();
			public int _nextTime;
		}
		public class ScenarioDifficulty
		{
			public int _difficultyLevel;
			public int _nextTime;
		}
	}
	#endregion
}
