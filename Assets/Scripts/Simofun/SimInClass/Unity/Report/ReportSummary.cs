﻿using System;
using Sinifta.Report;

[Serializable]
public class ReportSummary
{
	public string Username { get; set; }
	public string Date { get; set; }

	public ReportSummary() { }

	public ReportSummary(EndGameReport report)
	{
		Username = report.Username;
		Date = report.Date;
	}
}
