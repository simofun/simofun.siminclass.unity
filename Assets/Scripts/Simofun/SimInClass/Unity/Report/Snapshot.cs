﻿using KarmaFramework.API;
using KarmaFramework.Localization;
using KarmaFramework.ScenarioCore;
using Sinifta.Environment;
using Sinifta.Report;
using System.Collections.Generic;
using System.Linq;

public class Snapshot // See "RecordLog.cs" for the mirror of this class on the server side 
{
    public int ScenarioTime;
    public string ScenarioId { get; set; }
    public int StarCount { get; set; }
    public int Score { get; set; }
    public Dictionary<string, int> PositiveTpackToCount { get; set; }
    public Dictionary<string, int> NegativeTpackToCount { get; set; }

    public string TeacherTravelAmount { get; set; }
    public int FocusCount { get; set; }
    public int EyeContactCount { get; set; }
    public int BoardUseCount { get; set; }
    public int LayoutIndex { get; set; }
    public Dictionary<string, string> InitialFlow { get; set; }
    public Dictionary<string, string> Activities { get; set; }
    public List<TabDurationEntry> TabDurations { get; private set; }
    public int[] ConcentrationValues { get; set; }
    public int[] EnjoymentValues { get; set; }
    public int[] KnowledgeValues { get; set; }

    public Snapshot(GameState gs, LectureResult result)
    {
        ScenarioId = gs.Scenario.Name;
        Score = result.Score;
        PositiveTpackToCount = result.TpackPositiveCounts;
        NegativeTpackToCount = result.TpackNegativeCounts;
        TeacherTravelAmount = result.TeacherTravelAmount.ToString("F1");
        FocusCount = result.FocusCount;
        EyeContactCount = result.EyeContactCount;
        BoardUseCount = result.BoardUseCount;
        LayoutIndex = (int)(gs.Environment as Classroom).InitialState;

        InitialFlow = new Dictionary<string, string>();
        /*foreach (var triggerActionsTuple in gs.Scenario.LectureActions)
        {
            var timeInt = (int)(triggerActionsTuple.Triggers[0] as TimeTrigger).Time;
            var time = timeInt.ToString();
            var lecName = triggerActionsTuple.Actions[0].GetType().Name;
            InitialFlow[time] = lecName;
        }*/

        Activities = new Dictionary<string, string>();
        foreach (var activityResult in result.ActivityResults)
        {
            var time = activityResult.StartTime.ToString();
            var lecName = Localizer.Instance.GetString(activityResult.ActionName);
            Activities[time] = lecName;
        }

        ConcentrationValues = result.TeacherSnapshots.Select(x => (int)x.x).ToArray();
        EnjoymentValues = result.TeacherSnapshots.Select(x => (int)x.y).ToArray();
        KnowledgeValues = result.TeacherSnapshots.Select(x => (int)x.z).ToArray();
        //<Orbay>//Linq Exercises
        //Interface changed
        //result.TabDurations.ForEach(tabs =>
        //                            {
        //                                this.TabDurations.Add(tabs);
        //                            });



    }
}