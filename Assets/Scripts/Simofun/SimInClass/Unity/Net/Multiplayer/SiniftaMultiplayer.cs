﻿using Sinifta.Networking;

namespace Simofun.SimInClass.Unity.Net.Multiplayer
{
	using KarmaFramework.CharacterCore;
	using KarmaFramework.UI;
	using Simofun.Networking.Core;
	using Simofun.Networking.Core.Events;
	using Simofun.Unity.DesignPatterns.Singleton;
	using System.Collections;
	using UnityEngine;
	using Zenject;

	public class SiniftaMultiplayer : SimGlobalSingletonBase<SiniftaMultiplayer>
	{
		#region Static Fields
		static ActorRegistry actorRegistery;
		#endregion

		#region Fields
		IConnectionProvider multiplayerProvider;

		MenuNavigator navigator;

		PlayerSyncer ownedSyncer;
		#endregion

		#region Construct
		[Inject]
		public virtual void Construct(ActorRegistry actorRegistery, MenuNavigator navigator)
		{
			SiniftaMultiplayer.actorRegistery = actorRegistery;
			this.navigator = navigator;

			this.multiplayerProvider = SimNetwork.Instance;
		}
		#endregion

		#region Unity Methods
		protected virtual void Start()
		{
			this.multiplayerProvider.Connect();

			this.RegisterEventHandlers();
		}
		#endregion

		#region Public Static Methods
		public static ActorFacade GetActor(string name) => actorRegistery.GetActor(name);
		#endregion

		#region Public Methods
		public void CreateRoom(string roomName) => this.multiplayerProvider.JoinOrCreateRoom(roomName);

		public void JoinRoom(string roomName) => this.multiplayerProvider.JoinRoom(roomName);

		public void NavigatorProceed()
		{
			this.navigator.Proceed();
			this.navigator.NextButton.gameObject.SetActive(
				this.navigator.NextButton.gameObject.activeSelf
					&& this.multiplayerProvider.IsConnected
					&& this.multiplayerProvider.IsMaster);
		}

		public void StartGame()
		{
			this.ownedSyncer = this.multiplayerProvider.Instantiate("Multiplayer/PlayerSyncer")
				.GetComponent<PlayerSyncer>();
			this.StartCoroutine(this.WaitForInstantiateCoroutine());
		}
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Registration
		protected virtual void RegisterEventHandlers()
		{
			if (this.multiplayerProvider.IsConnected)
			{
				this.multiplayerProvider.ConnectionSuccessfull += this.HandleOnConnectionSuccessful;
				this.multiplayerProvider.RoomCreateFailed += this.HandleOnRoomCreateFailed;
				this.multiplayerProvider.JoinedRoom += this.HandleOnJoinedRoom;
				this.multiplayerProvider.JoinRoomFailed += this.HandleOnJoinRoomFailed;
			}
		}
		#endregion

		void HandleOnConnectionSuccessful(object sender, ConnectionSuccessfullEventArgs e)
		{
			this.multiplayerProvider.Me.NickName = Random.Range(0, 100).ToString();
		}

		void HandleOnJoinedRoom(object sender, RoomEventArgs e) => SiniftaMultiplayerView.Instance.OnJoinedRoom();

		void HandleOnJoinRoomFailed(object sender, RoomFailedEventArgs e)
		{
			/*SiniftaMultiplayerView.Instance.OnJoinRoomFailed(e.Message);*/
			Debug.LogError(e.Message);
		}

		void HandleOnRoomCreateFailed(object sender, RoomFailedEventArgs e)
		{
			// TODO: Set an UI Alert for the user
			Debug.LogError(e.Message);
		}
		#endregion
		#endregion

		#region Methods
		IEnumerator WaitForInstantiateCoroutine()
		{
			yield return new WaitForEndOfFrame();

			this.ownedSyncer.HandleStartGameEvent();
		}
		#endregion
	}
}
