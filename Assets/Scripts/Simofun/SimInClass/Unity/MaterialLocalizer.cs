﻿using System;
using System.Linq;
using UnityEngine;
using KarmaFramework.Localization;

public class MaterialLocalizer : MonoBehaviour {

    [Serializable]
    public struct MaterialLocalizerNode
    {
        public LanguageType Ltype;
        public Material Material;
    }
    [SerializeField]
    private MaterialLocalizerNode[] _materialNodes = new MaterialLocalizerNode[2];
    public MeshRenderer MeshRenderer;

    // Use this for initialization
    void Awake()
    {
        Init();
    }

    public void Init()
    {
        var rendererComp = this.GetComponent<MeshRenderer>();
        if (rendererComp)
        {
            MeshRenderer = rendererComp;

            var material = _materialNodes.SingleOrDefault(x => x.Ltype == Localizer.Instance.CurrentLanguage);    
            if ( material.Material != null)
            {
                this.gameObject.SetActive(true);
                MeshRenderer.material = material.Material;
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }
    }    

}
