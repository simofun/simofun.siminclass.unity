﻿#if !UNITY_ANDROID && !UNITY_IOS
#define NOT_MOBILE
#endif

using KarmaFramework;
using KarmaFramework.API;
using KarmaFramework.KarmaUtils;
using KarmaFramework.ScenarioCore;
using KarmaFramework.Views;
using Simofun.Karma.UniRx.Unity;
using Simofun.Karma.Unity.Domain.Model;
using Simofun.Karma.Unity.Web.Api;
using Simofun.Karma.Web.Api.Model;
using Simofun.SimInClass.Unity.Events;
using Sinifta.Actors;
using Sinifta.Environment;
using Sinifta.Interactables;
using Sinifta.Report;
using Sinifta.UI.ActivityUI;
using Sinifta.UI.BoardUI;
using Sinifta.UI.TabletUI;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

/// <summary>
/// Just testing some mechanics, not final version
/// </summary>
public class GameSetup : MonoBehaviour
{
	#region Unity Fields
	[SerializeField]
	Button StartButton;

	[SerializeField]
	TextAsset Localization;

	[SerializeField]
	GameObject TutorialPanel;

	[SerializeField]
	StudentTooltipView StudentTooltipView;

	[SerializeField]
	DetailedReportView detailedReport;

	[SerializeField]
	MobileScoreView scoreView;
	#endregion

	#region Fields
	readonly CompositeDisposable disposables = new CompositeDisposable();

	ActorRegistry registry;

	Board blackBoard;

	Board smartBoard;

	bool isFinished;

	bool isStarted;

	Classroom classroom;

	GameState gameState;

	IScene scene;

	IKarmaWebApiRestClient webApiClient;

	Scenario scenario;

	LectureRecord lectureRecord;

	List<StudentFacade> students;

	StudentFacade.Factory npcFactory;

	Tablet tablet;

	Activity activity;

	TeacherController teacherController;

	TeacherFacade.Factory playerFactory;
	#endregion

	#region Construct
	[Inject]
	public virtual void Construct(
		ActorRegistry registry,
		TeacherFacade.Factory playerFactory,
		StudentFacade.Factory npcFactory,
		GameState gameState,
		[Inject(Id = "BlackBoard")] Board blackBoard,
		[Inject(Id = "SmartBoard")] Board smartBoard,
		Tablet tablet,
		Activity activity,
		IScene scene,
		IKarmaWebApiRestClient webApiClient)
	{
		this.registry = registry;
		this.playerFactory = playerFactory;
		this.npcFactory = npcFactory;
		this.gameState = gameState;
		this.blackBoard = blackBoard;
		this.smartBoard = smartBoard;
		this.tablet = tablet;
		this.activity = activity;
		this.scene = scene;
		this.webApiClient = webApiClient;

		this.classroom = gameState.Environment as Classroom;
		this.lectureRecord = new LectureRecord();
		this.scenario = gameState.Scenario;
	}
	#endregion

	#region Unity Methods
	protected virtual void Awake()
	{
		this.StartButton.onClick.AddListener(() => 
		{
			if (this.gameState.IsTutorial)
			{
				this.OpenTutorialPanel();
			}
			this.isStarted = true;
			KarmaMessageBus.Publish(new PauseEvent() { State = false });
			this.StartButton.transform.parent.gameObject.SetActive(false);
		});
		this.isFinished = false;
	}

	protected virtual void Start()
	{
		this.playerFactory.Create(new Teacher());

		this.gameState.NPCs.OfType<Student>().ToList().ForEach(s => this.npcFactory.Create("Prefabs/Students/", s));
		//this.scenario.CreateFlow();
		this.classroom.CreateEnvironment(this.gameState);
		this.blackBoard.CreateApps();
		this.smartBoard.CreateApps();
		this.tablet.CreateApp();
		this.activity.CreateApp();
		this.students = this.registry.GetNPCs().OfType<StudentFacade>().ToList();
		this.students.ForEach(s => s.Bind(this.StudentTooltipView));

		FindObjectOfType<FlowView>().Bind(this.scenario);
		FindObjectOfType<EndGameView>().Bind(this.gameState, this.scenario);
		FindObjectOfType<BlackBoardView>().Bind(this.blackBoard);
		FindObjectOfType<SmartBoardView>().Bind(this.smartBoard);
		FindObjectOfType<TabletView>().Bind(this.tablet);
		FindObjectOfType<ActivityView>().Bind(this.activity);
		this.teacherController = FindObjectsOfType<TeacherController>()[0];


		KarmaMessageBus.OnEvent<EndGameEvent>().Subscribe(ev => {
			FindObjectsOfType<ProjectContext>().ToList().ForEach(c => Destroy(c.gameObject));
			classroom.Dispose();
			TimeManager.Instance.ResetTimeScale();
			SceneManager.LoadScene(this.scene.MainMenu);
		}).AddTo(this.disposables);

		KarmaMessageBus.Publish(new PauseEvent() { State = true });
	}

	protected virtual void Update()
	{
		if (!this.isStarted)
		{
			return;
		}

		if (Input.GetKeyDown(KeyCode.Q) && !this.isFinished)
		{
			Time.timeScale += 2;
		}

		if (Input.GetKeyDown(KeyCode.E) && !this.isFinished)
		{
			Time.timeScale -= 2;
		}

		this.scenario.Tick();
		this.lectureRecord.Update(this.students, this.teacherController);

		if (this.scenario != null && !this.isFinished)
		{
			if (KarmaTime.Time >= this.scenario.Duration && !this.isFinished)
			{
				KarmaCursor.Mode = KarmaCursorMode.Normal;

				this.isFinished = true;
				var newVector = new SerializableVector3(
					this.lectureRecord.TeacherSnapshots.Average(x => x.x),
					this.lectureRecord.TeacherSnapshots.Average(x => x.y),
					this.lectureRecord.TeacherSnapshots.Average(x => x.z));

			var result = this.lectureRecord.Result;
			detailedReport.SetResults(new EndGameReport(this.scenario, this.classroom, result, newVector), true);
			scoreView.gameObject.SetActive(true);
#if NOT_MOBILE
				this.webApiClient.SendProgress(
					new KarmaLevelInfo
					{
						ApplicationUserId = InterLevelData.AppConfig.UserInfo.Id,
						Level = int.Parse(this.scenario.Name),
						Score = result.Score
					});
#else
				MobileLevelScore.Instance.SetScore(this.scenario.Name, result.Score);
#endif

			}
		}
	}

	protected virtual void OnDestroy() => this.disposables.Dispose();
	#endregion

	#region Methods
	void OpenTutorialPanel() => this.TutorialPanel.SetActive(true);

	void Replay()
	{
		TimeManager.Instance.ResetTimeScale();

		SceneManager.LoadScene(this.scene.Game);
	}
	#endregion
}
