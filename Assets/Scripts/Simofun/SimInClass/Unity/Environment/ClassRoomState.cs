﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Sinifta.Environment
{
    public class ClassRoomState
    {
        public List<SeatLocation> SeatLocations { get; set; }
        public List<DeskLocation> DeskLocations { get; set; }
        [XmlAttribute]
        public int Id { get; set; }
        [XmlElement]
        public string Description { get; set; }
    }


    public class SeatLocation
    {
        [XmlIgnore]
        public float PosX { get; set; }
        [XmlIgnore]
        public float PosY { get; set; }
        [XmlIgnore]
        public float PosZ { get; set; }
        [XmlIgnore]
        public float RotX { get; set; }
        [XmlIgnore]
        public float RotY { get; set; }
        [XmlIgnore]
        public float RotZ { get; set; }
        [XmlAttribute]
        public string Position
        {
            get { return PosX + "," + PosY + "," + PosZ; }
            set
            {
                PosX = float.Parse(value.Split(',')[0], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                PosY = float.Parse(value.Split(',')[1], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                PosZ = float.Parse(value.Split(',')[2], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
            }
        }
        [XmlAttribute]
        public string Rotation
        {
            get { return RotX + "," + RotY + "," + RotZ; }
            set
            {
                RotX = float.Parse(value.Split(',')[0], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                RotY = float.Parse(value.Split(',')[1], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                RotZ = float.Parse(value.Split(',')[2], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
            }
        }
    }
    public class DeskLocation
    {
        [XmlIgnore]
        public float PosX { get; set; }
        [XmlIgnore]
        public float PosY { get; set; }
        [XmlIgnore]
        public float PosZ { get; set; }
        [XmlIgnore]
        public float RotX { get; set; }
        [XmlIgnore]
        public float RotY { get; set; }
        [XmlIgnore]
        public float RotZ { get; set; }
        [XmlAttribute]
        public string Position
        {
            get { return PosX + "," + PosY + "," + PosZ; }
            set
            {
                PosX = float.Parse(value.Split(',')[0], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                PosY = float.Parse(value.Split(',')[1], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                PosZ = float.Parse(value.Split(',')[2], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
            }
        }
        [XmlAttribute]
        public string Rotation
        {
            get { return RotX + "," + RotY + "," + RotZ; }
            set
            {
                RotX = float.Parse(value.Split(',')[0], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                RotY = float.Parse(value.Split(',')[1], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                RotZ = float.Parse(value.Split(',')[2], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
            }
        }
    }
}
