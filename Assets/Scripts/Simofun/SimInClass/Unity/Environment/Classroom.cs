﻿namespace Sinifta.Environment
{
	using KarmaFramework.EnvironmentCore;
	using Simofun.Karma.UniRx.Unity;
	using Simofun.Karma.Unity.Events;
    using Simofun.SimInClass.Unity.UI;
    using Sinifta.Actors;
	using Sinifta.AI;
	using Sinifta.Interactables;
    using System.Collections.Generic;
	using System.Linq;
	using System.Xml.Serialization;
	using UniRx;
	using UnityEngine;

	public enum Placement
	{
		Classroom,

		Corridor
	}

	public enum ClassroomState
	{
		Standart = 0,

		StudyGroup = 1,

		UShape = 2,

		OShape = 3
	}

	public class Classroom : GameEnvironment, System.IDisposable
	{
		#region Public Fields
		public ClassroomState InitialState;

		public ClassroomState CurrentState;
		#endregion

		#region Fields
		readonly CompositeDisposable disposables = new CompositeDisposable();

		[XmlIgnore]
		public readonly Dictionary<ClassroomState, List<Transform>> _waypoints = new Dictionary<ClassroomState, List<Transform>>();

		GameState _gameState;
		List<Transform> _chairs = new List<Transform>();

		List<Transform> _desks = new List<Transform>();

		List<Transform> _windows = new List<Transform>();

		Transform _boards;
		ClassroomStatePanel _classroomStatePanel;
		#endregion

		[XmlAttribute]
		public int SeatCount;
		[XmlAttribute]
		public int GroupWorkCount;
		[XmlAttribute]
		public float Width;
		[XmlAttribute]
		public float Height;
		[XmlArray]
		public List<ClassRoomState> ClassRoomStates { get; set; }

		[XmlIgnore]
		public Transform PlayerStart { get; private set; }
		[XmlIgnore]
		public Bounds SpecBounds { get; private set; }
		[XmlIgnore]
		public Vector3 PresentationSlot { get; private set; }
		[XmlIgnore]
		public Vector3 CorridorSlot { get; private set; }
		[XmlIgnore]
		public Vector3 ClassroomBack { get; private set; }
		[XmlIgnore]
		public Dictionary<Board.BoardType, GameObject> BoardDict;

		[XmlIgnore]
		public List<StudentFacade> students;

		public Classroom()
		{
			//SeatCount = element.GetIntAttr("SeatCount");
			//GroupWorkCount = element.GetIntAttr("GroupWorkCount");
			//ClassRoomStates = new List<ClassRoomState>();
			//var states = element.Element("ClassRoomStates");
			//foreach (var item in states.Elements())
			//{
			//    var stream = XMLExtensions.GenerateStreamFromString(item.ToString());
			//    var state = new XmlSerializer(typeof(ClassRoomState)).Deserialize(stream) as ClassRoomState;
			//    ClassRoomStates.Add(state);
			//}
			InitialState = ClassroomState.UShape;
		}

		~Classroom()
		{
			Dispose();
		}

		public override void CreateEnvironment(GameState gameState)
		{
			_gameState = gameState;
			InitialState = (ClassroomState)_gameState.Scenario.EnvironmentState;

			students = _registry.GetNPCs().OfType<StudentFacade>().ToList();
			var classGO = Object.Instantiate(Resources.Load<GameObject>("Prefabs/Classroom/" + this.VisualName));
			_windows = GameObject.FindGameObjectsWithTag("Window").Select(x => x.transform).ToList();
			SpecBounds = classGO.transform.Find("SpecBounds").GetComponent<BoxCollider>().bounds;
			PlayerStart = classGO.transform.Find("TeacherStart");
			PresentationSlot = classGO.transform.Find("PresentationSlot").position;
			CorridorSlot = classGO.transform.Find("CorridorSlot").position;
			ClassroomBack = classGO.transform.Find("ClassroomBack").position;
			_boards = GameObject.Find("SmartBoard").transform;
			_classroomStatePanel = GameObject.FindObjectOfType<ClassroomStatePanel>();
			_classroomStatePanel.CloseClassroomStatePanel();
			// Smartboard / blackboard
			BoardDict = new Dictionary<Board.BoardType, GameObject>();
			var smart = _boards.Find("ekran2/SpecSmartBoardScreen").gameObject;
			var black = _boards.Find("ekran1/SpecBlackBoardScreen").gameObject;
			BoardDict.Add(Board.BoardType.SmartBoard, smart);
			BoardDict.Add(Board.BoardType.BlackBoard, black);

			
			CreateDesksAndChairs(classGO);
			RemoveUnusedSeats(students.Select(s => s.GetPlacement()).ToArray());
			foreach (var student in students)
			{
				student.Chair = GetChair(student.GetPlacement());
				student.TableTop = GetTableTop(student.GetPlacement());
				student.ChairSlot = GetChairSlot(student.GetPlacement());
				student.DeskStandPosSlot = GetDeskStandPos(student.GetPlacement());
			}

			KarmaMessageBus.OnEvent<ClassroomChangeEvent>()
				.Subscribe(ev =>
                {
					this._classroomStatePanel.OpenClassroomStatePanel();
					this.OnEnvironmentStateChanged(ev.State);
				}).AddTo(this.disposables);

			var wpLectureParent = classGO.transform.Find("Waypoints_Lecture");
			if (wpLectureParent)
			{
				_waypoints.Add(ClassroomState.Standart, new List<Transform>());
				foreach (Transform wpLecture in wpLectureParent)
				{
					_waypoints[ClassroomState.Standart].Add(wpLecture);
				}
			}

			var wpGroupsParent = classGO.transform.Find("Waypoints_GroupWork");
			if (wpGroupsParent)
			{
				_waypoints.Add(ClassroomState.StudyGroup, new List<Transform>());
				foreach (Transform wpGroupWork in wpGroupsParent)
				{
					_waypoints[ClassroomState.StudyGroup].Add(wpGroupWork);
				}
			}

			var wpUShapeParent = classGO.transform.Find("Waypoints_UShape");
			if (wpUShapeParent)
			{
				_waypoints.Add(ClassroomState.UShape, new List<Transform>());
				foreach (Transform wpUShape in wpUShapeParent)
				{
					_waypoints[ClassroomState.UShape].Add(wpUShape);
				}
			}
			var wpOShapeParent = classGO.transform.Find("Waypoints_OShape");
			if (wpOShapeParent)
			{
				_waypoints.Add(ClassroomState.OShape, new List<Transform>());
				foreach (Transform wpOShape in wpOShapeParent)
				{
					_waypoints[ClassroomState.OShape].Add(wpOShape);
				}
			}

			OnEnvironmentStateChanged(InitialState);

			var player = _registry.GetPlayer();
			//player.Transform.GetComponent<FirstPersonController>().enabled = false;
			player.Transform.position = PlayerStart.localPosition;
			player.Transform.rotation = PlayerStart.rotation;
			//player.Transform.GetComponent<FirstPersonController>().enabled = true;
		}

		public void RemoveUnusedSeats(int[] occupiedIds)
		{
			var chairCount = _chairs.Count;
			for (var i = 0; i < chairCount; i++)
			{
				if (!occupiedIds.Contains(i))
				{
					Object.DestroyImmediate(_chairs[i].gameObject);
					Object.DestroyImmediate(_desks[i].gameObject);
				}
			}

			_chairs.RemoveAll(x => x == null);
			_desks.RemoveAll(x => x == null);
		}

		private void CreateDesksAndChairs(GameObject classGo)
		{
			var classRoomState = ClassRoomStates.First(x => x.Id == (int)InitialState);
			var desksParent = classGo.transform.Find("ogr_masalari");

			for (var i = 0; i < classRoomState.DeskLocations.Count; i++)
			{
				var deskLoc = classRoomState.DeskLocations[i];

				var desk = Object.Instantiate(Resources.Load<GameObject>("Prefabs/StudentEnvironment/StudentDesk"));
				desk.transform.SetParent(desksParent);
				desk.transform.position = new Vector3(deskLoc.PosX, deskLoc.PosY, deskLoc.PosZ) + _boards.position;
				desk.transform.eulerAngles = new Vector3(deskLoc.RotX, deskLoc.RotY, deskLoc.RotZ);
				desk.transform.name = "ogr_masalari_" + (i + 1);
				_desks.Add(desk.transform);

				var propPath = string.Format("Prefabs/StudentEnvironment/DeskProps/DeskProps_{0}", Random.Range(1, 11).ToString());
				var deskProps = Object.Instantiate(Resources.Load<GameObject>(propPath));
				deskProps.transform.SetParent(desk.transform);

				deskProps.transform.localPosition = new Vector3(0f, -0.025f, 0f);
				deskProps.transform.localEulerAngles = Vector3.zero;
			}

			var chairsParent = classGo.transform.Find("ogr_sandalyeleri");
			for (var i = 0; i < classRoomState.SeatLocations.Count; i++)
			{
				var seatLoc = classRoomState.SeatLocations[i];
				var chair = Object.Instantiate(Resources.Load<GameObject>("Prefabs/StudentEnvironment/StudentChair"));
				chair.transform.SetParent(chairsParent);
				chair.transform.position = new Vector3(seatLoc.PosX, seatLoc.PosY, seatLoc.PosZ) + _boards.position; ;
				chair.transform.eulerAngles = new Vector3(seatLoc.RotX, seatLoc.RotY, seatLoc.RotZ);
				chair.transform.name = "ogr_sandalyeleri_" + (i + 1);

				_chairs.Add(chair.transform);
			}
		}

		public Transform GetChair(int deskNumber)
		{
			return _chairs.Find(xfrm => int.Parse(xfrm.name.Split('_')[2]) - 1 == deskNumber);
		}

		public Transform GetChairSlot(int deskNumber)
		{
			return GetChair(deskNumber).Find("ChairSitSlot");
		}

		public Transform GetDeskStandPos(int deskNumber)
		{
			return GetDesk(deskNumber).Find("ChairStandPosSlot_1");
		}

		public Transform GetDesk(int deskNumber)
		{
			return _desks.Find(xfrm => int.Parse(xfrm.name.Split('_')[2]) - 1 == (deskNumber));
		}

		public Transform GetTableTop(int deskNumber)
		{
			return GetDesk(deskNumber).Find("DeskSlot_01");
		}

		public Transform GetClosestWindow(Transform avatarTransform)
		{
			return _windows.Aggregate((curMin, x) =>
					Vector3.Distance(x.transform.position, avatarTransform.position)
						< Vector3.Distance(curMin.transform.position, avatarTransform.position)
					? x
					: curMin);
		}

		public class ClassroomChangeEvent : KarmaBaseUnityEvent
		{
			public ClassroomState State { get; set; }
		}

		public void OnEnvironmentStateChanged(ClassroomState state)
		{
			NavmeshBaker.Instance.ClearNavmesh();
			if (state == CurrentState)
				return;
			CurrentState = state;
			SiniftaUtility.InitNavigation(_waypoints[CurrentState]);


			var classRoomState = ClassRoomStates.First(x => x.Id == (int)state);
			foreach (var chairTransform in _chairs)
			{
				var chair = classRoomState.SeatLocations[int.Parse(chairTransform.name.Split('_')[2]) - 1];
				chairTransform.position = new Vector3
				{
					x = chair.PosX,
					y = chair.PosY,
					z = chair.PosZ,
				} + _boards.position;

				chairTransform.eulerAngles = new Vector3
				{
					x = chair.RotX,
					y = chair.RotY,
					z = chair.RotZ,
				};
			}
			for (var i = 0; i < _desks.Count; i++)
			{
				_desks[i].position = new Vector3
				{
					x = classRoomState.DeskLocations[i].PosX,
					y = classRoomState.DeskLocations[i].PosY,
					z = classRoomState.DeskLocations[i].PosZ,
				} + _boards.position;

				_desks[i].eulerAngles = new Vector3
				{
					x = classRoomState.DeskLocations[i].RotX,
					y = classRoomState.DeskLocations[i].RotY,
					z = classRoomState.DeskLocations[i].RotZ,
				};
			}
		   
			foreach (var student in students)
			{
				student.Transform.position = student.ChairSlot.position;// + student.ChairSlot.forward * 0.286f;
				student.Transform.rotation = student.Chair.rotation;
				student.NavMeshWarp(student.ChairSlot.position);
			}

			NavmeshBaker.Instance.BakeNavmesh(0);
		}

        public void Dispose()
        {
			disposables.Dispose();
        }
    }
}
