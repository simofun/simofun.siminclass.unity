 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node 
{
		public string Name { get; private set; }
		public Vector3 Position { get; private set; }
		public HashSet<Node> Neighbors { get; private set; }

		public Node(string name, Vector3 pos)
		{
			Name = name;
			Position = pos;
			Neighbors = new HashSet<Node>();
		}
}
