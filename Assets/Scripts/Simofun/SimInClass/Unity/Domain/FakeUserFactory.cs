﻿namespace Simofun.SimInClass.Unity.Domain
{
	using KarmaFramework;

	public class FakeUserFactory
	{
		public UserInfo CreateTestUser() => new UserInfo
		{
			Id = "80693942-f0cd-4488-9eed-822012626c28",
			Name = "test",
			Surname = "user",
			Email = "test@test",
			EmailConfirmed = true,
			HasMultiplayerPermission = false,
			TrialDaysLeft = 999
		};
	}
}
