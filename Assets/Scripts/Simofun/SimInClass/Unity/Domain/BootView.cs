﻿#if UNITY_ANDROID || UNITY_IOS
#define MOBILE
#endif

namespace Simofun.SimInClass.Unity.Domain
{
	using Simofun.SimInClass.Unity.HardCodeds;
	using Sirenix.OdinInspector;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using UnityEngine.Video;

	public class BootView : MonoBehaviour
	{
		#region Unity Fields
		[Title(nameof(BootView), "References")]
		[SerializeField]
		VideoPlayer videoPlayer;
		#endregion

		#region Fields
		int nextScene;
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Awake()
		{
			this.videoPlayer.loopPointReached += this.HandleOnVideoFinished;
			this.videoPlayer.Play();

			this.nextScene = 1;

#if !MOBILE
			this.nextScene = Scenes.Login;
#else
			// Login disabled for mobile. Inject login dependencies here.
			KarmaFramework.API.InterLevelData.AppConfig.UserInfo = new FakeUserFactory().CreateTestUser();
			this.nextScene = Scenes.MainMenu;
#endif

#if UNITY_EDITOR
			this.SkipVideo();
#endif
		}

		/// <inheritdoc />
		protected virtual void Update()
		{
			if (Input.GetMouseButtonDown(0))
			{
				this.SkipVideo();
			}
		}
		#endregion

		#region Methods
		#region Event Handlers
		void HandleOnVideoFinished(VideoPlayer vp) => SceneManager.LoadScene(this.nextScene);
		#endregion

		void SkipVideo()
		{
			this.videoPlayer.Stop();
			this.HandleOnVideoFinished(this.videoPlayer);
		}
		#endregion
	}
}
