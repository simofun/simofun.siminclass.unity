﻿namespace Simofun.SimInClass.Unity.Domain.Model
{
	using Simofun.Karma.Unity.Domain.Model;
	using Simofun.SimInClass.Unity.HardCodeds;

	public class Scene : IScene
	{
		public int Game => Scenes.Game;

		public int Login => Scenes.Login;

		public int MainMenu => Scenes.MainMenu;
	}
}
