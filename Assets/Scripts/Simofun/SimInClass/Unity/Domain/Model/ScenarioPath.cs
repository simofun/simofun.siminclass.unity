﻿#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
#define MOBILE
#endif

namespace Simofun.SimInClass.Unity.Domain.Model
{
	using KarmaFramework.Localization;
	using Simofun.Karma.Unity.Domain.Model;
	using UnityEngine;

	public class ScenarioPath : PathBase, IScenarioPath
	{
		#region Fields
		const string rootDirName = "Config";

		const string directoryName = "Scenario";
		#endregion

		#region Properties
		public override string DirectoryName => directoryName;

		public override string RootPath =>
			System.IO.Path.Combine(
				this.StreamingAssetsPath,
				rootDirName,
				Localizer.Instance.CurrentLanguage.ToString());
		#endregion

		#region Protected Properties
		protected string StreamingAssetsPath =>
#if MOBILE
			MobileStreamingAssets.Path;
#else
			Application.streamingAssetsPath;
#endif
		#endregion
	}
}
