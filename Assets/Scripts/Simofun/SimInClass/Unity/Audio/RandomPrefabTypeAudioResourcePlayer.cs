﻿using Sinifta.UI;

namespace Simofun.SimInClass.Unity.Audio
{
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;

	[RequireComponent(typeof(AudioSource))]
	public class RandomPrefabTypeAudioResourcePlayer : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		List<PrefabType> audioClips = new List<PrefabType>();

		[SerializeField]
		bool invokeOnAwake;

		[SerializeField]
		bool invokeOnStart = true;
		#endregion

		#region Fields
		AudioSource audioSource;
		#endregion

		#region Unity Methods
		/// <inheritdoc/>
		protected virtual void Awake()
		{
			this.audioSource = this.GetComponent<AudioSource>();

			if (this.invokeOnAwake)
			{
				this.Execute();
			}
		}

		/// <inheritdoc/>
		protected virtual void Start()
		{
			if (this.invokeOnStart)
			{
				this.Execute();
			}
		}
		#endregion

		#region Public Methods
		public void Execute()
		{
			if (!this.audioClips.Any())
			{
				return;
			}

			this.audioSource.loop = true;
			this.audioSource.clip = SiniftaResources.Load<AudioClip>(
				this.audioClips[Random.Range(0, this.audioClips.Count - 1)]);
			this.audioSource.Play();
		}
		#endregion
	}
}
