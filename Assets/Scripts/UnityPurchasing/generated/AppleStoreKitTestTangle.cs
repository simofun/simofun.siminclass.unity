// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("v+jujKqNqLu0lzv1O0qwvLS8q7WIj46I56qwiY2Njo+KjIqIj46I5+7J0s/Y9tTJjaOqsI+NjYmNjIyKeRS2DnLfskYJAUpmOeOVVvqwTFAxXMxm9/hOkKQrvus8G/lTyMrm7p+NsLu0lzv1O0qwvLy8uL2+P7yysbXuydLP2PbUyYy2jbS7vui5u662jbS7vui5u66/6O6Mqo2ou7SXO7m+sbXuydLP2PbUyYysjbK7vui5oK68vEK5uI2+vLxCjbO7vuigsrx9+pX2Vud8lrTrkRgXtL7Gj4xhVVYK/TyAOoDN7Gx54DAYYcLX5adj3hYqJ5Z+l2VtD1xo57CEptLalRT8GMO0MAeAgHDN6mVQUmhnynUuuW7LrU9N1FFw3yTMRN03OfWFeQA2yYysjbK7vui5t7G17snSz9j21MlyxRHgB4Pi7ZbhlDFgf2qXqmKfCZc79TtKsLy8tri9jeKMrI2yu77oAH1roP0j6gSFLcrL728V9s3TBdp/Rlvir3y/vry9vB6GjYSNsru+6I3ijKyNsru+6Lm+sbXuydLP2PbUt7G17snSz9j21MmMrI2yu77oubahUrQ1xdhzPE8wz1q6p9Obr0qPDHLZgdA95I4xd4u7l6ymMPRztULb3cuK71FCEaTgsaiSN7JINHVqlqA2EUgiDbSFHtPGdpvtt6dcNGzPy2pZxRUSFdAafGfVkNtxcpS27ZFevY0/vLe/P7y8vWbCLYGRzrJmKpvamAQW0Y7YEGV/J8X1slRxfJqweyRGDjfoIlKglQXil2zOIoj6igb7vEK5ub6/vzmNq7u+6KCYvLxCubFa0l2mW7DVcJjTfsu1SPe2E8fJga8+azrF0CQA4w63XSVzS1d+M9DUjbe7tZa7vLi4ur6+jbC7tJc79Tv1O0qwvLS8q7XuydLP2PbUyY0/vJHIW/EMr25AXqLjsMAF5WodwVnCOMX1c314r6zXsrETkrh30sfZwp32Lga3RRycg3XcZJl146LIg2cuZoysjbK7vui5trG17snSz9j21MmMsAaFLda7eUGwWCjtcCx1B5hJ5XS2hPF28tN9DuIKKAcEFeD3yRZS0o0/vsmNP7/hHb6/vL+/vLyNsLu0jKcsPQQCWLF2gQaz+d+bR7mUC/RKsLy8tri9vj+8vL0PvV2BTFVU36NRliy4GoDy");
        private static int[] order = new int[] { 5,7,6,18,24,13,24,11,24,28,37,42,40,21,34,28,25,42,32,27,24,32,22,36,43,27,26,34,35,33,33,42,41,33,40,39,38,39,39,39,43,41,43,43,44 };
        private static int key = 189;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
