// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("QcLMw/NBwsnBQcLCw3SuQIfKq4wsREdrgGAHCvFZBXi0pnZNwRGdanwqEnlIYpYyj2F52c43Z0ojFbuygKoBVnyOwj8zT6q1Y/b1vjF06Jj6PI+ZHnCvm7CaNYNufK2jrPf70YfwQgP0eXCmxOaVbYRE5rfAsIIn+4OWkpsOsYrHEwd+gRzwn4bCDQxFasaS1piS6LwKQnzppk3tbap5OYA0bjsy+r/DevzCGKzaI1xLj7NwcVP/SIz6odv7ZzYsihIM/sMTi8aDeXBpCgy5a+gp8cqc0JLAh/zzudcXkwAdOPMAFC4UUqlrq+9ma+J580HC4fPOxcrpRYtFNM7CwsLGw8CJOtIHJFQVpPmTMMgmNrGNfAyE3rLAqUtnagouCMHAwsPC");
        private static int[] order = new int[] { 1,7,10,5,5,6,6,8,13,11,11,12,13,13,14 };
        private static int key = 195;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
