// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Toon/Basic Outline" {
	Properties {
		_ColorBoost ("Color Boost", Range(0.1, 2.0)) = 1.0
		_Color ("Main Color", Color) = (.5,.5,.5,1)
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
		_Outline ("Outline width", Range (.002, 0.05)) = .005
		_MainTex ("Base (RGB)", 2D) = "white" { }
		_ToonShade ("ToonShader Cubemap(RGB)", CUBE) = "" { }
	}
	
	CGINCLUDE
	#include "UnityCG.cginc"
	#pragma target 3.0
	
	struct appdata {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};

	struct v2f {
		float4 pos : SV_POSITION;
		UNITY_FOG_COORDS(0)
		fixed4 color : COLOR;
		fixed2 offset : TEXCOORD0;
	};
	
	uniform float _Outline;
	uniform float4 _OutlineColor;
	
	float hash(float n)
	{
		return frac(sin(n)*43758.5453);
	}

	// Black magic. Returns [-1, 1]
	// (I swear these people have nothing better to do with their lives)
	float perlinNoise(float3 x)
	{
		float3 p = floor(x);
			float3 f = frac(x);

			f = f*f*(3.0 - 2.0*f);
		float n = p.x + p.y*57.0 + 113.0*p.z;

		return lerp(lerp(lerp(hash(n + 0.0), hash(n + 1.0), f.x),
			lerp(hash(n + 57.0), hash(n + 58.0), f.x), f.y),
			lerp(lerp(hash(n + 113.0), hash(n + 114.0), f.x),
			lerp(hash(n + 170.0), hash(n + 171.0), f.x), f.y), f.z);
	}

	v2f vert(appdata v) 
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);

		float3 norm = normalize(mul((float3x3)UNITY_MATRIX_IT_MV, v.normal));
		float2 offset = TransformViewToProjection(norm.xy);

		o.offset = offset *_Outline;
		o.pos.xy += o.offset;
		o.color = _OutlineColor;

		UNITY_TRANSFER_FOG(o,o.pos);
		return o;
	}
	ENDCG

	SubShader {
		Tags { "RenderType"="Opaque" }
		UsePass "Toon/Basic/BASE"
		Pass
		{
			Stencil
			{
				Ref 1
				Comp NotEqual
			}

			Name "OUTLINE"
			Tags { "LightMode" = "Always" }
			Cull Front
			ZWrite On
			ColorMask RGB
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			fixed4 frag(v2f i) : SV_Target
			{
				UNITY_APPLY_FOG(i.fogCoord, i.color);
				fixed4 c = i.color;
				fixed t = _SinTime.x * perlinNoise(float3(i.offset.x, i.offset.y, _CosTime.x) / 10);
				c.a = lerp(0.3, 1, t);
				return c;
			}
			ENDCG
		}
	}
	
	Fallback "Toon/Basic"
}
